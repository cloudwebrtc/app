# Famedly Talk Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.0] - 2020-01-07
### New features
### Fixed

## [0.7.0] - 2019-12-31
### New features
- The status header now actually displays the connection status
- Enhance contact list design
#### Fixed
- Fixed app title: Famedly Talk -> Famedly
- Add to tasks button was hidden in web
- Better exception handling
- Fix user lists

## [0.6.0] - 2019-12-24
### New features
- Implement typing indicators
- Switch to opt-out sentry
- Improved security for the directory using a token access
#### Fixed
- Fixed a bug where archive rooms are displayed not correctly
- Fixed a sentry bug on logout
- Send correct msgtypes when sending files

## [0.5.0] - 2019-12-17
### New features
- Added privacy policy and license viewer
- Send passport and set power levels correctly when starting a request with a contact
- Implement message redactions
 - Click on Desktop Notification should now open the room
 - Add email integration
### Fixed
- App crashes when replied to media events
- Unread counter doesn't go away when there is no internet
- Push Notifications now registered on first login

## [0.4.0] - 2019-12-10
### New features
- Mute notifications per room now possible
- New design for the navigation pages
- More integration tests
- Send passport and set power levels correctly when starting a request with a contact
### Fixed
- Changing permissions destroys the power levels of a room
- Only *.famedly.de homeservers are allowed now
- Fix PlayStore tracks
- Fix a lot of little bugs

## [0.3.0] - 2019-12-03
### New features
- Add replies
- Add enhanced broadcasts
### Fixed
- Fix invite notifications
- A lot of minor bugs

## [0.2.3] - 2019-11-20
### New features
- User avatars and displaynames in groupchats will now be loaded when needed
- Add request rooms slide action
### Fixed
- Broadcasts to contact points with the same mxid broke the app
- Warning in fastlane fixed
- Make "Famedly" uppercase on the app name
- User will no longer see his/her own contact points
- File events were not clickable
- Fix minor bugs from sentry
- Fix broken invite room

## [0.2.2] - 2019-11-12
### New features
- Unread bubbles in the bottom bar
- New make-password-visible button at login
### Fixed
- Performance leak
- Fix a bug in membership event parsing

## [0.1.3] - 2019-10-22
### Fixed
- Multiple Bug fixes


## [0.1.2] - 2019-10-01
### Fixed
- Multiple Bug fixes

## [0.1.1] - 2019-09-12
### Added
- [Colors] Add colors of new design

### Cleanup
- [Events] remove ignore immutable and instead fix the warning properly
- [CallingWarning] remove ignore comment
- [Warning] use global text theme and add todo to later on use real theme data instead of local defining
- [TaskRoomsPage] remove no longer true todo comment and print function

### Removed
- [AsyncList] remove unused AsyncList and Utils
- [Application] remove unused Application class
- [DummyData] remove unused DummyData
- [Organisation] remove unused Organisation class

### Fixed
- Alpha Track deployment failed
- Update fastlane
- Remove generating release changelog from CHANGELOG.md
- Prevent Notifications from erroring caused by a missing sender
- Avatar loading on iOS does not further cause errors
- iOS and Android Notification showing now happens platform specific

### Added
- New Settings Page design
- New Security Settings Section
- New Password Change Function
- Scroll down button for Chat Rooms
- Implementing the new Group Details Design and MemberList Design and User Details Design

## [0.1.0] - 2019-07-30
### Added
- Choose your server
- Login to your existing account / Logout
- Create and join private chats
- Create and join group chats
- Manage group chats
- Send textmessages 
- Send emojis
- List your chat rooms
- Archive your chat rooms
- Manage your archive
- Push notifications

[Unreleased]: https://gitlab.com/famedly/app/compare/e7a6a3dc92bbfda2fa8eabe5d9b93e11bbb3fb02...master
[0.1.3]: https://gitlab.com/famedly/app/compare/f3ed5e0e7eb7b356238c388e12277a573b0bd08b...e7a6a3dc92bbfda2fa8eabe5d9b93e11bbb3fb02
[0.1.2]: https://gitlab.com/famedly/app/compare/156b313e5ca139c4c615263355eb30dfa73a196e...f3ed5e0e7eb7b356238c388e12277a573b0bd08b
[0.1.1]: https://gitlab.com/famedly/app/compare/955a53912e63178283e704bcb2b2c03dee11ca86...156b313e5ca139c4c615263355eb30dfa73a196e
[0.1.0]: https://gitlab.com/famedly/app/compare/7fec5d72cd1060a080b9abda7b8e9696b445e32e...955a53912e63178283e704bcb2b2c03dee11ca86
