# Famedly

[![Contact-us](https://gitlab.com/famedly/brand-and-assets/uploads/269e61c68d45216f563430c25f2a6dbe/banner.png)](https://matrix.to/#/@larodar:famedly.de)


This repository is part of the source code of Famedly. For more information take a look at [famedly.com](https://famedly.com) or contact us by info@famedly.de.

Software for healthcare should be open source so we publish our source code at [gitlab.com/famedly](https://gitlab.com/famedly). We also provide latest stable releases of our software at [famedly.com/download](https://famedly.com/download).

For licensing information, see the attached LICENSE file and the list of third-party licenses at [famedly.com/legal/licenses](https://famedly.com/legal).

If you compile the open source software that we make available to develop your own mobile, desktop or embeddable application, and cause that application to connect to our servers for any purposes, you have to aggree to our Terms of Service. In short, if you choose to connect to our servers, you will need to request an access key and certain restrictions apply as follows:  

* You agree not to change the way the open source software connects and interacts with our servers
* You agree not to weaken any of the security features of the open source software
* You agree not to use the open source software to gather data
* You agree not to use our servers to store data for purposes other than the intended and original functionality of the Software
* You acknowledge that you are solely responsible for any and all updates to your software

No license is granted to the Famedly trademark and its associated logos, all of which will continue to be owned exclusively by Famedly GmbH. Any use of the Famedly trademark and/or its associated logos is expressly prohibited without the express prior written consent of Famedly GmbH.

# Famedly Talk

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software:

* [Flutter](https://flutter.dev/)

### Installing

#### For Android:
2. Setup [Flutter for Android](https://flutter.dev/docs/get-started/install/macos#install-android-studio)
3. Start an emulator or connect your device

#### For iOS:
2. Setup [Flutter for iOS](https://flutter.dev/docs/get-started/install/macos#ios-setup)
3. Launch the simulator from spotlight

#### For Desktop:
2. [Check out the wiki](https://gitlab.com/famedly/app/wikis/How-to-build-for-Famedly-web)

#### [Android and IOS] Clone the repo and launch with flutter:
```
git clone https://gitlab.com/famedly/app.git
cd app
flutter run
```

#### [Desktop] Clone the repo and launch with hover:
```
git clone https://gitlab.com/famedly/app.git
cd app
hover run
```

## Running the tests

#### Widget Tests
```
flutter test test
```

#### Instrumental Tests
```
flutter drive --target=test_driver/app.dart
```

## Built With

* [Flutter](https://flutter.dev/)
* [Fastlane](https://fastlane.tools/)

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Christian Pauly** - *Initial work* - [christianpauly](https://gitlab.com/christianpauly)
* **Marcel Radzio** - *Initial work* - [MTRNord](https://gitlab.com/MTRNord)

See also the list of [contributors](https://gitlab.com/famedly/app/graphs/master) who participated in this project.

## License

This project is licensed under the GNU AGPLv3 License - see the [LICENSE](LICENSE) file for details

## Acknowledgments 
