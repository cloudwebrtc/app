import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';

enum FocusPage { FIRST, SECOND }

class AdaptivePageLayout extends StatelessWidget {
  final Widget firstScaffold;
  final Widget secondScaffold;
  final FocusPage primaryPage;
  final double minWidth;

  AdaptivePageLayout(
      {this.firstScaffold,
      this.secondScaffold,
      this.primaryPage = FocusPage.SECOND,
      this.minWidth = FamedlyStyles.columnWidth,
      Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      if (orientation == Orientation.portrait ||
          MediaQuery.of(context).size.width <
              minWidth * 2) if (primaryPage == FocusPage.FIRST)
        return firstScaffold;
      else
        return secondScaffold;
      return Row(
        children: <Widget>[
          Container(
            width: minWidth,
            child: firstScaffold,
          ),
          Container(
            width: 1,
            color: FamedlyColors.pale_grey,
          ),
          Expanded(
            child: Container(
              child: secondScaffold,
            ),
          )
        ],
      );
    });
  }
}
