import 'package:famedly/components/DateDisplay.dart';
import 'package:famedly/models/tasks/tasklist.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

class AddTaskModal extends StatefulWidget {
  final TaskList taskList;

  AddTaskModal(this.taskList);

  @override
  State<StatefulWidget> createState() => AddTaskModalState();
}

class AddTaskModalState extends State<AddTaskModal> {
  final TextEditingController textController = TextEditingController();
  final TextEditingController descController = TextEditingController();
  bool showDescTextField = false;
  DateTime taskTime;

  void submit() async {
    final String text = textController.text;
    if (text.isEmpty) return;
    final success = await CustomDialogs(context).tryRequestWithLoadingDialogs(
        widget.taskList.create(
            title: text, description: descController.text, date: taskTime));
    if (success != false) Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      key: Key("AddTaskModal"),
      child: Container(
        margin:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        height:
            130.0 + (showDescTextField ? 31 : 0) + (taskTime != null ? 41 : 0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(15),
                topRight: const Radius.circular(15))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextField(
              key: Key("NewTaskTextField"),
              onTap: () {
                setState(() {});
              },
              onSubmitted: (s) => submit(),
              controller: textController,
              autofocus: true,
              decoration: InputDecoration(
                hintText: locale.tr(context).newTask,
                hintStyle:
                    TextStyle(color: FamedlyColors.cloudy_blue, fontSize: 17),
                contentPadding: EdgeInsets.all(25),
                border: InputBorder.none,
                disabledBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                focusedErrorBorder: InputBorder.none,
              ),
            ),
            showDescTextField
                ? TextField(
                    key: Key("DescriptionTextField"),
                    controller: descController,
                    style: TextStyle(fontSize: 13),
                    decoration: InputDecoration(
                      hintText: locale.tr(context).addDetails,
                      hintStyle: TextStyle(
                          color: FamedlyColors.cloudy_blue, fontSize: 13),
                      contentPadding: EdgeInsets.only(
                          left: 25, right: 25, top: 0, bottom: 15),
                      border: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      focusedErrorBorder: InputBorder.none,
                    ),
                  )
                : Container(),
            taskTime != null
                ? Padding(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: DateDisplay(taskTime))
                : Container(),
            Padding(
              padding: EdgeInsets.only(left: 10, right: 10),
              child: Row(
                children: <Widget>[
                  IconButton(
                    key: Key("ShowDescTextFieldIconButton"),
                    icon: Icon(Icons.menu,
                        color: showDescTextField
                            ? FamedlyColors.slate
                            : FamedlyColors.aqua_marine),
                    onPressed: () {
                      setState(() {
                        showDescTextField = !showDescTextField;
                      });
                    },
                    iconSize: 20,
                  ),
                  IconButton(
                    icon: Icon(Icons.calendar_today,
                        color: FamedlyColors.aqua_marine),
                    onPressed: () {
                      DatePicker.showDateTimePicker(context,
                          theme: DatePickerTheme(
                              itemStyle:
                                  TextStyle(color: FamedlyColors.cloudy_blue),
                              doneStyle:
                                  TextStyle(color: FamedlyColors.aqua_marine)),
                          showTitleActions: true, onConfirm: (date) {
                        setState(() {
                          taskTime = date;
                        });
                      }, currentTime: DateTime.now(), locale: LocaleType.de);
                    },
                    iconSize: 20,
                  ),
                  Expanded(child: Container()),
                  FlatButton(
                    key: Key("CreateFlatButton"),
                    child: Text(locale.tr(context).create),
                    textColor: FamedlyColors.aqua_marine,
                    onPressed: submit,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
