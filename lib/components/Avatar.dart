import 'package:cached_network_image/cached_network_image.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

/// Generates the widget for each room item.
class Avatar extends StatelessWidget {
  final String name;
  final MxContent mxContent;
  final double size;
  final double textSize;

  Avatar(this.mxContent,
      {this.size = 45, this.name = "", this.textSize = 14, Key key})
      : super(key: key);

  String get twoLetters {
    List<String> words = name.split(" ");
    if (words.length > 1 && words[0].length > 0 && words[1].length > 0)
      return words[0].substring(0, 1) + words[1].substring(0, 1);
    else if (words[0].length > 1 && words[0].length > 0)
      return words[0].substring(0, 2);
    else if (words[0].length == 1)
      return words[0];
    else
      return "@";
  }

  Widget get placeHolder => Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          color: FamedlyColors.cloudy_blue,
          shape: BoxShape.circle,
        ),
        child: Center(
          child: Text(
            twoLetters.toUpperCase(),
            style: TextStyle(
              color: Color(0xffffffff),
              fontSize: textSize,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
              letterSpacing: 0,
            ),
          ),
        ),
      );

  Widget avatarImage(BuildContext context) {
    final String url = mxContent == null
        ? ""
        : mxContent.getThumbnail(Matrix.of(context).client,
            width: size * MediaQuery.of(context).devicePixelRatio,
            height: size * MediaQuery.of(context).devicePixelRatio,
            method: ThumbnailMethod.scale);
    return Container(
      width: size,
      height: size,
      child: ClipOval(
        child: kIsWeb
            ? Image.network(url, fit: BoxFit.cover)
            : CachedNetworkImage(
                imageUrl: url,
                fit: BoxFit.cover,
                placeholder: (context, url) => placeHolder,
              ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget avatarWidget;
    if (mxContent?.mxc != null && mxContent.mxc != "")
      avatarWidget = avatarImage(context);
    else
      avatarWidget = placeHolder;
    return avatarWidget;
  }
}
