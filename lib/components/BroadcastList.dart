import 'dart:async';

import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/roomsList/RequestItem.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/requests/BroadcastContent.dart';
import 'package:famedly/models/requests/RequestContent.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

typedef SetTitle = void Function(String t);

class BroadcastList extends StatefulWidget {
  final SetTitle setTitle;
  final TextEditingController filterTextGetter;
  final String requestId;

  /// Widget shown when there are no
  /// chats to list.
  final Widget emptyListPlaceholder;

  BroadcastList(
    this.filterTextGetter,
    this.requestId, {
    this.setTitle,
    this.emptyListPlaceholder,
    Key key,
  }) : super(key: key);

  @override
  BroadcastListState createState() => new BroadcastListState();
}

class BroadcastListState extends State<BroadcastList> {
  RoomList roomList;
  List<Room> rooms;
  Room broadcastRoom;
  BroadcastContent broadcastContent;

  MatrixState matrix;
  String filterText = null;

  void handleFilter() {
    if (widget.filterTextGetter == null) return;
    setState(() {
      filterText = widget.filterTextGetter.text;
    });
  }

  void rebuildView() {
    roomList = null;
    setState(() {});
  }

  void updateView() {
    if (!mounted) return;
    if (roomList != null) setState(() {});
  }

  @override
  initState() {
    super.initState();
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.addListener(handleFilter);
    }
  }

  Future<RoomList> getList() async {
    RoomList list;
    if (roomList != null) list = roomList;
    list = await matrix.client.getRoomList(onUpdate: updateView);
    return list;
  }

  @override
  void dispose() {
    roomList?.eventSub?.cancel();
    roomList?.firstSyncSub?.cancel();
    roomList?.roomSub?.cancel();
    roomList = null;
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.removeListener(handleFilter);
    }
    super.dispose();
  }

  List<BroadcastContactsContent> filterContacts() {
    return broadcastContent?.contacts?.where((contact) {
      if (filterText == null || filterText == "") return true;
      return contact.organisationName
          .toLowerCase()
          .contains(filterText.toLowerCase());
    })?.toList();
  }

  List<Room> filterRooms() {
    return rooms?.where((room) {
      if (filterText == null || filterText == "") return true;
      return Request(room)
          .organisation
          .toLowerCase()
          .contains(filterText.toLowerCase());
    })?.toList();
  }

  Widget _buildItemFallback(
      List<Room> roomsFiltered, BuildContext context, int index) {
    if (index >= roomsFiltered.length) return Container();
    return RequestItem(
      key: Key("request_$index"),
      room: roomsFiltered[index],
    );
  }

  Widget _buildItem(List<BroadcastContactsContent> contacts,
      BuildContext context, int index) {
    if (index >= contacts.length) return Container();
    final contact = contacts[index];
    for (final room in rooms) {
      final r = Request(room);
      if (r.content.contactId == contact.contactId) {
        return RequestItem(
          key: Key("request_$index"),
          room: room,
        );
      }
    }
    return RequestItem(
      key: Key("request_$index"),
      room: broadcastRoom,
      contacts: [contact],
    );
  }

  Request _getRequestFromContact(BroadcastContactsContent contact) {
    for (final room in rooms) {
      final r = Request(room);
      if (r.content.contactId == contact.contactId) {
        return r;
      }
    }
    return Request(broadcastRoom, contact: contact);
  }

  int _getRsortNumber(Request req) {
    if (req.remoteAccepted()) return 3;
    if (req.remoteNeedInfo()) return 2;
    if (req.remoteRejected()) return 1;
    return 0;
  }

  int _compareRequests(Request req1, Request req2) {
    int r1sort = _getRsortNumber(req1);
    int r2sort = _getRsortNumber(req2);
    if (r1sort != r2sort) return r2sort - r1sort;
    if (req1.room.timeCreated != req2.room.timeCreated)
      return req1.room.timeCreated < req2.room.timeCreated ? 1 : -1;
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    matrix = Matrix.of(context);
    return FutureBuilder(
        future: getList(),
        builder: (BuildContext context, AsyncSnapshot<RoomList> snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );

          roomList = snapshot.data;
          rooms = [];
          broadcastRoom = null;
          for (final room in roomList.rooms) {
            if (!Request.isRequestRoom(room)) continue;
            if (room.states[Request.mRequestNameSpace] == null) continue;
            final content = RequestContent.fromJson(
                room.states[Request.mRequestNameSpace].content);
            if (content.requestId != widget.requestId) continue;
            if (room.states[Request.mBroadcastNameSpace] != null) {
              // we have a broadcast room
              broadcastRoom = room;
              broadcastContent = BroadcastContent.fromJson(
                  room.states[Request.mBroadcastNameSpace].content);
              widget.setTitle(broadcastContent.title);
            } else {
              // we have a normal room
              rooms.add(room);
              widget.setTitle(content.requestTitle);
            }
          }
          if (broadcastRoom != null) {
            final contacts = filterContacts();

            if (contacts == null || contacts.isEmpty) {
              return widget.emptyListPlaceholder ?? Container();
            }
            contacts.sort((BroadcastContactsContent c1,
                    BroadcastContactsContent c2) =>
                _compareRequests(
                    _getRequestFromContact(c1), _getRequestFromContact(c2)));
            return ListView.builder(
              shrinkWrap: true,
              itemCount: contacts.length,
              itemBuilder: (context, index) => _buildItem(
                contacts,
                context,
                index,
              ),
            );
          } else {
            // fallback, no broadcast room
            final roomsFiltered = filterRooms();

            if (roomsFiltered == null || roomsFiltered.isEmpty) {
              return widget.emptyListPlaceholder ?? Container();
            }
            roomsFiltered.sort((Room r1, Room r2) =>
                _compareRequests(Request(r1), Request(r2)));
            return ListView.builder(
              shrinkWrap: true,
              itemCount: roomsFiltered.length,
              itemBuilder: (context, index) => _buildItemFallback(
                roomsFiltered,
                context,
                index,
              ),
            );
          }
        });
  }
}
