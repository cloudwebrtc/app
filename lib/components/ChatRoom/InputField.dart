import 'dart:async';
import 'dart:io';

import 'package:famedly/components/ChatRoom/eventWidgets/text_event_content.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedly/utils/MatrixFileWebPicker.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime_type/mime_type.dart';
import '../CustomPopupMenuItem.dart';

/// Defines a Class with listeners to call
abstract class InputListener {
  onSend(String message);
}

class InputField extends StatefulWidget {
  final InputListener _listener;
  final Room room;

  final Event replyTo;

  final VoidCallback onCloseReply;

  InputField(
    this._listener,
    this.room, {
    Key key,
    this.replyTo,
    this.onCloseReply,
  }) : super(key: key);

  @override
  InputFieldState createState() => InputFieldState();
}

class InputFieldState extends State<InputField> {
  //IconData _fabIcon = Icons.send; // When voice messages are possible default to Icons.mic and replace Icons.send instances.
  String text;
  TextEditingController _inputController = TextEditingController();
  bool isTyping = false;
  static Duration typingCoolDownDuration = Duration(seconds: 2);
  Timer isTypingCoolDown;
  static Duration typingTimeoutDuration = Duration(seconds: 30);
  Timer isTypingTimeout;

  BorderSide defaultBorderSide() => BorderSide(width: 1.0, color: Colors.grey);

  OutlineInputBorder defaultOutlineInputBorder() => OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
        borderSide: defaultBorderSide(),
      );

  bool mic_or_send = true;
  FocusNode inputFocus = FocusNode();

  void submit() {
    if (text != null && text.trim() != '') {
      _inputController.clear();
      widget._listener.onSend(text);
      text = "";

      if (widget.replyTo != null) {
        widget.onCloseReply();
      }
    }
  }

  Future<void> sendMatrixFile(MatrixFile file) async {
    switch (mime(file.path).split("/")[0]) {
      case "image":
        await widget.room.sendImageEvent(file);
        break;
      case "audio":
        await widget.room.sendAudioEvent(file);
        break;
      case "video":
        await widget.room.sendVideoEvent(file);
        break;
      default:
        await widget.room.sendFileEvent(file);
        break;
    }
  }

  void pickAndSendFile(BuildContext context, bool pickImage,
      {ImageSource imageSource}) async {
    MatrixFile file;
    CustomDialogs dialogs = CustomDialogs(context);
    if (kIsWeb) {
      file = await MatrixFileWebPicker.startFilePicker(imagePicker: pickImage);
      dialogs.showLoadingDialog();
    } else {
      final File tempFile = pickImage
          ? await ImagePicker.pickImage(
              source: imageSource,
              imageQuality: 50,
              maxWidth: 1600,
              maxHeight: 1600)
          : await FilePicker.getFile(type: FileType.ANY);
      if (tempFile == null) return;
      dialogs.showLoadingDialog();
      file =
          MatrixFile(bytes: await tempFile.readAsBytes(), path: tempFile.path);
    }
    if (file != null) {
      await dialogs.tryRequestWithErrorSnackbar(sendMatrixFile(file));
      dialogs.hideLoadingDialog();
    }
  }

  Widget _buildReply(BuildContext context) {
    if (widget.replyTo != null) {
      return Padding(
        padding: EdgeInsets.only(
          top: 12,
          bottom: 16,
        ),
        child: TextReplyEventContent(
          repliedEvent: widget.replyTo,
          elevation: 4,
          onTapClose: widget.onCloseReply,
        ),
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    inputFocus.addListener(
      () {
        if (inputFocus.hasFocus && mic_or_send) {
          setState(() {
            mic_or_send = false;
          });
        } else {
          setState(() {
            mic_or_send = true;
          });
        }
      },
    );

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: FamedlyColors.slate.withOpacity(0.05),
            blurRadius: 1.0,
            spreadRadius: 0.0,
            offset: Offset(
              0.0,
              -2.0,
            ),
          )
        ],
      ),
      child: Padding(
          padding: EdgeInsets.all(15).copyWith(
            top: widget.replyTo != null ? 0 : null,
          ),
          child: Column(
            children: <Widget>[
              _buildReply(context),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    height: 40,
                    width: 40,
                    child: PopupMenuButton(
                      onSelected: (value) {
                        switch (value) {
                          case 'document':
                            pickAndSendFile(context, false);
                            break;
                          case 'gallery':
                            pickAndSendFile(context, true,
                                imageSource: ImageSource.gallery);
                            break;
                          case 'camera':
                            pickAndSendFile(context, true,
                                imageSource: ImageSource.camera);
                            break;
                        }
                      },
                      itemBuilder: (BuildContext context) {
                        List<PopupMenuEntry> entries = <PopupMenuEntry<String>>[
                          PopupMenuItem<String>(
                            value: 'gallery',
                            child: CustomPopupMenuItem(
                                title: locale.tr(context).gallery,
                                iconData: FamedlyIcons.image),
                          ),
                          PopupMenuItem<String>(
                            value: 'document',
                            child: CustomPopupMenuItem(
                                title: locale.tr(context).document,
                                iconData: FamedlyIcons.document),
                          ),
                        ];
                        if (!kIsWeb)
                          entries.add(PopupMenuItem<String>(
                            value: 'camera',
                            child: CustomPopupMenuItem(
                                title: locale.tr(context).camera,
                                iconData: FamedlyIcons.camera),
                          ));
                        return entries;
                      },
                      child: Material(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(40),
                        key: Key("attachment"),
                        elevation: 1,
                        child: Icon(
                          FamedlyIcons.attachment,
                          color: FamedlyColors.bluey_grey,
                          size: 20,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 12, height: 40),
                  Expanded(
                    child: TextField(
                      maxLines: 7,
                      minLines: 1,
                      textInputAction: kIsWeb
                          ? TextInputAction.done
                          : TextInputAction.newline,
                      keyboardType: TextInputType.text,
                      onSubmitted: (String text) {
                        submit();
                        FocusScope.of(context).requestFocus(inputFocus);
                      },
                      key: Key("input"),
                      style: TextStyle(
                          color: FamedlyColors.metallic_blue, fontSize: 15),
                      controller: _inputController,
                      onChanged: (String text) {
                        setState(() {
                          this.isTypingCoolDown?.cancel();
                          this.isTypingCoolDown =
                              Timer(typingCoolDownDuration, () {
                            this.isTypingCoolDown = null;
                            this.isTyping = false;
                            widget.room.sendTypingInfo(false);
                          });
                          this.isTypingTimeout ??=
                              Timer(typingTimeoutDuration, () {
                            this.isTypingTimeout = null;
                            this.isTyping = false;
                          });
                          if (!this.isTyping) {
                            this.isTyping = true;
                            widget.room.sendTypingInfo(true,
                                timeout: typingTimeoutDuration.inMilliseconds);
                          }
                          this.text = text;
                        });
                      },
                      focusNode: inputFocus,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(11),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(
                                style: BorderStyle.solid,
                                color: FamedlyColors.pale_grey)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: BorderSide(
                                style: BorderStyle.solid,
                                color: FamedlyColors.pale_grey)),
                        hintText: 'Nachricht',
                        hintStyle: TextStyle(
                            color: FamedlyColors.cloudy_blue, fontSize: 15),
                      ),
                      textCapitalization: TextCapitalization.sentences,
                    ),
                  ),
                  SizedBox(width: 12, height: 40),
                  Container(
                    height: 40,
                    width: 40,
                    child: Material(
                      key: Key("sendButton"),
                      borderRadius: BorderRadius.circular(40),
                      child: InkWell(
                        onTap: submit,
                        child: Icon(
                          FamedlyIcons.send,
                          color: Colors.white,
                          size: 28,
                        ),
                      ),
                      elevation: 1,
                      color: HexColor("#1fdcca"),
                    ),
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
