import 'package:famedly/models/requests/request.dart';
import 'package:famedly/components/FamedlyIconButton.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

class RequestStickyHeader extends StatefulWidget {
  final Room room;
  final void Function() onAccept;
  final void Function() onReject;
  final void Function() onReply;

  RequestStickyHeader({
    this.room,
    this.onAccept,
    this.onReject,
    this.onReply,
    Key key,
  }) : super(key: key);

  @override
  RequestStickyHeaderState createState() => RequestStickyHeaderState();
}

class RequestStickyHeaderState extends State<RequestStickyHeader> {
  bool expanded = false;
  Request req;

  Widget getBodyText(BuildContext context) {
    if (expanded) {
      return Flexible(
        flex: 1,
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 25),
          child: SingleChildScrollView(
            child: Text(
              req.content.body,
              style: TextStyle(
                color: Colors.black,
                fontSize: 15,
                letterSpacing: 0,
              ),
            ),
          ),
        ),
      );
    } else {
      return Flexible(
        flex: 1,
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 15, bottom: 15),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Flexible(
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    padding: EdgeInsets.only(right: 27.0),
                    child: Text(
                      req.content.body,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        letterSpacing: 0,
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: FamedlyIconButton(
                  iconData: Icons.reply,
                  color: FamedlyColors.aqua_marine,
                  size: 40,
                  backgroundColor: Colors.white,
                  onTap: () {
                    if (widget.onReply != null) widget.onReply();
                  }
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    req = Request(widget.room);
    bool showAcceptDeny = (req.isAuthor() && req.remoteAccepted()) || !req.isAuthor();

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 15,
      ),
      child: Container(
        constraints: BoxConstraints(
          maxHeight: expanded ? 294 : 124,
        ),
        //height: 294,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20),
          ),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color(0x0c000000),
              offset: Offset(0, -1),
              blurRadius: 1,
              spreadRadius: 0,
            ),
            BoxShadow(
              color: FamedlyColors.black_10,
              offset: Offset(0, 2),
              blurRadius: 2,
              spreadRadius: 0,
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
                top: 20,
                bottom: expanded ? 25 : 5,
              ),
              child: Row(
                children: <Widget>[
                  Flexible(
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Container(
                        padding: EdgeInsets.only(right: 13.0),
                        child: Text(
                          req.title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: FamedlyColors.slate,
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: IconButton(
                      icon: Icon(
                        expanded
                            ? Icons.keyboard_arrow_up
                            : Icons.keyboard_arrow_down,
                      ),
                      onPressed: () {
                        setState(() {
                          expanded = !expanded;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
            getBodyText(context),
            expanded
                ? Align(
                    alignment: Alignment.bottomCenter,
                    child: ClipRRect(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                      child: Container(
                        color: FamedlyColors.light_grey,
                        height: 80,
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: showAcceptDeny ? Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: 15, left: 20, bottom: 15),
                                    child: FamedlyIconButton(
                                      iconData: Icons.check,
                                      color: FamedlyColors.aqua_marine,
                                      size: 50,
                                      backgroundColor: (!req.youAccepted() && !req.youRejected()) || req.youAccepted() ? Colors.white : FamedlyColors.light_grey,
                                      onTap: () {
                                        if (!req.youAccepted() && !req.youRejected() && widget.onAccept != null) widget.onAccept();
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: 15, left: 20, bottom: 15),
                                    child: FamedlyIconButton(
                                      iconData: Icons.cancel,
                                      color: FamedlyColors.grapefruit,
                                      size: 50,
                                      backgroundColor: (!req.youAccepted() && !req.youRejected()) || req.youRejected() ? Colors.white : FamedlyColors.light_grey,
                                      onTap: () {
                                        if (!req.youAccepted() && !req.youRejected() && widget.onReject != null) widget.onReject();
                                      },
                                    ),
                                  )
                                ],
                              ) : Container(),
                            ),
                            Expanded(
                              flex: 1,
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: 25, right: 15, bottom: 15),
                                    child: FamedlyIconButton(
                                      iconData: Icons.reply,
                                      color: FamedlyColors.aqua_marine,
                                      size: 40,
                                      backgroundColor: Colors.white,
                                      onTap: () {
                                        if (widget.onReply != null) widget.onReply();
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                : SizedBox(
                    height: 0,
                  ),
          ],
        ),
      ),
    );
  }
}
