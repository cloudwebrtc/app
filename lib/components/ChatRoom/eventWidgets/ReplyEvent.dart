/*import 'package:famedlysdk/src/Event.dart' as MatrixEvent;
import 'package:famedlysdk/src/Room.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import './event.dart';
import '../../../styles/colors.dart';
import '../../Matrix.dart';

class ReplyEvent extends Event {
  final MatrixEvent.Event replyEvent;

  ReplyEvent(
    this.replyEvent, {
    Key key,
    Room room,
    MatrixEvent.Event prevEvent,
    MatrixEvent.Event nextEvent,
  }) : super(
          (replyEvent),
          room: room,
          key: key,
          prevEvent: prevEvent,
          nextEvent: nextEvent,
          doubleBubble: true,
        );

  String _strip_mx_reply_tag(String htmlString) =>
      htmlString.replaceAll(RegExp(r'<mx-reply>[\s\S]+<\/mx-reply>'), "");

  @override
  Widget content(BuildContext context, MatrixState matrix,
      bool prevEventSameSender, bool nextEventSameSender) {
    final Color textColor =
        replyEvent.status >= 0 ? Colors.black87 : FamedlyColors.grapefruit;

    final MatrixEvent.Event relatedEvent = null; //this.replyEvent.replyEvent;

    final double defaultRadius = 18.0;
    final double noneRadius = 1.0;

    double topLeft = defaultRadius;
    double topRight = defaultRadius;
    double bottomLeft = defaultRadius;
    double bottomRight = defaultRadius;
    double bottomRight_redLine = defaultRadius;
    double topLeft_redLine = defaultRadius;
    double topRight_redLine = defaultRadius;

    if (prevEventSameSender && isMe(matrix)) topRight = noneRadius;
    if (nextEventSameSender && isMe(matrix)) bottomRight = noneRadius;
    if (prevEventSameSender && !isMe(matrix)) topLeft = noneRadius;
    if (nextEventSameSender && !isMe(matrix)) bottomLeft = noneRadius;
    //if (!isMe(matrix)) topLeft = noneRadius;
    if (isMe(matrix)) topRight = noneRadius;
    if (!isMe(matrix)) bottomLeft = noneRadius;
    if (isMe(matrix)) bottomRight = noneRadius;
    if (!isMe(matrix)) bottomRight_redLine = noneRadius;
    if (isMe(matrix)) topLeft_redLine = noneRadius;
    if (!isMe(matrix)) topRight_redLine = noneRadius;

    BorderRadius redLine_radius = BorderRadius.only(
      topLeft: Radius.circular(topLeft_redLine),
      topRight: Radius.circular(topRight_redLine),
      bottomLeft: Radius.circular(bottomLeft),
      bottomRight: Radius.circular(bottomRight_redLine),
    );

    BorderRadius radius = BorderRadius.only(
      topLeft: Radius.circular(topLeft),
      topRight: Radius.circular(topRight),
      bottomLeft: Radius.circular(bottomLeft),
      bottomRight: Radius.circular(bottomRight),
    );

    return Column(
      children: <Widget>[
        Padding(
          child: Container(
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  blurRadius: 5.0,
                  spreadRadius: 2.0,
                  color: HexColor("#0f041f1c"),
                ),
              ],
              color: HexColor("#EEF0F0"),
              borderRadius: radius,
              border: messageBubbleBorder,
            ),
            child: Column(
              children: <Widget>[
                IntrinsicHeight(
                  child: Row(
                    children: <Widget>[
                      /*
                      disable because of complexity
                      CustomPaint(
                        painter: RedLinePainter(),
                        child: Container(),
                      ),*/
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: redLine_radius,
                          border: Border.all(width: 0, style: BorderStyle.none),
                          color: Colors.red,
                        ),
                        width: 3,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 16.0, top: 10.0, right: 8.0, bottom: 10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              relatedEvent.sender.calcDisplayname(),
                              style: TextStyle(
                                color: HexColor("#95acb9"),
                              ),
                            ),
                            Divider(),
                            Html(
                              useRichText: true,
                              data: _strip_mx_reply_tag(
                                  (relatedEvent.formattedText != null &&
                                          relatedEvent.formattedText.isNotEmpty)
                                      ? relatedEvent.formattedText
                                      : relatedEvent.getBody()),
                              defaultTextStyle: TextStyle(
                                color: HexColor("#4e6e81"),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          padding: const EdgeInsets.only(top: 4, bottom: 0, left: 4, right: 4),
        ),
        Padding(
          padding:
              const EdgeInsets.only(top: 11, bottom: 11, left: 20, right: 20),
          child: Html(
            useRichText: true,
            data: _strip_mx_reply_tag(this.replyEvent.formattedText),
            defaultTextStyle: TextStyle(
              color: textColor,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              fontSize: 15.0,
            ),
          ),
        ),
      ],
    );
  }
}
/*
class RedLinePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();

    paint.color = Colors.red;
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = 3;

    var startPointBeforeCurve = Offset(0, (size.height - (size.height / 7)));
    var endPointBeforeCurve =
        Offset(0, (0 + (size.height / 5) - (size.height / 19)));
    var startPointLowerCurve = Offset(0, (size.height - (size.height / 5)));

    // Method to convert degree to radians
    num degToRad(num deg) => deg * (Math.pi / 180.0);

    // RESPECT THE MAGIC NUMBERS :D
    var path = Path();
    path.moveTo(startPointLowerCurve.dx, startPointLowerCurve.dy);
    path.addArc(
        Rect.fromLTRB(0, (size.height - (size.height / 3)),
            (0 + (size.width / 8)), size.height),
        degToRad(180),
        degToRad(-90));
    path.moveTo(startPointBeforeCurve.dx, startPointBeforeCurve.dy);
    path.lineTo(endPointBeforeCurve.dx, endPointBeforeCurve.dy);
    path.moveTo(endPointBeforeCurve.dx, endPointBeforeCurve.dy);
    path.addArc(
        Rect.fromLTRB(0, 0, (0 + (size.width / 8)), (0 + ((size.height / 3)))),
        degToRad(180),
        degToRad(40));
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
*/
*/
