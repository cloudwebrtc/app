import 'package:famedlysdk/src/Event.dart' as MatrixEvent;
import 'package:flutter/material.dart';

import '../../../styles/colors.dart';

class EmoteEventContent extends StatelessWidget {
  final MatrixEvent.Event emoteEvent;

  EmoteEventContent(
    this.emoteEvent, {
    Key key
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color textColor = emoteEvent.status >= 0
        ? Theme.of(context).brightness == Brightness.light
        ? Colors.black87
        : Colors.grey
        : FamedlyColors.grapefruit;

    return Text(
      '*${emoteEvent.sender.calcDisplayname()} ${emoteEvent.getBody()}',
      style: TextStyle(
        color: textColor,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
        fontSize: 15.0,
      ),
    );
  }
}
