import 'dart:math';

import 'package:famedly/components/dialogs/ConfirmDialog.dart';
import 'package:famedly/components/dialogs/InformDialog.dart';
import 'package:famedly/components/FamedlyIconButton.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/tasks/task.dart';
import 'package:famedly/models/tasks/tasklist.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:famedlysdk/src/Event.dart' as MatrixEvent;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../config/FamedlyLocalizations.dart';
import '../../../styles/colors.dart';
import '../../Avatar.dart';
import '../../Matrix.dart';
//krille help.. context wird makiert :()

const String _emailPrefix = "_email_";

/// The main Event Widget logic
class Event extends StatelessWidget {
  /// The user who sent the message
  final MatrixEvent.Event event;

  final MatrixEvent.Event prevEvent;
  final MatrixEvent.Event nextEvent;
  final Room room;

  final bool hasBubble;

  final bool doubleBubble;

  /// Builder that returns a content [Widget] for this [Event].
  final Widget content;

  /// Called when the user chooses to reply to this event
  final VoidCallback onReply;

  static const bubblePadding = const EdgeInsets.only(
    top: 11,
    bottom: 11,
    left: 20,
    right: 20,
  );

  static const double defaultRadius = 18.0;
  static const double noneRadius = 1.0;

  static BorderRadius bubbleRadiusFor(
    MatrixEvent.Event event, {
    @required MatrixState matrix,
    @required MatrixEvent.Event previousEvent,
    @required MatrixEvent.Event nextEvent,
    bool isMine,
  }) {
    final prevEventSameSender = hasSameSender(previousEvent, event);
    final nextEventSameSender = hasSameSender(nextEvent, event);

    double topLeft = defaultRadius;
    double topRight = defaultRadius;
    double bottomLeft = defaultRadius;
    double bottomRight = defaultRadius;

    isMine ??= Event.isMine(event, matrix);

    if (prevEventSameSender && isMine) topRight = noneRadius;
    if (nextEventSameSender && isMine) bottomRight = noneRadius;
    if (prevEventSameSender && !isMine) topLeft = noneRadius;
    if (nextEventSameSender && !isMine) bottomLeft = noneRadius;
    if (event.type == EventTypes.Reply) topLeft = noneRadius;
    if (event.type == EventTypes.Reply && isMine) topRight = noneRadius;

    return BorderRadius.only(
      topLeft: Radius.circular(topLeft),
      topRight: Radius.circular(topRight),
      bottomLeft: Radius.circular(bottomLeft),
      bottomRight: Radius.circular(bottomRight),
    );
  }

  static double getPageWidth(BuildContext context) =>
      MediaQuery.of(context).size.width > FamedlyStyles.columnWidth * 2
          ? MediaQuery.of(context).size.width - FamedlyStyles.columnWidth
          : MediaQuery.of(context).size.width;

  static double getMaxWidth(BuildContext context) =>
      min(FamedlyStyles.columnWidth, getPageWidth(context) - 95);

  static bool hasSameSender(MatrixEvent.Event a, MatrixEvent.Event b) {
    return a != null &&
        a.typeKey == "m.room.message" &&
        a.sender.id == b.sender.id;
  }

  Event(
    this.event, {
    @required this.content,
    Key key,
    this.room,
    this.prevEvent,
    this.nextEvent,
    // TODO: CHECk bubble
    this.hasBubble = true,
    this.doubleBubble = false,
    this.onReply,
  }) : super(key: key);

  BorderRadius _bubbleRadius(MatrixState matrix) => Event.bubbleRadiusFor(
        event,
        previousEvent: prevEvent,
        nextEvent: nextEvent,
        matrix: matrix,
      );

  Widget _avatar(BuildContext context, bool prevEventSameSender) {
    MatrixState matrix = Matrix.of(context);
    final double size = 32;
    if (_isMe(matrix))
      return Container();
    else if (prevEventSameSender)
      return Container(width: size);
    else
      return Padding(
        padding: EdgeInsets.only(top: 4),
        child: InkWell(
          borderRadius: BorderRadius.circular(size),
          onTap: () {
            if (Request.isRequestRoom(room)) return;
            Navigator.of(context)
                .pushNamed("/room/${room.id}/member/${event.sender.id}");
          },
          child: Avatar(
            event.sender.avatarUrl,
            size: size,
            name: event.sender.calcDisplayname(),
          ),
        ),
      );
  }

  static Color messageBubbleColor(
    BuildContext context,
    MatrixEvent.Event event,
    MatrixState matrix,
  ) {
    if (isMine(event, matrix) && event.status == 2) {
      return Theme.of(context).brightness == Brightness.light
          ? FamedlyColors.light_grey
          : Colors.black54;
    }

    return Theme.of(context).brightness == Brightness.light
        ? Colors.white
        : Colors.black;
  }

  static Border messageBubbleBorder(MatrixEvent.Event event) {
    if (event.status == 0 || event.status == 1)
      return Border.all(width: 1, color: FamedlyColors.bluey_grey);
    else if (event.status == -1)
      return Border.all(width: 1, color: FamedlyColors.grapefruit);
    return Border.all(width: 0, style: BorderStyle.none);
  }

  void onMessageBubbleContext(
      BuildContext context, List<PopupMenuItem<String>> list) async {
    MatrixState matrix = Matrix.of(context);
    final RenderBox bar = context.findRenderObject();
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();
    final RelativeRect position = RelativeRect.fromRect(
      Rect.fromPoints(
        bar.localToGlobal(bar.size.bottomLeft(Offset.zero), ancestor: overlay),
        bar.localToGlobal(bar.size.bottomRight(Offset.zero), ancestor: overlay),
      ),
      Offset.zero & overlay.size,
    );
    final String result = await showMenu<String>(
      context: context,
      position: position,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      items: list,
    );
    switch (result) {
      case "copy":
        Clipboard.setData(new ClipboardData(text: event.getBody()));
        break;
      case "browser":
        launch(MxContent(event.content["url"]).getDownloadLink(matrix.client));
        break;
      case "addToTasks":
        CustomDialogs dialogs = CustomDialogs(context);
        final Task newTask = await dialogs.tryRequestWithLoadingDialogs(
            TaskList(matrix.client).create(
                title: event
                    .getBody()
                    .substring(0, min<int>(200, event.getBody().length)),
                description:
                    "${event.sender.calcDisplayname()} - ${event.time.toTimeString()}"));
        if (newTask != null)
          dialogs.showSnackBar(null, locale.tr(context).addedToTasks);
        break;
      case "delete":
        event.remove();
        break;
      case "redact":
        showDialog(
          context: context,
          builder: (BuildContext context) => ConfirmDialog(
            text: locale.tr(context).deleteEventForAll,
            callback: (BuildContext context) {
              CustomDialogs(context)
                  .tryRequestWithLoadingDialogs(event.redact());
            },
          ),
        );
        break;
      case "reply":
        if (onReply != null) {
          onReply();
        }

        break;
    }
  }

  Widget _messageBubble(
    BuildContext context,
    MatrixState matrix,
  ) {
    final radius = _bubbleRadius(matrix);

    final BoxDecoration bubbleDecoration = BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: Color(0x0c000000),
          offset: Offset(0, -1),
          blurRadius: 1,
          spreadRadius: 0,
        ),
        BoxShadow(
          color: Color(0x19000000),
          offset: Offset(0, 1),
          blurRadius: 1,
          spreadRadius: 0,
        )
      ],
      color: Event.messageBubbleColor(context, event, matrix),
      borderRadius: radius,
      border: Event.messageBubbleBorder(event),
    );

    List<PopupMenuItem<String>> list = <PopupMenuItem<String>>[
      PopupMenuItem<String>(
        child: Text(
          "${locale.tr(context).receivedAt(event.time.toEventTimeString())}",
          style: TextStyle(
            fontStyle: FontStyle.italic,
            color: FamedlyColors.bluey_grey,
          ),
        ),
      ),
      //PopupMenuItem<String>(child: new Text("Weiterleiten"), value: 'forward'),
    ];
    if (!event.redacted) {
      if ([
            EventTypes.Text,
            EventTypes.Reply,
            EventTypes.Emote,
            EventTypes.Notice
          ].indexOf(event.type) !=
          -1) {
        if (!kIsWeb)
          list.insert(
            1,
            PopupMenuItem<String>(
              key: Key("EventActionsCopy"),
              child: new Text(locale.tr(context).copy),
              value: 'copy',
            ),
          );
        list.add(
          PopupMenuItem<String>(
            key: Key("EventActionsAddToTasks"),
            child: new Text(locale.tr(context).addToTasks),
            value: 'addToTasks',
          ),
        );
      }
      if (event.status == -1)
        list.add(
          PopupMenuItem<String>(
            key: Key("EventActionsDelete"),
            child: new Text(locale.tr(context).deleteMessage),
            value: 'delete',
          ),
        );
      if (event.canRedact)
        list.add(
          PopupMenuItem<String>(
            key: Key("EventActionsRedact"),
            child: new Text(locale.tr(context).deleteMessage),
            value: 'redact',
          ),
        );
      if (onReply != null &&
          room.canSendDefaultMessages &&
          room.membership == Membership.join) {
        list.add(
          PopupMenuItem<String>(
            key: Key("EventActionsReply"),
            child: Text(locale.tr(context).reply),
            value: 'reply',
          ),
        );
      }
    }

    return Builder(builder: (context) {
      return InkWell(
        splashColor: FamedlyColors.slate,
        borderRadius: radius,
        key: Key("MessageBubble"),
        child: this.hasBubble
            ? Container(
                constraints:
                    BoxConstraints(maxWidth: Event.getMaxWidth(context)),
                padding: !this.doubleBubble
                    ? Event.bubblePadding
                    : const EdgeInsets.only(
                        top: 2,
                        bottom: 11,
                        left: 2,
                        right: 2,
                      ),
                decoration: bubbleDecoration,
                child: content,
              )
            : Container(
                decoration: bubbleDecoration,
                child: ClipRRect(borderRadius: radius, child: content),
                constraints:
                    BoxConstraints(maxWidth: Event.getMaxWidth(context)),
              ),
        onTap: () {
          if (event.type == EventTypes.Image) {
            final MxContent thumbnail = MxContent(event.content["url"] ?? "");
            Navigator.of(context).pushNamed(
              "/room/imageView/${thumbnail == null ? "" : thumbnail.mxc.toString().replaceFirst("mxc://", "")}",
            );
          } else {
            onMessageBubbleContext(context, list);
          }
        },
        onLongPress: () => onMessageBubbleContext(context, list),
      );
    });
  }

  Widget title(
      BuildContext context, MatrixState matrix, bool prevEventSameSender) {
    if (prevEventSameSender || _isMe(matrix) || room.isDirectChat)
      return Container();
    else {
      Widget decoration = Container();
      if (event.sender.id.startsWith("@" + _emailPrefix)) {
        decoration = FamedlyIconButton(
          iconData: Icons.alternate_email,
          onTap: () {
            InformDialog.show(
              context: context,
              text: locale.tr(context).contactIsEmail,
            );
          },
          iconSize: 14,
          size: 16,
        );
      }
      return Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: Row(
              children: <Widget>[
                decoration,
                Text(
                  event.sender.calcDisplayname(),
                  style: TextStyle(
                      fontSize: 12.0, color: FamedlyColors.bluey_grey),
                ),
              ],
            ),
          ),
        ],
      );
    }
  }

  Widget _statusRow({List<Widget> children}) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: children,
      ),
    );
  }

  Widget get status {
    if ((event.status == 0 || event.status == 1) &&
        !(nextEvent != null && nextEvent.status == event.status))
      return _statusRow(children: <Widget>[
        Icon(
          FamedlyIcons.clock,
          color: FamedlyColors.bluey_grey,
          size: 14,
        ),
        Container(width: 5, height: 5),
        Text(
          "wird gesendet…",
          style: TextStyle(
            color: FamedlyColors.bluey_grey,
            fontSize: 13,
          ),
        )
      ]);
    if (event.status == -1)
      return _statusRow(children: <Widget>[
        Icon(
          FamedlyIcons.info,
          color: FamedlyColors.grapefruit,
          size: 14,
        ),
        Container(width: 5, height: 5),
        Text(
          "Fehler:",
          style: TextStyle(
            color: FamedlyColors.grapefruit,
            fontSize: 13,
          ),
        ),
        Container(width: 2, height: 5),
        InkWell(
          onTap: () {
            event.sendAgain();
          },
          child: Text(
            "Erneut senden",
            style: TextStyle(
              color: FamedlyColors.bluey_grey,
              decoration: TextDecoration.underline,
              fontSize: 13,
            ),
          ),
        )
      ]);
    return Container();
  }

  Widget get date {
    if (prevEvent == null || !event.time.sameEnvironment(prevEvent.time))
      return Container(
        padding: EdgeInsets.all(10.0),
        child: Text(
          event.time.toEventTimeString(),
          style: TextStyle(
            fontSize: 14.0,
            color: FamedlyColors.bluey_grey,
          ),
        ),
      );
    return Container();
  }

  Widget receipts(BuildContext context) {
    //if (nextEvent != null) return Container();
    List<Widget> rowChildren = [];
    MatrixState matrix = Matrix.of(context);
    List receipts = event.receipts;
    receipts.removeWhere(
      (r) => r.user.id == matrix.client.userID || r.user.id == event.sender.id,
    );
    for (int i = 0; i < min(receipts.length, 5); i++) {
      final User user = receipts[i].user;
      rowChildren.add(
        Padding(
          padding: EdgeInsets.only(left: 4),
          child: Avatar(
            user.avatarUrl,
            textSize: 8,
            name: user.calcDisplayname(),
            size: 16,
          ),
        ),
      );
    }

    if (rowChildren.length == 0) return Container();
    final int more = receipts.length - rowChildren.length;
    if (more > 0)
      rowChildren.insert(
        0,
        Text(
          "$more +",
          style: TextStyle(fontSize: 13, color: FamedlyColors.bluey_grey),
        ),
      );

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      child: Row(
        children: rowChildren,
        mainAxisAlignment: MainAxisAlignment.end,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    MatrixState matrix = Matrix.of(context);

    final prevEventSameSender = hasSameSender(prevEvent, event);
    final nextEventSameSender = hasSameSender(nextEvent, event);

    double topMargin = 10;
    double bottomMargin = 10;
    if (prevEventSameSender) {
      topMargin = 1;
    }
    if (nextEventSameSender) {
      bottomMargin = 1;
    }

    return Container(
      margin: EdgeInsets.only(
        top: topMargin,
        bottom: bottomMargin,
        left: 15.0,
        right: 15.0,
      ),
      child: Column(
        children: <Widget>[
          date,
          title(context, matrix, prevEventSameSender),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _avatar(context, prevEventSameSender),
              Container(width: 10),
              Expanded(
                child: Container(
                  alignment: _isMe(matrix)
                      ? Alignment.centerRight
                      : Alignment.centerLeft,
                  child: _messageBubble(
                    context,
                    matrix,
                  ),
                ),
              ),
            ],
          ),
          status,
          receipts(context),
        ],
      ),
    );
  }

  static bool isMine(MatrixEvent.Event event, MatrixState matrix) =>
      (event.sender.id == matrix.client.userID &&
          event.typeKey != Request.mRequestNameSpace) ||
      (event.typeKey == Request.mRequestNameSpace &&
          (event.content["creator"] ?? event.sender.id) ==
              matrix.client.userID);

  bool _isMe(MatrixState matrix) => isMine(event, matrix);
}
