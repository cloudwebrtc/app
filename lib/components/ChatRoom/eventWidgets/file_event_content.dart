import 'package:famedly/utils/MatrixFileWebPicker.dart';
import 'package:famedlysdk/src/Event.dart' as MatrixEvent;
import 'package:famedlysdk/src/utils/MxContent.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import './event.dart';
import '../../../styles/colors.dart' as FamedlyColors;
import '../../Matrix.dart';

class FileEventContent extends StatelessWidget {
  final MatrixEvent.Event fileEvent;

  final MxContent file;

  final MatrixEvent.Event prevEvent;
  final MatrixEvent.Event nextEvent;

  FileEventContent(
    this.fileEvent, {
    Key key,
    this.prevEvent,
    this.nextEvent,
  })  : file = MxContent(fileEvent.content["url"] ?? ""),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final matrix = Matrix.of(context);
    // TODO combine with next text message of same user if present
    final double defaultRadius = 18.0;
    final double noneRadius = 1.0;

    double topLeft = defaultRadius;
    double topRight = defaultRadius;
    double bottomLeft = defaultRadius;
    double bottomRight = defaultRadius;

    final prevEventSameSender = Event.hasSameSender(prevEvent, fileEvent);
    final nextEventSameSender = Event.hasSameSender(nextEvent, fileEvent);

    final isMe = Event.isMine(fileEvent, matrix);

    if (prevEventSameSender && isMe) topRight = noneRadius;
    if (nextEventSameSender && isMe) bottomRight = noneRadius;
    if (prevEventSameSender && !isMe) topLeft = noneRadius;
    if (nextEventSameSender && !isMe) bottomLeft = noneRadius;

    BorderRadius radius = BorderRadius.only(
      topLeft: Radius.circular(topLeft),
      topRight: Radius.circular(topRight),
      bottomLeft: Radius.circular(bottomLeft),
      bottomRight: Radius.circular(bottomRight),
    );

    String filename = this.fileEvent.content.containsKey("filename")
        ? this.fileEvent.content["filename"]
        : this.fileEvent.getBody();

    String mimetype;
    int lastDot = filename.lastIndexOf('.', filename.length - 1);
    if (lastDot != -1) {
      String extension = filename.substring(lastDot + 1);
      mimetype = extension;
    } else
      mimetype = "";

    String filesize_fixed;
    if (fileEvent.content.containsKey("info") &&
        fileEvent.content["info"].containsKey("size") &&
        fileEvent.content["info"]["size"] is int) {
      int filesize = fileEvent.content["info"]["size"];
      if (filesize != 0) {
        filesize_fixed = (filesize / 1000000).toString();
      } else {
        filesize_fixed = "unknown Size";
      }
    } else {
      filesize_fixed = "unknown Size";
    }

    return GestureDetector(
      onTap: () {
        kIsWeb
            ? MatrixFileWebPicker.openUrl(file.getDownloadLink(matrix.client))
            : launch(file.getDownloadLink(matrix.client));
      },
      child: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              blurRadius: 5.0,
              spreadRadius: 2.0,
              color: FamedlyColors.HexColor("#0f041f1c"),
            ),
          ],
          color: Event.messageBubbleColor(context, fileEvent, matrix),
          borderRadius: radius,
          border: Event.messageBubbleBorder(fileEvent),
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding:
              EdgeInsets.only(left: 8.0, top: 8.0, right: 8.0, bottom: 0),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.insert_drive_file,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 11,
                  ),
                  Expanded(
                    child: Text(
                      // As Riot doesn't follow spec we need this check
                      filename,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: FamedlyColors.HexColor("#4e6e81"),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              color: FamedlyColors.HexColor("#0f041f1c"),
            ),
            Padding(
              padding:
              EdgeInsets.only(left: 8.0, top: 0, right: 8.0, bottom: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    mimetype,
                    style: TextStyle(
                      color: FamedlyColors.HexColor("#95acb9"),
                    ),
                  ),
                  Text(
                    filesize_fixed == "unknown Size"
                        ? filesize_fixed
                        : "$filesize_fixed MB",
                    style: TextStyle(
                      color: FamedlyColors.HexColor("#95acb9"),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
