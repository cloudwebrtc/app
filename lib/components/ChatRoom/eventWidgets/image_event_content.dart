import 'package:cached_network_image/cached_network_image.dart';
import 'package:famedlysdk/src/Event.dart' as MatrixEvent;
import 'package:famedlysdk/src/utils/MxContent.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import './event.dart';
import '../../Matrix.dart';

class ImageEventContent extends StatelessWidget {
  final MatrixEvent.Event imageEvent;

  final MxContent thumbnail;

  ImageEventContent(this.imageEvent, {Key key})
      : thumbnail = MxContent(imageEvent.content["url"] ?? ""),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final matrix = Matrix.of(context);

    final double size = Event.getMaxWidth(context);
    final String url = thumbnail.getThumbnail(
      matrix.client,
      width: size * MediaQuery.of(matrix.context).devicePixelRatio,
      height: size * MediaQuery.of(matrix.context).devicePixelRatio,
      method: ThumbnailMethod.scale,
    );
    return kIsWeb
        ? Image.network(url)
        : CachedNetworkImage(
            placeholder: (BuildContext context, String url) => Container(
              width: size,
              height: size,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
            width: size,
            height: size,
            fit: BoxFit.cover,
            imageUrl: url,
          );
  }
}
