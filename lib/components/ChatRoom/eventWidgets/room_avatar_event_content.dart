import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

List<TextSpan> roomAvatarEventContent(BuildContext context, Event event) {
  String username = event.sender.calcDisplayname();
  return <TextSpan>[
    TextSpan(
      text: '$username',
      style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
    ),
    TextSpan(
      text: locale.tr(context).changedRoomAvatarTo,
      style: TextStyle(fontFamily: "Roboto"),
    ),
  ];
}
