import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

List<TextSpan> roomCreateEventContent(BuildContext context, Event event) {
  return <TextSpan>[
    TextSpan(
      text: '${event.sender.calcDisplayname()}',
      style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
    ),
    TextSpan(
      text: locale.tr(context).createdTheRoom,
      style: TextStyle(fontFamily: "Roboto"),
    ),
  ];
}
