import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

List<TextSpan> roomMembershipEventContent(BuildContext context, Event event) {
  String username = event.stateKeyUser.calcDisplayname();
  String prev_membership;
  Map<String, dynamic> prev_content;
  if (event.unsigned != null) {
    if (event.unsigned.containsKey("prev_content")) {
      prev_content = event.unsigned["prev_content"];
      if (prev_content.containsKey("membership")) {
        prev_membership = event.unsigned["prev_content"]["membership"];
      }
    }
  }

  FamedlyLocalizations localizations = locale.tr(context);
  String membership = event.content["membership"];

  switch (membership) {
    case "invite":
      return <TextSpan>[
        TextSpan(
          text: '$username',
          style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
        ),
        TextSpan(
          text: localizations.gotInvitedToRoom,
          style: TextStyle(fontFamily: "Roboto"),
        ),
      ];
    case "join":
      if (prev_membership != null && prev_membership == "join") {
        if (prev_content != null) {
          if (prev_content["avatar_url"] != event.content["avatar_url"]) {
            return <TextSpan>[
              TextSpan(
                text: '$username',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: "Roboto",
                ),
              ),
              TextSpan(
                text: localizations.hasChangedAvatar,
                style: TextStyle(fontFamily: "Roboto"),
              ),
            ];
          } else if (prev_content["displayname"] !=
              event.content["displayname"]) {
            return <TextSpan>[
              TextSpan(
                text: '$username',
                style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: "Roboto",
                ),
              ),
              TextSpan(
                text: localizations.hasChangedDisplayname,
                style: TextStyle(
                  fontFamily: "Roboto",
                ),
              ),
            ];
          }
        }
      }
      return <TextSpan>[
        TextSpan(
          text: '$username',
          style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
        ),
        TextSpan(
          text: localizations.hasJoinedTheRoom,
          style: TextStyle(fontFamily: "Roboto"),
        ),
      ];
    case "knock":
      // Not implemented but in spec
      return <TextSpan>[
        TextSpan(
          text: localizations.unableToParseMembershipEvent,
          style: TextStyle(fontFamily: "Roboto"),
        ),
      ];
    case "leave":
      if (prev_membership != null && prev_membership == "invite") {
        if (event.sender.stateKey == event.stateKeyUser.stateKey) {
          <TextSpan>[
            TextSpan(
              text: '$username',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
            TextSpan(
              text: localizations.rejectedInvitation,
              style: TextStyle(fontFamily: "Roboto"),
            ),
          ];
        } else {
          <TextSpan>[
            TextSpan(
              text: event.sender.calcDisplayname(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
            TextSpan(
              text: localizations.revokedInvitation,
              style: TextStyle(fontFamily: "Roboto"),
            ),
            TextSpan(
              text: '$username.',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
          ];
        }
      } else if (prev_membership != null && prev_membership == "join") {
        if (event.sender.stateKey != event.stateKeyUser.stateKey) {
          return <TextSpan>[
            TextSpan(
              text: event.sender.calcDisplayname(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
            TextSpan(
                text: localizations.hasKicked,
                style: TextStyle(fontFamily: "Roboto")),
            TextSpan(
              text: '$username',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
            TextSpan(text: '.', style: TextStyle(fontFamily: "Roboto")),
          ];
        }
      } else if (prev_membership != null && prev_membership == "ban") {
        if (event.sender.id != event.stateKeyUser.id) {
          return <TextSpan>[
            TextSpan(
              text: event.sender.calcDisplayname(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
            TextSpan(
              text: localizations.hasUnbanned,
              style: TextStyle(fontFamily: "Roboto"),
            ),
            TextSpan(
              text: '$username',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontFamily: "Roboto",
              ),
            ),
            TextSpan(
              text: '.',
              style: TextStyle(fontFamily: "Roboto"),
            ),
          ];
        }
      }
      return <TextSpan>[
        TextSpan(
          text: '$username',
          style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
        ),
        TextSpan(
            text: localizations.hasLeftRoom,
            style: TextStyle(fontFamily: "Roboto")),
      ];
    case "ban":
      return <TextSpan>[
        TextSpan(
          text: '$username',
          style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
        ),
        TextSpan(
          text: localizations.hasBeenBanned,
          style: TextStyle(fontFamily: "Roboto"),
        ),
      ];
  }

  return <TextSpan>[
    TextSpan(
      text: localizations.unableToParseMembershipEvent,
      style: TextStyle(fontFamily: "Roboto"),
    ),
  ];
}
