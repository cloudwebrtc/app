import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

List<TextSpan> roomNameEventContent(BuildContext context, Event event) {
  String username = event.sender.calcDisplayname();
  String new_name = event.content["name"];
  return <TextSpan>[
    TextSpan(
      text: '$username',
      style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
    ),
    TextSpan(
        text: locale.tr(context).changedRoomNameTo,
        style: TextStyle(fontFamily: "Roboto")),
    TextSpan(
      text: '"$new_name".',
      style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
    ),
  ];
}
