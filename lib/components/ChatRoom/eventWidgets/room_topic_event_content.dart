import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

List<TextSpan> roomTopicEventContent(BuildContext context, Event event) {
  String username = event.sender.calcDisplayname();
  String new_topic = event.content["topic"];
  return <TextSpan>[
    TextSpan(
      text: '$username',
      style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
    ),
    TextSpan(
        text: locale.tr(context).changedRoomTopicTo,
        style: TextStyle(fontFamily: "Roboto")),
    TextSpan(
      text: '"$new_topic".',
      style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
    ),
  ];
}
