import 'package:famedlysdk/famedlysdk.dart';
import 'package:famedlysdk/src/Event.dart' as MatrixEvent;
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../../../styles/colors.dart';

class StateEvent extends StatelessWidget {
  final MatrixEvent.Event event;
  final MatrixEvent.Event prevEvent;
  final MatrixEvent.Event nextEvent;
  final Room room;

  final List<TextSpan> content;

  StateEvent(
    this.event, {
    Key key,
    this.room,
    this.prevEvent,
    this.nextEvent,
    this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment(0, 0),
      margin: EdgeInsets.all(8),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          // Note: Styles for TextSpans must be explicitly defined.
          // Child text spans will inherit styles from parent
          style: TextStyle(
            fontSize: 14.0,
            color: FamedlyColors.bluey_grey,
            fontFamily: "Roboto",
          ),
          children: content,
        ),
      ),
    );
  }
}
