import 'package:famedly/components/FamedlyIconButton.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/utils/MatrixFileWebPicker.dart';
import 'package:famedlysdk/famedlysdk.dart' hide Event;
import 'package:famedlysdk/src/Event.dart' as MatrixEvent;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../styles/colors.dart';
import '../../Matrix.dart';
import 'event.dart';

class TextEventContent extends StatelessWidget {
  final MatrixEvent.Event textEvent;

  final MatrixEvent.Event prevEvent;
  final MatrixEvent.Event nextEvent;
  final Timeline timeline;

  TextEventContent(
    this.textEvent, {
    @required this.prevEvent,
    @required this.nextEvent,
    @required this.timeline,
    Key key,
  }) : super(key: key);

  String replyEventId() {
    final relatesTo = textEvent.content['m.relates_to'];

    if (relatesTo == null) {
      return null;
    }

    final inReplyTo = relatesTo['m.in_reply_to'];
    if (inReplyTo == null) {
      return null;
    }

    return inReplyTo['event_id'];
  }

  String stripReply() {
    final lines = textEvent.text.split('\n');
    lines.removeWhere((line) => line.startsWith('>'));

    return lines.where((l) => l.isNotEmpty).join('\n');
  }

  @override
  Widget build(BuildContext context) {
    if (textEvent.redacted) {
      String redactedBecause = textEvent.redactedBecause.content["reason"];
      if (redactedBecause == null || redactedBecause.isEmpty) {
        redactedBecause = locale.tr(context).messageHasBeenRemoved;
      }
      return Padding(
        padding: Event.bubblePadding,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(Icons.not_interested,
                color: FamedlyColors.dark_purple, size: 14),
            SizedBox(width: 8),
            _Linkify(text: redactedBecause, color: FamedlyColors.dark_purple),
          ],
        ),
      );
    } else if (textEvent.type == EventTypes.Reply) {
      final repliedToId = replyEventId();
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FutureBuilder<MatrixEvent.Event>(
            future: timeline.getEventById(repliedToId),
            builder: (context, snapshot) {
              final event = snapshot.data ??
                  MatrixEvent.Event(
                    content: {
                      "msgtype": "m.text",
                      "body": "...",
                    },
                    typeKey: "m.room.message",
                    eventId: "loadingevent",
                    time: textEvent.time,
                    senderId: textEvent.senderId,
                    room: textEvent.room,
                  );

              return TextReplyEventContent(
                repliedEvent: event,
                prevToReplyEvent: prevEvent,
                replyEvent: textEvent,
                nextToReplyEvent: nextEvent,
              );
            },
          ),
          Padding(
            padding: Event.bubblePadding.copyWith(),
            child: _Linkify(text: stripReply()),
          ),
        ],
      );
    } else {
      return Padding(
        padding: Event.bubblePadding,
        child: _Linkify(text: textEvent.text),
      );
    }
  }
}

class TextReplyEventContent extends StatelessWidget {
  final MatrixEvent.Event repliedEvent;

  // Note these are previous and next to the event that is *replying* to
  // `repliedEvent`, not `repliedEvent` itself.
  final MatrixEvent.Event prevToReplyEvent;
  final MatrixEvent.Event replyEvent;
  final MatrixEvent.Event nextToReplyEvent;

  final double elevation;

  final VoidCallback onTapClose;

  TextReplyEventContent({
    Key key,
    @required MatrixEvent.Event repliedEvent,
    this.prevToReplyEvent,
    this.replyEvent,
    this.nextToReplyEvent,
    this.elevation = 0,
    this.onTapClose,
  })  : this.repliedEvent = (repliedEvent.type == EventTypes.Text ||
                repliedEvent.type == EventTypes.Reply)
            ? repliedEvent
            : MatrixEvent.Event(
                eventId: repliedEvent.eventId,
                status: repliedEvent.status,
                typeKey: repliedEvent.typeKey,
                room: repliedEvent.room,
                roomId: repliedEvent.roomId,
                senderId: repliedEvent.senderId,
                time: repliedEvent.time,
                content: {
                    "msgType": "m.text",
                    "body": repliedEvent.content["body"] ??
                        // Here is no context so this can't be localized.
                        // Normally this should never appear in matrix.
                        "Message",
                  }),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    BorderRadius radius;

    if (replyEvent != null) {
      radius = Event.bubbleRadiusFor(
        replyEvent,
        matrix: Matrix.of(context),
        previousEvent: prevToReplyEvent,
        nextEvent: nextToReplyEvent,
      );
      radius = BorderRadius.only(
        topLeft: radius.topLeft,
        topRight: radius.topRight,
        bottomRight: Radius.circular(Event.noneRadius),
        bottomLeft: Radius.circular(Event.noneRadius),
      );
    } else {
      radius = BorderRadius.circular(Event.noneRadius);
    }

    return Padding(
      padding: EdgeInsets.only(
        top: 3,
        left: 3,
        right: 3,
      ),
      child: Container(
        width: double.infinity,
        child: Material(
          color: FamedlyColors.pale_grey,
          borderRadius: radius,
          elevation: elevation,
          child: CustomPaint(
            foregroundPainter: ReplyBorderPainter(
              radius,
            ),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: Event.bubblePadding.copyWith(
                    left: 14,
                    right: 14 - ReplyBorderPainter.lineWidth,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: FamedlyColors.cloudy_blue,
                              width: 2,
                            ),
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(
                            bottom: 4,
                          ),
                          child: Text(
                            repliedEvent.sender.displayName,
                            style: TextStyle(
                              fontSize: 13,
                              color: FamedlyColors.bluey_grey,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 4),
                      Text(
                        repliedEvent.text,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                if (onTapClose != null)
                  Positioned(
                    top: 4,
                    right: 4,
                    child: FamedlyIconButton(
                      iconData: FamedlyIcons.cancel,
                      size: 18,
                      iconSize: 16,
                      elevation: 0,
                      onTap: onTapClose,
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Linkify extends StatelessWidget {
  final String text;
  final Color color;

  const _Linkify({Key key, this.text, this.color = FamedlyColors.slate})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Linkify(
      humanize: true,
      onOpen: (link) =>
          kIsWeb ? MatrixFileWebPicker.openUrl(link.url) : launch(link.url),
      text: text,
      style: TextStyle(
        fontSize: 15,
        color: color,
      ),
    );
  }
}

class ReplyBorderPainter extends CustomPainter {
  static const lineWidth = 4.0;

  final BorderRadius radius;

  ReplyBorderPainter(this.radius);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.clipRRect(
      RRect.fromRectAndCorners(
        Rect.fromPoints(Offset(0, 0), Offset(size.width, size.height)),
        topLeft: radius.topLeft,
        bottomLeft: radius.bottomLeft,
      ),
    );
    canvas.drawRect(
      Rect.fromPoints(Offset(0, 0), Offset(lineWidth, size.height)),
      Paint()..color = FamedlyColors.grapefruit,
    );
  }

  @override
  bool shouldRepaint(ReplyBorderPainter oldDelegate) =>
      radius != oldDelegate.radius;
}
