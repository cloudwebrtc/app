import 'dart:async';

import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import '../../Avatar.dart';

class TypingEvent extends StatefulWidget {
  final Room room;
  const TypingEvent(this.room);

  @override
  _TypingEventState createState() => _TypingEventState();
}

class _TypingEventState extends State<TypingEvent> {
  Timer animationTimer;
  bool shadowVisible = true;

  @override
  void dispose() {
    animationTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final User typingUser = widget.room.typingUsers.firstWhere(
        (u) => u.id.toLowerCase() != widget.room.client.userID.toLowerCase(),
        orElse: () => null);
    if (typingUser == null) return Container();
    animationTimer ??= Timer.periodic(
      Duration(seconds: 1),
      (t) => setState(() => shadowVisible = !shadowVisible),
    );
    return Container(
      margin: EdgeInsets.only(
        top: 10,
        bottom: 10,
        left: 15,
        right: 15,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          AnimatedContainer(
            width: 32,
            height: 32,
            duration: Duration(seconds: 1),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              boxShadow: [
                BoxShadow(
                  color:
                      shadowVisible ? FamedlyColors.bluey_grey : Colors.white,
                  blurRadius: shadowVisible ? 1.0 : 0,
                  spreadRadius: shadowVisible ? 5.0 : 0,
                ),
              ],
            ),
            child: Avatar(
              typingUser.avatarUrl,
              size: 32,
              name: typingUser.calcDisplayname(),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Container(
              height: 32,
              alignment: Alignment.centerLeft,
              child: Text(
                locale.tr(context).isTyping,
                style: TextStyle(
                  color: FamedlyColors.bluey_grey,
                  fontSize: 14,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
