import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

List<TextSpan> unknownEventContent(BuildContext context, Event event) {
  String username = event.sender.calcDisplayname();
  String type = event.typeKey;
  return <TextSpan>[
    TextSpan(
      text: '$username',
      style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
    ),
    TextSpan(
      text: locale.tr(context).sentUnknownEvent,
      style: TextStyle(fontFamily: "Roboto"),
    ),
    TextSpan(
      text: '"$type".',
      style: TextStyle(fontWeight: FontWeight.bold, fontFamily: "Roboto"),
    ),
  ];
}
