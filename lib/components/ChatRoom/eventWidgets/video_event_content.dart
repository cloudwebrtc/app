import 'package:famedlysdk/src/Event.dart' as MatrixEvent;
import 'package:famedlysdk/src/utils/MxContent.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

import '../../../styles/colors.dart' as FamedlyColors;
import '../../Matrix.dart';

/// Needs a full reload of the app to update when changing the listener!!!!!
class VideoEventContent extends StatelessWidget {
  final MatrixEvent.Event videoEvent;

  final MxContent video;

  VideoEventContent(
    this.videoEvent, {
    Key key,
  })  : video = MxContent(videoEvent.content["url"] ?? ""),
        super(key: key);

  @override
  Widget build(BuildContext context) =>
      VideoMagic(videoUrl: video.getDownloadLink(Matrix.of(context).client));
}

class VideoMagic extends StatefulWidget {
  final String videoUrl;

  const VideoMagic({Key key, @required this.videoUrl}) : super(key: key);

  @override
  _VideoAppState createState() => _VideoAppState();
}

class _VideoAppState extends State<VideoMagic> {
  _VideoAppState() {
    listener = () {
      if (first) {
        setState(() {
          imageFadeAnim = FadeAnimation(
            child: FloatingActionButton(
              onPressed: onTap,
              backgroundColor: FamedlyColors.HexColor("#95acb9"),
              child: Icon(
                Icons.play_arrow,
              ),
            ),
          );
          first = false;
        });
      } else {
        if (!_controller.value.initialized) {
          return null;
        }

        // Show reset to first minute
        if (_controller.value.duration.inMilliseconds ==
                _controller.value.position.inMilliseconds &&
            !reseted) {
          setState(() {
            reseted = true;
            imageFadeAnim = FadeAnimation(
              child: FloatingActionButton(
                onPressed: onTap,
                backgroundColor: FamedlyColors.HexColor("#95acb9"),
                child: Icon(
                  Icons.replay,
                ),
              ),
            );
          });
        }
        return null;
      }
    };
  }

  FadeAnimation imageFadeAnim;
  VoidCallback listener;
  bool first = true;
  bool reseted = false;

  VideoPlayerController _controller;

  VoidCallback onTap() {
    if (!_controller.value.initialized) {
      return null;
    }

    debugPrint(_controller.value.duration.inMilliseconds.toString());
    debugPrint(_controller.value.position.inMilliseconds.toString());

    if (_controller.value.duration.inMilliseconds !=
        _controller.value.position.inMilliseconds) {
      if (_controller.value.isPlaying) {
        setState(() {
          if (reseted) {
            reseted = false;
          }
          imageFadeAnim = FadeAnimation(
            child: FloatingActionButton(
              onPressed: onTap,
              backgroundColor: FamedlyColors.HexColor("#95acb9"),
              child: Icon(
                Icons.play_arrow,
              ),
            ),
          );
        });

        _controller.pause();
      } else {
        if (reseted) {
          reseted = false;
        }
        setState(() {
          imageFadeAnim = FadeAnimation(
            child: FloatingActionButton(
              onPressed: onTap,
              backgroundColor: FamedlyColors.HexColor("#95acb9"),
              child: Icon(
                Icons.pause,
              ),
            ),
          );
        });
        _controller.play();
      }
    } else {
      _controller.seekTo(Duration());
      setState(() {
        if (reseted) {
          reseted = false;
        }
        imageFadeAnim = FadeAnimation(
          child: FloatingActionButton(
            onPressed: onTap,
            backgroundColor: FamedlyColors.HexColor("#95acb9"),
            child: Icon(
              Icons.play_arrow,
            ),
          ),
        );
      });
      _controller.pause();
    }
    return null;
  }

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.videoUrl)
      ..initialize()
      ..addListener(listener);
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> children = <Widget>[
      GestureDetector(
        child: VideoPlayer(_controller),
        onTap: onTap,
      ),
      Align(
        alignment: Alignment.bottomCenter,
        child: VideoProgressIndicator(
          _controller,
          colors: VideoProgressColors(
            bufferedColor: Color.fromRGBO(31, 220, 202, 0.5),
            playedColor: Color.fromRGBO(31, 220, 202, 1),
          ),
          allowScrubbing: false,
        ),
      ),
      Center(child: imageFadeAnim),
      Center(
        child: _controller.value.isBuffering
            ? const CircularProgressIndicator()
            : null,
      ),
    ];

    return _controller.value.initialized
        ? AspectRatio(
            aspectRatio: _controller.value.aspectRatio,
            child: Stack(
              fit: StackFit.passthrough,
              children: children,
            ),
          )
        : Container();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.removeListener(listener);
    _controller.dispose();
  }
}

class FadeAnimation extends StatefulWidget {
  FadeAnimation(
      {this.child, this.duration = const Duration(milliseconds: 700)});

  final FloatingActionButton child;
  final Duration duration;

  @override
  _FadeAnimationState createState() => _FadeAnimationState();
}

class _FadeAnimationState extends State<FadeAnimation>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(duration: widget.duration, vsync: this);
    animationController.addListener(() {
      if (mounted) {
        setState(() {});
      }
    });
    animationController.forward(from: 0.0);
  }

  @override
  void deactivate() {
    animationController.stop();
    super.deactivate();
  }

  @override
  void didUpdateWidget(FadeAnimation oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.child.child != widget.child.child) {
      animationController.forward(from: 0.0);
    }
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Magic to make each stay if not playing
    return widget.child.child.toString() == Icon(Icons.play_arrow).toString() ||
            widget.child.child.toString() == Icon(Icons.replay).toString()
        ? widget.child
        : animationController.isAnimating
            ? Opacity(
                opacity: 1.0 - animationController.value,
                child: widget.child,
              )
            : Container();
  }
}
