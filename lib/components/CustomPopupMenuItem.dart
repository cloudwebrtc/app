import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';

class CustomPopupMenuItem extends StatelessWidget {
  final IconData iconData;
  final String title;

  const CustomPopupMenuItem({@required this.title, @required this.iconData});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Icon(iconData, color: FamedlyColors.bluey_grey),
        SizedBox(width: 10),
        Text(title),
      ],
    );
  }
}
