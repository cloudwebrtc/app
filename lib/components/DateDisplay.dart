import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateDisplay extends StatelessWidget {
  final DateTime date;

  DateDisplay(this.date);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            border: Border.all(color: FamedlyColors.cloudy_blue)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(6),
              child: Icon(Icons.calendar_today,
                  color: FamedlyColors.aqua_marine, size: 12),
            ),
            Padding(
                padding: EdgeInsets.only(right: 6),
                child: Text(DateFormat("dd.MM.yyyy hh:mm").format(date),
                    style: TextStyle(
                        color: FamedlyColors.cloudy_blue, fontSize: 13)))
          ],
        ));
  }
}
