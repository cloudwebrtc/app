import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:flutter/material.dart';

class DocumentViewer extends StatelessWidget {
  final String document;

  const DocumentViewer(this.document, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String title = locale.tr(context).documentNotFound;
    String text = locale.tr(context).documentNotFound;
    switch (document.toLowerCase()) {
      case "privacypolicy":
        title = locale.tr(context).privacyPolicy;
        text = locale.tr(context).privacyPolicyText;
        break;
      case "license":
        title = locale.tr(context).license;
        text = locale.tr(context).licenseText;
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(text),
        ),
      ),
    );
  }
}
