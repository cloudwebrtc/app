import 'dart:async';

import 'package:famedly/components/Avatar.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/models/directory/directory.dart';
import 'package:famedly/models/directory/organisation.dart';
import 'package:famedly/models/directory/subtype.dart';
import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';

class FacilityList extends StatefulWidget {
  final TextEditingController filterTextGetter;
  final String tag;
  final List<Subtype> subtypeList;

  FacilityList(
    this.filterTextGetter, {
    this.tag,
    this.subtypeList,
    Key key,
  }) : super(key: key);

  @override
  FacilityListState createState() => FacilityListState();
}

class FacilityListState extends State<FacilityList> {
  String filterText = null;
  List<Organisation> orgaList;

  void handleFilter() {
    if (widget.filterTextGetter == null) return;
    setState(() {
      filterText = widget.filterTextGetter.text;
    });
  }

  @override
  initState() {
    super.initState();
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.addListener(handleFilter);
    }
  }

  Future<List<Organisation>> getOrganisations(BuildContext context) async {
    if (orgaList != null) return orgaList;
    Directory directory = Directory(Matrix.of(context).client);
    return directory.search();
  }

  @override
  void dispose() {
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.removeListener(handleFilter);
    }
    super.dispose();
  }

  String getTagDescriptionFromID(String tagID) {
    if (widget.subtypeList == null) return "...";
    final String desc = widget.subtypeList
        .singleWhere((Subtype t) => t.id == tagID)
        .description;
    return desc;
  }

  bool filter(Organisation organisation) =>
      (!widget.tag.isEmpty && organisation.orgType != widget.tag) ||
      (filterText != null &&
          filterText != "" &&
          !(organisation.name +
                  organisation.location.country +
                  organisation.location.state +
                  organisation.location.street)
              .toLowerCase()
              .contains(filterText.toLowerCase()));

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Organisation>>(
        future: getOrganisations(context),
        builder:
            (BuildContext context, AsyncSnapshot<List<Organisation>> snapshot) {
          if (!snapshot.hasData)
            return Center(child: CircularProgressIndicator());
          if (snapshot.data == null) {
            return Center(
                child: Text(locale.tr(context).oopsSomethingWentWrong +
                    " " +
                    locale.tr(context).organisationServerIsNotResponding));
          }
          orgaList = snapshot.data;

          orgaList.sort((a, b) => a.name.compareTo(b.name));

          if (orgaList.length == 0)
            return Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  locale.tr(context).noFacilitiesWereFoundForThisContactPoint,
                  textAlign: TextAlign.center,
                ),
              ),
            );

          return ListView.separated(
            separatorBuilder: (BuildContext context, int index) =>
                filter(orgaList[index])
                    ? Container()
                    : Divider(
                        color: Theme.of(context).brightness == Brightness.light
                            ? FamedlyColors.pale_grey
                            : Colors.black38,
                        indent: 15,
                        thickness: 1,
                      ),
            itemCount: orgaList.length,
            itemBuilder: (BuildContext context, int index) {
              Organisation organisation = orgaList[index];
              if (filter(organisation)) return Container();
              return Material(
                borderRadius: BorderRadius.circular(5),
                color: Colors.white,
                child: ListTile(
                    onTap: () {
                      Navigator.of(context)
                          .pushNamed("/facilities/${organisation.id}");
                    },
                    leading: Avatar(null, name: organisation.name),
                    title: Text(organisation.name),
                    subtitle: Row(
                      children: <Widget>[
                        Container(
                            margin: EdgeInsets.only(right: 5),
                            decoration: BoxDecoration(
                                color: FamedlyColors.pale_grey,
                                borderRadius: BorderRadius.circular(19)),
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 5),
                            child: Text(
                                Directory.subtypeCache[organisation.orgType]
                                    .description,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: FamedlyColors.metallic_blue,
                                    fontSize: 10))),
                        Text(organisation.location?.city ?? ""),
                      ],
                    )),
              );
            },
          );
        });
  }
}

class ContactPoint {
  final Organisation organisation;
  final int contactPointID;

  const ContactPoint({this.organisation, this.contactPointID});
}
