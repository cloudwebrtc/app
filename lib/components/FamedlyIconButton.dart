import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';

class FamedlyIconButton extends StatelessWidget {
  final Key key;
  final IconData iconData;
  final onTap;
  final double size;
  final double iconSize;
  final Color color;
  final Color backgroundColor;
  final double elevation;

  FamedlyIconButton({
    this.key,
    this.iconData,
    this.onTap,
    this.size = 35,
    this.iconSize = 24,
    this.color = FamedlyColors.bluey_grey,
    this.backgroundColor,
    this.elevation = 1,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      margin: EdgeInsets.all(1.5),
      child: Material(
        elevation: elevation,
        color: backgroundColor != null
            ? backgroundColor
            : (Theme.of(context).brightness == Brightness.light)
                ? FamedlyColors.light_grey
                : Theme.of(context).backgroundColor,
        type: MaterialType.circle,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: InkWell(
          child: Center(
            child: Icon(
              iconData,
              size: iconSize,
              color: color,
            ),
          ),
          onTap: onTap,
        ),
      ),
    );
  }
}
