import 'package:famedly/components/AdaptivePageLayout.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/views/EmptyPage.dart';
import 'package:famedly/views/OrganisationPickerPage.dart';
import 'package:famedly/views/RoomsPage.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

class HomePicker extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Client client = Matrix.of(context).client;
    return StreamBuilder(
      stream: client.connection.onLoginStateChanged.stream,
      builder: (BuildContext context, AsyncSnapshot<LoginState> snapshot) {
        LoginState loginState =
            snapshot.data ?? Matrix.of(context).loginPlugin.currentLoginState;
        if (loginState == null)
          return Container(
              child: Center(
            child: CircularProgressIndicator(),
          ));
        else if (loginState == LoginState.logged)
          return AdaptivePageLayout(
              firstScaffold: RoomsPage(),
              secondScaffold: EmptyPage(),
              primaryPage: FocusPage.FIRST);
        else
          return OrganisationPickerPage();
      },
    );
  }
}
