import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IntroPage extends StatelessWidget {
  final Color color;
  final String title;
  final String text1;
  final String text2;
  final String image;

  IntroPage(
      {Key key,
      @required this.color,
      @required this.title,
      @required this.text1,
      @required this.text2,
      @required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: this.color,
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              left: 114,
              bottom: 54,
              child: SvgPicture.asset(
                this.image,
                width: 224,
                height: 224,
                color: Color(0xCCFFFFFF),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 43, right: 77, top: 73),
              child: Center(
                child: Column(
                  children: <Widget>[
                    Text(
                      this.title,
                      key: Key("title"),
                      style: const TextStyle(
                          color: Colors.black87,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: 34.0),
                    ),
                    SizedBox(
                      height: 34.0,
                    ),
                    Text(
                      this.text1,
                      key: Key("text1"),
                      style: const TextStyle(
                          color: Colors.black87,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                          fontSize: 20.0),
                    ),
                    SizedBox(
                      height: 50.0,
                    ),
                    Text(
                      this.text2,
                      key: Key("text2"),
                      style: const TextStyle(
                          color: Colors.black87,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                          fontSize: 20.0),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
