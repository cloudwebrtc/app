import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:flutter/material.dart';

class LoadingDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Row(
        children: <Widget>[
          CircularProgressIndicator(),
          SizedBox(width: 16),
          Text(
            locale.tr(context).loading,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
