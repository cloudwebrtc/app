import 'dart:core';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/models/plugins/AccountStorePlugin.dart';
import 'package:famedly/models/plugins/ContactDiscoveryPlugin.dart';
import 'package:famedly/models/plugins/DesktopNotificationPlugin.dart';
import 'package:famedly/models/plugins/LoginPlugin.dart';
import 'package:famedly/models/plugins/MatrixErrorPlugin.dart';
import 'package:famedly/models/plugins/PushNotificationPlugin.dart';
import 'package:famedly/models/plugins/RoomsPlugin.dart';
import 'package:famedlysdk/famedlysdk.dart';
import '../utils/sqfliteStore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// Use this widget as root for your widget tree and get access to the client
/// connection by calling [Matrix.of(context)].
class Matrix extends StatefulWidget {
  final Widget child;

  final onGlobalError;

  final blockUiDialog;

  final String clientName;

  final Client client;

  Matrix(
      {this.child,
      this.onGlobalError,
      this.blockUiDialog,
      this.clientName,
      this.client,
      Key key})
      : super(key: key);

  @override
  MatrixState createState() => MatrixState();

  /// Returns the (nearest) Client instance of your application.
  static MatrixState of(BuildContext context) {
    MatrixState newState =
        (context.dependOnInheritedWidgetOfExactType<_InheritedMatrix>()).data;
    newState.context = context;
    return newState;
  }
}

class MatrixState extends State<Matrix> {
  Client client;
  BuildContext context;
  FamedlyLocalizations localizations;

  /// The id of a room which should not send notifications.
  String activeRoomID;

  bool get isInDebugMode {
    // Assume you're in production mode
    bool inDebugMode = false;

    // Assert expressions are only evaluated during development. They are ignored
    // in production. Therefore, this code only sets `inDebugMode` to true
    // in a development environment.
    assert(inDebugMode = true);

    return inDebugMode;
  }

  @override
  void initState() {
    accountStorePlugin = AccountStorePlugin(this, widget.clientName);

    if (widget.client == null) {
      print("[Matrix] Init matrix client");
      client = Client(widget.clientName, debug: isInDebugMode);
      if (!kIsWeb)
        client.store = Store(client);
      else {
        print("[Web] Web platform detected - Store disabled!");
        accountStorePlugin.loadAccount();
      }
    } else {
      client = widget.client;
    }

    desktopNotificationPlugin = DesktopNotificationPlugin(this);
    pushNotificationPlugin = PushNotificationPlugin(this);
    contactDiscoveryPlugin = ContactDiscoveryPlugin(this);
    loginPlugin = LoginPlugin(this, widget.clientName);
    matrixErrorPlugin = MatrixErrorPlugin(this);
    roomsPlugin = RoomsPlugin(this);

    super.initState();
  }

  DesktopNotificationPlugin desktopNotificationPlugin;
  PushNotificationPlugin pushNotificationPlugin;
  ContactDiscoveryPlugin contactDiscoveryPlugin;
  AccountStorePlugin accountStorePlugin;
  LoginPlugin loginPlugin;
  MatrixErrorPlugin matrixErrorPlugin;
  RoomsPlugin roomsPlugin;

  @override
  Widget build(BuildContext context) {
    localizations = locale.tr(context);
    return _InheritedMatrix(
      data: this,
      child: widget.child,
    );
  }
}

/// Only has MyInheritedState as field.
class _InheritedMatrix extends InheritedWidget {
  final MatrixState data;

  _InheritedMatrix({Key key, this.data, Widget child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedMatrix old) {
    bool update = old.data.client.accessToken != this.data.client.accessToken ||
        old.data.client.userID != this.data.client.userID ||
        old.data.client.matrixVersions != this.data.client.matrixVersions ||
        old.data.client.lazyLoadMembers != this.data.client.lazyLoadMembers ||
        old.data.client.deviceID != this.data.client.deviceID ||
        old.data.client.deviceName != this.data.client.deviceName ||
        old.data.client.homeserver != this.data.client.homeserver;
    return update;
  }
}
