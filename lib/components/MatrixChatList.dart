import 'dart:async';

import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/roomsList/RequestItem.dart';
import 'package:famedly/components/roomsList/item.dart';
import 'package:famedly/models/directory/directory.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

typedef RoomFilter = bool Function(Room room);

class MatrixChatList extends StatefulWidget {
  final RoomFilter filter;
  final onlyLeft;
  final TextEditingController filterTextGetter;
  final String activeChatID;

  /// Widget shown when there are no
  /// chats to list.
  final Widget emptyListPlaceholder;

  MatrixChatList(
    this.filterTextGetter, {
    this.onlyLeft = false,
    this.filter,
    this.activeChatID,
    this.emptyListPlaceholder,
    Key key,
  }) : super(key: key);

  @override
  MatrixChatListState createState() => new MatrixChatListState();
}

class MatrixChatListState extends State<MatrixChatList> {
  RoomList roomList;

  MatrixState matrix;
  String filterText = null;

  void handleFilter() {
    if (widget.filterTextGetter == null) return;
    setState(() {
      filterText = widget.filterTextGetter.text;
    });
  }

  void rebuildView() {
    roomList = null;
    setState(() {});
  }

  void updateView() {
    if (!mounted) return;
    if (roomList != null) setState(() {});
  }

  List<Room> filter() {
    return roomList?.rooms
        ?.where(
          (room) => !(room.id == null ||
              (room.canonicalAlias != null &&
                  room.canonicalAlias == matrix.contactDiscoveryPlugin.alias) ||
              room.directChatMatrixID == Directory.jasonMxid ||
              (widget.filter != null && !widget.filter(room)) ||
              (filterText != null &&
                  filterText != "" &&
                  !room.displayname
                      .toLowerCase()
                      .contains(filterText.toLowerCase()))),
        )
        ?.toList();
  }

  @override
  initState() {
    super.initState();
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.addListener(handleFilter);
    }
  }

  Widget _buildItem(List<Room> rooms, BuildContext context, int index) {
    if (Request.isRequestRoom(rooms[index]))
      return RequestItem(
        key: Key("room_$index"),
        room: rooms[index],
      );
    else
      return RoomItem(
        key: Key("room_$index"),
        item: rooms[index],
        activeChat: widget.activeChatID == rooms[index].id,
      );
  }

  Future<RoomList> getList(String filterText) async {
    RoomList list;
    if (roomList != null) {
      list = roomList;
      return list;
    }
    if (widget.onlyLeft) {
      list =
          RoomList(rooms: await matrix.client.archive, client: matrix.client);
    } else {
      list = await matrix.client.getRoomList(
        onUpdate: updateView,
      );
    }

    roomList = list;

    return list;
  }

  @override
  void dispose() {
    roomList?.eventSub?.cancel();
    roomList?.firstSyncSub?.cancel();
    roomList?.roomSub?.cancel();
    pushRuleSub?.cancel();
    roomList = null;
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.removeListener(handleFilter);
    }
    super.dispose();
  }

  StreamSubscription<UserUpdate> pushRuleSub;

  @override
  Widget build(BuildContext context) {
    matrix = Matrix.of(context);

    pushRuleSub ??= matrix.client.connection.onUserEvent.stream
        .where((u) => u.eventType == "m.push_rules")
        .listen((u) => setState(() {}));

    return FutureBuilder<RoomList>(
        future: getList(filterText),
        builder: (BuildContext context, AsyncSnapshot<RoomList> snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );

          var filtered = filter();

          if (filtered == null || filtered.isEmpty) {
            return widget.emptyListPlaceholder ?? Container();
          }

          if (roomList.rooms.isEmpty) {
            return widget.emptyListPlaceholder ?? Container();
          }

          return ListView.builder(
            shrinkWrap: true,
            itemCount: filtered.length,
            itemBuilder: (context, index) => _buildItem(
              filtered,
              context,
              index,
            ),
          );
        });
  }
}
