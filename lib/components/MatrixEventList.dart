import 'dart:async';

import 'package:famedly/components/ChatRoom/eventWidgets/typing_event.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedlysdk/famedlysdk.dart' hide Event;
import 'package:famedlysdk/src/Event.dart' as MatrixEvent;
import 'package:flutter/material.dart';

import 'package:famedly/components/ChatRoom/eventWidgets/file_event_content.dart';
import 'ChatRoom/eventWidgets/emote_event_content.dart';
import 'ChatRoom/eventWidgets/event.dart';
import 'ChatRoom/eventWidgets/image_event_content.dart';
import 'ChatRoom/eventWidgets/room_avatar_event_content.dart';
import 'ChatRoom/eventWidgets/room_membership_event_content.dart';
import 'ChatRoom/eventWidgets/room_name_event_content.dart';
import 'ChatRoom/eventWidgets/room_topic_event_content.dart';
import 'ChatRoom/eventWidgets/state_event.dart';
import 'ChatRoom/eventWidgets/text_event_content.dart';
import 'ChatRoom/eventWidgets/video_event_content.dart';

typedef OnReply = void Function(MatrixEvent.Event event);

class MatrixEventList extends StatefulWidget {
  final Room room;

  /// How much events should be requested at once, when the user scrolls to the
  /// top of the list? Defaults to 100.
  final int historyRequestCount;

  final String searchQuery;

  final OnReply onReply;

  MatrixEventList(
    this.room, {
    this.onReply,
    this.historyRequestCount = 100,
    this.searchQuery,
    Key key,
  }) : super(key: key);

  @override
  MatrixEventListState createState() => new MatrixEventListState();
}

class MatrixEventListState extends State<MatrixEventList> {
  Timeline timeline;

  String fullyReadId;

  double showArrowDown = 0.0;

  final ScrollController _scrollController = new ScrollController();

  void updateView() {
    if (!mounted) return;
    if (timeline != null) setState(() {});
  }

  Widget _buildEvent(
    BuildContext context,
    MatrixEvent.Event event,
    MatrixEvent.Event nextKey,
    MatrixEvent.Event prevKey,
  ) {
    final onReply = () => widget.onReply(event);

    if (event.redacted) {
      return Event(
        event,
        room: widget.room,
        prevEvent: prevKey,
        nextEvent: nextKey,
        hasBubble: false,
        onReply: onReply,
        content: TextEventContent(
          event,
          prevEvent: prevKey,
          nextEvent: nextKey,
          timeline: timeline,
        ),
      );
    }

    switch (event.type) {
      case EventTypes.Image:
        return Event(
          event,
          room: widget.room,
          prevEvent: prevKey,
          nextEvent: nextKey,
          hasBubble: false,
          onReply: onReply,
          content: ImageEventContent(event),
        );
      case EventTypes.RoomMember:
        return StateEvent(
          event,
          room: widget.room,
          key: Key(event.eventId),
          prevEvent: prevKey,
          nextEvent: nextKey,
          content: roomMembershipEventContent(context, event),
        );
      case EventTypes.RoomTopic:
        return StateEvent(
          event,
          room: widget.room,
          key: Key(event.eventId),
          prevEvent: prevKey,
          nextEvent: nextKey,
          content: roomTopicEventContent(context, event),
        );
      case EventTypes.RoomName:
        if (event.content["name"] == Request.mRequestNameSpace) {
          return Container();
        }

        return StateEvent(
          event,
          room: widget.room,
          key: Key(event.eventId),
          prevEvent: prevKey,
          nextEvent: nextKey,
          content: roomNameEventContent(context, event),
        );
      case EventTypes.RoomAvatar:
        return StateEvent(
          event,
          room: widget.room,
          key: Key(event.eventId),
          prevEvent: prevKey,
          nextEvent: nextKey,
          content: roomAvatarEventContent(context, event),
        );
      /*case EventTypes.RoomCreate:
        return RoomCreateEvent(events[index],
            room: widget.room,
            key: Key(event.eventId),
            prevEvent: prevKey,
            nextEvent: nextKey);*/
      case EventTypes.Emote:
        return Event(
          event,
          room: widget.room,
          prevEvent: prevKey,
          nextEvent: nextKey,
          content: EmoteEventContent(event),
        );
      case EventTypes.File:
      case EventTypes.Audio:
        return Event(
          event,
          room: widget.room,
          prevEvent: prevKey,
          nextEvent: nextKey,
          doubleBubble: true,
          onReply: onReply,
          content: FileEventContent(event),
        );
      case EventTypes.Video:
        return Event(
          event,
          room: widget.room,
          prevEvent: prevKey,
          nextEvent: nextKey,
          hasBubble: false,
          onReply: onReply,
          content: VideoEventContent(event),
        );
      case EventTypes.Reply:
      case EventTypes.Text:
      case EventTypes.Notice:
        return Event(
          event,
          room: widget.room,
          prevEvent: prevKey,
          nextEvent: nextKey,
          hasBubble: false,
          onReply: onReply,
          content: TextEventContent(
            event,
            prevEvent: prevKey,
            nextEvent: nextKey,
            timeline: timeline,
          ),
        );
      case EventTypes.Unknown:
        if (event.typeKey == Request.mRequestNameSpace) {
          // we need to create a new dummy event, as the creator is actually someone else
          if (event.content["msgtype"] == null) {
            // fallback for missing msgtype
            event.content["msgtype"] = "m.text";
          }
          final evt = MatrixEvent.Event(
            status: event.status,
            content: event.content,
            typeKey:
                "m.room.message", // we pretend that we are a normal message event now and try again
            eventId: event.eventId,
            roomId: event.roomId,
            senderId: event.content["creator"] ?? event.sender.id,
            time: event.time,
            unsigned: event.unsigned,
            prevContent: event.prevContent,
            stateKey: event.stateKey,
            room: event.room,
          );
          return _buildEvent(context, evt, nextKey, prevKey);
        }

        return Container();
      default:
        return Container();
    }
  }

  Widget _buildItem(
    BuildContext context,
    List<MatrixEvent.Event> events,
    int index,
  ) {
    if (index == -1) return TypingEvent(widget.room);
    if (index >= events.length) return Container();

    // Get keys from next and previous events
    MatrixEvent.Event nextKey;
    MatrixEvent.Event prevKey;
    if (index > 0) nextKey = events[index - 1];
    if (index < events.length - 1) prevKey = events[index + 1];

    final event = events[index];

    return _buildEvent(context, event, nextKey, prevKey);
  }

  List<MatrixEvent.Event> filter(List<MatrixEvent.Event> events) {
    if (widget.searchQuery != null && widget.searchQuery.isNotEmpty) {
      return events
          .where((event) => event.text.toLowerCase().contains(
                widget.searchQuery.toLowerCase(),
              ))
          .toList(growable: false);
    } else {
      return events;
    }
  }

  Future<List<MatrixEvent.Event>> getList() async {
    if (timeline != null) return filter(timeline.events);

    Timeline newTimeline;
    if (widget.room.client.store != null)
      newTimeline = await widget.room.getTimeline(onUpdate: updateView);
    else {
      widget.room.prev_batch = "";
      newTimeline = Timeline(
        room: widget.room,
        events: [],
        onUpdate: updateView,
      );
      await widget.room.requestHistory();
    }

    final events = filter(newTimeline.events);

    if (events.length < widget.historyRequestCount &&
        events.length > 0 &&
        events[events.length - 1].type != EventTypes.RoomCreate) {
      newTimeline.requestHistory(historyCount: widget.historyRequestCount);
    }

    timeline = newTimeline;

    return filter(timeline.events);
  }

  sendReadReceipt(BuildContext context) async {
    if (timeline.events.length == 0 ||
        !Matrix.of(context).desktopNotificationPlugin.hasFocus) return;
    final String currentEventId = timeline.events[0].eventId;
    if (fullyReadId != currentEventId && timeline.events[0].status > 1) {
      fullyReadId = currentEventId;
      widget.room.sendReadReceipt(currentEventId);
    }
  }

  @override
  void initState() {
    _scrollController.addListener(() async {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (timeline.events.length > 0 &&
            timeline.events[timeline.events.length - 1].type !=
                EventTypes.RoomCreate) {
          await timeline.requestHistory(
              historyCount: widget.historyRequestCount);
        }
      }

      //debugPrint(_scrollController.position.pixels.toString());
      if (_scrollController.position.pixels > 200) {
        setState(() {
          showArrowDown = 1.0;
        });
      } else {
        setState(() {
          showArrowDown = 0.0;
        });
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    timeline?.sub?.cancel();
    timeline = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    fullyReadId ??= widget.room.fullyRead;

    return FutureBuilder(
        future: getList(),
        builder: (
          BuildContext context,
          AsyncSnapshot<List<MatrixEvent.Event>> snapshot,
        ) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );

          final events = snapshot.data;
          Matrix.of(context).activeRoomID = widget.room.id;
          sendReadReceipt(context);

          return Stack(
            children: <Widget>[
              ListView.builder(
                controller: _scrollController,
                reverse: true,
                itemCount: events.length + 1,
                itemBuilder: (context, index) => _buildItem(
                  context,
                  events,
                  index - 1,
                ),
              ),
              if (widget.searchQuery != null)
                Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: RaisedButton(
                      onPressed: () {
                        timeline.requestHistory(
                          historyCount: widget.historyRequestCount,
                        );
                      },
                      child: Text('Load more'),
                    ),
                  ),
                ),
              AnimatedOpacity(
                duration: Duration(milliseconds: 250),
                opacity: showArrowDown,
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 108),
                    child: Container(
                      width: 63,
                      height: 60,
                      decoration: new BoxDecoration(
                        color: Theme.of(context).brightness == Brightness.light
                            ? Colors.white
                            : Colors.black54,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(22),
                          bottomLeft: Radius.circular(22),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Color(0x14000000),
                            offset: Offset(0, 2),
                            blurRadius: 5,
                            spreadRadius: 0,
                          )
                        ],
                      ),
                      child: Material(
                        borderRadius: BorderRadius.circular(22),
                        color: Theme.of(context).brightness == Brightness.light
                            ? Colors.white
                            : Colors.black54,
                        child: IconButton(
                            icon: Icon(
                              Icons.arrow_drop_down_circle,
                              size: 37.5,
                              color: Color(0xff1fdcca),
                            ),
                            onPressed: () {
                              _scrollController.animateTo(
                                  _scrollController.position.minScrollExtent,
                                  duration: Duration(milliseconds: 300),
                                  curve: ElasticOutCurve());
                            }),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
}
