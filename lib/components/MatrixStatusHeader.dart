import 'dart:async';

import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import 'Matrix.dart';

class MatrixStatusheader extends StatefulWidget {
  @override
  _MatrixStatusHeaderState createState() => _MatrixStatusHeaderState();
}

class _MatrixStatusHeaderState extends State<MatrixStatusheader> {
  static bool connected;
  StreamSubscription syncStreamSub;
  Timer timer;

  void onSync(sync) {
    timer?.cancel();
    timer = null;
    setState(() => connected = true);
  }

  @override
  void dispose() {
    syncStreamSub?.cancel();
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Client client = Matrix.of(context).client;
    timer ??= Timer(Duration(seconds: client.connection.syncTimeoutSec + 3),
        () => setState(() => connected = false));
    syncStreamSub ??= client.connection.onSync.stream.listen(onSync);

    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 10.0),
          child: AnimatedContainer(
            duration: Duration(milliseconds: 500),
            width: 10,
            height: 10,
            decoration: BoxDecoration(
              color: connected == null
                  ? Colors.yellow
                  : connected ? Color(0xff41b5dc) : Colors.red,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
            ),
          ),
        ),
        Text(
          connected == null
              ? locale.tr(context).loading
              : connected
                  ? locale.tr(context).youAreConnectedSecurly
                  : locale.tr(context).notConnected,
          style: TextStyle(
            color: FamedlyColors.bluey_grey,
            fontSize: 12,
          ),
        ),
      ],
    );
  }
}
