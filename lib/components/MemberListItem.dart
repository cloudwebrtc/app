import 'package:famedly/components/UserSettingsPopupMenu.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'Avatar.dart';
import 'Matrix.dart';

class MemberListItem extends StatelessWidget {
  final User contact;

  const MemberListItem(this.contact);

  String _getMembershipString(Membership membership, BuildContext context) {
    switch (membership) {
      case Membership.join:
        return locale.tr(context).connected;
      case Membership.ban:
        return locale.tr(context).banned;
      case Membership.invite:
        return locale.tr(context).invited;
      case Membership.leave:
        return locale.tr(context).leftRoom;
    }
    return locale.tr(context).connected;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).brightness == Brightness.light
          ? Colors.white
          : Colors.black,
      child: InkWell(
        onLongPress: () => UserSettingsPopupMenu(context, contact).show(),
        child: Container(
          height: 77,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 16, right: 20),
                      child: Avatar(
                        contact.avatarUrl,
                        size: 44,
                        name: contact.calcDisplayname(),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(
                              contact.calcDisplayname(),
                              style: TextStyle(
                                color: Color(0xff4d576d),
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              ),
                            ),
                            Visibility(
                              visible: contact != null &&
                                  contact.powerLevel != null &&
                                  contact.powerLevel >= 100,
                              child: Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: Container(
                                  width: 59,
                                  height: 18,
                                  decoration: BoxDecoration(
                                    color: Color(0xff1fdcca),
                                    borderRadius: BorderRadius.circular(3),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        left: 13, right: 13, top: 3, bottom: 2),
                                    child: Text(
                                      locale.tr(context).admin,
                                      style: TextStyle(
                                        color: Color(0xffffffff),
                                        fontSize: 11,
                                        fontWeight: FontWeight.w600,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Visibility(
                              visible: contact != null &&
                                  contact.powerLevel != null &&
                                  contact.powerLevel >= 50 &&
                                  contact.powerLevel < 100,
                              child: Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: Container(
                                  height: 18,
                                  decoration: BoxDecoration(
                                    color: Color(0xff1fdcca),
                                    borderRadius: BorderRadius.circular(3),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        left: 13, right: 13, top: 3, bottom: 2),
                                    child: Text(
                                      locale.tr(context).moderator,
                                      style: TextStyle(
                                        color: Color(0xffffffff),
                                        fontSize: 11,
                                        fontWeight: FontWeight.w600,
                                        fontStyle: FontStyle.normal,
                                        letterSpacing: 0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        Text(
                          _getMembershipString(contact.membership, context),
                          style: TextStyle(
                            color: Color(0xff95acb9),
                            fontSize: 13,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 16),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Color(0xff95acb9),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
        onTap: () {
          if (contact.id != Matrix.of(context).client.userID)
            Navigator.of(context)
                .pushNamed("/room/${contact.room.id}/member/${contact.id}");
          else
            Navigator.of(context).pushNamed("/settings");
        },
      ),
    );
  }
}
