import 'dart:async';

import 'package:famedly/models/requests/request.dart';
import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

import 'Matrix.dart';
import 'NavScaffold.dart';
import 'UnreadBubble.dart';

class NavBottomNavigationBar extends StatefulWidget {
  final ActivePage activePage;

  NavBottomNavigationBar({
    @required this.activePage,
  });

  _NavBottomNavigationBarState createState() => _NavBottomNavigationBarState();
}

class _NavBottomNavigationBarState extends State<NavBottomNavigationBar> {
  StreamSubscription roomUpdateSub;

  @override
  void dispose() {
    roomUpdateSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    roomUpdateSub ??= Matrix.of(context)
        .client
        .connection
        .onRoomUpdate
        .stream
        .listen((r) => setState(() {}));
    return BottomAppBar(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 40),
            child: Row(
              children: <Widget>[
                FlatButton(
                  color: widget.activePage == ActivePage.REQUESTS
                      ? FamedlyColors.slate
                      : null,
                  highlightColor: FamedlyColors.light_grey,
                  key: Key("RequestsButton"),
                  padding: EdgeInsets.all(0),
                  onPressed: () {
                    if (widget.activePage != ActivePage.REQUESTS) {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          "/requests", (route) => false);
                    }
                  },
                  child: Row(
                    children: <Widget>[
                      Text(
                        locale.tr(context).requests,
                        style: TextStyle(
                          color: widget.activePage == ActivePage.REQUESTS
                              ? FamedlyColors.aqua_marine
                              : FamedlyColors.slate,
                          fontSize: 17,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: UnreadBubble(
                          Matrix.of(context)
                              .client
                              .roomList
                              .rooms
                              .where((r) => Request.isRequestRoom(r))
                              .toList(),
                          showNumbers: false,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 40),
            child: Row(
              children: <Widget>[
                FlatButton(
                  highlightColor:
                      Theme.of(context).brightness == Brightness.light
                          ? FamedlyColors.light_grey
                          : null,
                  color: widget.activePage == ActivePage.CHATS
                      ? FamedlyColors.slate
                      : null,
                  key: Key("ChatsButton"),
                  padding: EdgeInsets.all(0),
                  onPressed: () {
                    if (widget.activePage != ActivePage.CHATS) {
                      Navigator.of(context)
                          .pushNamedAndRemoveUntil("/rooms", (route) => false);
                    }
                  },
                  child: Row(
                    children: <Widget>[
                      Text(
                        locale.tr(context).chats,
                        style: TextStyle(
                          color: widget.activePage == ActivePage.CHATS
                              ? FamedlyColors.aqua_marine
                              : FamedlyColors.slate,
                          fontSize: 17,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: UnreadBubble(
                          Matrix.of(context)
                              .client
                              .roomList
                              .rooms
                              .where((r) => !Request.isRequestRoom(r))
                              .toList(),
                          showNumbers: false,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      shape: CircularNotchedRectangle(),
      notchMargin: 4.0,
    );
  }
}
