import 'package:famedly/components/FamedlyIconButton.dart';

import 'package:famedly/styles/colors.dart';
import 'package:famedly/components/SearchBar.dart';
import 'package:flutter/material.dart';
import 'NavBottomNavigationBar.dart';

enum ActivePage { REQUESTS, CHATS, TASKS }

class NavScaffold extends StatelessWidget {
  final Key key;
  final Widget title;
  final Widget leading;
  final List<Widget> actions;
  final Widget body;
  final Widget editButton;
  final TextEditingController filterTextController;
  final onTextChanged;

  /// If provided, it will override [title], [leading], [actions],
  /// [editButton] and [filterTestController].
  final AppBar appBar;

  /// If provided a bottom navigation with requests, chats and tasks
  /// buttons will be visible, highlighting the active page.
  final ActivePage activePage;

  NavScaffold(
      {this.key,
      this.title,
      this.leading,
      this.actions,
      this.body,
      this.editButton,
      this.activePage,
      this.filterTextController,
      this.onTextChanged,
      this.appBar});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      appBar: appBar != null
          ? appBar
          : PreferredSize(
              preferredSize: Size.fromHeight(130.0),
              child: AppBar(
                actions: actions,
                leading: leading,
                title: title,
                automaticallyImplyLeading: leading == null,
                bottom: PreferredSize(
                  preferredSize: const Size.fromHeight(70.0),
                  child: Padding(
                    padding: EdgeInsets.all(17.5),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: 40,
                            child: SearchBar(
                              filterTextController: filterTextController,
                              onTextChanged: onTextChanged,
                            ),
                          ),
                        ),
                        editButton != null ? editButton : Container(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
      body: body != null ? body : Container(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: activePage != null
          ? SizedBox(
              width: 66,
              height: 66,
              child: FloatingActionButton(
                key: Key("NavToTasksFloatingActionButton"),
                heroTag: null,
                elevation: 2,
                focusElevation: 3,
                highlightElevation: 4,
                backgroundColor:
                    Theme.of(context).brightness == Brightness.light
                        ? Color(0xffffffff)
                        : Theme.of(context).backgroundColor,
                child: Icon(
                  Icons.assignment,
                  size: 27,
                  color: activePage == ActivePage.TASKS
                      ? FamedlyColors.aqua_marine
                      : FamedlyColors.bluey_grey,
                ),
                onPressed: () {
                  if (activePage != ActivePage.TASKS) {
                    Navigator.of(context).pushNamed("/tasks");
                  }
                },
              ),
            )
          : null,
      bottomNavigationBar: activePage != null
          ? NavBottomNavigationBar(
              activePage: activePage,
            )
          : null,
    );
  }
}

class EditScaffoldButton extends StatelessWidget {
  final IconData iconData;
  final onTap;

  EditScaffoldButton({Key key, this.iconData, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 15),
      child: FamedlyIconButton(
        size: 40,
        iconSize: 22,
        backgroundColor: Theme.of(context).brightness == Brightness.light
            ? FamedlyColors.slate
            : null,
        iconData: iconData,
        color: FamedlyColors.aqua_marine,
        onTap: onTap,
      ),
    );
  }
}
