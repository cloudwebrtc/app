import 'package:famedly/components/Avatar.dart';
import 'package:famedly/components/FamedlyIconButton.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

typedef void AvatarActionFunction();

class ProfileScaffold extends StatelessWidget {
  final Key key;
  final Widget title;
  final List<Widget> actions;
  final Widget body;
  final MxContent profileContent;
  final String profileName;
  final List<ProfileScaffoldButton> buttons;
  final AvatarActionFunction onAvatarTap;

  ProfileScaffold(
      {this.key,
      this.title,
      this.actions,
      this.body,
      this.profileContent,
      this.profileName,
      this.onAvatarTap,
      this.buttons = const []});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: key,
        backgroundColor: Theme.of(context).brightness == Brightness.light
            ? FamedlyColors.light_grey
            : null,
        appBar: AppBar(
          actions: actions,
          title: title,
          elevation: 0,
        ),
        body: ListView(
          children: <Widget>[
            Container(
              height: 160,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      stops: [0.0, 0.5, 0.5, 1.0],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Theme.of(context).brightness == Brightness.light
                            ? Colors.white
                            : Colors.black12,
                        Theme.of(context).brightness == Brightness.light
                            ? Colors.white
                            : Colors.black12,
                        Theme.of(context).brightness == Brightness.light
                            ? FamedlyColors.light_grey
                            : Theme.of(context).backgroundColor,
                        Theme.of(context).brightness == Brightness.light
                            ? FamedlyColors.light_grey
                            : Theme.of(context).backgroundColor,
                      ])),
              child: Center(
                child: Container(
                  height: 160,
                  width: 160,
                  child: Stack(
                    children: <Widget>[
                      Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color:
                                Theme.of(context).brightness == Brightness.light
                                    ? Colors.white
                                    : Colors.black54,
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0x05041f1c),
                                  offset: Offset(0, 0),
                                  blurRadius: 10,
                                  spreadRadius: 1),
                              BoxShadow(
                                  color: Color(0x02041f1c),
                                  offset: Offset(0, 1),
                                  blurRadius: 1,
                                  spreadRadius: 0),
                              BoxShadow(
                                  color: Color(0x0c041f1c),
                                  offset: Offset(0, 2),
                                  blurRadius: 10,
                                  spreadRadius: 0),
                            ],
                            shape: BoxShape.circle,
                          ),
                          height: 130,
                          width: 130,
                          child: Center(
                            child: InkWell(
                              borderRadius: BorderRadius.circular(124),
                              onTap: () {
                                if (profileContent != null &&
                                    profileContent.mxc != null &&
                                    profileContent.mxc != "")
                                  Navigator.of(context).pushNamed(
                                      "/room/imageView/${profileContent == null ? "" : profileContent.mxc.toString().replaceFirst("mxc://", "")}");
                              },
                              child: Avatar(
                                profileContent,
                                size: 124,
                                name: profileName,
                                textSize: 30,
                              ),
                            ),
                          ),
                        ),
                      ),
                      onAvatarTap == null
                          ? Container()
                          : Positioned(
                              top: 62,
                              right: 0,
                              child: Material(
                                elevation: 2,
                                color: Theme.of(context).brightness ==
                                        Brightness.light
                                    ? Colors.white
                                    : Colors.black,
                                borderRadius: BorderRadius.circular(35),
                                child: InkWell(
                                  onTap: onAvatarTap,
                                  child: Container(
                                      width: 35,
                                      height: 35,
                                      child: Icon(FamedlyIcons.plus,
                                          color: FamedlyColors.cloudy_blue)),
                                ),
                              ))
                    ],
                  ),
                ),
              ),
            ),
            buttons.length > 0
                ? Padding(
                    padding: EdgeInsets.only(
                      top: 15,
                      left: 42,
                      right: 42,
                      bottom: 30,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: buttons,
                    ),
                  )
                : Container(),
            body != null ? body : Container()
          ],
        ));
  }
}

class ProfileScaffoldButton extends StatelessWidget {
  final IconData iconData;
  final onTap;

  ProfileScaffoldButton({Key key, this.iconData, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FamedlyIconButton(
      size: 60,
      iconSize: 24,
      backgroundColor: Theme.of(context).brightness == Brightness.light
          ? Colors.white
          : Colors.black87,
      iconData: iconData,
      onTap: onTap,
    );
  }
}
