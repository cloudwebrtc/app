import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:flutter/material.dart';

typedef OnTap = void Function();

class RequestGroupListTile extends StatelessWidget {
  const RequestGroupListTile({
    Key key,
    this.iconData = FamedlyIcons.archive,
    this.title = "",
    this.unread = 0,
    this.highlightUnread = false,
    this.onTap,
  }) : super(key: key);

  final IconData iconData;
  final String title;
  final int unread;
  final bool highlightUnread;
  final OnTap onTap;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          onTap: onTap,
          title: Text(title,
              style: TextStyle(
                  color: Theme.of(context).brightness == Brightness.light
                      ? FamedlyColors.slate
                      : Color(0xff95acb9),
                  fontSize: 15)),
          leading: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Icon(iconData,
                color: Theme.of(context).brightness == Brightness.light
                    ? FamedlyColors.slate
                    : Color(0xff95acb9)),
          ),
          trailing: highlightUnread
              ? Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                      color: FamedlyColors.grapefruit,
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                      child: Text(unread != null ? unread.toString() : "",
                          style: TextStyle(color: Colors.white, fontSize: 10))),
                )
              : Text(unread != null ? unread.toString() : "",
                  style:
                      TextStyle(fontSize: 15, color: FamedlyColors.bluey_grey)),
        ),
        Row(
          children: <Widget>[
            Container(
              width: 70,
              height: 1,
              color: Colors.transparent,
            ),
            Expanded(
              child: Container(
                height: 1,
                color: Theme.of(context).brightness == Brightness.light
                    ? FamedlyColors.pale_grey
                    : Colors.black,
              ),
            ),
          ],
        )
      ],
    );
  }
}
