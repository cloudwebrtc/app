import 'dart:async';

import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/roomsList/RequestItem.dart';
import 'package:famedly/components/roomsList/RequestBundleItem.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/requests/RequestContent.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

typedef RoomFilter = bool Function(Room room);

class RequestsList extends StatefulWidget {
  final RoomFilter filter;
  final onlyLeft;
  final TextEditingController filterTextGetter;
  final String activeChatID;

  /// Widget shown when there are no
  /// chats to list.
  final Widget emptyListPlaceholder;

  RequestsList(
    this.filterTextGetter, {
    this.onlyLeft = false,
    this.filter,
    this.activeChatID,
    this.emptyListPlaceholder,
    Key key,
  }) : super(key: key);

  @override
  RequestsListState createState() => new RequestsListState();
}

class RequestsListState extends State<RequestsList> {
  RoomList roomList;
  List<RequestBundle> requests;

  MatrixState matrix;
  String filterText = null;

  void handleFilter() {
    if (widget.filterTextGetter == null) return;
    setState(() {
      filterText = widget.filterTextGetter.text;
    });
  }

  void rebuildView() {
    roomList = null;
    setState(() {});
  }

  void updateView() {
    if (!mounted) return;
    if (roomList != null) setState(() {});
  }

  @override
  initState() {
    super.initState();
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.addListener(handleFilter);
    }
  }

  List<RequestBundle> filter() {
    return requests?.where((bundle) {
      if (filterText == null || filterText == "") return true;
      if (bundle.broadcastRoom != null) {
        final req = Request(bundle.broadcastRoom);
        return req.title.toLowerCase().contains(filterText.toLowerCase());
      } else if (bundle.rooms.length > 0) {
        final req = Request(bundle.rooms[0]);
        return req.title.toLowerCase().contains(filterText.toLowerCase());
      } else {
        return false;
      }
    })?.toList();
  }

  Widget _buildItem(
      List<RequestBundle> doRequests, BuildContext context, int index) {
    if (index >= doRequests.length) return Container();
    final request = doRequests[index];
    if (request.broadcastRoom != null) {
      final req = Request(request.broadcastRoom);
      if (req.isAuthor() || request.rooms.length > 1) {
        return RequestBundleItem(
          key: Key("request_$index"),
          request: request,
        );
      } else if (request.rooms.length == 1) {
        return RequestItem(
          key: Key("request_$index"),
          room: request.rooms[0],
        );
      } else {
        return RequestItem(
          key: Key("request_$index"),
          room: request.broadcastRoom,
          contacts: Request.getBroadcastContacts(
              request.broadcastRoom, matrix.client.userID),
        );
      }
    } else if (request.rooms.length != 1) {
      return RequestBundleItem(
        key: Key("request_$index"),
        request: request,
      );
    } else {
      return RequestItem(
        key: Key("request_$index"),
        room: request.rooms[0],
      );
    }
  }

  Future<RoomList> getList() async {
    RoomList list;
    if (roomList != null) list = roomList;
    if (widget.onlyLeft) {
      list =
          RoomList(rooms: await matrix.client.archive, client: matrix.client);
    } else
      list = await matrix.client.getRoomList(onUpdate: updateView);
    return list;
  }

  @override
  void dispose() {
    roomList?.eventSub?.cancel();
    roomList?.firstSyncSub?.cancel();
    roomList?.roomSub?.cancel();
    roomList = null;
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.removeListener(handleFilter);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    matrix = Matrix.of(context);

    return FutureBuilder(
        future: getList(),
        builder: (BuildContext context, AsyncSnapshot<RoomList> snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );

          roomList = snapshot.data;
          requests = [];
          for (final room in roomList.rooms) {
            if (!Request.isRequestRoom(room)) continue;
            if (widget.filter != null && !widget.filter(room)) continue;
            if (room.states[Request.mRequestNameSpace] == null) {
              // generate a random place holder ID
              final uuid = new Uuid();
              requests.add(new RequestBundle(id: uuid.v4(), rooms: [room]));
              continue;
            }
            final req = Request(room);
            final RequestContent content = RequestContent.fromJson(
                room.states[Request.mRequestNameSpace].content);
            if (req.content.requestId == null) {
              // we are an old request without an ID.
              // let's just assign a random one for displaying and continue as normal
              final uuid = new Uuid();
              requests.add(new RequestBundle(id: uuid.v4(), rooms: [room]));
              continue;
            }
            if (!req.isAuthor()) {
              // we aren't the author so we don't bundle this with other reqeusts
              final uuid = new Uuid();
              if (req.broadcast != null) {
                final bundle = new RequestBundle(id: uuid.v4(), rooms: []);
                bundle.broadcastRoom = room;
                requests.add(bundle);
              } else {
                requests.add(new RequestBundle(id: uuid.v4(), rooms: [room]));
              }
              continue;
            }
            RequestBundle r = null;
            for (final rr in requests) {
              if (rr.id == req.content.requestId) {
                r = rr;
                break;
              }
            }
            if (r == null) {
              // create new request object
              if (req.broadcast != null) {
                final bundle =
                    new RequestBundle(id: content.requestId, rooms: []);
                bundle.broadcastRoom = room;
                requests.add(bundle);
              } else {
                requests.add(
                    new RequestBundle(id: content.requestId, rooms: [room]));
              }
            } else {
              // add room to existing request object
              if (req.broadcast != null) {
                r.broadcastRoom = room;
              } else {
                r.rooms.add(room);
              }
            }
          }

          final filteredRequests = filter();

          if (filteredRequests == null || filteredRequests.isEmpty) {
            return widget.emptyListPlaceholder ?? Container();
          }

          return ListView.builder(
            shrinkWrap: true,
            itemCount: requests.length,
            itemBuilder: (context, index) => _buildItem(
              filteredRequests,
              context,
              index,
            ),
          );
        });
  }
}
