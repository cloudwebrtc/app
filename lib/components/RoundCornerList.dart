import 'package:flutter/material.dart';

typedef Widget ItemGeneratorFunction<T>(
  BuildContext context,
  T item,
  ActionFunction target,
  Icon icon,
  dynamic pop,
);

typedef void ActionFunction();

class RoundListGen<T> extends StatelessWidget {
  final List<T> items;
  final List<Icon> icons;
  final List<ActionFunction> targets;
  final ItemGeneratorFunction<T> itemGeneratorFunction;
  final bool iconsActivated;

  RoundListGen(
      {Key key,
      @required this.itemGeneratorFunction,
      @required this.items,
      this.icons,
      @required this.targets,
      this.iconsActivated = true})
      : super(key: key) {
    assert(
        this.items.isNotEmpty, "The items list needs to contain some content");
    if (this.iconsActivated)
      assert(this.icons.isNotEmpty,
          "The icons list needs to contain some content");
    assert(this.targets.isNotEmpty,
        "The targets list needs to contain some content");
    assert(this.targets.length == this.items.length,
        "The targets list needs to be the same length as the items list");
    if (this.iconsActivated)
      assert(this.icons.length == this.items.length,
          "The targets list needs to be the same length as the items list");
  }

  @override
  Widget build(BuildContext context) {
    dynamic pop = () {
      Navigator.of(context).pop();
    };

    return Padding(
      padding: EdgeInsets.only(top: 6, left: 6, right: 6, bottom: 56),
      child: Container(
        decoration: new BoxDecoration(
          color: Color(0xffedf0f2),
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              color: Color(0x0c000000),
              offset: Offset(0, -1),
              blurRadius: 1,
              spreadRadius: 0,
            ),
            BoxShadow(
              color: Color(0x19000000),
              offset: Offset(0, 1),
              blurRadius: 1,
              spreadRadius: 0,
            ),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: ListView.separated(
            separatorBuilder: (context, index) => this.iconsActivated
                ? Padding(
                    padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width - 91),
                    // We basicly do the line in a inverse way ^^
                    child: Container(
                      height: 1,
                      color: Colors.white,
                    ),
                  )
                : Padding(
                    padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width - 26),
                    // We basicly do the line in a inverse way ^^
                    child: Container(
                      height: 1,
                      color: Colors.white,
                    ),
                  ),
            itemCount: items.length,
            shrinkWrap: true,
            itemBuilder: (context, index) => this.itemGeneratorFunction(
              context,
              items[index],
              targets[index],
              this.iconsActivated ? icons[index] : null,
              pop,
            ),
          ),
        ),
      ),
    );
  }
}
