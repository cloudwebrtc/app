import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';

class SentrySwitchListTile extends StatefulWidget {
  @override
  _SentrySwitchListTileState createState() => _SentrySwitchListTileState();
}

class _SentrySwitchListTileState extends State<SentrySwitchListTile> {
  bool value = false;

  final LocalStorage storage = LocalStorage("FamedlyLocalStorage");

  void update() {
    setState(() => value = storage.getItem("sentry") ?? true);
  }

  @override
  Widget build(BuildContext context) {
    storage.ready.whenComplete(update);
    return SwitchListTile(
      title: Text(locale.tr(context).sendCrashReportsToFamedly),
      value: value,
      onChanged: (bool b) async {
        await storage.ready;
        storage.setItem("sentry", b);
        update();
      },
    );
  }
}
