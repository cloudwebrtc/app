import 'package:flutter/material.dart';

class SettingsMenuItem extends StatelessWidget {
  final Icon icon;
  final String title;
  final Widget data;
  final GestureTapCallback onTap;

  const SettingsMenuItem(
    this.icon,
    this.title,
    this.data,
    this.onTap, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).brightness == Brightness.light
          ? Colors.white
          : Colors.black26,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      elevation: 0,
      child: InkWell(
        child: Container(
          height: 64,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 16, right: 20),
                      child: this.icon,
                    ),
                    Text(
                      this.title,
                      style: TextStyle(
                        color: Theme.of(context).brightness == Brightness.light
                            ? Color(0xff4d576d)
                            : Color(0xff95acb9),
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                        letterSpacing: 0,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 20),
                      child: this.data,
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 16),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Color(0xff95acb9),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
        onTap: this.onTap,
      ),
    );
  }
}
