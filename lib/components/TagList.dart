import 'dart:async';

import 'package:famedly/components/Matrix.dart';

import 'package:famedly/models/directory/directory.dart';
import 'package:famedly/models/directory/tag.dart';
import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

class TagChecked {
  final String id;
  final String description;
  bool checked;

  TagChecked({this.id, this.description, this.checked});
}

List<TagChecked> tagList;

class TagList extends StatefulWidget {
  final TextEditingController filterTextGetter;

  static TagListState of(BuildContext context) =>
      context.findAncestorStateOfType<State<TagList>>();

  TagList(
    this.filterTextGetter, {
    Key key,
  }) : super(key: key);

  @override
  TagListState createState() => TagListState();

  List<TagChecked> getTagList() {
    return tagList;
  }
}

class TagListState extends State<TagList> {
  String filterText = null;

  void handleFilter() {
    if (widget.filterTextGetter == null) return;
    setState(() {
      filterText = widget.filterTextGetter.text;
    });
  }

  @override
  initState() {
    super.initState();
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.addListener(handleFilter);
    }
  }

  Future<List<TagChecked>> getTags(BuildContext context) async {
    if (tagList != null) return tagList;
    Directory directory = Directory(Matrix.of(context).client);
    List<Tag> tagsDirectory = await directory.getTags();
    List<TagChecked> tags = [];
    // add the checked property
    for (final tag in tagsDirectory) {
      tags.add(new TagChecked(
          id: tag.id, description: tag.description, checked: false));
    }
    return tags;
  }

  @override
  void dispose() {
    if (widget.filterTextGetter != null) {
      widget.filterTextGetter.removeListener(handleFilter);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<TagChecked>>(
        future: getTags(context),
        builder:
            (BuildContext context, AsyncSnapshot<List<TagChecked>> snapshot) {
          if (!snapshot.hasData)
            return Center(child: CircularProgressIndicator());
          if (snapshot.data == null) {
            return Center(
                child: Text(locale.tr(context).oopsSomethingWentWrong +
                    " " +
                    locale.tr(context).organisationServerIsNotResponding));
          }
          tagList = snapshot.data;
          if (tagList.length == 0)
            return Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  locale.tr(context).noContactPointsVisibleToYou,
                  textAlign: TextAlign.center,
                ),
              ),
            );
          return ListView.separated(
            separatorBuilder: (BuildContext context, int index) {
              return Divider(color: FamedlyColors.pale_grey, indent: 15);
            },
            itemCount: tagList.length,
            itemBuilder: (BuildContext context, int index) {
              if ((filterText != null &&
                  filterText != "" &&
                  !tagList[index]
                      .description
                      .toLowerCase()
                      .contains(filterText.toLowerCase()))) {
//                tagList[index].checked = false;
                return Container();
              }
              return ListTile(
                trailing: Checkbox(
                  value: tagList[index].checked,
                  onChanged: (v) {
                    setState(() {
                      tagList[index].checked = v;
                    });
                  },
                ),
                title: Text(tagList[index].description),
                onTap: () {
                  setState(() {
                    tagList[index].checked = !tagList[index].checked;
                  });
                },
              );
            },
          );
        });
  }
}
