import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

class UnreadBubble extends StatelessWidget {
  const UnreadBubble(this.rooms, {Key key, this.showNumbers = true})
      : super(key: key);

  final List<Room> rooms;
  final bool showNumbers;

  static const double padding = 2.0;
  static const double size = 20.0;

  @override
  Widget build(BuildContext context) {
    int count = 0;
    for (final room in rooms) {
      count += room.notificationCount;
      if (room.membership == Membership.invite) count++;
    }
    if (count > 99) count = 99;

    if (count > 0)
      return Container(
        margin: EdgeInsets.all(padding),
        decoration: BoxDecoration(
          color: FamedlyColors.grapefruit,
          borderRadius: BorderRadius.all(Radius.circular(size)),
        ),
        width: showNumbers ? size : size / 2,
        height: showNumbers ? size : size / 2,
        child: showNumbers
            ? Center(
                child: Text(
                  count.toString(),
                  key: key,
                  style: TextStyle(
                    color: Color(0xffffffff),
                    fontSize: 10,
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                  ),
                ),
              )
            : Container(),
      );
    return showNumbers
        ? Container(
            width: 1,
            height: size,
            margin: EdgeInsets.all(padding),
          )
        : Container(
            width: 0,
            height: 0,
          );
  }
}
