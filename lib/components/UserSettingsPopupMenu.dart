import 'package:famedly/components/dialogs/ConfirmDialog.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import '../config/FamedlyLocalizations.dart';

class UserSettingsPopupMenu extends StatelessWidget {
  final BuildContext context;
  final User user;

  const UserSettingsPopupMenu(this.context, this.user);

  Widget build(BuildContext context) => PopupMenuButton(
        key: Key("userSettingsPopupMenu"),
        itemBuilder: (context) => _itemList,
        onSelected: (result) => this._onSelected(result),
      );

  /// Whether there are any actions in this popupmenu
  bool get hasActions => _itemList.length > 0;

  void _onSelected(String result) {
    switch (result) {
      case "admin":
        showDialog(
          context: context,
          builder: (c) => ConfirmDialog(
            text: locale.tr(context).askMakeAdmin(user.calcDisplayname()),
            callback: (c) => CustomDialogs(context)
                .tryRequestWithLoadingDialogs(user.setPower(100)),
          ),
        );
        break;
      case "kick":
        showDialog(
          context: context,
          builder: (c) => ConfirmDialog(
            text: locale.tr(context).askKickUser(user.calcDisplayname()),
            callback: (c) => CustomDialogs(context)
                .tryRequestWithLoadingDialogs(user.kick()),
          ),
        );
        break;
    }
  }

  List<PopupMenuItem<String>> get _itemList {
    List<PopupMenuItem<String>> list = <PopupMenuItem<String>>[];
    if (user.canChangePowerLevel)
      list.add(PopupMenuItem<String>(
          key: Key("UserSettingsPopupMenuAdminButton"),
          child: Text(locale.tr(context).makeAnAdmin),
          value: "admin"));
    if (user.canKick &&
        user.membership != Membership.leave &&
        user.membership != Membership.ban)
      list.add(PopupMenuItem<String>(
          key: Key("UserSettingsPopupMenuKickButton"),
          child: Text(locale.tr(context).kick),
          value: "kick"));
    return list;
  }

  void show() async {
    List<PopupMenuItem<String>> list = this._itemList;
    if (list.length == 0) return;

    final RenderBox bar = context.findRenderObject();
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();
    final RelativeRect position = RelativeRect.fromRect(
      Rect.fromPoints(
        bar.localToGlobal(bar.size.bottomLeft(Offset.zero), ancestor: overlay),
        bar.localToGlobal(bar.size.bottomRight(Offset.zero), ancestor: overlay),
      ),
      Offset.zero & overlay.size,
    );

    final String result = await showMenu<String>(
      context: context,
      position: position,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      items: list,
    );
    _onSelected(result);
  }
}
