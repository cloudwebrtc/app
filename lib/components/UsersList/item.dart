import 'package:famedly/components/Avatar.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

/// Generates the widget for each room item.
class UserItem extends StatelessWidget {
  final User contact;

  final VoidCallback onTap;

  UserItem({key, @required this.contact, @required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Avatar(contact.avatarUrl,
            size: 45, name: contact.calcDisplayname()),
        title: Text(contact.calcDisplayname()),
        onTap: () {
          onTap();
        });
  }
}
