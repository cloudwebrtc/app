import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedly/components/dialogs/ConfirmDialog.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

typedef PostArchiveAction = void Function();

class ArchiveRoomDialog extends StatelessWidget {
  final Room room;
  final PostArchiveAction postArchiveAction;

  const ArchiveRoomDialog({Key key, this.room, this.postArchiveAction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConfirmDialog(
      key: Key("ArchiveRoomDialog"),
      text: locale.tr(context).confirmarchivingdialog,
      ok: locale.tr(context).archiveAction.toUpperCase(),
      autopopContext: false,
      callback: (BuildContext context) async {
        final success = await CustomDialogs(context)
            .tryRequestWithLoadingDialogs(room.leave());
        if (success != false) {
          Navigator.of(context)?.pop();
          if (postArchiveAction != null) postArchiveAction();
        }
      },
    );
  }
}
