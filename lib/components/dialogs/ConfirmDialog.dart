import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';

class ConfirmDialog extends StatelessWidget {
  final String text;
  final String back;
  final String ok;
  final void Function(BuildContext context) callback;
  final bool autopopContext;

  const ConfirmDialog({
    Key key,
    this.text,
    this.back,
    this.ok,
    this.callback,
    this.autopopContext = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      key: Key("ConfirmDialog"),
      content: Text(text),
      actions: <Widget>[
        FlatButton(
            key: Key("ConfirmDialogBackButton"),
            child: Text(back ?? locale.tr(context).back.toUpperCase(),
                style: TextStyle(color: FamedlyColors.aqua_marine)),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        FlatButton(
          key: Key("ConfirmDialogOkButton"),
          child: Text(ok ?? locale.tr(context).yes.toUpperCase(),
              style: TextStyle(color: FamedlyColors.aqua_marine)),
          onPressed: () {
            if (autopopContext) Navigator.of(context).pop();
            if (callback != null) callback(context);
          },
        ),
      ],
    );
  }
}
