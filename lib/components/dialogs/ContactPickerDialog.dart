import 'package:flutter/material.dart';
import 'package:famedly/models/requests/BroadcastContent.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/components/dialogs/PickerDialog.dart';

typedef PickCallback = void Function(BroadcastContactsContent contact);

class ContactPickerDialog extends StatelessWidget {
  final List<BroadcastContactsContent> contacts;
  final PickCallback callback;

  const ContactPickerDialog({Key key, this.contacts, this.callback}) : super(key: key);

  Widget _buildItem(BuildContext context, BroadcastContactsContent contact) {
    return ListTile(
      title: Text(contact.organisationName,
        style: TextStyle(
          fontSize: 15,
          color: FamedlyColors.slate,
          fontWeight: FontWeight.w600,
        ),
      ),
      subtitle: Text(contact.contactDescription,
        style: TextStyle(
          fontSize: 13,
          color: FamedlyColors.slate,
        ),
      ),
      onTap: () {
        this.callback(contact);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return PickerDialog<BroadcastContactsContent>(
      items: this.contacts,
      title: locale.tr(context).interactAsWho,
      buildItem: _buildItem,
    );
  }
}
