import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';

class InformDialog extends StatelessWidget {
  final String text;
  final String back;
  final void Function(BuildContext context) callback;
  final bool autopopContext;

  const InformDialog({
    Key key,
    this.text,
    this.back,
    this.callback,
    this.autopopContext = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Text(text),
      actions: <Widget>[
        FlatButton(
          child: Text(back ?? locale.tr(context).back.toUpperCase(), style: TextStyle(color: FamedlyColors.aqua_marine)),
          onPressed: () {
            if (autopopContext) Navigator.of(context).pop();
            if (callback != null) callback(context);
          }
        ),
      ],
    );
  }

  static void show({Key key, BuildContext context, String text, String back, void Function(BuildContext context) callback, bool autopopContext = true}) {
    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return InformDialog(
          key: key,
          text: text,
          back: back,
          callback: callback,
          autopopContext: autopopContext,
        );
      },
    );
  }
}
