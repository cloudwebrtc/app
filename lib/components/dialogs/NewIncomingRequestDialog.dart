import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:famedly/models/requests/BroadcastContent.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedly/components/dialogs/ConfirmDialog.dart';
import 'package:famedly/components/dialogs/ContactPickerDialog.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

class NewIncomingRequestDialogButton extends StatelessWidget {
  final Color color;
  final String text;
  final onTap;

  const NewIncomingRequestDialogButton({Key key, this.color, this.text, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        decoration: new BoxDecoration(
          color: color,
          borderRadius: new BorderRadius.all(Radius.circular(10)),
        ),
        height: 50,
        alignment: Alignment(0.0, 0.0),
        margin: EdgeInsets.all(5),
        padding: EdgeInsets.all(16),
        child: Text(text, style: TextStyle(
          color: Color(0xFFFFFFFF),
        )),
      ),
      onTap: onTap,
    );
  }
}

class NewIncomingRequestDialog extends StatelessWidget {
  final Room room;
  final List<BroadcastContactsContent> contacts;

  const NewIncomingRequestDialog({Key key, this.room, this.contacts}) : super(key: key);

  List<BroadcastContactsContent> getUpdatedContactsList() {
    List<BroadcastContactsContent> requestContacts;
    if (Request(room).broadcast != null && (this.contacts == null || this.contacts.length == 0)) {
      requestContacts = Request.getBroadcastContacts(room, room.client.userID);
    }
    if (requestContacts == null) {
      requestContacts = this.contacts;
    }
    return requestContacts;
  }

  void getContact(BuildContext context, cb) {
    final requestContacts = getUpdatedContactsList();
    if (requestContacts == null) {
      cb(null);
      return;
    }
    if (requestContacts.length == 1) {
      cb(requestContacts[0]);
      return;
    } else {
      // we need to show a contact picker
      showDialog(
        context: context,
        builder: (BuildContext ctx) {
          return ContactPickerDialog(
            contacts: requestContacts,
            callback: (BroadcastContactsContent contact) {
              Navigator.of(ctx).pop();
              cb(contact);
            },
          );
        }
      );
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    final req = Request(room);
    if (!req.isRequest) {
      return null; // something went really wrong
    }
    List<Widget> popupActions = [];
    if (room.membership != Membership.leave) {
      popupActions.add(NewIncomingRequestDialogButton(color: FamedlyColors.yellow, text: locale.tr(context).needInfoInquiry, onTap: () async {
        getContact(context, (BroadcastContactsContent contact) async {
          CustomDialogs dialogs = CustomDialogs(context);
          final reqq = Request(room, contact: contact);
          final roomId = await dialogs.tryRequestWithLoadingDialogs(reqq.needInfo());
          if (roomId != false) {
            final requestContacts = getUpdatedContactsList();
            if (roomId != room.id && requestContacts != null && requestContacts.length <= 1) {
              // we need to leave the old broadcast room
              await dialogs.tryRequestWithLoadingDialogs(room.leave());
            }
            Navigator.of(context)
              .pushReplacementNamed("/room/incomingRequests/${roomId}");
          }
        });
      }));
      popupActions.add(NewIncomingRequestDialogButton(color: FamedlyColors.aqua_marine, text: locale.tr(context).acceptInquiry, onTap: () async {
        getContact(context, (BroadcastContactsContent contact) async {
          CustomDialogs dialogs = CustomDialogs(context);
          final reqq = Request(room, contact: contact);
          final roomId = await dialogs.tryRequestWithLoadingDialogs(reqq.accept());
          if (roomId != false) {
            final requestContacts = getUpdatedContactsList();
            if (roomId != room.id && requestContacts != null && requestContacts.length <= 1) {
              // we need to leave the old broadcast room
              await dialogs.tryRequestWithLoadingDialogs(room.leave());
            }
            Navigator.of(context)
            .pushReplacementNamed("/room/incomingRequests/${roomId}");
          }
        });
      }));
      popupActions.add(NewIncomingRequestDialogButton(color: FamedlyColors.grapefruit, text: locale.tr(context).rejectInquiry, onTap: () async {
        getContact(context, (BroadcastContactsContent contact) {
          showDialog(
            context: context,
            builder: (BuildContext ctx) {
              return ConfirmDialog(
                text: locale.tr(context).rejectAndArchiveInquiryWarning,
                callback: (BuildContext ctx) async {
                  CustomDialogs dialogs = CustomDialogs(context);
                  final reqq = Request(room, contact: contact);
                  final success = await dialogs.tryRequestWithLoadingDialogs(reqq.reject());
                  if (success != false) {
                    final requestContacts = getUpdatedContactsList();
                    if ((requestContacts != null && requestContacts.length <= 1) || requestContacts == null) {
                      // we need to leave the old broadcast room
                      await dialogs.tryRequestWithLoadingDialogs(room.leave());
                    }
                    Navigator.of(context)
                    .pushNamedAndRemoveUntil("/incomingRequests", (route) => route.isFirst);
                  }
                }
              );
            },
          );
        });
      }));
    } else {
      popupActions.add(Text(locale.tr(context).archivedInquiryNotice));
    }
    return Dialog(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            alignment: Alignment(-1.0, -1.0),
            margin: EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                Text(req.title,
                  style: TextStyle(
                    fontSize: 15,
                    color: FamedlyColors.slate,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(req.content.body),
              ],
            ),
          ),
          Container(
            color: FamedlyColors.light_grey,
            height: 1,
          ),
          Container(
            alignment: Alignment(0.0, 0.0),
            margin: EdgeInsets.all(20),
            child: Column(
              children: popupActions,
            ),
          ),
        ],
      ),
    );
  }
}
