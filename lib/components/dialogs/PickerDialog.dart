import 'package:flutter/material.dart';
import 'package:famedly/styles/colors.dart';

class PickerDialog<T> extends StatelessWidget {
  final List<T> items;
  final Widget Function(BuildContext context, T item) buildItem;
  final String title;

  const PickerDialog({Key key, this.items, this.buildItem, this.title}) : super(key: key);

  Widget _buildItem(BuildContext context, int index) {
    if (index >= this.items.length) return Container();
    final item = this.items[index];
    return buildItem(context, item);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            alignment: Alignment(-1.0, -1.0),
            margin: EdgeInsets.all(20),
            child: Text(title,
              style: TextStyle(
                fontSize: 15,
                color: FamedlyColors.slate,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Container(
            color: FamedlyColors.light_grey,
            height: 1,
          ),
          ListView.builder(
            shrinkWrap: true,
            itemCount: this.items.length,
            itemBuilder: _buildItem,
          ),
        ],
      ),
    );
  }
}
