import 'package:famedly/components/UnreadBubble.dart';

import 'package:famedly/styles/colors.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/requests/RequestContent.dart';
import 'package:famedly/models/requests/BroadcastContent.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

/// Generates the widget for each request item.
class RequestBundleItem extends StatelessWidget {
  final RequestBundle request;

  const RequestBundleItem({Key key, @required this.request}) : super(key: key);

  Color get newsColor {
    for (final room in request.rooms) {
      if (room.highlightCount > 0) return Color(0xffff5252);
      if (room.notificationCount > 0) return FamedlyColors.emerald;
    }
    return Colors.grey;
  }

  @override
  Widget build(BuildContext context) {
    String displayname = "";
    String description = "";
    ChatTime creationTime = null;
    int accepted = 0;
    int needInfo = 0;
    int rejected = 0;
    if (request.broadcastRoom != null) {
      final broadcast = BroadcastContent.fromJson(request.broadcastRoom.states[Request.mBroadcastNameSpace].content);
      displayname = broadcast.title;
      description = locale.tr(context).broadcast + ": " + broadcast.contacts.length.toString() + " " + locale.tr(context).contacts;
      creationTime = request.broadcastRoom.timeCreated;

      for (final contact in broadcast.contacts) {
        Request r;
        for (final room in request.rooms) {
          Request rr = Request(room);
          if (rr.content.contactId == contact.contactId) {
            r = rr;
            break;
          }
        }
        if (r == null) {
          r = Request(request.broadcastRoom, contact: contact);
        }
        if (r.remoteAccepted()) {
          accepted++;
        }
        if (r.remoteNeedInfo()) {
          needInfo++;
        }
        if (r.remoteRejected()) {
          rejected++;
        }
      }
    } else {
      // fallback, no broadcast room present
      displayname = locale.tr(context).broadcast;
      description = locale.tr(context).broadcast + ": " + request.rooms.length.toString() + " " + locale.tr(context).contacts;
      if (request.rooms.length > 0) {
        final state = request.rooms[0].states[Request.mRequestNameSpace];
        if (state != null) {
          final content = RequestContent.fromJson(state.content);
          if (content.requestTitle != "") {
            displayname = content.requestTitle;
          }
        }
      }

      creationTime = request.rooms[0].timeCreated;
      for (final room in request.rooms) {
        if (room.timeCreated < creationTime) {
          creationTime = room.timeCreated;
        }
      }

      for (final room in request.rooms) {
        final r = Request(room);
        if (r.remoteAccepted()) {
          accepted++;
        }
        if (r.remoteNeedInfo()) {
          needInfo++;
        }
        if (r.remoteRejected()) {
          rejected++;
        }
      }
    }

    return ListTile(
      title: Text(displayname,
          style: TextStyle(
            fontSize: 15,
            color: FamedlyColors.slate,
            fontWeight: FontWeight.w600,
          )),
      subtitle: Text(description,
          style: TextStyle(
            fontSize: 13,
            color: FamedlyColors.slate,
          )),
      trailing: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(accepted.toString(),
                  style: TextStyle(
                    color: FamedlyColors.aqua_marine,
                  )),
                Text("/"),
                Text(needInfo.toString(),
                  style: TextStyle(
                    color: FamedlyColors.yellow,
                  )),
                Text("/"),
                Text(rejected.toString(),
                  style: TextStyle(
                    color: FamedlyColors.grapefruit,
                  )),
                UnreadBubble(request.rooms),
              ],
              mainAxisSize: MainAxisSize.min,
            ),
            Text(
              creationTime.toString(),
              key: Key("time_created"),
              style: TextStyle(
                color: FamedlyColors.bluey_grey,
                fontSize: 13,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            ),
          ],
        ),
      ),
      onTap: () async {
        Navigator.of(context).pushNamed("/broadcast/" + request.id);
      },
    );
  }
}
