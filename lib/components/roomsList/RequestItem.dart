import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/UnreadBubble.dart';
import 'package:famedly/components/dialogs/ArchiveRoomDialog.dart';

import 'package:famedly/models/requests/BroadcastContent.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:famedly/components/dialogs/NewIncomingRequestDialog.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:famedly/components/dialogs/InformDialog.dart';

/// Generates the widget for each room item.
class RequestItem extends StatelessWidget {
  final Room room;
  final List<BroadcastContactsContent> contacts;

  const RequestItem({Key key, @required this.room, this.contacts})
      : super(key: key);

  Color get newsColor {
    if (room.highlightCount > 0) return FamedlyColors.grapefruit;
    if (room.notificationCount > 0) return FamedlyColors.emerald;
    return Colors.grey;
  }

  @override
  Widget build(BuildContext context) {
    Request req;
    if (this.contacts != null && this.contacts.length == 1) {
      req = Request(room, contact: contacts[0]);
    } else {
      req = Request(room);
    }
    bool multiContact = this.contacts != null && this.contacts.length != 1;
    int numContacts = this.contacts != null ? this.contacts.length : 1;
    String title = req.title;
    String organisation =
        req.organisation ?? locale.tr(context).incominginquiries;
    String description =
        req.description ?? locale.tr(context).NoDescriptionFound;
    if (multiContact) {
      organisation = locale.tr(context).broadcast;
      description = numContacts.toString() + " " + locale.tr(context).contacts;
    }

    Color statusColor = FamedlyColors.cloudy_blue;
    if (req.remoteAccepted()) {
      statusColor = FamedlyColors.aqua_marine;
    }
    if (req.remoteNeedInfo()) {
      statusColor = FamedlyColors.yellow;
    }
    if (req.remoteRejected()) {
      statusColor = FamedlyColors.grapefruit;
    }

    List<Widget> secondarySwipeActions =
        room.membership != Membership.leave && this.contacts == null
            ? [
                IconSlideAction(
                  color: Color(0xffff5252),
                  icon: FamedlyIcons.trash,
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext innerContext) {
                        // return object of type Dialog
                        return ArchiveRoomDialog(
                          room: room,
                          postArchiveAction: () {},
                        );
                      },
                    );
                  },
                ),
              ]
            : null;

    return Slidable(
      actionPane: SlidableBehindActionPane(),
      secondaryActions: secondarySwipeActions,
      child: Material(
        color: Colors.white,
        child: Row(
          children: <Widget>[
            Container(
              color: statusColor,
              width: 5,
              height: 70,
            ),
            Expanded(
              child: ListTile(
                title: Text(title,
                    style: TextStyle(
                      fontSize: 15,
                      color: FamedlyColors.slate,
                      fontWeight: FontWeight.w600,
                    )),
                subtitle: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(organisation,
                          style: TextStyle(
                            fontSize: 13,
                            color: room.membership == Membership.invite
                                ? FamedlyColors.grapefruit
                                : FamedlyColors.slate,
                          )),
                      Text(description,
                          style: TextStyle(
                            fontSize: 13,
                            color: FamedlyColors.slate,
                          )),
                    ],
                  ),
                ),
                trailing: Container(
                  child: Column(
                    children: <Widget>[
                      this.contacts != null && this.contacts.length == 1 ? Container(width: 1) : UnreadBubble([room]),
                      Text(
                        room.timeCreated.toString(),
                        key: Key("time_created"),
                        style: TextStyle(
                          color: FamedlyColors.bluey_grey,
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () async {
                  if (await Matrix.of(context)
                      .roomsPlugin
                      .joinRoomAndWait(context, room)) {
                    req = Request(room);
                    List<BroadcastContactsContent> requestContacts;
                    if (req.broadcast != null &&
                        (this.contacts == null || this.contacts.length == 0)) {
                      requestContacts = Request.getBroadcastContacts(
                          room, room.client.userID);
                    }
                    if (requestContacts != null || this.contacts != null) {
                      if (req.isAuthor()) {
                        // inform the user that there is no associated room
                        showDialog(
                          context: context,
                          builder: (BuildContext ctx) {
                            return InformDialog(
                              text: locale
                                  .tr(context)
                                  .warnNoRoomInBroadcastDialog,
                            );
                          },
                        );
                      } else {
                        // we need to do the initial popup
                        showDialog(
                            context: context,
                            builder: (BuildContext ctx) {
                              return NewIncomingRequestDialog(
                                room: room,
                                contacts: requestContacts ?? this.contacts,
                              );
                            });
                      }
                    } else {
                      if (room.membership == Membership.leave)
                        Navigator.of(context).pushReplacementNamed(
                            "/room/archivedRequests/${room.id}");
                      else if (!req.isAuthor()) {
                        if (!req.youAccepted() &&
                            !req.youRejected() &&
                            !req.youNeedInfo()) {
                          // we need to do the initial popup
                          showDialog(
                              context: context,
                              builder: (BuildContext ctx) {
                                return NewIncomingRequestDialog(
                                  room: room,
                                );
                              });
                        } else {
                          Navigator.of(context).pushReplacementNamed(
                              "/room/incomingRequests/${room.id}");
                        }
                      } else
                        Navigator.of(context).pushReplacementNamed(
                            "/room/outgoingRequests/${room.id}");
                    }
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
