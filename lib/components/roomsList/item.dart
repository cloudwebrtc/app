import 'package:famedly/components/Avatar.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/UnreadBubble.dart';
import 'package:famedly/components/dialogs/ArchiveRoomDialog.dart';

import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

/// Generates the widget for each room item.
class RoomItem extends StatelessWidget {
  final Room item;
  final bool activeChat;

  const RoomItem({Key key, @required this.item, this.activeChat = false})
      : super(key: key);

  String _calculateLastMessage(BuildContext context) {
    var localizations = locale.tr(context);
    if (this.item.membership == Membership.invite)
      return localizations.youHaveBeenInvitedToChat;
    if (this.item.lastEvent == null) return "";

    // Calculate sender name prefix
    String senderName = this.item.lastEvent.sender != null
        ? this.item.lastEvent.sender.calcDisplayname()
        : localizations.someone;
    String senderNameOrYou = senderName;
    if (item.lastEvent.sender.id == Matrix.of(context).client.userID)
      senderNameOrYou = localizations.you;
    else if (item.lastEvent.senderId == item.directChatMatrixID)
      senderNameOrYou = "";
    if (senderNameOrYou.isNotEmpty) senderNameOrYou += ": ";

    if (this.item.lastEvent.redacted) {
      return localizations.messageHasBeenRemoved;
    }

    switch (this.item.lastEvent.type) {
      case EventTypes.Image:
        return localizations.sentAnImage(senderName);
      case EventTypes.Video:
        return localizations.sentAVideo(senderName);
      case EventTypes.Audio:
        return localizations.sentAnAudio(senderName);
      case EventTypes.File:
        return localizations.sentAFile(senderName);
      case EventTypes.Location:
        return localizations.sentALocation(senderName);
      case EventTypes.Text:
        return "$senderNameOrYou${this.item.lastMessage}";
      case EventTypes.Notice:
        return "$senderNameOrYou${this.item.lastMessage}";
      case EventTypes.Emote:
        return "$senderNameOrYou${this.item.lastMessage}";
      case EventTypes.Reply:
        return "$senderNameOrYou${this.item.lastMessage}";
      default:
        return localizations.noMessagesFoundYet;
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> secondarySwipeActions = item.membership != Membership.leave
        ? [
            IconSlideAction(
              color: Color(0xffff5252),
              icon: FamedlyIcons.trash,
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext innerContext) {
                    // return object of type Dialog
                    return ArchiveRoomDialog(
                      room: item,
                      postArchiveAction: () {},
                    );
                  },
                );
              },
            ),
          ]
        : null;

    List<Widget> badges = [Container(width: 0.1, height: UnreadBubble.size)];
    if (item.pushRuleState != PushRuleState.notify)
      badges.add(Icon(FamedlyIcons.notifyOff,
          size: 16, color: FamedlyColors.bluey_grey));
    if (item.notificationCount > 0)
      badges.add(UnreadBubble(
        [item],
        key: Key("notification_count"),
      ));
    return Slidable(
      actionPane: SlidableBehindActionPane(),
      actions: item.membership != Membership.leave
          ? <Widget>[
              IconSlideAction(
                color: FamedlyColors.pale_grey,
                iconWidget: Icon(
                    item.pushRuleState == PushRuleState.notify
                        ? FamedlyIcons.notification
                        : FamedlyIcons.notifyOff,
                    color: FamedlyColors.slate),
                onTap: () async {
                  PushRuleState newState = PushRuleState.notify;
                  if (item.pushRuleState == PushRuleState.notify) {
                    newState = PushRuleState.mentions_only;
                  }
                  CustomDialogs dialogs = CustomDialogs(context);
                  dialogs.showLoadingDialog();
                  final Future pushRuleUpdate = item
                      .client.connection.onUserEvent.stream
                      .where((u) => u.eventType == "m.push_rules")
                      .first;
                  await dialogs.tryRequestWithErrorSnackbar(
                      item.setPushRuleState(newState));
                  await pushRuleUpdate;
                  dialogs.hideLoadingDialog();
                },
              ),
            ]
          : null,
      secondaryActions: secondarySwipeActions,
      child: Material(
        color: activeChat ? FamedlyColors.light_grey : Colors.white,
        child: Column(
          children: <Widget>[
            InkWell(
              onTap: () async {
                if (activeChat) return;
                if (await Matrix.of(context)
                    .roomsPlugin
                    .joinRoomAndWait(context, item))
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      "/room/${item.id}", (route) => route.isFirst);
              },
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 16.0, bottom: 16, left: 10, right: 10),
                    child: Avatar(this.item.avatar,
                        size: 40,
                        name: locale.tr(context).trRoomName(item.displayname)),
                  ),
                  Expanded(
                    child: new Container(
                      padding: new EdgeInsets.only(left: 8.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            locale.tr(context).trRoomName(item.displayname),
                            // Fixme replace with actual data
                            key: Key("room_name_${this.item.id}"),
                            style: TextStyle(
                              color: Theme.of(context).brightness ==
                                      Brightness.light
                                  ? Color(0xff4d576d)
                                  : Color(0xff95acb9),
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            _calculateLastMessage(context).trimRight(),
                            key: Key("last_message"),
                            maxLines: 1,
                            style: TextStyle(
                              color: item.membership == Membership.invite
                                  ? FamedlyColors.aqua_marine
                                  : Color(0xff95acb9),
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              fontStyle: FontStyle.normal,
                              letterSpacing: 0,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 16.0),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: badges,
                        ),
                        Text(
                          this.item.timeCreated.toString(),
                          key: Key("time_created"),
                          style: TextStyle(
                            color:
                                Theme.of(context).brightness == Brightness.light
                                    ? Color(0xff95acb9)
                                    : Color(0xffa9c0ce),
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Container(
                  width: 70,
                  height: 1,
                  color: Colors.transparent,
                ),
                Expanded(
                  child: Container(
                    height: 1,
                    color: Theme.of(context).brightness == Brightness.light
                        ? FamedlyColors.pale_grey
                        : Colors.black,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
