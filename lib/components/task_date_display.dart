import 'package:famedly/models/tasks/task.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TaskDateDisplay extends StatelessWidget {
  final Task task;

  const TaskDateDisplay({Key key, this.task}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        border: Border.all(color: FamedlyColors.cloudy_blue),
      ),
      child: Padding(
        padding: EdgeInsets.all(9),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                DateFormat("dd.MM.yyyy hh:mm").format(task.date),
                style: TextStyle(
                  color: FamedlyColors.aqua_marine,
                  fontSize: 15,
                ),
              ),
            ),
            InkWell(
              onTap: () => CustomDialogs(context)
                  .tryRequestWithLoadingDialogs(task.setDate(null)),
              child: Icon(Icons.clear),
            ),
          ],
        ),
      ),
    );
  }
}
