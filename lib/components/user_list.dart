import 'package:famedly/components/Matrix.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'UsersList/item.dart';

typedef UserCallback = void Function(User user);

class UserList extends StatelessWidget {
  final Widget header;
  final UserCallback onTap;
  final String roomId;

  const UserList({Key key, this.header, this.onTap, this.roomId = null})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    MatrixState matrix = Matrix.of(context);

    return FutureBuilder<List<User>>(
      future: matrix.contactDiscoveryPlugin.loadContacts(roomId: this.roomId),
      builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
        if (!snapshot.hasData) {
          return Container(
            color: Theme.of(context).brightness == Brightness.light
                ? Colors.white
                : Colors.black54,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }

        List<User> contactList = snapshot.data;
        contactList.sort(
          (a, b) => a
              .calcDisplayname()
              .toUpperCase()
              .compareTo(b.calcDisplayname().toUpperCase()),
        );

        return ListView.builder(
          key: Key("UsersListView"),
          // Let the ListView know how many items it needs to build
          itemCount: contactList.length * 2 + (header != null ? 1 : 0),
          // Provide a builder function. This is where the magic happens! We'll
          // convert each item into a Widget based on the type of item it is.
          itemBuilder: (context, index) {
            if (header != null) {
              if (index == 0) return header;
              index--;
            }
            if (index > contactList.length - 1) return Container();
            final item = contactList[index];
            User prevItem = item;
            if (index > 0) {
              prevItem = contactList[index - 1];
            }

            if (index == 0 ||
                prevItem.calcDisplayname().substring(0, 1).toUpperCase() !=
                    item.calcDisplayname().substring(0, 1).toUpperCase()) {
              contactList.insert(index, item);

              return _buildHeadingItem(item.calcDisplayname().toUpperCase());
            } else {
              return UserItem(
                contact: item,
                onTap: () => onTap(item),
              );
            }
          },
        );
      },
    );
  }

  ListTile _buildHeadingItem(String letter) {
    return ListTile(
      title: Text(
        letter != null && letter.length > 0 ? letter.substring(0, 1) : "?",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );
  }
}
