import 'dart:async';

import 'package:famedly/l10n/messages_all.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class FamedlyLocalizations {
  static Future<FamedlyLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return new FamedlyLocalizations();
    });
  }

  static FamedlyLocalizations of(BuildContext context) {
    return Localizations.of<FamedlyLocalizations>(
        context, FamedlyLocalizations);
  }

  // Dummy function to prevent loops
  FamedlyLocalizations tr(BuildContext context) => this;

//             <===> A <===>

  String get abort => Intl.message("abort");

  String get acceptBroadcastNotice => Intl.message(
      "Are you sure you want to accept this inquiry and reject+archive all others in this broadcast?",
      name: "acceptBroadcastNotice");

  String get acceptInquiry => Intl.message("Accept Inquiry");

  String get addedToTasks => Intl.message('Has been added to your tasks.');

  String get addDate => Intl.message('Add date');

  String get addDetails => Intl.message("Add details");

  String get addDescription => Intl.message('Add description');

  String get address => Intl.message("Address");

  String get addRoomDescription => Intl.message('Add description');

  String get addRoomName => Intl.message('Add room name');

  String get addSubtask => Intl.message('Add subtask');

  String get addToTasks => Intl.message('Add to tasks');

  String get admin => Intl.message("Admin");

  String get advancedSearch => Intl.message('Advanced search');

  String get all => Intl.message('All', name: 'all', desc: 'chatroom tabs');

  String get areYouSure => Intl.message('Are you sure you want to sign out?');

  String get archive => Intl.message('Archive');

  String get archiveAction => Intl.message('Archive', name: 'archiveAction');

  String get archivedInquiryNotice =>
      Intl.message("Archived Inquiry", name: "archivedInquiryNotice");

  String get archiveCleared => Intl.message('Archive cleared');

  String askKickUser(String user) => Intl.message('Kick $user from this group?',
      name: "askKickUser", args: [user]);

  String askMakeAdmin(String user) =>
      Intl.message('Make $user an Admin of this group?',
          name: "askMakeAdmin", args: [user]);

  String get audio => Intl.message('Audio');

//               <===> B <===>

  String get back => Intl.message('Back');

  String get banned => Intl.message('Banned');

  String get broadcast => Intl.message("Broadcast");
  String get browseFamedly => Intl.message('"Browse Famedly"');

//              <===> C <===>

  String get call_warning_text => Intl.message(
        'Currently, calling via famedly is not yet supported. We will let you know when you can dispose of the old-fashioned telephone lists. :)',
        name: 'call_warning_text',
      );

  String get call_warning_title => Intl.message('Calling');

  String get camera => Intl.message("Camera");

  String get cancel => Intl.message('Cancel');

  String get careServices => Intl.message('Care services');

  String get careTakeover => Intl.message('Care takeover');

  String get category => Intl.message("category");

  String get changepassword => Intl.message("Change password");

  String get changedRoomAvatarTo => Intl.message(' changed the room avatar.');

  String get changedRoomNameTo => Intl.message(' changed the room name to ');

  String get changedRoomTopicTo => Intl.message(' changed the room topic to ');

  String get chatArchive => Intl.message('Chat Archive');

  String get chatDetails => Intl.message('Chat Details');

  String get chats => Intl.message('Chats');

  String get clear => Intl.message('Clear');

  String get clearArchive => Intl.message('Clear archive');

  String get clearCompletedTasks => Intl.message('Clear completed tasks');

  String get clearWarningText =>
      Intl.message('The deletion can not be undone. Delete anyway?',
          name: 'clearWarningText');

  String get confirmarchivingdialog =>
      Intl.message('Archive this conversation?');

  String get connected => Intl.message('Connected');

  String get contact => Intl.message('Contact');

  String get contactIsEmail => Intl.message(
      "This contact is actually an e-mail, thus not part of the famedly network.",
      name: "contactIsEmail");

  String get contacts => Intl.message('Contacts');

  String get contactpoints => Intl.message("Contact points");

  String get copy => Intl.message('copy');

  String get create => Intl.message("Create");

  String get createdTheRoom => Intl.message(' created the room.');

//              <===> D <===>

  String get deleteMessage => Intl.message("delete message");

  String get deleteEventForAll =>
      Intl.message('Delete message for all participants?');

  String get departmentPlaceholder => Intl.message('Department');

  String get doneToArchive => Intl.message('Done to archive');

  String get document => Intl.message('Document');

  String get documentNotFound => Intl.message('Document not found');

//              <===> E <===>

  String get enterTextText => Intl.message('Please enter some text');

  String get enterPasswordText => Intl.message('Please enter your password');

  String get enterUsernameText => Intl.message('Please enter your username');

  String get entriesnotagree => Intl.message("The entries do not agree");

  String get error => Intl.message("Error");

  String get externalInquiryWarning => Intl.message(
      "This inquiry is an external inquiry: it will leave the famedly network.",
      name: "externalInquiryWarning");

  String get externalRequests => Intl.message('External requests');

//              <===> F <===>

  String get facilities => Intl.message("Facilities");

  String get filesTitle => Intl.message('Files');

//              <===> G <===>

  String get gallery => Intl.message('Gallery');

  String get generalRequests => Intl.message('General requests');

  String get give => Intl.message("give");

  String gotInvited(String username) =>
      Intl.message('$username got invited to the room.',
          name: 'gotInvited', args: [username]);

  String get gotIt => Intl.message('Got it');

  String get gotInvitedToRoom => Intl.message(' got invited to the room.');

//              <===> H <===>

  String hasArchivedConversation(String user) =>
      Intl.message('$user has archived your conversation.',
          name: "hasArchivedConversation", args: [user]);

  String get hasBeenBanned => Intl.message(' has been banned from this room.');

  String hasBeenInvited(String displayName) =>
      Intl.message('$displayName has been invited.',
          name: "hasBeenInvited", args: [displayName]);

  String get hasChangedAvatar => Intl.message(' has changed the avatar.');

  String get hasChangedDisplayname =>
      Intl.message(' has changed the displayname.');

  String get hasJoinedTheRoom => Intl.message(' has joined the room.');

  String get hasKicked => Intl.message(' has kicked ');

  String get hasLeftRoom => Intl.message(' has left the room.');

  String get hasUnbanned => Intl.message(' has unbanned ');

  String get hint => Intl.message('Hint');

  String get hintTitle => Intl.message('Hint:');

//              <===> I <===>

  String get imagesTitle => Intl.message('Images');

  String get information => Intl.message("Information");

  String get incomming => Intl.message('Incomming');

  String get incominginquiries => Intl.message("Incoming inquiries");

  String get inquiry => Intl.message("inquiry");

  String get inquiries => Intl.message("Inquiries");

  String get interactAsWho =>
      Intl.message("As whom do you want to interact?", name: "interactAsWho");

  String get introPage1_1 => Intl.message(
      'We look forward to welcoming you at famedly, the future in medicine.',
      name: 'introPage1_1',
      desc: 'Intro Page 1 Text 1');

  String get introPage1_2 => Intl.message(
      'On the following pages we would like to introduce you to what awaits you.',
      name: 'introPage1_2',
      desc: 'Intro Page 1 Text 2');

  String get introPage2_1 => Intl.message(
      'With famedly they can reach every provider easily and safely.',
      name: 'introPage2_1',
      desc: 'Intro Page 2 Text 1');

  String get introPage2_2 =>
      Intl.message('Focus on working with patients and leave the rest to us.',
          name: 'introPage2_2', desc: 'Intro Page 2 Text 2');

  String get introPage3_1 => Intl.message(
      'With famedly it will be possible to control any medical device or software.',
      name: 'introPage3_1',
      desc: 'Intro Page 3 Text 1');

  String get introPage3_2 =>
      Intl.message('That means fast and easy working from a surface for them.',
          name: 'introPage3_2', desc: 'Intro Page 3 Text 2');

  String get introTitle1 =>
      Intl.message('Welcome', name: 'introTitle1', desc: 'Intro Page 1 title');

  String get introTitle2 => Intl.message('Talk to each other',
      name: 'introTitle2', desc: 'Intro Page 2 title');

  String get introTitle3 => Intl.message('Create connections',
      name: 'introTitle3', desc: 'Intro Page 3 title');

  String get invalidUsernameOrPassword =>
      Intl.message('Invalid username or password');

  String get invite => Intl.message('Invite');

  String get invited => Intl.message('Invited');

  String get inviteUser => Intl.message('Invite user', name: 'inviteUser');

  String inviteUserToChat(String displayName) =>
      Intl.message('Invite $displayName to this chat?',
          name: "inviteUserToChat", args: [displayName]);

  String get isTyping => Intl.message('is typing...');

//              <===> J <===>

//              <===> K <===>

  String get kick => Intl.message('Kick');

//              <===> L <===>

  String get lastSearchResults => Intl.message('Last search results:');

  String get leftRoom => Intl.message('Left the room');

  String get leaveAction => Intl.message('Leave group');

  String get license => Intl.message("License");

  String get loading => Intl.message('Loading... Please wait');

  String get location => Intl.message('Location');

  String get locationOrCountryCode => Intl.message('Location / Country Code');

  String get login => Intl.message('Login');

  String get loginWithOrganisation => Intl.message('Login with organisation');

  String get logout => Intl.message('Sign out');

//              <===> M <===>

  String get makeAnAdmin => Intl.message('Make An Admin');

  String get mediaTitle => Intl.message('Media');

  String get message => Intl.message("Message");

  String get messages =>
      Intl.message('Messages', name: 'messages', desc: 'chatroom tabs');

  String get messageHasBeenRemoved => Intl.message("Message has been removed!");

  String get membersTitle => Intl.message('Members');

  String get moderator => Intl.message("Moderator");

  String get mute => Intl.message('Mute');

  String get myTasks => Intl.message('My tasks');

//              <===> N <===>

  String get name => Intl.message('Name');

  String get needInfoInquiry => Intl.message("Request Information");

  String get newBroadcast => Intl.message("New Broadcast");

  String get new_group => Intl.message('New group');

  String get new_message => Intl.message('New message');

  String get newTask => Intl.message("New task");

  String get next => Intl.message('Next');

  String get no => Intl.message('No');

  String get noChats => Intl.message('No chats');

  String get noContactPointsVisibleToYou => Intl.message(
      "There currently seems to be no contact points visible to you ... :-(");

  String get NoDescriptionFound => Intl.message("No description found");

  String get noFacilitiesWereFoundForThisContactPoint =>
      Intl.message("No facilities were found for this contact point ... :-(");

  String get noInquiries => Intl.message('No inquiries');

  String get noPermissionForThisAction =>
      Intl.message('You do not have permission for this action.');

  String get noMessagesFoundYet => Intl.message('No messages found yet');

  String get noTasks => Intl.message('No tasks');

  String get notConnected => Intl.message('Not connected');

  String get notice => Intl.message('Notice:');

  String get notVerified => Intl.message('Not yet verified');

  String get notYetImplemented =>
      Intl.message('This feature has not been implemented yet.');

//       <===> O <===>

  String get oopsSomethingWentWrong =>
      Intl.message('Oops! Something went wrong...');

  String get openInBrowser => Intl.message("Open in Browser");

  String get organisation => Intl.message('Organisation');

  String get organisationServerIsNotResponding =>
      Intl.message('Organisation server is not responding');

  String get organisationServerIsNotSupported => Intl.message(
      'Organisation server is not supported and may be out of date');

  String get outgoing => Intl.message('Outgoing');

  String get outgoinginquiries => Intl.message("Outgoing inquiries");

//              <===> P <===>
  String get password => Intl.message('Password');

  String get pickEmail =>
      Intl.message("Pick an email which you want to contact.",
          name: "pickEmail");

  String get please => Intl.message("Please");

  String get pleaseFillOutTextField =>
      Intl.message('Please fill out text field');

  String get privacyPolicy => Intl.message("Privacy Policy");

//              <===> Q <===>

//              <===> R <===>

  String receivedAt(String time) =>
      Intl.message('Received $time', name: "receivedAt", args: [time]);

  String get rejectAndArchiveInquiryWarning =>
      Intl.message("Are you sure you want to reject and archive the inquiry?",
          name: "rejectAndArchiveInquiryWarning");

  String get rejectedInvitation =>
      Intl.message(' has rejected the invitation.');

  String get rejectInquiry => Intl.message("Reject Inquiry");

  String get reply => Intl.message('Reply');

  String get requests => Intl.message("Requests");

  String get revokedInvitation =>
      Intl.message(' has revoked the invitation of ');

  String get roomDescription => Intl.message('Room description');

  String get roomName => Intl.message('Room name');

  String get rooms =>
      Intl.message('Rooms', name: 'rooms', desc: 'chatroom tabs');

//              <===> S <===>

  String get screenlock => Intl.message("Screen lock");

  String get search => Intl.message('Search');

  String get searchResults => Intl.message("Search results");

  String get searchQuery => Intl.message("Search query");

  String get secret => Intl.message("Secret");

  String get security => Intl.message('Security');

  String get sendCrashReportsToFamedly =>
      Intl.message('Send crash reports to Famedly');

  String sentAnAudio(String senderName) =>
      Intl.message('$senderName sent an audio message.',
          name: 'sentAnAudio', args: [senderName]);

  String sentAnImage(String senderName) =>
      Intl.message('$senderName sent an image.',
          name: 'sentAnImage', args: [senderName]);

  String sentAFile(String senderName) =>
      Intl.message('$senderName sent a file.',
          name: 'sentAFile', args: [senderName]);

  String sentAVideo(String senderName) =>
      Intl.message('$senderName sent a video.',
          name: 'sentAVideo', args: [senderName]);

  String sentALocation(String senderName) =>
      Intl.message('$senderName sent a location.',
          name: 'sentALocation', args: [senderName]);

  String sentFrom(String user) =>
      Intl.message('From $user', name: "sentFrom", args: [user]);

  String get sendPrivateMessage => Intl.message('Send Private Message');

  String get sentUnknownEvent =>
      Intl.message(' sent an unknown event of type ');

  String get serverAddress => Intl.message('Server address');

  String get set => Intl.message('Set');

  String get setChatDescription => Intl.message('Set Chat Description');

  String get setChatName => Intl.message('Set Chat Name');

  String get settings => Intl.message('Settings');

  String get settingsTitle => Intl.message('Settings');

  String showMore(int count) =>
      Intl.message("Show more ($count)", name: "showMore", args: [count]);

  String get skip => Intl.message('Skip');

  String get someone => Intl.message('someone');

  String get sort => Intl.message('Sort');

  String get startNewConversation => Intl.message('Start new conversation');

  String get storage => Intl.message('Storage');

  String get successfullVerified => Intl.message('Successfull verified');

//              <===> T <===>

  String get taskArchive => Intl.message('Task Archive');

  String get tasks => Intl.message('Tasks');

  String get title =>
      Intl.message('Famedly', name: 'title', desc: 'The application title');

  String get tooManyRequests =>
      Intl.message('Too many requests. Please wait some minutes.');

  String get torepeat => Intl.message("Wiederholen");

  String trRoomName(String trRoomName) =>
      trRoomName == "Empty chat" || trRoomName.isEmpty
          ? Intl.message('$trRoomName', name: 'trRoomName', args: [trRoomName])
          : trRoomName;

//              <===> U <===>

  String get unableToParseMembershipEvent =>
      Intl.message('*** Unable to parse membership event ***');

  String get unsupportedRoomVersion => Intl.message(
      'At least one of the invitees is on an incompatible home server and therefore could not be invited. The conversation may have been created anyway.');

  String get username => Intl.message('Username');

  String get usernameIncorrect => Intl.message('The username is not correct');

//              <===> V <===>

  String get video => Intl.message('Video');

  String get videosTitle => Intl.message('Videos');

//              <===> W <===>

  String get warnNoRoomInBroadcastDialog => Intl.message(
      "You aren't able to communicate with the contact until they accept the inquiry or ask for more information!",
      name: "warnNoRoomInBroadcastDialog");

  String get welcometext => Intl.message(
      "Please choose your organization or report your organization directly online and join the famous network. ");

  String get welcometofamedly => Intl.message("Welcome to famedly");

  String get willBeSent => Intl.message("will be sent ...");

//              <===> X <===>

//              <===> Y <===>

  String get yes => Intl.message('Yes');

  String get you => Intl.message('You');

  String get youAreConnectedSecurly =>
      Intl.message('You are connected securly.');

  String get youHaveBeenInvitedToChat =>
      Intl.message('You have been invited to chat');

  String get yourconcern => Intl.message("Your concern");

  String get yourmessage => Intl.message("Your message");

  String get yourtitle => Intl.message("Your title");

//              <===> Z <===>

//              <===> Documents <===>
  String get licenseText => Intl.message("""
                  GNU AFFERO GENERAL PUBLIC LICENSE
                       Version 3, 19 November 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

                            Preamble

  The GNU Affero General Public License is a free, copyleft license for
software and other kinds of works, specifically designed to ensure
cooperation with the community in the case of network server software.

  The licenses for most software and other practical works are designed
to take away your freedom to share and change the works.  By contrast,
our General Public Licenses are intended to guarantee your freedom to
share and change all versions of a program--to make sure it remains free
software for all its users.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
them if you wish), that you receive source code or can get it if you
want it, that you can change the software or use pieces of it in new
free programs, and that you know you can do these things.

  Developers that use our General Public Licenses protect your rights
with two steps: (1) assert copyright on the software, and (2) offer
you this License which gives you legal permission to copy, distribute
and/or modify the software.

  A secondary benefit of defending all users' freedom is that
improvements made in alternate versions of the program, if they
receive widespread use, become available for other developers to
incorporate.  Many developers of free software are heartened and
encouraged by the resulting cooperation.  However, in the case of
software used on network servers, this result may fail to come about.
The GNU General Public License permits making a modified version and
letting the public access it on a server without ever releasing its
source code to the public.

  The GNU Affero General Public License is designed specifically to
ensure that, in such cases, the modified source code becomes available
to the community.  It requires the operator of a network server to
provide the source code of the modified version running there to the
users of that server.  Therefore, public use of a modified version, on
a publicly accessible server, gives the public access to the source
code of the modified version.

  An older license, called the Affero General Public License and
published by Affero, was designed to accomplish similar goals.  This is
a different license, not a version of the Affero GPL, but Affero has
released a new version of the Affero GPL which permits relicensing under
this license.

  The precise terms and conditions for copying, distribution and
modification follow.

                       TERMS AND CONDITIONS

  0. Definitions.

  "This License" refers to version 3 of the GNU Affero General Public License.

  "Copyright" also means copyright-like laws that apply to other kinds of
works, such as semiconductor masks.

  "The Program" refers to any copyrightable work licensed under this
License.  Each licensee is addressed as "you".  "Licensees" and
"recipients" may be individuals or organizations.

  To "modify" a work means to copy from or adapt all or part of the work
in a fashion requiring copyright permission, other than the making of an
exact copy.  The resulting work is called a "modified version" of the
earlier work or a work "based on" the earlier work.

  A "covered work" means either the unmodified Program or a work based
on the Program.

  To "propagate" a work means to do anything with it that, without
permission, would make you directly or secondarily liable for
infringement under applicable copyright law, except executing it on a
computer or modifying a private copy.  Propagation includes copying,
distribution (with or without modification), making available to the
public, and in some countries other activities as well.

  To "convey" a work means any kind of propagation that enables other
parties to make or receive copies.  Mere interaction with a user through
a computer network, with no transfer of a copy, is not conveying.

  An interactive user interface displays "Appropriate Legal Notices"
to the extent that it includes a convenient and prominently visible
feature that (1) displays an appropriate copyright notice, and (2)
tells the user that there is no warranty for the work (except to the
extent that warranties are provided), that licensees may convey the
work under this License, and how to view a copy of this License.  If
the interface presents a list of user commands or options, such as a
menu, a prominent item in the list meets this criterion.

  1. Source Code.

  The "source code" for a work means the preferred form of the work
for making modifications to it.  "Object code" means any non-source
form of a work.

  A "Standard Interface" means an interface that either is an official
standard defined by a recognized standards body, or, in the case of
interfaces specified for a particular programming language, one that
is widely used among developers working in that language.

  The "System Libraries" of an executable work include anything, other
than the work as a whole, that (a) is included in the normal form of
packaging a Major Component, but which is not part of that Major
Component, and (b) serves only to enable use of the work with that
Major Component, or to implement a Standard Interface for which an
implementation is available to the public in source code form.  A
"Major Component", in this context, means a major essential component
(kernel, window system, and so on) of the specific operating system
(if any) on which the executable work runs, or a compiler used to
produce the work, or an object code interpreter used to run it.

  The "Corresponding Source" for a work in object code form means all
the source code needed to generate, install, and (for an executable
work) run the object code and to modify the work, including scripts to
control those activities.  However, it does not include the work's
System Libraries, or general-purpose tools or generally available free
programs which are used unmodified in performing those activities but
which are not part of the work.  For example, Corresponding Source
includes interface definition files associated with source files for
the work, and the source code for shared libraries and dynamically
linked subprograms that the work is specifically designed to require,
such as by intimate data communication or control flow between those
subprograms and other parts of the work.

  The Corresponding Source need not include anything that users
can regenerate automatically from other parts of the Corresponding
Source.

  The Corresponding Source for a work in source code form is that
same work.

  2. Basic Permissions.

  All rights granted under this License are granted for the term of
copyright on the Program, and are irrevocable provided the stated
conditions are met.  This License explicitly affirms your unlimited
permission to run the unmodified Program.  The output from running a
covered work is covered by this License only if the output, given its
content, constitutes a covered work.  This License acknowledges your
rights of fair use or other equivalent, as provided by copyright law.

  You may make, run and propagate covered works that you do not
convey, without conditions so long as your license otherwise remains
in force.  You may convey covered works to others for the sole purpose
of having them make modifications exclusively for you, or provide you
with facilities for running those works, provided that you comply with
the terms of this License in conveying all material for which you do
not control copyright.  Those thus making or running the covered works
for you must do so exclusively on your behalf, under your direction
and control, on terms that prohibit them from making any copies of
your copyrighted material outside their relationship with you.

  Conveying under any other circumstances is permitted solely under
the conditions stated below.  Sublicensing is not allowed; section 10
makes it unnecessary.

  3. Protecting Users' Legal Rights From Anti-Circumvention Law.

  No covered work shall be deemed part of an effective technological
measure under any applicable law fulfilling obligations under article
11 of the WIPO copyright treaty adopted on 20 December 1996, or
similar laws prohibiting or restricting circumvention of such
measures.

  When you convey a covered work, you waive any legal power to forbid
circumvention of technological measures to the extent such circumvention
is effected by exercising rights under this License with respect to
the covered work, and you disclaim any intention to limit operation or
modification of the work as a means of enforcing, against the work's
users, your or third parties' legal rights to forbid circumvention of
technological measures.

  4. Conveying Verbatim Copies.

  You may convey verbatim copies of the Program's source code as you
receive it, in any medium, provided that you conspicuously and
appropriately publish on each copy an appropriate copyright notice;
keep intact all notices stating that this License and any
non-permissive terms added in accord with section 7 apply to the code;
keep intact all notices of the absence of any warranty; and give all
recipients a copy of this License along with the Program.

  You may charge any price or no price for each copy that you convey,
and you may offer support or warranty protection for a fee.

  5. Conveying Modified Source Versions.

  You may convey a work based on the Program, or the modifications to
produce it from the Program, in the form of source code under the
terms of section 4, provided that you also meet all of these conditions:

    a) The work must carry prominent notices stating that you modified
    it, and giving a relevant date.

    b) The work must carry prominent notices stating that it is
    released under this License and any conditions added under section
    7.  This requirement modifies the requirement in section 4 to
    "keep intact all notices".

    c) You must license the entire work, as a whole, under this
    License to anyone who comes into possession of a copy.  This
    License will therefore apply, along with any applicable section 7
    additional terms, to the whole of the work, and all its parts,
    regardless of how they are packaged.  This License gives no
    permission to license the work in any other way, but it does not
    invalidate such permission if you have separately received it.

    d) If the work has interactive user interfaces, each must display
    Appropriate Legal Notices; however, if the Program has interactive
    interfaces that do not display Appropriate Legal Notices, your
    work need not make them do so.

  A compilation of a covered work with other separate and independent
works, which are not by their nature extensions of the covered work,
and which are not combined with it such as to form a larger program,
in or on a volume of a storage or distribution medium, is called an
"aggregate" if the compilation and its resulting copyright are not
used to limit the access or legal rights of the compilation's users
beyond what the individual works permit.  Inclusion of a covered work
in an aggregate does not cause this License to apply to the other
parts of the aggregate.

  6. Conveying Non-Source Forms.

  You may convey a covered work in object code form under the terms
of sections 4 and 5, provided that you also convey the
machine-readable Corresponding Source under the terms of this License,
in one of these ways:

    a) Convey the object code in, or embodied in, a physical product
    (including a physical distribution medium), accompanied by the
    Corresponding Source fixed on a durable physical medium
    customarily used for software interchange.

    b) Convey the object code in, or embodied in, a physical product
    (including a physical distribution medium), accompanied by a
    written offer, valid for at least three years and valid for as
    long as you offer spare parts or customer support for that product
    model, to give anyone who possesses the object code either (1) a
    copy of the Corresponding Source for all the software in the
    product that is covered by this License, on a durable physical
    medium customarily used for software interchange, for a price no
    more than your reasonable cost of physically performing this
    conveying of source, or (2) access to copy the
    Corresponding Source from a network server at no charge.

    c) Convey individual copies of the object code with a copy of the
    written offer to provide the Corresponding Source.  This
    alternative is allowed only occasionally and noncommercially, and
    only if you received the object code with such an offer, in accord
    with subsection 6b.

    d) Convey the object code by offering access from a designated
    place (gratis or for a charge), and offer equivalent access to the
    Corresponding Source in the same way through the same place at no
    further charge.  You need not require recipients to copy the
    Corresponding Source along with the object code.  If the place to
    copy the object code is a network server, the Corresponding Source
    may be on a different server (operated by you or a third party)
    that supports equivalent copying facilities, provided you maintain
    clear directions next to the object code saying where to find the
    Corresponding Source.  Regardless of what server hosts the
    Corresponding Source, you remain obligated to ensure that it is
    available for as long as needed to satisfy these requirements.

    e) Convey the object code using peer-to-peer transmission, provided
    you inform other peers where the object code and Corresponding
    Source of the work are being offered to the general public at no
    charge under subsection 6d.

  A separable portion of the object code, whose source code is excluded
from the Corresponding Source as a System Library, need not be
included in conveying the object code work.

  A "User Product" is either (1) a "consumer product", which means any
tangible personal property which is normally used for personal, family,
or household purposes, or (2) anything designed or sold for incorporation
into a dwelling.  In determining whether a product is a consumer product,
doubtful cases shall be resolved in favor of coverage.  For a particular
product received by a particular user, "normally used" refers to a
typical or common use of that class of product, regardless of the status
of the particular user or of the way in which the particular user
actually uses, or expects or is expected to use, the product.  A product
is a consumer product regardless of whether the product has substantial
commercial, industrial or non-consumer uses, unless such uses represent
the only significant mode of use of the product.

  "Installation Information" for a User Product means any methods,
procedures, authorization keys, or other information required to install
and execute modified versions of a covered work in that User Product from
a modified version of its Corresponding Source.  The information must
suffice to ensure that the continued functioning of the modified object
code is in no case prevented or interfered with solely because
modification has been made.

  If you convey an object code work under this section in, or with, or
specifically for use in, a User Product, and the conveying occurs as
part of a transaction in which the right of possession and use of the
User Product is transferred to the recipient in perpetuity or for a
fixed term (regardless of how the transaction is characterized), the
Corresponding Source conveyed under this section must be accompanied
by the Installation Information.  But this requirement does not apply
if neither you nor any third party retains the ability to install
modified object code on the User Product (for example, the work has
been installed in ROM).

  The requirement to provide Installation Information does not include a
requirement to continue to provide support service, warranty, or updates
for a work that has been modified or installed by the recipient, or for
the User Product in which it has been modified or installed.  Access to a
network may be denied when the modification itself materially and
adversely affects the operation of the network or violates the rules and
protocols for communication across the network.

  Corresponding Source conveyed, and Installation Information provided,
in accord with this section must be in a format that is publicly
documented (and with an implementation available to the public in
source code form), and must require no special password or key for
unpacking, reading or copying.

  7. Additional Terms.

  "Additional permissions" are terms that supplement the terms of this
License by making exceptions from one or more of its conditions.
Additional permissions that are applicable to the entire Program shall
be treated as though they were included in this License, to the extent
that they are valid under applicable law.  If additional permissions
apply only to part of the Program, that part may be used separately
under those permissions, but the entire Program remains governed by
this License without regard to the additional permissions.

  When you convey a copy of a covered work, you may at your option
remove any additional permissions from that copy, or from any part of
it.  (Additional permissions may be written to require their own
removal in certain cases when you modify the work.)  You may place
additional permissions on material, added by you to a covered work,
for which you have or can give appropriate copyright permission.

  Notwithstanding any other provision of this License, for material you
add to a covered work, you may (if authorized by the copyright holders of
that material) supplement the terms of this License with terms:

    a) Disclaiming warranty or limiting liability differently from the
    terms of sections 15 and 16 of this License; or

    b) Requiring preservation of specified reasonable legal notices or
    author attributions in that material or in the Appropriate Legal
    Notices displayed by works containing it; or

    c) Prohibiting misrepresentation of the origin of that material, or
    requiring that modified versions of such material be marked in
    reasonable ways as different from the original version; or

    d) Limiting the use for publicity purposes of names of licensors or
    authors of the material; or

    e) Declining to grant rights under trademark law for use of some
    trade names, trademarks, or service marks; or

    f) Requiring indemnification of licensors and authors of that
    material by anyone who conveys the material (or modified versions of
    it) with contractual assumptions of liability to the recipient, for
    any liability that these contractual assumptions directly impose on
    those licensors and authors.

  All other non-permissive additional terms are considered "further
restrictions" within the meaning of section 10.  If the Program as you
received it, or any part of it, contains a notice stating that it is
governed by this License along with a term that is a further
restriction, you may remove that term.  If a license document contains
a further restriction but permits relicensing or conveying under this
License, you may add to a covered work material governed by the terms
of that license document, provided that the further restriction does
not survive such relicensing or conveying.

  If you add terms to a covered work in accord with this section, you
must place, in the relevant source files, a statement of the
additional terms that apply to those files, or a notice indicating
where to find the applicable terms.

  Additional terms, permissive or non-permissive, may be stated in the
form of a separately written license, or stated as exceptions;
the above requirements apply either way.

  8. Termination.

  You may not propagate or modify a covered work except as expressly
provided under this License.  Any attempt otherwise to propagate or
modify it is void, and will automatically terminate your rights under
this License (including any patent licenses granted under the third
paragraph of section 11).

  However, if you cease all violation of this License, then your
license from a particular copyright holder is reinstated (a)
provisionally, unless and until the copyright holder explicitly and
finally terminates your license, and (b) permanently, if the copyright
holder fails to notify you of the violation by some reasonable means
prior to 60 days after the cessation.

  Moreover, your license from a particular copyright holder is
reinstated permanently if the copyright holder notifies you of the
violation by some reasonable means, this is the first time you have
received notice of violation of this License (for any work) from that
copyright holder, and you cure the violation prior to 30 days after
your receipt of the notice.

  Termination of your rights under this section does not terminate the
licenses of parties who have received copies or rights from you under
this License.  If your rights have been terminated and not permanently
reinstated, you do not qualify to receive new licenses for the same
material under section 10.

  9. Acceptance Not Required for Having Copies.

  You are not required to accept this License in order to receive or
run a copy of the Program.  Ancillary propagation of a covered work
occurring solely as a consequence of using peer-to-peer transmission
to receive a copy likewise does not require acceptance.  However,
nothing other than this License grants you permission to propagate or
modify any covered work.  These actions infringe copyright if you do
not accept this License.  Therefore, by modifying or propagating a
covered work, you indicate your acceptance of this License to do so.

  10. Automatic Licensing of Downstream Recipients.

  Each time you convey a covered work, the recipient automatically
receives a license from the original licensors, to run, modify and
propagate that work, subject to this License.  You are not responsible
for enforcing compliance by third parties with this License.

  An "entity transaction" is a transaction transferring control of an
organization, or substantially all assets of one, or subdividing an
organization, or merging organizations.  If propagation of a covered
work results from an entity transaction, each party to that
transaction who receives a copy of the work also receives whatever
licenses to the work the party's predecessor in interest had or could
give under the previous paragraph, plus a right to possession of the
Corresponding Source of the work from the predecessor in interest, if
the predecessor has it or can get it with reasonable efforts.

  You may not impose any further restrictions on the exercise of the
rights granted or affirmed under this License.  For example, you may
not impose a license fee, royalty, or other charge for exercise of
rights granted under this License, and you may not initiate litigation
(including a cross-claim or counterclaim in a lawsuit) alleging that
any patent claim is infringed by making, using, selling, offering for
sale, or importing the Program or any portion of it.

  11. Patents.

  A "contributor" is a copyright holder who authorizes use under this
License of the Program or a work on which the Program is based.  The
work thus licensed is called the contributor's "contributor version".

  A contributor's "essential patent claims" are all patent claims
owned or controlled by the contributor, whether already acquired or
hereafter acquired, that would be infringed by some manner, permitted
by this License, of making, using, or selling its contributor version,
but do not include claims that would be infringed only as a
consequence of further modification of the contributor version.  For
purposes of this definition, "control" includes the right to grant
patent sublicenses in a manner consistent with the requirements of
this License.

  Each contributor grants you a non-exclusive, worldwide, royalty-free
patent license under the contributor's essential patent claims, to
make, use, sell, offer for sale, import and otherwise run, modify and
propagate the contents of its contributor version.

  In the following three paragraphs, a "patent license" is any express
agreement or commitment, however denominated, not to enforce a patent
(such as an express permission to practice a patent or covenant not to
sue for patent infringement).  To "grant" such a patent license to a
party means to make such an agreement or commitment not to enforce a
patent against the party.

  If you convey a covered work, knowingly relying on a patent license,
and the Corresponding Source of the work is not available for anyone
to copy, free of charge and under the terms of this License, through a
publicly available network server or other readily accessible means,
then you must either (1) cause the Corresponding Source to be so
available, or (2) arrange to deprive yourself of the benefit of the
patent license for this particular work, or (3) arrange, in a manner
consistent with the requirements of this License, to extend the patent
license to downstream recipients.  "Knowingly relying" means you have
actual knowledge that, but for the patent license, your conveying the
covered work in a country, or your recipient's use of the covered work
in a country, would infringe one or more identifiable patents in that
country that you have reason to believe are valid.

  If, pursuant to or in connection with a single transaction or
arrangement, you convey, or propagate by procuring conveyance of, a
covered work, and grant a patent license to some of the parties
receiving the covered work authorizing them to use, propagate, modify
or convey a specific copy of the covered work, then the patent license
you grant is automatically extended to all recipients of the covered
work and works based on it.

  A patent license is "discriminatory" if it does not include within
the scope of its coverage, prohibits the exercise of, or is
conditioned on the non-exercise of one or more of the rights that are
specifically granted under this License.  You may not convey a covered
work if you are a party to an arrangement with a third party that is
in the business of distributing software, under which you make payment
to the third party based on the extent of your activity of conveying
the work, and under which the third party grants, to any of the
parties who would receive the covered work from you, a discriminatory
patent license (a) in connection with copies of the covered work
conveyed by you (or copies made from those copies), or (b) primarily
for and in connection with specific products or compilations that
contain the covered work, unless you entered into that arrangement,
or that patent license was granted, prior to 28 March 2007.

  Nothing in this License shall be construed as excluding or limiting
any implied license or other defenses to infringement that may
otherwise be available to you under applicable patent law.

  12. No Surrender of Others' Freedom.

  If conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot convey a
covered work so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you may
not convey it at all.  For example, if you agree to terms that obligate you
to collect a royalty for further conveying from those to whom you convey
the Program, the only way you could satisfy both those terms and this
License would be to refrain entirely from conveying the Program.

  13. Remote Network Interaction; Use with the GNU General Public License.

  Notwithstanding any other provision of this License, if you modify the
Program, your modified version must prominently offer all users
interacting with it remotely through a computer network (if your version
supports such interaction) an opportunity to receive the Corresponding
Source of your version by providing access to the Corresponding Source
from a network server at no charge, through some standard or customary
means of facilitating copying of software.  This Corresponding Source
shall include the Corresponding Source for any work covered by version 3
of the GNU General Public License that is incorporated pursuant to the
following paragraph.

  Notwithstanding any other provision of this License, you have
permission to link or combine any covered work with a work licensed
under version 3 of the GNU General Public License into a single
combined work, and to convey the resulting work.  The terms of this
License will continue to apply to the part which is the covered work,
but the work with which it is combined will remain governed by version
3 of the GNU General Public License.

  14. Revised Versions of this License.

  The Free Software Foundation may publish revised and/or new versions of
the GNU Affero General Public License from time to time.  Such new versions
will be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

  Each version is given a distinguishing version number.  If the
Program specifies that a certain numbered version of the GNU Affero General
Public License "or any later version" applies to it, you have the
option of following the terms and conditions either of that numbered
version or of any later version published by the Free Software
Foundation.  If the Program does not specify a version number of the
GNU Affero General Public License, you may choose any version ever published
by the Free Software Foundation.

  If the Program specifies that a proxy can decide which future
versions of the GNU Affero General Public License can be used, that proxy's
public statement of acceptance of a version permanently authorizes you
to choose that version for the Program.

  Later license versions may give you additional or different
permissions.  However, no additional obligations are imposed on any
author or copyright holder as a result of your choosing to follow a
later version.

  15. Disclaimer of Warranty.

  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

  16. Limitation of Liability.

  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS
THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY
GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE
USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF
DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD
PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),
EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES.

  17. Interpretation of Sections 15 and 16.

  If the disclaimer of warranty and limitation of liability provided
above cannot be given local legal effect according to their terms,
reviewing courts shall apply local law that most closely approximates
an absolute waiver of all civil liability in connection with the
Program, unless a warranty or assumption of liability accompanies a
copy of the Program in return for a fee.

                     END OF TERMS AND CONDITIONS

            How to Apply These Terms to Your New Programs

  If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these terms.

  To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
state the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    famedly talk
    Copyright (C) 2019  famedly

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

  If your software can interact with users remotely through a computer
network, you should also make sure that it provides a way for users to
get its source.  For example, if your program is a web application, its
interface could display a "Source" link that leads users to an archive
of the code.  There are many ways you could offer source, and different
solutions will be better for different programs; see section 13 for the
specific requirements.

  You should also get your employer (if you work as a programmer) or school,
if any, to sign a "copyright disclaimer" for the program, if necessary.
For more information on this, and how to apply and follow the GNU AGPL, see
<http://www.gnu.org/licenses/>.

  """);

  String get privacyPolicyText => Intl.message("""
Bedingungen für die Nutzung von Famedly

§ 1 Vertragsgegenstand und Geltungsbereich

(1) Die Famedly GmbH (nachfolgend „Anbieter“) hat eine digitale Kommunikationslösung (nachfolgend „Famedly“) für Unternehmen aus der Gesundheitsbranche (nachfolgend „Kunden“) entwickelt. Mitarbeiter der Kunden sowie zugelassene Einzelpersonen, die vom Anbieter freigeschaltet worden sind (nachfolgend „autorisierte Nutzer“), können Famedly  für die interne und externe Kommunikation sowie zum Austausch von Dokumenten nutzen. Kunden, die mit dem Anbieter Vertrag über die Bereitstellung von Famedly abgeschlossen haben (nachfolgend „Kundenvertrag“) können während der Laufzeit des Kundenvertrags im vertraglich vereinbarten Umfang bestimmte Personen für die Nutzung von Famedly zu autorisieren. 
(2) Das Teilnahmeangebot des Anbieters richtet sich nur an Personen, die das 18. Lebensjahr vollendet haben.
(3) Die vorliegenden Nutzungsbedingungen regeln die vertraglichen Rechtsbeziehungen zwischen dem Anbieter und dem autorisierten Nutzer (nachfolgend auch „Nutzungsvertrag“).
(4) Sofern die Parteien nicht ausdrücklich etwas anderes vereinbaren, stellt der Anbieter dem autorisierten Nutzer Leistungen nach diesem Nutzungsvertrag unentgeltlich zur Verfügung.

§ 2 Verhältnis Kunden und autorisierten Nutzern

Der Kunde steht für Handlungen und Unterlassungen der von ihm autorisierten Nutzer wie für eigene Handlungen und Unterlassungen ein und verpflichtet sie zur vertragsgemäßen Nutzung von Famedly. Es liegt ausschließlich in der Verantwortung des Kunden, 
- die autorisierten Nutzer über alle relevanten Vereinbarungen und Einstellungen zu informieren, die die Verarbeitung der Kundendaten beeinträchtigen könnten,
- alle Rechte, Genehmigungen oder Zustimmungen von den autorisierten Nutzern einzuholen, die für die rechtmäßige Benutzung der Kundendaten und den Betrieb von Famedly erforderlich sind,
- zu gewährleisten, dass die Übertragung und Verarbeitung von Kundendaten im Rahmen des Kundenvertrags rechtmäßig ist und
- auf alle Streitfälle mit autorisierten Nutzern im Zusammenhang mit den Kundendaten, Famedly oder der Nichteinhaltung der Nutzungsbedingungen zu reagieren und sie beizulegen. 

§ 3 Registrierung zu Famedly und Vertragsschluss

(1) Voraussetzung für die Nutzung von Famedly ist die Registrierung sowie die Zustimmung zu diesen Nutzungsbedingungen. Die bei der Registrierung abgefragten Daten sind vollständig und korrekt anzugeben. Der Vertrag zwischen dem Nutzer und dem Anbieter kommt zustande, sobald die Registrierung abgeschlossen ist und der Nutzer den Nutzungsbedingungen zugestimmt hat.
(2) Der autorisierte Nutzer ist verpflichtet, die im Rahmen des Registrierungsprozesses angegebenen Daten aktuell zu halten. Tritt während der Laufzeit der Nutzungsvereinbarung eine Änderung ein, so hat der autorisierte Nutzer die entsprechenden Angaben in den Einstellungen unverzüglich zu korrigieren. Können die Angaben in den Einstellungen nicht geändert werden, ist der autorisierte Nutzer verpflichtet, die geänderten Daten in Textform mitzuteilen. Dies gilt insbesondere für den Fall, dass ein Nutzer – etwa infolge eines Arbeitgeberwechsels – nicht mehr für die Nutzung autorisiert ist.

§ 4 Umfang der erlaubten Nutzung und Verfügbarkeit

(1) Der Anbieter räumt dem autorisierten Nutzer ein auf die Laufzeit des Nutzungsvertrags beschränktes, widerrufliches, nicht ausschließliches und nicht übertragbares Nutzungsrecht an Famedly und den darin bereitgestellten Inhalten und Funktionen ein. 
(2) Der Anspruch auf Nutzung der über Famedly verfügbaren Funktionen besteht nur im Rahmen der technischen und betrieblichen Möglichkeiten des Anbieters. Der Anbieter bemüht sich um eine möglichst unterbrechungsfreie Nutzbarkeit des Portals und seiner Funktionen. Jedoch können durch technische Störungen (wie z.B. Unterbrechung der Stromversorgung, Hardware- und Softwarefehler, technische Probleme in den Datenleitungen) zeitweilige Beschränkungen oder Unterbrechungen auftreten.
(3) Um Famedly nutzen zu können, benötigt der autorisierte Nutzer ein Endgerät, das die gültigen System- und Kompatibilitätsanforderungen erfüllt sowie über einen funktionierenden Internetzugang und kompatible Software verfügt.
(4) Sofern der Anbieter während der Laufzeit neue Versionen, Upgrades oder Updates an Famedly vornimmt, gelten die vorstehenden Rechte auch für diese. Der autorisierte Nutzer ist dafür verantwortlich, in seinem Einflussbereich die technischen Voraussetzungen für die Nutzung der über Famedly verfügbaren Funktionen zu schaffen.

§ 5 Pflichten der autorisierten Nutzer

Der autorisierte Nutzer hat insbesondere folgende Pflichten:
a)	Die bereitgestellten Funktionen dürfen nicht missbräuchlich oder rechtswidrig genutzt werden, insbesondere
-	dürfen keine gesetzlich verbotenen Inhalte hochgeladen, verwendet oder übersandt werden, wie z. B. unerwünschte und unverlangte Werbung.
-	dürfen keine Informationen mit rechts- oder sittenwidrigen Inhalten übermittelt oder eingestellt werden und es darf nicht auf solche Informationen hingewiesen werden. Dazu zählen vor allem Informationen, die im Sinne der §§ 130, 130a und 131 StGB der Volksverhetzung dienen, zu Straftaten anleiten oder Gewalt verherrlichen oder verharmlosen, sexuell anstößig sind, im Sinne des § 184 StGB pornografisch sind, geeignet sind, Kinder oder Jugendliche sittlich schwer zu gefährden oder in ihrem Wohl zu beeinträchtigen oder das Ansehen des Anbieters schädigen können. 
-	ist dafür Sorge zu tragen, dass durch die Inanspruchnahme einzelner Funktionalitäten und insbesondere durch die Einstellung oder das Teilen von Inhalten keinerlei Beeinträchtigungen für den Anbieter oder Dritte entstehen. 
-	sind die nationalen und internationalen Urheber- und Marken-, Patent-, Namens- und Kennzeichenrechte sowie sonstigen gewerblichen Schutzrechte und Persönlichkeitsrechte Dritter zu beachten.
b)	Persönliche Zugangsdaten dürfen nicht an Dritte weitergegeben werden und sind vor dem Zugriff durch Dritte geschützt aufzubewahren. Zugangsdaten sollten zur Sicherheit in regelmäßigen Abständen geändert werden. Soweit Anlass zu der Vermutung besteht, dass unberechtigte Personen von den Zugangsdaten Kenntnis erlangt haben, hat der autorisierte Nutzer diese unverzüglich zu ändern.
c)	Der Anbieter und seine Erfüllungsgehilfen sind von sämtlichen Ansprüchen Dritter freizustellen, die auf einer rechtswidrigen Verwendung von Famedly und der hiermit verbundenen Leistungen durch den autorisierten Nutzer beruhen oder mit seiner Billigung erfolgen oder die aus datenschutzrechtlichen, urheberrechtlichen oder sonstigen Pflichtverstößen des autorisierten Nutzers resultieren. Erkennt der autorisierte Nutzer oder muss er erkennen, dass ein solcher Verstoß droht, besteht die Pflicht zur unverzüglichen Unterrichtung des Anbieters. Eine Pflicht zur Freistellung besteht nicht, sofern der autorisierte Nutzer die Pflichtverletzung nicht zu vertreten hat.
d)	Der autorisierte Nutzer stellt sicher, dass sich auf seinen Geräten keine Schadsoftware (z.B. Computerviren, Trojaner, etc.) befinden, die zu Schäden oder Beeinträchtigungen der Hard- oder Software des Anbieters oder über Famedly vernetzte autorisierte Nutzer führen können. Entsprechendes gilt bezüglich der vom autorisierten Nutzer verwendeten Fremdsoftware einschließlich besonderer Verschlüsselungssoftware. 

§ 6 Haftungsbeschränkung

(1) Der Anbieter haftet aus diesen Nutzungsbedingungen nur bei Verletzung einer vertragswesentlichen Pflicht, deren Verletzung die Erreichung des Vertragszwecks gefährdet oder deren Erfüllung die ordnungsgemäße Durchführung des Vertrags erst ermöglicht und auf deren Einhaltung der autorisierte Nutzer regelmäßig vertrauen darf (sog. „Kardinalpflicht“), und nur für den vertragstypischen, voraussehbaren Schaden. 
(2) Die vorstehende Haftungsbeschränkung gilt nicht bei Schäden, die auf Vorsatz oder grober Fahrlässigkeit beruhen, Personenschäden (Verletzung von Leben, Körper, Gesundheit) sowie Garantieübernahmen.
(3) Der Anbieter haftet nicht, wenn die einen Anspruch gegen den Anbieter begründenden Umstände auf einem ungewöhnlichen und unvorhersehbaren Ereignis beruhen, auf das der Anbieter keinen Einfluss hat und dessen Folgen trotz Anwendung der gebotenen Sorgfalt nicht hätten vermieden werden können oder von dem Anbieter auf Grund einer gesetzlichen Verpflichtung herbeigeführt wurden.
(4) Der Anbieter haftet ferner nicht für Ausfälle oder Störungen in der außerhalb des Verantwortungsbereichs von dem Anbieter liegenden technischen Infrastruktur (höhere Gewalt). 
(5) Gesetzliche Haftungsbeschränkungen im Zusammenhang mit unentgeltlichen Leistungen des Anbieters (z.B. Haftungsbeschränkung auf Vorsatz und grobe Fahrlässigkeit nach § 599 BGB) bleiben unberührt.
(6) Eine Haftung nach dem Produkthaftungsgesetz bleibt unberührt.

§ 7 Laufzeit 

(1) Wurde der autorisierte Nutzer auf Grundlage eines Kundenvertrags freigeschaltet, endet der Nutzungsvertrag spätestens mit Ablauf des jeweiligen Kundenvertrags. 
(2) Im Übrigen kann der Vertrag von jeder Partei jederzeit gekündigt werden. Jede Kündigung muss in Textform erfolgen. Abweichend hiervon kann die Kündigung durch den Anbieter auch durch Sperrung des jeweiligen Nutzerkontos erfolgen.
(3) Nach Beendigung des Nutzungsvertrags ist der Anbieter berechtigt, sämtliche Daten, Einstellungen und Zugangsdaten des Nutzers zu löschen. 

§ 8 Änderungen der Nutzungsbedingungen

Der Anbieter behält sich vor, diese Nutzungsbedingungen jederzeit mit Wirksamkeit auch innerhalb der bestehenden Vertragsverhältnisse zu ändern. Über derartige Änderungen setzt der Anbieter die autorisierte Nutzer mindestens 30 Kalendertage vor dem geplanten Inkrafttreten der Änderungen in Kenntnis. Sofern der autorisierte Nutzer nicht innerhalb von 30 Tagen ab Zugang der Mitteilung widerspricht und die Inanspruchnahme von Famedly auch nach Ablauf der Widerspruchsfrist fortsetzt, so gelten die Änderungen ab Fristablauf als wirksam vereinbart. Im Fall eines Widerspruchs wird der Vertrag zu den bisherigen Bedingungen fortgesetzt. In der Änderungsmitteilung wird der Anbieter den autorisierten Nutzer auf sein Widerspruchsrecht und auf die Folgen hinweisen.

§ 9 Streitbeilegungsverfahren

(1) Der Anbieter nimmt nicht an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teil. Dem Anbieter ist vielmehr daran gelegen, Streitigkeiten mit Nutzern im direkten Kontakt zu klären. Nutzer können sich hierzu an Dr. Phillipp Kurtz, c/o Famedly GmbH, Altensteinstraße 40, 14195 Berlin, E-Mail: p.kurtz@famedly.com wenden. 
(2) Informationen zur Online-Streitbeilegung nach Artikel 14 Abs. 1 der EU-Verordnung über Online-Streitbeilegung in Verbraucherangelegenheiten (ODR-VO): Die EU-Kommission stellt eine Plattform zur Online-Streitbeilegung (OS-Plattform) verbraucherrechtlicher Streitigkeiten, die aus Online-Kaufverträgen und Online-Dienstleistungsverträgen resultieren, bereit. Diese Plattform ist im Internet unter http://ec.europa.eu/consumers/odr/ erreichbar.

§10 Abschlussbestimmungen

(1) Die Nutzungsbedingungen unterliegen dem Recht der Bundesrepublik.
(2) Gerichtsstand für alle aus den Nutzungsbedingungen entstehenden Streitigkeiten zwischen dem Anbieter und den autorisierten Nutzer ist, soweit eine solche Gerichtsstandsvereinbarung zulässig ist, Berlin. 
(3) Sollte eine Bestimmung dieser Nutzungsbedingungen unwirksam sein oder werden, bleibt die Wirksamkeit der übrigen Bestimmungen hiervon unberührt. 
***

  """);
}

class FamedlyLocalizationsDelegate
    extends LocalizationsDelegate<FamedlyLocalizations> {
  const FamedlyLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'de'].contains(locale.languageCode);
  }

  @override
  Future<FamedlyLocalizations> load(Locale locale) {
    return FamedlyLocalizations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<FamedlyLocalizations> old) {
    return false;
  }
}

class locale {
  static FamedlyLocalizations tr(BuildContext context) {
    FamedlyLocalizations famedlyLocalizations =
        FamedlyLocalizations.of(context);
    if (famedlyLocalizations != null)
      return famedlyLocalizations;
    else
      return new FamedlyLocalizations();
  }
}
