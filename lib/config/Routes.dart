import 'package:famedly/components/AdaptivePageLayout.dart';
import 'package:famedly/components/DocumentViewer.dart';
import 'package:famedly/components/HomePicker.dart';
import 'package:famedly/views/ArchivesPage.dart';
import 'package:famedly/views/BroadcastListPage.dart';
import 'package:famedly/views/ChatRoom.dart';
import 'package:famedly/views/ContactsPage.dart';
import 'package:famedly/views/EmptyPage.dart';
import 'package:famedly/views/FacilityPage.dart';
import 'package:famedly/views/FacilityPickerPage.dart';
import 'package:famedly/views/ImageView.dart';
import 'package:famedly/views/InvitePage.dart';
import 'package:famedly/views/LoginPage.dart';
import 'package:famedly/views/MediaList.dart';
import 'package:famedly/views/MemberListPage.dart';
import 'package:famedly/views/NewBroadcastPage.dart';
import 'package:famedly/views/NewGroupPage.dart';
import 'package:famedly/views/RequestCompositingPage.dart';
import 'package:famedly/views/RequestsListPage.dart';
import 'package:famedly/views/RequestsPage.dart';
import 'package:famedly/views/RoomSettings.dart';
import 'package:famedly/views/RoomsPage.dart';
import 'package:famedly/views/SettingsPage.dart';
import 'package:famedly/views/TagPickerPage.dart';
import 'package:famedly/views/TaskPage.dart';
import 'package:famedly/views/TasksPage.dart';
import 'package:famedly/views/UserViewer.dart';
import 'package:famedly/views/event_search_results_page.dart';
import 'package:famedly/views/intro.dart';
import 'package:famedly/views/not_implemented_warnings/CallingWarning.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_device_type/flutter_device_type.dart';

class FamedlyRoutes {
  final BuildContext context;

  const FamedlyRoutes(this.context);

  Route getRoute(RouteSettings settings) {
    final List<String> routeTiles = settings.name.split("/");

    switch (routeTiles[1]) {
      case '':
        return _mainRoute(HomePicker());
      case 'login':
        return _defaultRoute(LoginPage());
      case 'intro':
        return _defaultRoute(Intro());
      case 'contacts':
        return _defaultRoute(AdaptivePageLayout(
            firstScaffold: RoomsPage(), secondScaffold: ContactsPage()));
      case 'newGroup':
        return _defaultRoute(AdaptivePageLayout(
            firstScaffold: RoomsPage(), secondScaffold: NewGroupPage()));
      case 'broadcast':
        return _defaultRoute(AdaptivePageLayout(
          firstScaffold: BroadcastListPage(requestId: routeTiles[2]),
          secondScaffold: EmptyPage(),
          primaryPage: FocusPage.FIRST,
        ));
      case 'requests':
        return _mainRoute(AdaptivePageLayout(
          secondScaffold: EmptyPage(),
          primaryPage: FocusPage.FIRST,
          firstScaffold: RequestsPage(),
        ));
      case 'incomingRequests':
        return _defaultRoute(AdaptivePageLayout(
            secondScaffold: EmptyPage(),
            primaryPage: FocusPage.FIRST,
            firstScaffold: RequestsListPage(onlyInbox: true)));
      case 'outgoingRequests':
        return _defaultRoute(AdaptivePageLayout(
            secondScaffold: EmptyPage(),
            primaryPage: FocusPage.FIRST,
            firstScaffold: RequestsListPage(onlyInbox: false)));
      case 'archivedRequests':
        return _defaultRoute(AdaptivePageLayout(
            secondScaffold: EmptyPage(),
            primaryPage: FocusPage.FIRST,
            firstScaffold: RequestsListPage(onlyArchive: true)));
      case 'chatArchive':
        return _defaultRoute(AdaptivePageLayout(
            firstScaffold: RoomsPage(), secondScaffold: ArchivesPage()));
      case 'settings':
        return _defaultRoute(AdaptivePageLayout(
            firstScaffold: RoomsPage(), secondScaffold: SettingsPage()));
      case 'documents':
        if (routeTiles.length > 2 && !routeTiles[2].isEmpty) {
          return _defaultRoute(AdaptivePageLayout(
              firstScaffold: SettingsPage(),
              secondScaffold: DocumentViewer(routeTiles[2])));
        }
        return _defaultRoute(AdaptivePageLayout(
            firstScaffold: SettingsPage(), secondScaffold: DocumentViewer("")));

      case 'call':
        return _defaultRoute(AdaptivePageLayout(
            firstScaffold: RoomsPage(),
            secondScaffold: CallingWarningView("", "")));
      case 'tasks':
        if (routeTiles.length > 2 && !routeTiles[2].isEmpty) {
          return _defaultRoute(AdaptivePageLayout(
              secondScaffold: EmptyPage(),
              primaryPage: FocusPage.FIRST,
              firstScaffold: TaskPage(id: routeTiles[2])));
        }
        return _mainRoute(AdaptivePageLayout(
            secondScaffold: EmptyPage(),
            primaryPage: FocusPage.FIRST,
            firstScaffold: TasksPage()));
      case 'newBroadcast':
        if (routeTiles.length > 2 && routeTiles[2] == "verify") {
          return _defaultRoute(AdaptivePageLayout(
            secondScaffold: EmptyPage(),
            primaryPage: FocusPage.FIRST,
            firstScaffold: NewBroadcastPage(settings.arguments),
          ));
        }
        return _defaultRoute(AdaptivePageLayout(
            secondScaffold: EmptyPage(),
            primaryPage: FocusPage.FIRST,
            firstScaffold: TagPickerPage()));
      case 'requestCompositing':
        return _defaultRoute(AdaptivePageLayout(
            firstScaffold: routeTiles.length > 2
                ? FacilityPage(routeTiles[2])
                : TagPickerPage(),
            secondScaffold: RequestCompositingPage(settings.arguments)));
      case 'facilities':
        if (routeTiles.length > 2 && !routeTiles[2].isEmpty)
          return _defaultRoute(AdaptivePageLayout(
              secondScaffold: EmptyPage(),
              primaryPage: FocusPage.FIRST,
              firstScaffold: FacilityPage(routeTiles[2])));
        return _defaultRoute(AdaptivePageLayout(
            secondScaffold: EmptyPage(),
            primaryPage: FocusPage.FIRST,
            firstScaffold: FacilityPickerPage()));
      case 'room':
        if (routeTiles.length == 3 && !routeTiles[2].isEmpty)
          return _defaultRoute(AdaptivePageLayout(
              firstScaffold: RoomsPage(activeChatID: routeTiles[2]),
              secondScaffold: ChatRoom(id: routeTiles[2])));
        if (routeTiles.length == 4 &&
            routeTiles[2] == "incomingRequests" &&
            !routeTiles[3].isEmpty)
          return _defaultRoute(AdaptivePageLayout(
              firstScaffold: RequestsListPage(onlyInbox: true),
              secondScaffold: ChatRoom(id: routeTiles[3])));
        if (routeTiles.length == 4 &&
            routeTiles[2] == "outgoingRequests" &&
            !routeTiles[3].isEmpty)
          return _defaultRoute(AdaptivePageLayout(
              firstScaffold: RequestsListPage(onlyInbox: false),
              secondScaffold: ChatRoom(id: routeTiles[3])));
        if (routeTiles.length == 4 &&
            routeTiles[2] == "archivedRequests" &&
            !routeTiles[3].isEmpty)
          return _defaultRoute(AdaptivePageLayout(
              firstScaffold: RequestsListPage(onlyArchive: true),
              secondScaffold: ChatRoom(id: routeTiles[3])));
        if (routeTiles.length == 4 && routeTiles[3] == "members")
          return _defaultRoute(AdaptivePageLayout(
              firstScaffold: RoomsPage(),
              secondScaffold: MemberListPage(routeTiles[2])));
        if (routeTiles.length == 5 && routeTiles[3] == "search") {
          return _defaultRoute(
            AdaptivePageLayout(
              firstScaffold: RoomsPage(),
              secondScaffold: EventSearchResultsPage(
                id: routeTiles[2],
                searchQuery: routeTiles[4],
              ),
            ),
          );
        }
        if (routeTiles.length == 4 && routeTiles[3] == "invite")
          return _defaultRoute(AdaptivePageLayout(
              firstScaffold: RoomsPage(),
              secondScaffold: InvitePage(id: routeTiles[2])));
        if (routeTiles.length == 5 && routeTiles[3] == "member")
          return _defaultRoute(AdaptivePageLayout(
              firstScaffold: RoomsPage(),
              secondScaffold: UserViewer(routeTiles[2], routeTiles[4])));
        if (routeTiles.length == 5 && routeTiles[2] == "imageView")
          return _mainRoute(
              ImageView(mxcurl: routeTiles[3] + "/" + routeTiles[4]));
        if (routeTiles.length == 5 && routeTiles[3] == "media")
          return _defaultRoute(
              MediaList(roomID: routeTiles[2], type: routeTiles[4]));
        if (routeTiles.length == 6 && routeTiles[4] == "member")
          return _defaultRoute(AdaptivePageLayout(
              firstScaffold: RoomsPage(),
              secondScaffold: MediaList(
                  roomID: routeTiles[2],
                  type: routeTiles[5],
                  userID: routeTiles[4])));
        if (routeTiles.length == 4 && routeTiles[3] == "settings")
          return _defaultRoute(AdaptivePageLayout(
              firstScaffold: RoomsPage(),
              secondScaffold: RoomSettings(id: routeTiles[2])));
        return null;
      case 'rooms':
        return _mainRoute(AdaptivePageLayout(
          firstScaffold: RoomsPage(),
          secondScaffold: EmptyPage(),
          primaryPage: FocusPage.FIRST,
        ));
      default:
        return null;
    }
  }

  Route _defaultRoute(Widget builder) {
    return context != null && Device.screenWidth < 800
        ? CupertinoPageRoute(
            builder: (BuildContext context) => builder,
          )
        : FadeRoute(page: builder);
  }

  Route _mainRoute(Widget builder) {
    return FadeRoute(page: builder);
  }
}

class FadeRoute extends PageRouteBuilder {
  final Widget page;
  FadeRoute({this.page})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
        );
}
