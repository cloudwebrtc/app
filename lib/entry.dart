import 'package:famedly/components/LoadingDialog.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/config/Routes.dart';
import 'package:famedly/styles/colors.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

/// This is the entry class used to define Themes and the page to load on start.
class FamedlyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Matrix(
      onGlobalError: (String title, String error, BuildContext localContext) {
        Flushbar(
          title: title,
          message: error,
          duration: Duration(seconds: 5),
          flushbarPosition: FlushbarPosition.BOTTOM,
          flushbarStyle: FlushbarStyle.GROUNDED,
        )..show(localContext);
      },
      clientName: "Famedly",
      blockUiDialog: (onCancel) => LoadingDialog(),
      child: Builder(builder: (context) {
        return MaterialApp(
          title: "Famedly",
          theme: Themes.famedlyLightTheme,
          //darkTheme: Themes.famedlyDarkTheme,
          localizationsDelegates: [
            FamedlyLocalizationsDelegate(),
            GlobalCupertinoLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('de', ''), // German
            const Locale('en', ''), // English
          ],
          locale: Locale("de", ""),
          onGenerateRoute: FamedlyRoutes(context).getRoute,
          initialRoute: "/",
        );
      }),
    );
  }
}
