// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a de locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'de';

  static m0(user) => "${user} aus der Gruppe entfernen?";

  static m1(user) => "${user} zum Administrator machen?";

  static m2(username) => "${username} wurde in die Unterhaltung eingeladen.";

  static m3(user) => "${user} hat eure gemeinsame Unterhaltung archiviert.";

  static m4(displayName) => "${displayName} wurde in die Gruppe eingeladen.";

  static m5(displayName) => "${displayName} in diesen Chat einladen?";

  static m6(time) => "Empfangen: ${time}";

  static m7(senderName) => "${senderName} hat eine Datei gesendet.";

  static m8(senderName) => "${senderName} hat einen Standort geteilt.";

  static m9(senderName) => "${senderName} hat ein Video gesendet.";

  static m10(senderName) => "${senderName} hat eine Sprachnachricht gesendet.";

  static m11(senderName) => "${senderName} hat ein Bild gesendet.";

  static m12(user) => "Von: ${user}";

  static m13(count) => "Zeige mehr (${count})";

  static m14(trRoomName) => "Leere Unterhaltung";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    " changed the room avatar." : MessageLookupByLibrary.simpleMessage(" Hat den Raum-Avatar geändert."),
    " changed the room name to " : MessageLookupByLibrary.simpleMessage(" hat den Titel der Unterhaltung geändert zu "),
    " changed the room topic to " : MessageLookupByLibrary.simpleMessage(" hat das Thema der Unterhaltung geändert zu "),
    " created the room." : MessageLookupByLibrary.simpleMessage(" hat die Unterhaltung gestartet."),
    " got invited to the room." : MessageLookupByLibrary.simpleMessage(" wurde in die Unterhaltung eingeladen."),
    " has been banned from this room." : MessageLookupByLibrary.simpleMessage(" wurde von der Unterhaltung verbannt."),
    " has changed the avatar." : MessageLookupByLibrary.simpleMessage(" hat das Profilbild geändert."),
    " has changed the displayname." : MessageLookupByLibrary.simpleMessage(" Raum hat den Anzeigenamen geändert."),
    " has joined the room." : MessageLookupByLibrary.simpleMessage(" ist der Unterhaltung beigetreten."),
    " has kicked " : MessageLookupByLibrary.simpleMessage(" hat hinausgeworfen: "),
    " has left the room." : MessageLookupByLibrary.simpleMessage(" hat die Unterhaltung verlassen."),
    " has rejected the invitation." : MessageLookupByLibrary.simpleMessage(" hat die Einladung abgelehnt."),
    " has revoked the invitation of " : MessageLookupByLibrary.simpleMessage(" hat die Einladung zurückgezogen von "),
    " has unbanned " : MessageLookupByLibrary.simpleMessage(" hat die Verbannung aufgehoben von "),
    " sent an unknown event of type " : MessageLookupByLibrary.simpleMessage(" hat ein unbekanntes Ereignis geschickt vom Typ "),
    "\"Browse Famedly\"" : MessageLookupByLibrary.simpleMessage("\"Famedly durchsuchen\""),
    "*** Unable to parse membership event ***" : MessageLookupByLibrary.simpleMessage("*** Mitgliedschaftsereignis konnte nicht geladen werden ***"),
    "Accept Inquiry" : MessageLookupByLibrary.simpleMessage("Anfrage annehmen"),
    "Add date" : MessageLookupByLibrary.simpleMessage("Datum hinzufügen"),
    "Add description" : MessageLookupByLibrary.simpleMessage("Beschreibung hinzufügen"),
    "Add details" : MessageLookupByLibrary.simpleMessage("Details hinzufügen"),
    "Add room name" : MessageLookupByLibrary.simpleMessage("Raumnamen hinzufügen"),
    "Add subtask" : MessageLookupByLibrary.simpleMessage("Unteraufgabe hinzufügen"),
    "Add to tasks" : MessageLookupByLibrary.simpleMessage("Zu Aufgaben hinzufügen"),
    "Address" : MessageLookupByLibrary.simpleMessage("Adresse"),
    "Advanced search" : MessageLookupByLibrary.simpleMessage("Erweiterte Suche"),
    "Archive" : MessageLookupByLibrary.simpleMessage("Archiv"),
    "Archive cleared" : MessageLookupByLibrary.simpleMessage("Archiv erfolgreich geleert"),
    "Archive this conversation?" : MessageLookupByLibrary.simpleMessage("Diese Konversation archivieren?"),
    "Are you sure you want to sign out?" : MessageLookupByLibrary.simpleMessage("Möchten Sie sich wirklich abmelden?"),
    "At least one of the invitees is on an incompatible home server and therefore could not be invited. The conversation may have been created anyway." : MessageLookupByLibrary.simpleMessage("Mindestens eine der eingeladenen BenutzerInnen befindet sich auf einem inkompatiblen Organisationsserver und konnte daher nicht eingeladen werden. Die Unterhaltung wurde möglicherweise trotzdem erstellt."),
    "Audio" : MessageLookupByLibrary.simpleMessage("Audio"),
    "Back" : MessageLookupByLibrary.simpleMessage("Zurück"),
    "Banned" : MessageLookupByLibrary.simpleMessage("Verbannt"),
    "Broadcast" : MessageLookupByLibrary.simpleMessage("Rundruf"),
    "Calling" : MessageLookupByLibrary.simpleMessage("Anrufen"),
    "Camera" : MessageLookupByLibrary.simpleMessage("Kamera"),
    "Cancel" : MessageLookupByLibrary.simpleMessage("Abbrechen"),
    "Care services" : MessageLookupByLibrary.simpleMessage("Pflegedienstleistung"),
    "Care takeover" : MessageLookupByLibrary.simpleMessage("Pflegeübernahme"),
    "Change password" : MessageLookupByLibrary.simpleMessage("Passwort ändern"),
    "Chat Archive" : MessageLookupByLibrary.simpleMessage("Chat Archiv"),
    "Chat Details" : MessageLookupByLibrary.simpleMessage("Chat-Details"),
    "Chats" : MessageLookupByLibrary.simpleMessage("Chats"),
    "Clear" : MessageLookupByLibrary.simpleMessage("Löschen"),
    "Clear archive" : MessageLookupByLibrary.simpleMessage("Archiv leeren"),
    "Clear completed tasks" : MessageLookupByLibrary.simpleMessage("Erledigte Aufgaben löschen"),
    "Connected" : MessageLookupByLibrary.simpleMessage("Verbunden"),
    "Contact" : MessageLookupByLibrary.simpleMessage("Kontakt"),
    "Contact points" : MessageLookupByLibrary.simpleMessage("Kontaktstellen"),
    "Contacts" : MessageLookupByLibrary.simpleMessage("Kontakte"),
    "Create" : MessageLookupByLibrary.simpleMessage("Erstellen"),
    "Delete message for all participants?" : MessageLookupByLibrary.simpleMessage("Nachricht für alle Teilnehmer löschen?"),
    "Department" : MessageLookupByLibrary.simpleMessage("Abteilung"),
    "Document" : MessageLookupByLibrary.simpleMessage("Dokument"),
    "Document not found" : MessageLookupByLibrary.simpleMessage("Dokument nicht gefunden"),
    "Done to archive" : MessageLookupByLibrary.simpleMessage("Erledigte in Archiv"),
    "Error" : MessageLookupByLibrary.simpleMessage("Error"),
    "External requests" : MessageLookupByLibrary.simpleMessage("Neue Anfrage"),
    "Facilities" : MessageLookupByLibrary.simpleMessage("Einrichtungen"),
    "Files" : MessageLookupByLibrary.simpleMessage("Dokumente"),
    "Gallery" : MessageLookupByLibrary.simpleMessage("Galerie"),
    "General requests" : MessageLookupByLibrary.simpleMessage("Allgemeine Anfrage"),
    "Got it" : MessageLookupByLibrary.simpleMessage("Verstanden"),
    "Has been added to your tasks." : MessageLookupByLibrary.simpleMessage("Wurde zu deinen Aufgaben hinzugefügt."),
    "Hint:" : MessageLookupByLibrary.simpleMessage("Hinweis:"),
    "Images" : MessageLookupByLibrary.simpleMessage("Bilder"),
    "Incoming inquiries" : MessageLookupByLibrary.simpleMessage("Eingehende Anfragen"),
    "Incomming" : MessageLookupByLibrary.simpleMessage("Eingang"),
    "Information" : MessageLookupByLibrary.simpleMessage("Information"),
    "Inquiries" : MessageLookupByLibrary.simpleMessage("Anfragen"),
    "Invalid username or password" : MessageLookupByLibrary.simpleMessage("Ungültiger Benutzername oder Passwort."),
    "Invite" : MessageLookupByLibrary.simpleMessage("Einladen"),
    "Invited" : MessageLookupByLibrary.simpleMessage("Eingeladen"),
    "Kick" : MessageLookupByLibrary.simpleMessage("Aus dem Chat entfernen"),
    "Last search results:" : MessageLookupByLibrary.simpleMessage("Letzte Suchergebnisse:"),
    "Leave group" : MessageLookupByLibrary.simpleMessage("Gruppe verlassen"),
    "Left the room" : MessageLookupByLibrary.simpleMessage("Hat den Raum verlassen"),
    "License" : MessageLookupByLibrary.simpleMessage("Lizenz"),
    "Loading... Please wait" : MessageLookupByLibrary.simpleMessage("Wird geladen ..."),
    "Location" : MessageLookupByLibrary.simpleMessage("Standort"),
    "Location / Country Code" : MessageLookupByLibrary.simpleMessage("Ort / PLZ"),
    "Login" : MessageLookupByLibrary.simpleMessage("Login"),
    "Login with organisation" : MessageLookupByLibrary.simpleMessage("Bei Organisation anmelden"),
    "Make An Admin" : MessageLookupByLibrary.simpleMessage("Zum Admin machen"),
    "Media" : MessageLookupByLibrary.simpleMessage("Medien"),
    "Members" : MessageLookupByLibrary.simpleMessage("Mitglieder"),
    "Message has been removed!" : MessageLookupByLibrary.simpleMessage("Nachricht wurde entfernt!"),
    "Moderator" : MessageLookupByLibrary.simpleMessage("Moderator"),
    "Mute" : MessageLookupByLibrary.simpleMessage("Stummschalten"),
    "My tasks" : MessageLookupByLibrary.simpleMessage("Meine Aufgaben"),
    "Name" : MessageLookupByLibrary.simpleMessage("Name"),
    "New Broadcast" : MessageLookupByLibrary.simpleMessage("Neuer Rundruf"),
    "New group" : MessageLookupByLibrary.simpleMessage("Neue Gruppe"),
    "New message" : MessageLookupByLibrary.simpleMessage("Neue Nachricht"),
    "New task" : MessageLookupByLibrary.simpleMessage("Neue Aufgabe"),
    "Next" : MessageLookupByLibrary.simpleMessage("Weiter"),
    "No" : MessageLookupByLibrary.simpleMessage("Nein"),
    "No chats" : MessageLookupByLibrary.simpleMessage("Keine Unterhaltung"),
    "No facilities were found for this contact point ... :-(" : MessageLookupByLibrary.simpleMessage("Für diese Kontakpunkte wurden keine Einrichtungen gefunden ... :-("),
    "No inquiries" : MessageLookupByLibrary.simpleMessage("Keine Anfragen"),
    "No messages found yet" : MessageLookupByLibrary.simpleMessage("Keine Nachrichten bisher gefunden."),
    "No tasks" : MessageLookupByLibrary.simpleMessage("Keine Aufgaben"),
    "Not connected" : MessageLookupByLibrary.simpleMessage("Nicht verbunden"),
    "Not yet verified" : MessageLookupByLibrary.simpleMessage("Noch nicht verifiziert"),
    "Notice:" : MessageLookupByLibrary.simpleMessage("Hinweis:"),
    "Oops! Something went wrong..." : MessageLookupByLibrary.simpleMessage("Hoppla! Etwas ist schief gelaufen..."),
    "Open in Browser" : MessageLookupByLibrary.simpleMessage("In Browser öffnen"),
    "Organisation" : MessageLookupByLibrary.simpleMessage("Verwaltung"),
    "Organisation server is not responding" : MessageLookupByLibrary.simpleMessage("Der Organisationsserver antwortet nicht."),
    "Organisation server is not supported and may be out of date" : MessageLookupByLibrary.simpleMessage("Dieser Organisationsserver wird nicht unterstützt und ist möglicherweise veraltet."),
    "Outgoing" : MessageLookupByLibrary.simpleMessage("Ausgang"),
    "Outgoing inquiries" : MessageLookupByLibrary.simpleMessage("Ausgehende Anfragen"),
    "Password" : MessageLookupByLibrary.simpleMessage("Passwort"),
    "Please" : MessageLookupByLibrary.simpleMessage("Bitte"),
    "Please choose your organization or report your organization directly online and join the famous network. " : MessageLookupByLibrary.simpleMessage("Bitte wählen Sie Ihre Organisation aus oder melden Sie Ihre Organisation direkt online und treten Sie dem Famedly-Netzwerk bei."),
    "Please enter some text" : MessageLookupByLibrary.simpleMessage("Bitte gebe etwas ein"),
    "Please enter your password" : MessageLookupByLibrary.simpleMessage("Bitte gebe dein Password ein"),
    "Please enter your username" : MessageLookupByLibrary.simpleMessage("Bitte gebe deinen Benutzernamen ein"),
    "Please fill out text field" : MessageLookupByLibrary.simpleMessage("Bitte Textfeld ausfüllen!"),
    "Privacy Policy" : MessageLookupByLibrary.simpleMessage("Datenschutzvereinbarung"),
    "Reject Inquiry" : MessageLookupByLibrary.simpleMessage("Anfrage ablehnen"),
    "Reply" : MessageLookupByLibrary.simpleMessage("Antworten"),
    "Request Information" : MessageLookupByLibrary.simpleMessage("Weitere Informationen"),
    "Requests" : MessageLookupByLibrary.simpleMessage("Anfragen"),
    "Room description" : MessageLookupByLibrary.simpleMessage("Raumbeschreibung"),
    "Room name" : MessageLookupByLibrary.simpleMessage("Raumname"),
    "Screen lock" : MessageLookupByLibrary.simpleMessage("Bildschirmsperre"),
    "Search" : MessageLookupByLibrary.simpleMessage("Suchen"),
    "Search query" : MessageLookupByLibrary.simpleMessage("Suchanfrage"),
    "Search results" : MessageLookupByLibrary.simpleMessage("Suchergebnisse"),
    "Secret" : MessageLookupByLibrary.simpleMessage("Geheimnis"),
    "Security" : MessageLookupByLibrary.simpleMessage("Sicherheit"),
    "Send Private Message" : MessageLookupByLibrary.simpleMessage("Private Nachricht senden"),
    "Send crash reports to Famedly" : MessageLookupByLibrary.simpleMessage("Absturzberichte an Famedly senden"),
    "Server address" : MessageLookupByLibrary.simpleMessage("Serveradresse"),
    "Set" : MessageLookupByLibrary.simpleMessage("Fertig"),
    "Set Chat Description" : MessageLookupByLibrary.simpleMessage("Chat-Beschreibung festlegen"),
    "Set Chat Name" : MessageLookupByLibrary.simpleMessage("Chatnamen festlegen"),
    "Settings" : MessageLookupByLibrary.simpleMessage("Einstellungen"),
    "Sign out" : MessageLookupByLibrary.simpleMessage("Abmelden"),
    "Skip" : MessageLookupByLibrary.simpleMessage("Überspringen"),
    "Sort" : MessageLookupByLibrary.simpleMessage("Sortieren"),
    "Start new conversation" : MessageLookupByLibrary.simpleMessage("Neue Unterhaltung starten"),
    "Storage" : MessageLookupByLibrary.simpleMessage("Ablage"),
    "Successfull verified" : MessageLookupByLibrary.simpleMessage("Erfolgreich verifiziert"),
    "Task Archive" : MessageLookupByLibrary.simpleMessage("Anfragen Archiv"),
    "Tasks" : MessageLookupByLibrary.simpleMessage("Anfragen"),
    "The entries do not agree" : MessageLookupByLibrary.simpleMessage("Die Eingaben stimmen nicht überein"),
    "The username is not correct" : MessageLookupByLibrary.simpleMessage("Dieser Benutzername ist nicht korrekt"),
    "There currently seems to be no contact points visible to you ... :-(" : MessageLookupByLibrary.simpleMessage("Momentan sind keine Kontaktpunkte für Sie sichtbar ... :-("),
    "This feature has not been implemented yet." : MessageLookupByLibrary.simpleMessage("Diese Funktion wurde noch nicht implementiert."),
    "Too many requests. Please wait some minutes." : MessageLookupByLibrary.simpleMessage("Zu viele Anfragen. Bitte warten Sie einige Minuten."),
    "Username" : MessageLookupByLibrary.simpleMessage("Benutzername"),
    "Video" : MessageLookupByLibrary.simpleMessage("Video"),
    "Videos" : MessageLookupByLibrary.simpleMessage("Videos"),
    "Welcome to famedly" : MessageLookupByLibrary.simpleMessage("Willkommen bei Famedly"),
    "Wiederholen" : MessageLookupByLibrary.simpleMessage("Wiederholen"),
    "Yes" : MessageLookupByLibrary.simpleMessage("Ja"),
    "You" : MessageLookupByLibrary.simpleMessage("Du"),
    "You are connected securly." : MessageLookupByLibrary.simpleMessage("Sie sind sicher verbunden."),
    "You do not have permission for this action." : MessageLookupByLibrary.simpleMessage("Du hast nicht die Berechtigung für diese Aktion."),
    "You have been invited to chat" : MessageLookupByLibrary.simpleMessage("Du wurdest in die Unterhaltung eingeladen"),
    "Your concern" : MessageLookupByLibrary.simpleMessage("Ihr Anliegen"),
    "Your message" : MessageLookupByLibrary.simpleMessage("Deine Nachricht"),
    "Your title" : MessageLookupByLibrary.simpleMessage("Ihr Titel"),
    "abort" : MessageLookupByLibrary.simpleMessage("Abbrechen"),
    "acceptBroadcastNotice" : MessageLookupByLibrary.simpleMessage("Sind Sie sich sicher, dass sie diese Anfrage annehmen und alle anderen in diesem Rundruf ablehnen und archivieren wollen?"),
    "all" : MessageLookupByLibrary.simpleMessage("Alle"),
    "archiveAction" : MessageLookupByLibrary.simpleMessage("Archivieren"),
    "archivedInquiryNotice" : MessageLookupByLibrary.simpleMessage("Archivierte Anfrage"),
    "askKickUser" : m0,
    "askMakeAdmin" : m1,
    "call_warning_text" : MessageLookupByLibrary.simpleMessage("Aktuell wird das Telefonieren über famedly noch nicht unterstützt. Wir sagen Ihnen Bescheid, wann Sie die altmodischen Telefonlisten entsorgen können. :)"),
    "category" : MessageLookupByLibrary.simpleMessage("Kategorie"),
    "clearWarningText" : MessageLookupByLibrary.simpleMessage("Das Löschen kann nicht mehr rückgängig gemacht werden. Trotzdem löschen?"),
    "contactIsEmail" : MessageLookupByLibrary.simpleMessage("Dieser Kontakt ist eigendlich eine E-mail, also außerhalb des Famedly-Netwerkes."),
    "copy" : MessageLookupByLibrary.simpleMessage("Kopieren"),
    "delete message" : MessageLookupByLibrary.simpleMessage("Nachricht löschen"),
    "externalInquiryWarning" : MessageLookupByLibrary.simpleMessage("Diese Anfrage ist eine externe Anfrage, sie wird das Famedly-Netzwerk verlassen."),
    "give" : MessageLookupByLibrary.simpleMessage("geben"),
    "gotInvited" : m2,
    "hasArchivedConversation" : m3,
    "hasBeenInvited" : m4,
    "inquiry" : MessageLookupByLibrary.simpleMessage("Anfrage"),
    "interactAsWho" : MessageLookupByLibrary.simpleMessage("Als wen möchten sie agieren?"),
    "introPage1_1" : MessageLookupByLibrary.simpleMessage("Wir freuen uns Sie bei famedly, der Zukunft in der Medizin, begrüßen zu dürfen."),
    "introPage1_2" : MessageLookupByLibrary.simpleMessage("Auf den nächsten Seiten möchten wir Ihnen vorstellen, was Sie erwartet."),
    "introPage2_1" : MessageLookupByLibrary.simpleMessage("Mit famedly können sie jeden Versorger leicht und sicher erreichen."),
    "introPage2_2" : MessageLookupByLibrary.simpleMessage("Konzentrieren Sie sich auf die Arbeit mit Patienten und überlassen Sie uns den Rest."),
    "introPage3_1" : MessageLookupByLibrary.simpleMessage("Mit famedly wird es möglich sein, jedes medizinsche Gerät oder Software anzusteuern."),
    "introPage3_2" : MessageLookupByLibrary.simpleMessage("Das bedeutet für sie schnelles und einfaches Arbeiten aus einer Oberfläche."),
    "introTitle1" : MessageLookupByLibrary.simpleMessage("Herzlich Willkommen"),
    "introTitle2" : MessageLookupByLibrary.simpleMessage("Miteinander sprechen"),
    "introTitle3" : MessageLookupByLibrary.simpleMessage("Verbindungen schaffen"),
    "inviteUser" : MessageLookupByLibrary.simpleMessage("Benutzer einladen"),
    "inviteUserToChat" : m5,
    "is typing..." : MessageLookupByLibrary.simpleMessage("schreibt ..."),
    "messages" : MessageLookupByLibrary.simpleMessage("Nachrichten"),
    "pickEmail" : MessageLookupByLibrary.simpleMessage("Wählen Sie eine email aus, die Sie anschreiben möchten."),
    "receivedAt" : m6,
    "rejectAndArchiveInquiryWarning" : MessageLookupByLibrary.simpleMessage("Anfrage wirklich ablehnen und archivieren?"),
    "rooms" : MessageLookupByLibrary.simpleMessage("Gruppen"),
    "sentAFile" : m7,
    "sentALocation" : m8,
    "sentAVideo" : m9,
    "sentAnAudio" : m10,
    "sentAnImage" : m11,
    "sentFrom" : m12,
    "showMore" : m13,
    "someone" : MessageLookupByLibrary.simpleMessage("Jemand"),
    "title" : MessageLookupByLibrary.simpleMessage("Famedly"),
    "trRoomName" : m14,
    "warnNoRoomInBroadcastDialog" : MessageLookupByLibrary.simpleMessage("Sie können erst mit der Gegenseite kommunizieren, wenn diese die Anfrage annimmt oder nach weiteren Informationen fragt!")
  };
}
