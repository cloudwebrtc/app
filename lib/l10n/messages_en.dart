// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static m0(user) => "Kick ${user} from this group?";

  static m1(user) => "Make ${user} an Admin of this group?";

  static m2(username) => "${username} got invited to the room.";

  static m3(user) => "${user} has archived your conversation.";

  static m4(displayName) => "${displayName} has been invited.";

  static m5(displayName) => "Invite ${displayName} to this chat?";

  static m6(time) => "Received ${time}";

  static m7(senderName) => "${senderName} sent a file.";

  static m8(senderName) => "${senderName} sent a location.";

  static m9(senderName) => "${senderName} sent a video.";

  static m10(senderName) => "${senderName} sent an audio message.";

  static m11(senderName) => "${senderName} sent an image.";

  static m12(user) => "From ${user}";

  static m14(trRoomName) => "${trRoomName}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    " changed the room avatar." : MessageLookupByLibrary.simpleMessage(" changed the room avatar."),
    " changed the room name to " : MessageLookupByLibrary.simpleMessage(" changed the room name to "),
    " changed the room topic to " : MessageLookupByLibrary.simpleMessage(" changed the room topic to "),
    " created the room." : MessageLookupByLibrary.simpleMessage(" created the room."),
    " got invited to the room." : MessageLookupByLibrary.simpleMessage(" got invited to the room."),
    " has been banned from this room." : MessageLookupByLibrary.simpleMessage(" has been banned from this room."),
    " has changed the avatar." : MessageLookupByLibrary.simpleMessage(" has changed the avatar."),
    " has changed the displayname." : MessageLookupByLibrary.simpleMessage(" has changed the displayname."),
    " has joined the room." : MessageLookupByLibrary.simpleMessage(" has joined the room."),
    " has kicked " : MessageLookupByLibrary.simpleMessage(" has kicked "),
    " has left the room." : MessageLookupByLibrary.simpleMessage(" has left the room."),
    " has rejected the invitation." : MessageLookupByLibrary.simpleMessage(" has rejected the invitation."),
    " has revoked the invitation of " : MessageLookupByLibrary.simpleMessage(" has revoked the invitation of "),
    " has unbanned " : MessageLookupByLibrary.simpleMessage(" has unbanned "),
    " sent an unknown event of type " : MessageLookupByLibrary.simpleMessage(" sent an unknown event of type "),
    "\"Browse Famedly\"" : MessageLookupByLibrary.simpleMessage("\"Browse Famedly\""),
    "*** Unable to parse membership event ***" : MessageLookupByLibrary.simpleMessage("*** Unable to parse membership event ***"),
    "Add description" : MessageLookupByLibrary.simpleMessage("Add description"),
    "Add room name" : MessageLookupByLibrary.simpleMessage("Add room name"),
    "Advanced search" : MessageLookupByLibrary.simpleMessage("Advanced search"),
    "Archive" : MessageLookupByLibrary.simpleMessage("Archive"),
    "Archive cleared" : MessageLookupByLibrary.simpleMessage("Archive cleared"),
    "Archive this conversation?" : MessageLookupByLibrary.simpleMessage("Archive this conversation?"),
    "Are you sure you want to sign out?" : MessageLookupByLibrary.simpleMessage("Are you sure you want to sign out?"),
    "At least one of the invitees is on an incompatible home server and therefore could not be invited. The conversation may have been created anyway." : MessageLookupByLibrary.simpleMessage("At least one of the invitees is on an incompatible home server and therefore could not be invited. The conversation may have been created anyway."),
    "Audio" : MessageLookupByLibrary.simpleMessage("Audio"),
    "Back" : MessageLookupByLibrary.simpleMessage("Back"),
    "Calling" : MessageLookupByLibrary.simpleMessage("Calling"),
    "Cancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "Care services" : MessageLookupByLibrary.simpleMessage("Care services"),
    "Care takeover" : MessageLookupByLibrary.simpleMessage("Care takeover"),
    "Chat Archive" : MessageLookupByLibrary.simpleMessage("Chat Archive"),
    "Chat Details" : MessageLookupByLibrary.simpleMessage("Chat Details"),
    "Chats" : MessageLookupByLibrary.simpleMessage("Chats"),
    "Clear" : MessageLookupByLibrary.simpleMessage("Clear"),
    "Clear archive" : MessageLookupByLibrary.simpleMessage("Clear archive"),
    "Contact" : MessageLookupByLibrary.simpleMessage("Contact"),
    "Contacts" : MessageLookupByLibrary.simpleMessage("Contacts"),
    "Department" : MessageLookupByLibrary.simpleMessage("Department"),
    "Document" : MessageLookupByLibrary.simpleMessage("Document"),
    "Done to archive" : MessageLookupByLibrary.simpleMessage("Done to archive"),
    "External requests" : MessageLookupByLibrary.simpleMessage("External requests"),
    "Files" : MessageLookupByLibrary.simpleMessage("Files"),
    "Gallery" : MessageLookupByLibrary.simpleMessage("Gallery"),
    "General requests" : MessageLookupByLibrary.simpleMessage("General requests"),
    "Got it" : MessageLookupByLibrary.simpleMessage("Got it"),
    "Hint" : MessageLookupByLibrary.simpleMessage("Hint"),
    "Hint:" : MessageLookupByLibrary.simpleMessage("Hint:"),
    "Images" : MessageLookupByLibrary.simpleMessage("Images"),
    "Incomming" : MessageLookupByLibrary.simpleMessage("Incomming"),
    "Invalid username or password" : MessageLookupByLibrary.simpleMessage("Invalid username or password"),
    "Invite" : MessageLookupByLibrary.simpleMessage("Invite"),
    "Kick" : MessageLookupByLibrary.simpleMessage("Kick"),
    "Last search results:" : MessageLookupByLibrary.simpleMessage("Last search results:"),
    "Leave group" : MessageLookupByLibrary.simpleMessage("Leave group"),
    "Loading... Please wait" : MessageLookupByLibrary.simpleMessage("Loading... Please wait"),
    "Location" : MessageLookupByLibrary.simpleMessage("Location"),
    "Location / Country Code" : MessageLookupByLibrary.simpleMessage("Location / Country Code"),
    "Login" : MessageLookupByLibrary.simpleMessage("Login"),
    "Login with organisation" : MessageLookupByLibrary.simpleMessage("Login with organisation"),
    "Make An Admin" : MessageLookupByLibrary.simpleMessage("Make An Admin"),
    "Media" : MessageLookupByLibrary.simpleMessage("Media"),
    "Members" : MessageLookupByLibrary.simpleMessage("Members"),
    "Mute" : MessageLookupByLibrary.simpleMessage("Mute"),
    "Name" : MessageLookupByLibrary.simpleMessage("Name"),
    "New group" : MessageLookupByLibrary.simpleMessage("New group"),
    "New message" : MessageLookupByLibrary.simpleMessage("New message"),
    "Next" : MessageLookupByLibrary.simpleMessage("Next"),
    "No" : MessageLookupByLibrary.simpleMessage("No"),
    "Not yet verified" : MessageLookupByLibrary.simpleMessage("Not yet verified"),
    "Notice:" : MessageLookupByLibrary.simpleMessage("Notice:"),
    "Oops! Something went wrong..." : MessageLookupByLibrary.simpleMessage("Oops! Something went wrong..."),
    "Organisation" : MessageLookupByLibrary.simpleMessage("Organisation"),
    "Organisation server is not responding" : MessageLookupByLibrary.simpleMessage("Organisation server is not responding"),
    "Organisation server is not supported and may be out of date" : MessageLookupByLibrary.simpleMessage("Organisation server is not supported and may be out of date"),
    "Outgoing" : MessageLookupByLibrary.simpleMessage("Outgoing"),
    "Password" : MessageLookupByLibrary.simpleMessage("Password"),
    "Please enter some text" : MessageLookupByLibrary.simpleMessage("Please enter some text"),
    "Please enter your password" : MessageLookupByLibrary.simpleMessage("Please enter your password"),
    "Please enter your username" : MessageLookupByLibrary.simpleMessage("Please enter your username"),
    "Room description" : MessageLookupByLibrary.simpleMessage("Room description"),
    "Room name" : MessageLookupByLibrary.simpleMessage("Room name"),
    "Search" : MessageLookupByLibrary.simpleMessage("Search"),
    "Send Private Message" : MessageLookupByLibrary.simpleMessage("Send Private Message"),
    "Server address" : MessageLookupByLibrary.simpleMessage("Server address"),
    "Set" : MessageLookupByLibrary.simpleMessage("Set"),
    "Set Chat Description" : MessageLookupByLibrary.simpleMessage("Set Chat Description"),
    "Set Chat Name" : MessageLookupByLibrary.simpleMessage("Set Chat Name"),
    "Settings" : MessageLookupByLibrary.simpleMessage("Settings"),
    "Sign out" : MessageLookupByLibrary.simpleMessage("Sign out"),
    "Skip" : MessageLookupByLibrary.simpleMessage("Skip"),
    "Sort" : MessageLookupByLibrary.simpleMessage("Sort"),
    "Storage" : MessageLookupByLibrary.simpleMessage("Storage"),
    "Successfull verified" : MessageLookupByLibrary.simpleMessage("Successfull verified"),
    "Task Archive" : MessageLookupByLibrary.simpleMessage("Task Archive"),
    "Tasks" : MessageLookupByLibrary.simpleMessage("Tasks"),
    "The username is not correct" : MessageLookupByLibrary.simpleMessage("The username is not correct"),
    "This feature has not been implemented yet." : MessageLookupByLibrary.simpleMessage("This feature has not been implemented yet."),
    "Too many requests. Please wait some minutes." : MessageLookupByLibrary.simpleMessage("Too many requests. Please wait some minutes."),
    "Username" : MessageLookupByLibrary.simpleMessage("Username"),
    "Video" : MessageLookupByLibrary.simpleMessage("Video"),
    "Videos" : MessageLookupByLibrary.simpleMessage("Videos"),
    "Yes" : MessageLookupByLibrary.simpleMessage("Yes"),
    "You" : MessageLookupByLibrary.simpleMessage("You"),
    "You do not have permission for this action." : MessageLookupByLibrary.simpleMessage("You do not have permission for this action."),
    "all" : MessageLookupByLibrary.simpleMessage("All"),
    "archiveAction" : MessageLookupByLibrary.simpleMessage("Archive"),
    "askKickUser" : m0,
    "askMakeAdmin" : m1,
    "call_warning_text" : MessageLookupByLibrary.simpleMessage("Currently, calling via famedly is not yet supported. We will let you know when you can dispose of the old-fashioned telephone lists. :)"),
    "clearWarningText" : MessageLookupByLibrary.simpleMessage("The deletion can not be undone. Delete anyway?"),
    "copy" : MessageLookupByLibrary.simpleMessage("copy"),
    "gotInvited" : m2,
    "hasArchivedConversation" : m3,
    "hasBeenInvited" : m4,
    "introPage1_1" : MessageLookupByLibrary.simpleMessage("We look forward to welcoming you at famedly, the future in medicine."),
    "introPage1_2" : MessageLookupByLibrary.simpleMessage("On the following pages we would like to introduce you to what awaits you."),
    "introPage2_1" : MessageLookupByLibrary.simpleMessage("With famedly they can reach every provider easily and safely."),
    "introPage2_2" : MessageLookupByLibrary.simpleMessage("Focus on working with patients and leave the rest to us."),
    "introPage3_1" : MessageLookupByLibrary.simpleMessage("With famedly it will be possible to control any medical device or software."),
    "introPage3_2" : MessageLookupByLibrary.simpleMessage("That means fast and easy working from a surface for them."),
    "introTitle1" : MessageLookupByLibrary.simpleMessage("Welcome"),
    "introTitle2" : MessageLookupByLibrary.simpleMessage("Talk to each other"),
    "introTitle3" : MessageLookupByLibrary.simpleMessage("Create connections"),
    "inviteUser" : MessageLookupByLibrary.simpleMessage("Invite user"),
    "inviteUserToChat" : m5,
    "messages" : MessageLookupByLibrary.simpleMessage("Messages"),
    "receivedAt" : m6,
    "rooms" : MessageLookupByLibrary.simpleMessage("Rooms"),
    "sentAFile" : m7,
    "sentALocation" : m8,
    "sentAVideo" : m9,
    "sentAnAudio" : m10,
    "sentAnImage" : m11,
    "sentFrom" : m12,
    "someone" : MessageLookupByLibrary.simpleMessage("someone"),
    "title" : MessageLookupByLibrary.simpleMessage("Famedly"),
    "trRoomName" : m14
  };
}
