import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:device_info/device_info.dart';
import 'package:famedly/entry.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:localstorage/localstorage.dart';
import 'package:package_info/package_info.dart';
import 'package:sentry/sentry.dart';

final SentryClient _sentry = SentryClient(
    dsn: "https://f547434868e64292b70380ab05a5af26@sentry.io/1448986");

/// This function just loads the App itself.
void main() {
  Widget CustomErrorWidget() => Container(
      color: Colors.white,
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ClipOval(
            child: Image.asset(
              "assets/images/famedly_icon.png",
              width: 144,
              height: 144,
              fit: BoxFit.cover,
            ),
          ),
          Text(
              "Oops, something went wrong ... The app has crashed for unknown reasons and needs to be restarted.",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  decoration: TextDecoration.none)),
          RaisedButton(
            child: Text("Close", style: TextStyle(color: Colors.white)),
            color: Color(0xff1fdcca),
            onPressed: () {
              exit(0);
            },
          )
        ],
      )));

  // This captures errors reported by the Flutter framework.
  FlutterError.onError = (FlutterErrorDetails details) {
    if (isInDebugMode) {
      // In development mode, simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode, report to the application zone to report to
      // Sentry.
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(statusBarColor: Colors.white),
  );

  runZoned<Future<void>>(() async {
    ErrorWidget.builder = (FlutterErrorDetails details) => CustomErrorWidget();
    runApp(FamedlyApp());
  }, onError: (error, stackTrace) {
    // Whenever an error occurs, call the `_reportError` function. This sends
    // Dart errors to the dev console or Sentry depending on the environment.
    _reportError(error, stackTrace);
  });
}

bool get isInDebugMode {
  // Assume you're in production mode
  bool inDebugMode = false;

  // Assert expressions are only evaluated during development. They are ignored
  // in production. Therefore, this code only sets `inDebugMode` to true
  // in a development environment.
  assert(inDebugMode = true);

  return inDebugMode;
}

/// Reports the [exception] and optionally its [stackTrace] to Sentry.io.
///
/// Optionally allows specifying a [stackFrameFilter] that receives the
/// list of stack frames just before sending to allow modifying it.
Future<SentryResponse> captureException({
  @required dynamic exception,
  dynamic stackTrace,
}) async {
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  String device_display_name;
  String device_vendor;
  if (Platform.isAndroid) {
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    device_display_name = androidInfo.device;
    device_vendor = androidInfo.brand;
  } else if (Platform.isIOS) {
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    device_display_name = iosInfo.model;
    device_vendor = "Apple";
  } else {
    device_vendor = "PC";
    device_display_name = "Famedly Desktop";
  }
  final Event event = new Event(
    environment: "production",
    userContext: User(
      id: Random.secure().nextInt(400000000).toString(),
      // Full random to be sure that we cant recognize a person by its id.
      extras: {
        "version": packageInfo.buildNumber,
        "vendor": device_vendor,
        "device": device_display_name,
      },
    ),
    release: packageInfo.buildNumber,
    exception: exception,
    stackTrace: stackTrace,
  );
  return _sentry.capture(event: event);
}

Future<void> _reportError(dynamic error, dynamic stackTrace) async {
  final LocalStorage storage = LocalStorage('FamedlyLocalStorage');
  await storage.ready;
  final bool sentryOptIn = storage.getItem("sentry") ?? true;
  // Print the exception to the console
  print('Caught error: $error');
  if (isInDebugMode || !sentryOptIn) {
    // Print the full stacktrace in debug mode
    print(stackTrace);
    return;
  } else {
    // Send the Exception and Stacktrace to Sentry in Production mode
    captureException(
      exception: error,
      stackTrace: stackTrace,
    );
  }
}
