import 'package:famedly/entry.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// This function just loads the App itself.
void main() {
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  SystemChrome.setEnabledSystemUIOverlays([]);
  runApp(FamedlyApp());
}