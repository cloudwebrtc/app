import 'package:famedly/models/requests/RequestContent.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/directory/directory.dart';
import 'package:famedly/models/directory/visibility.dart';
import 'package:famedly/models/directory/organisation.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:famedly/utils/mxidEncode.dart';

const String _matrixUrlSchemeMask = "https://matrix.to/#/";
const String _emailSchemeMask = "mailto:";
const String _emailPrefix = "_email_";

/// Root Type for Contact
class Contact {
  /// The `Visibility` for this contact. When not supplied, a wildcard visibility is assumed.
  /// Is additionally limited by the visibility of the organisation it is contained in.
  final Visibility visibility;

  /// Who or what can be reached here? This is usually the name of a division.
  final String description;

  /// The URI of this contact, for example a phone number, email address or matrix ID.
  final String uri;

  String _matrixId;
  String get matrixId {
    if (_matrixId == null && uri.startsWith(_matrixUrlSchemeMask)) {
      _matrixId = uri.replaceFirst(_matrixUrlSchemeMask, "");
    }

    return _matrixId;
  }

  String _email;
  String get email {
    if (_email == null && uri.startsWith(_emailSchemeMask)) {
      _email = uri.replaceFirst(_emailSchemeMask, "");
    }
    return _email;
  }

  /// A [PASSporT](https://tools.ietf.org/html/rfc8225) (Personal Assertion Token),
  /// that assesses that the requesting entity is allowed to contact this contact.
  /// This token is generated by the directory on the fly
  /// when a contact is being queried through an organisations search.
  /// It should not be supplied when creating or updating a contact.
  /// The one supplied in the example is not accurate,
  /// because it is not asynchronously signed.
  /// How it is signed exactly depends on the configuration of the directory.
  final String passport;

  /// A list of tags for this contact. All tags listed on the organisation
  /// are merged into this list.
  final List<String> tags;

  // the ID of the contact
  final String id;

  // the organisation the contact belongs to
  Organisation organisation = null;

  Contact(
      {this.visibility, this.description, this.uri, this.passport, this.tags, this.id});

  static List<Contact> listFromJson(List<dynamic> jsonList) {
    List<Contact> contacts = [];
    jsonList.forEach((v) {
      contacts.add(new Contact.fromJson(v));
    });
    return contacts;
  }

  Contact.fromJson(Map<String, dynamic> json)
      : visibility = json['visibility'] != null
            ? new Visibility.fromJson(json['visibility'])
            : null,
        description = json['description'],
        uri = json['uri'],
        passport = json['passport'],
        tags = json['tags'].cast<String>(),
        id = json['id'];

  RequestContent getRequestState({
    String requestTitle,
    String requestBody,
    String requestId,
    Client client,
  }) {
    String requestedOrganisation = uri;
    if (matrixId != null) {
      requestedOrganisation = matrixId.split(":")[1];
    } else if (email != null) {
      requestedOrganisation = email.split("@")[1];
    }
    return RequestContent(
      creator: client.userID,
      requestedOrganisation: requestedOrganisation,
      requestingOrganisation: client.userID.split(":")[1],
      requestTitle: requestTitle,
      requestId: requestId,
      msgtype: "m.text",
      body: requestBody,
      organisationId: organisation?.id,
      organisationName: organisation?.name,
      contactId: id,
      contactDescription: description,
    );
  }

  List<Map<String, dynamic>> getInitialState({
    String requestTitle,
    String requestBody,
    String requestId,
    Client client,
  }) {
    return [
      {
        "type": Request.mRequestNameSpace,
        "content": getRequestState(
          requestTitle: requestTitle,
          requestBody: requestBody,
          requestId: requestId,
          client: client,
        ).toJson(),
      },
      {
        "type": "m.room.history_visibility",
        "content": {"history_visibility": "shared"}
      },
      {
        "type": "m.room.guest_access",
        "content": {"guest_access": "forbidden"}
      },
      {
        "type": Directory.mPassport,
        "content": {"token": passport},
      },
    ];
  }

  /// Starts an official request to this organisation at this contact point. Returns
  /// the Matrix room ID if successfull.
  Future<String> startRequest(
      {String requestTitle,
      String requestBody,
      String requestId,
      Client client}) async {
    // Only matrix.to links are supported for now.
    String roomId = "";
    if (matrixId != null) {
      roomId = await client.createRoom(params: {
        "invite": [matrixId],
        "visibility": "private",
        "preset": "private_chat",
        "name": Request.mRequestNameSpace,
        "initial_state": getInitialState(
          requestTitle: requestTitle,
          requestBody: requestBody,
          requestId: requestId,
          client: client,
        ),
        "power_level_content_override": {
          "events": {
            Request.mRequestStateNameSpace: 0,
          },
          "users": {
            client.userID: 100,
            matrixId: 100,
          },
        },
      });
    } else if (email != null) {
      String host = client.userID.split(":")[1];
      String mxid = "@" + _emailPrefix + str2mxid(email) + ":" + host;
      roomId = await client.createRoom(params: {
        "invite": [mxid],
        "visibility": "private",
        "preset": "private_chat",
        "name": requestTitle,
        "initial_state": getInitialState(
          requestTitle: requestTitle,
          requestBody: requestBody,
          requestId: requestId,
          client: client,
        ),
      });
    }

    return roomId;
  }

  // set the organisation
  void setOrganisation(Organisation org) {
    organisation = org;
  }
}
