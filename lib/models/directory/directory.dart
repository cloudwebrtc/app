import 'dart:convert';

import 'package:famedlysdk/famedlysdk.dart';
import 'package:http/http.dart' as http;

import 'organisation.dart';
import 'scope.dart';
import 'subtype.dart';
import 'tag.dart';

/// Will give you access to the Famedly Directory.
// WARNING: DO NOT! NEVER! EVER! DEFINE THE BUILDCONTEXT ON A CLASS LEVEL! THIS BREAKS TESTS!
class Directory {
  final Client client;

  static const String mNamespace = "com.famedly.talk.directory_token";
  static const String mPassport = "com.famedly.passport";
  static const String baseUrl = "https://directory.famedly.de/api/v1";
  static const String jasonMxid = "@jason:famedly.de";
  static const String jasonRequestNamespace = "com.famedly.jwt.request";
  static const String jasonResponseNamespace = "com.famedly.jwt.response";

  http.Client httpClient = http.Client();

  Directory(this.client);

  String get token => client.accountData.containsKey(mNamespace)
      ? client.accountData[mNamespace].content["token"]
      : "";

  String get tokenTyp {
    if (token.isEmpty) return "";
    Map<String, dynamic> typeObj = json.decode(
      String.fromCharCodes(
        base64.decode(
          base64.normalize(
            token.split(".")[0],
          ),
        ),
      ),
    );
    return typeObj["typ"];
  }

  String get tokenAlg {
    if (token.isEmpty) return "";
    Map<String, dynamic> typeObj = json.decode(
      String.fromCharCodes(
        base64.decode(
          base64.normalize(
            token.split(".")[0],
          ),
        ),
      ),
    );
    return typeObj["alg"];
  }

  bool get isAdmin {
    if (token.isEmpty) return false;
    Map<String, dynamic> typeObj = json.decode(
      String.fromCharCodes(
        base64.decode(
          base64.normalize(
            token.split(".")[0],
          ),
        ),
      ),
    );
    return typeObj["admin"];
  }

  bool get tokenExpired {
    if (token.isEmpty) return true;
    Map<String, dynamic> typeObj = json.decode(
      String.fromCharCodes(
        base64.decode(
          base64.normalize(
            token.split(".")[1],
          ),
        ),
      ),
    );
    return typeObj["exp"] * 1000 < DateTime.now().millisecondsSinceEpoch;
  }

  static Map<String, Organisation> organisationsCache = {};
  static Map<String, Subtype> subtypeCache = {};
  static Map<String, Tag> tagCache = {};

  /// Searches for Organisations and also caches the found entities, subtypes and tags.
  Future<List<Organisation>> search([List<String> tags]) async {
    String query = "";

    if (tags != null) {
      for (final tag in tags) {
        if (query != "") {
          query += "&";
        }
        query += "tags=" + Uri.encodeFull(tag);
      }
    }

    if (query != "") {
      query = "?$query";
    }
    final Map<String, dynamic> resp = await _apiRequest("GET", "/search$query");
    if (resp != null && resp["organisations"] is List<dynamic>) {
      // Get the organisations and cache them.
      List<Organisation> orgaList = [];
      for (int i = 0; i < resp["organisations"].length; i++) {
        Organisation newOrga = Organisation.fromJson(resp["organisations"][i]);
        orgaList.add(newOrga);
        if (tags == null) {
          organisationsCache[newOrga.id] = newOrga;
        }
      }
      // only cache if you don't have tags set
      if (tags == null) {
        // Cache the types
        if (resp["types"] is List<dynamic>) {
          for (int i = 0; i < resp["types"].length; i++) {
            Subtype newType = Subtype.fromJson(resp["types"][i]);
            subtypeCache[newType.id] = newType;
          }
        }
        // Cache the tags
        if (resp["tags"] is List<dynamic>) {
          for (int i = 0; i < resp["tags"].length; i++) {
            Tag newTag = Tag.fromJson(resp["tags"][i]);
            tagCache[newTag.id] = newTag;
          }
        }
      }
      // Return the organisations
      return orgaList;
    }
    return null;
  }

  /// Gets a list of all Organisation entities.
  Future<List<Organisation>> getOrganisations() async {
    final List<dynamic> resp = await _apiRequest("GET", "/organisations");
    if (resp != null) {
      List<Organisation> orgaList = [];
      for (int i = 0; i < resp.length; i++) {
        orgaList.add(Organisation.fromJson(resp[i]));
      }
      return orgaList;
    }
    return null;
  }

  /// Gets the details of a single instance of a Organisation.
  Future<Organisation> getOrganisation(String id) async {
    final String idEncoded = Uri.encodeFull(id);
    final Map<String, dynamic> resp =
        await _apiRequest("GET", "/organisations/$idEncoded");
    if (resp != null) {
      return Organisation.fromJson(resp);
    }
    return null;
  }

  /// Gets the details of a single instance of a Scope.
  Future<Scope> getScope(String id) async {
    final String idEncoded = Uri.encodeFull(id);
    final Map<String, dynamic> resp =
        await _apiRequest("GET", "/scopes/$idEncoded");
    if (resp != null) {
      return Scope.fromJson(resp);
    }
    return null;
  }

  /// Gets a list of all Tag entities.
  Future<List<Tag>> getTags() async {
    final List<dynamic> resp = await _apiRequest("GET", "/tags");
    if (resp != null && resp is List<dynamic>) {
      List<Tag> tagList = [];
      for (int i = 0; i < resp.length; i++) {
        tagList.add(Tag.fromJson(resp[i]));
      }
      return tagList;
    }
    return null;
  }

  /// Gets a list of all Tag entities.
  Future<List<Subtype>> getSubtypes() async {
    final List<dynamic> resp = await _apiRequest("GET", "/types");
    if (resp != null && resp is List<dynamic>) {
      List<Subtype> subtypeList = [];
      for (int i = 0; i < resp.length; i++) {
        subtypeList.add(Subtype.fromJson(resp[i]));
      }
      return subtypeList;
    }
    return null;
  }

  /// Gets the details of a single instance of a Tag.
  Future<Tag> getTag(String id) async {
    final String idEncoded = Uri.encodeFull(id);
    final Map<String, dynamic> resp =
        await _apiRequest("GET", "/tags/$idEncoded");
    if (resp != null) {
      return Tag.fromJson(resp);
    }
    return null;
  }

  /// Gets the details of a single instance of a Tag.
  Future<Subtype> getSubtype(String id) async {
    final String idEncoded = Uri.encodeFull(id);
    final Map<String, dynamic> resp =
        await _apiRequest("GET", "/types/$idEncoded");
    if (resp != null) {
      return Subtype.fromJson(resp);
    }
    return null;
  }

  Future<dynamic> requestToken() async {
    // Is the current token still valid?
    if ((this.token?.isNotEmpty ?? false) && !this.tokenExpired)
      return this.token;

    // Start or restore direct chat with jason
    final String jasonRoomId =
        await User(jasonMxid, room: Room(id: "1234", client: client))
            .startDirectChat();
    if (jasonRoomId == null) throw Exception("No connection to Jason");

    // Send request to the direct chat with json
    final String txid = "txid${DateTime.now().millisecondsSinceEpoch}";
    final Future<EventUpdate> responseFuture = client.connection.onEvent.stream
        .firstWhere((e) => e.eventType == jasonResponseNamespace);
    await client.connection.jsonRequest(
        type: HTTPType.PUT,
        action:
            "/client/r0/rooms/${jasonRoomId}/send/$jasonRequestNamespace/$txid",
        data: {});

    // Handle response
    final EventUpdate response = await responseFuture;
    final Map<String, dynamic> responseContent = response.content["content"];
    if (responseContent.containsKey("error") ||
        !responseContent.containsKey("ok"))
      throw Exception(responseContent["error"] ?? "Forbidden");
    final String token = responseContent["ok"]["token"];

    // Store token in account data
    await client.connection.jsonRequest(
        type: HTTPType.PUT,
        action: "/client/r0/user/${client.userID}/account_data/$mNamespace",
        data: {"token": token});
    return token;
  }

  Future<dynamic> _apiRequest(String method, String endpoint,
      [dynamic body]) async {
    // Get user Language

    // This would currently break tests. Therefor it is disabled :(
    /*String countrycode = Localizations.localeOf(context).countryCode;
    String languagecode = Localizations.localeOf(context).languageCode;

    debugPrint(
        "languageheader: $languagecode-$countrycode,$languagecode;q=0.7,en;q=0.3");*/
    if (token?.isEmpty ?? false) print("[Warning] No token specified!");

    String url = baseUrl + endpoint;
    Map<String, String> headers = {
      "Authorization": "Bearer $token",
      "Accept": "application/json",
      "Accept-Encoding": "gzip, deflate, br",
      "Accept-Language": "de-DE,de;q=0.7,*;q=0.3",
    };

    http.Response resp;
    switch (method) {
      case "DELETE":
        resp = await httpClient.delete(url, headers: headers);
        break;
      case "HEAD":
        resp = await httpClient.head(url, headers: headers);
        break;
      case "PATCH":
        resp = await httpClient.patch(url, headers: headers, body: body);
        break;
      case "POST":
        resp = await httpClient.post(url, headers: headers, body: body);
        break;
      case "PUT":
        resp = await httpClient.put(url, headers: headers, body: body);
        break;
      case "GET":
      default:
        resp = await httpClient.get(url, headers: headers);
        break;
    }

    if (resp.statusCode == 200 &&
            resp.headers["Content-Type"] == "application/json" ||
        resp.headers["content-type"] == "application/json") {
      String responseJson;
      try {
        responseJson = utf8.decode(resp.bodyBytes);
      } catch (e) {
        responseJson = resp.body;
      }
      try {
        final dynamic response = json.decode(responseJson);
        return response;
      } catch (e) {
        return null;
      }
    }
    if (resp.statusCode == 401 || resp.statusCode == 403) {
      await client.connection.jsonRequest(
          type: HTTPType.PUT,
          action: "/client/r0/user/${client.userID}/account_data/$mNamespace",
          data: {"token": ""});
    }
    return null;
  }
}
