/// A location, specified by an address, as well as coordinates.
class Location {
  /// When looking at a map, latitude lines run horizontally.
  final double latitude;

  /// When looking at a map, latitude lines run vertically.
  final double longitude;

  /// Name of the street.
  final String street;

  /// House number. Not a number datatype, because some countries
  /// involve letters here.
  final String number;

  /// Zip code. Not a number datatype, because some countries involve
  /// letters here.
  final String zipCode;

  /// Name of the city.
  final String city;

  /// Name of the state.
  final String state;

  /// Name of the country.
  final String country;

  Location({
    this.latitude,
    this.longitude,
    this.street,
    this.number,
    this.zipCode,
    this.city,
    this.state,
    this.country,
  });

  Location.fromJson(Map<String, dynamic> json)
      : latitude = json['latitude'],
        longitude = json['longitude'],
        street = json['street'],
        number = json['number'],
        zipCode = json['zip_code'],
        city = json['city'],
        state = json['state'],
        country = json['country'];
}
