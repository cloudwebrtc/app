import 'package:famedly/models/directory/contact.dart';
import 'package:famedly/models/directory/location.dart';
import 'package:famedly/models/directory/visibility.dart';

/// The root of the Organisation type's schema.
class Organisation {
  /// An unique identifier for this organisation.
  final String id;

  /// The name of the Organisation.
  final String name;

  /// A location, specified by an address, as well as coordinates.
  final Location location;

  /// ID of the [Subtype] of this organisation.
  final String orgType;

  /// List of tags this organisation has got.
  final List<String> tags;

  /// Organisation IDs of Suborganisations which belong to this organisation.
  final List<String> children;

  /// Visibility of this organisation.
  final Visibility visibility;

  /// How you can contact this organisation.
  final List<Contact> contacts;

  Organisation(
      {this.id,
      this.name,
      this.location,
      this.orgType,
      this.tags,
      this.children,
      this.visibility,
      this.contacts});

  Organisation.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        location = json['location'] != null
            ? new Location.fromJson(json['location'])
            : null,
        orgType = json['org_type'],
        tags = json['tags'].cast<String>(),
        children = json['children'].cast<String>(),
        visibility = json['visibility'] != null
            ? new Visibility.fromJson(json['visibility'])
            : null,
        contacts = json['contacts'] != null
            ? Contact.listFromJson(json['contacts'])
            : [] {
      for (final contact in contacts) {
        contact.setOrganisation(this);
      }
    }
}
