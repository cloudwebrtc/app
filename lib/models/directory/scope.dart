/// Organisation admins can create scopes, which allow specified subjects to access
/// items which use this scope in their visibility. Setting that scope on a contact
/// endpoint is only allowed if the organisation has been granted access to it.
/// A scope created by an organisation is automatically available to an organisation,
/// an API for giving organisations access to more scopes is not available yet.
class Scope {
  /// An unique identifier for this subtype.
  final String id;

  /// A regular expression, which matches the IDs of those who have
  /// access to things in this scope.
  final String subjects;

  Scope({this.id, this.subjects});

  Scope.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        subjects = json['subjects'];
}
