/// This data type models types, subtypes, subsubtypes and so on of organisations.
class Subtype {
  /// An unique identifier for this subtype.
  final String id;

  /// Parent of the type, if this is a subtype of another type.
  final String parent;

  /// Description of the type.
  final String description;

  Subtype({this.id, this.parent, this.description});

  Subtype.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        parent = json['parent'],
        description = json['description'];
}
