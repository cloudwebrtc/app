/// A tag is an abstract generic thing you attach to an organisation or contact.
/// Specific usages are up to the user.
class Tag {
  /// An unique identifier for this tag.
  final String id;

  /// Description of this tag.
  final String description;

  Tag({this.id, this.description});

  Tag.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        description = json['description'];
}
