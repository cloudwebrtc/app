/// Organisations and contacts can have visibilities set. If the
/// visibility set on an entity does not allow the client to see it, it is
/// excluded from the response. To check whether a client is allowedto access
/// a resource, the scopes from the JWT are compared with thevisibilities
/// set on the resource. Basically, a client is allowed to access aresource
/// when one of their scopes matches one in include,but not in exlude.
class Visibility {
  /// A list of scopes that are allowed to access the entity this visibility is set on.
  /// This can be a wildcard scope, which would be "*", that matches every scope.
  final List<String> include;

  /// A list of scopes that are not allowed to access the entity
  /// this visibility is set on, even when include contains a wildcard scope.
  final List<String> exclude;

  Visibility({this.include, this.exclude});

  Visibility.fromJson(Map<String, dynamic> json)
      : include = json['include'].cast<String>(),
        exclude = json['exclude'].cast<String>();
}
