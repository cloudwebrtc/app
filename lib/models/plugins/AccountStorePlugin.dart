import 'dart:convert';

import 'package:crypted_preferences/crypted_preferences.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/models/plugins/MatrixPlugin.dart';
import 'package:famedlysdk/famedlysdk.dart';

class AccountStorePlugin extends MatrixPlugin {
  final String clientName;
  AccountStorePlugin(MatrixState matrix, this.clientName) : super(matrix);

  /// Used to load the old account if there is no store available.
  void loadAccount() async {
    final preferences = await Preferences.preferences(path: './preferences.db');
    final credentialsStr = preferences.getString(clientName);
    if (credentialsStr == null || credentialsStr.isEmpty) {
      client.connection.onLoginStateChanged.add(LoginState.loggedOut);
      return;
    }
    print("[Matrix] Restoring account credentials from crypted preferences");
    final Map<String, dynamic> credentials = json.decode(credentialsStr);
    client.connection.connect(
      newDeviceID: credentials["deviceID"],
      newDeviceName: credentials["deviceName"],
      newHomeserver: credentials["homeserver"],
      newLazyLoadMembers: credentials["lazyLoadMembers"],
      //newMatrixVersions: credentials["matrixVersions"], // FIXME: wrong List type
      newToken: credentials["token"],
      newUserID: credentials["userID"],
    );
  }

  /// Used to save the current account persistently if there is no store available.
  void saveAccount() async {
    print("[Matrix] Save account credentials in crypted preferences");
    final Map<String, dynamic> credentials = {
      "deviceID": client.deviceID,
      "deviceName": client.deviceName,
      "homeserver": client.homeserver,
      "lazyLoadMembers": client.lazyLoadMembers,
      "matrixVersions": client.matrixVersions,
      "token": client.accessToken,
      "userID": client.userID,
    };
    final preferences = await Preferences.preferences(path: './preferences.db');
    preferences.setString(clientName, json.encode(credentials));
  }
}
