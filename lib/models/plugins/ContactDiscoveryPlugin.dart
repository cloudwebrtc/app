import 'package:famedly/components/Matrix.dart';
import 'package:famedly/models/directory/directory.dart';
import 'package:famedly/models/plugins/MatrixPlugin.dart';
import 'package:famedlysdk/famedlysdk.dart';

class ContactDiscoveryPlugin extends MatrixPlugin {
  ContactDiscoveryPlugin(MatrixState matrix) : super(matrix);

  String get alias =>
      "#famedly-contact-discovery:${client.isLogged() ? client.userID.split(":")[1] : ''}";

  Future<List<User>> loadContacts({String roomId = null}) async {
    List<User> list;
    Room contactDiscoveryRoom = matrix.client.roomList.getRoomByAlias(alias);
    if (contactDiscoveryRoom != null)
      list = await contactDiscoveryRoom.requestParticipants();
    else
      list = await matrix.client.loadFamedlyContacts();
    list.removeWhere((u) =>
        u.id == matrix.client.userID ||
        u.id == Directory.jasonMxid ||
        u.id.split(":")[1] != matrix.client.userID.split(":")[1]);
    if (roomId != null) {
      final Room room = matrix.client.getRoomById(roomId);
      List<User> userBlackList = await room.requestParticipants();
      list.removeWhere((u) =>
          userBlackList.indexWhere((l) => l.stateKey == u.stateKey) != -1);
    }
    return list;
  }
}
