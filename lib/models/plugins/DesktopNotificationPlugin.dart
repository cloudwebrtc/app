import 'dart:async';

import 'package:famedly/components/Matrix.dart';
import 'package:famedly/models/plugins/MatrixPlugin.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:universal_html/prefer_universal/html.dart' as darthtml;

class DesktopNotificationPlugin extends MatrixPlugin {
  DesktopNotificationPlugin(MatrixState matrix) : super(matrix) {
    if (kIsWeb) {
      onBrowserWindowFocus ??=
          darthtml.window.onFocus.listen((e) => hasFocus = true);
      onBrowserWindowBlur ??=
          darthtml.window.onBlur.listen((e) => hasFocus = false);
      registerDesktopNotifications();
    }
  }

  /// When this is in web, this returns whether the browser tab has focus.
  bool hasFocus = true;

  StreamSubscription<EventUpdate> onLocalNotification;
  StreamSubscription<darthtml.Event> onBrowserWindowFocus;
  StreamSubscription<darthtml.Event> onBrowserWindowBlur;

  void registerDesktopNotifications() async {
    await client.connection.onFirstSync.stream.first;
    if (darthtml.Notification.permission != "granted")
      await darthtml.Notification.requestPermission();
    onLocalNotification ??= client.connection.onEvent.stream
        .where((r) =>
            r.type == "timeline" && r.content["type"] == "m.room.message")
        .listen(sendDesktopNotification);
  }

  void sendDesktopNotification(EventUpdate eventUpdate) async {
    if (client.prevBatch == null ||
        client.prevBatch == "" ||
        (eventUpdate.roomID == matrix.activeRoomID &&
            matrix.desktopNotificationPlugin.hasFocus) ||
        darthtml.Notification.permission == "denied") return;
    final Room room = client.getRoomById(eventUpdate.roomID);
    if (room.notificationCount == 0) return;
    final Event event = Event.fromJson(eventUpdate.content, room);
    final String body = room.displayname == event.sender.calcDisplayname()
        ? event.getBody()
        : "${event.sender.calcDisplayname()}: ${event.getBody()}";
    final darthtml.Notification notification = darthtml.Notification(
      room.displayname,
      body: body,
      icon: event.sender.avatarUrl?.getThumbnail(client,
              width: 64, height: 64, method: ThumbnailMethod.crop) ??
          room.avatar?.getThumbnail(client,
              width: 64, height: 64, method: ThumbnailMethod.crop),
    );
    notification.onClick.listen((e) => matrix.roomsPlugin.enterRoom(room.id));
  }
}
