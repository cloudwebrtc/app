import 'dart:async';

import 'package:crypted_preferences/crypted_preferences.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/config/TestUser.dart';
import 'package:famedly/models/plugins/MatrixPlugin.dart';
import 'package:famedly/models/tasks/tasklist.dart';
import 'package:famedlysdk/famedlysdk.dart';

class LoginPlugin extends MatrixPlugin {
  final String clientName;
  LoginState currentLoginState;
  bool get isIntegrationTest =>
      client.homeserver?.contains(TestUser.homeserver) ?? false;
  LoginPlugin(MatrixState matrix, this.clientName) : super(matrix) {
    onLogin ??= client.connection.onLoginStateChanged.stream
        .listen(handleLoginStateChanged);
  }

  void handleLoginStateChanged(LoginState state) {
    currentLoginState = state;
  }

  StreamSubscription<LoginState> onLogin;

  void clean() async {
    print("[Matrix] Clear session...");
    final preferences = await Preferences.preferences(path: './preferences.db');
    preferences.setString(clientName, "");
  }

  Future<void> resetUser() async {
    print("[Integration Test] Testuser detected! Reset all rooms!");
    await client.connection.onFirstSync.stream.first;
    for (int i = 0; i < client.roomList.rooms.length; i++) {
      if (client.roomList.rooms[i].canonicalAlias
              .startsWith("#famedlyContactDiscovery") ||
          client.roomList.rooms[i].canonicalAlias ==
              matrix.contactDiscoveryPlugin.alias) continue;
      await client.roomList.rooms[i].leave();
      await client.roomList.rooms[i].forget();
    }
    RoomList archive = RoomList(rooms: await client.archive, client: client);
    for (int i = 0; i < archive.rooms.length; i++) {
      await archive.rooms[i].forget();
    }
    await client.connection.jsonRequest(
        type: HTTPType.PUT,
        action:
            "/client/r0/user/${client.userID}/account_data/${TaskList.mNamespace}",
        data: {});
  }
}
