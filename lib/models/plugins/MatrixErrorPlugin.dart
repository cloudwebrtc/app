import 'dart:async';

import 'package:famedly/components/Matrix.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/models/plugins/MatrixPlugin.dart';
import 'package:famedlysdk/famedlysdk.dart';

class MatrixErrorPlugin extends MatrixPlugin {
  MatrixErrorPlugin(MatrixState matrix) : super(matrix) {
    errorSub ??= client.connection.onError.stream.listen(handleError);
  }

  StreamSubscription<MatrixException> errorSub;

  FamedlyLocalizations get localizations => matrix.localizations;

  void showError(String error, {String title}) => matrix.context != null
      ? matrix.widget.onGlobalError(
          title ?? localizations.oopsSomethingWentWrong, error, matrix.context)
      : null;

  void showInfo(String info) =>
      matrix.widget.onGlobalError("", info, matrix.context);

  void handleError(MatrixException error) {
    if (client.homeserver == "https://example.com") return;
    String errorMsg;
    print(error.errcode);
    print(error.error);
    switch (error.errcode) {
      case "NO_RESPONSE":
        errorMsg = localizations.organisationServerIsNotResponding;
        break;
      case "NO_SUPPORT":
        errorMsg = localizations.organisationServerIsNotSupported;
        break;
      case "M_LIMIT_EXCEEDED":
        errorMsg = localizations.tooManyRequests;
        break;
      case "NO_CONNECTION":
        errorMsg = localizations.organisationServerIsNotResponding;
        break;
      case "M_FORBIDDEN":
        if (error.errorMessage
            .startsWith("You don't have permission to post that to the room."))
          errorMsg = localizations.noPermissionForThisAction;
        else if (error.errorMessage.startsWith("You cannot kick user"))
          errorMsg = localizations.noPermissionForThisAction;
        else
          errorMsg = localizations.invalidUsernameOrPassword;
        break;
      case "M_UNSUPPORTED_ROOM_VERSION":
        errorMsg = localizations.unsupportedRoomVersion;
        break;
      default:
        errorMsg =
            error.errorMessage.isEmpty ? error.errcode : error.errorMessage;
        break;
    }
    showError(errorMsg);
  }
}
