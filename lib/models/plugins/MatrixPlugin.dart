import 'package:famedly/components/Matrix.dart';
import 'package:famedlysdk/famedlysdk.dart';

abstract class MatrixPlugin {
  final MatrixState matrix;
  Client get client => matrix.client;

  const MatrixPlugin(this.matrix);
}
