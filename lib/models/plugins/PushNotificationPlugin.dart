import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/models/plugins/MatrixPlugin.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:globbing/globbing.dart';
import 'package:path_provider/path_provider.dart';

class PushNotificationPlugin extends MatrixPlugin {
  PushNotificationPlugin(MatrixState matrix) : super(matrix) {
    onLogin ??= client.connection.onLoginStateChanged.stream
        .listen(handleLoginStateChanged);
  }

  void handleLoginStateChanged(LoginState state) {
    // We need to disable it in debug mode to make sure integrationtests work
    if (state == LoginState.logged &&
        !matrix.loginPlugin.isIntegrationTest &&
        !kIsWeb) {
      setupNotifications();
    } else if (state == LoginState.loggedOut &&
        !matrix.loginPlugin.isIntegrationTest &&
        !kIsWeb) {
      _firebaseToken = "";
    }
  }

  StreamSubscription<LoginState> onLogin;

  static var httpClient = new HttpClient();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  String _firebaseToken = "";

  void setupNotifications() async {
    print("[Push] Setup push notifications");
    _firebaseMessaging.setAutoInitEnabled(true);
    _firebaseMessaging.requestNotificationPermissions();

    // Init notifications framework
    var initializationSettingsAndroid =
        AndroidInitializationSettings('notifications_icon');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    _flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: _onSelectNotification);

    // Handle firebase messages
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("[Push] Handle onMessage");
        print(message);
        _handleOnMessage(message);
      },
      onResume: _onSelectSystemTrayNotification,
      onLaunch: _onSelectSystemTrayNotification,
    );

    // Register App to pushserver when we got a firebase token
    final String token = await _firebaseMessaging.getToken();

    assert(token != null);
    print("[Push] Firebase token registered!");
    if (_firebaseToken == "") {
      _firebaseToken = token;
      print("[Push] _firebaseToken == ''");

      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      String device_display_name;
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        device_display_name = androidInfo.device;
      } else if (Platform.isIOS) {
        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        device_display_name = iosInfo.utsname.nodename;
      } else {
        device_display_name = "Famedly Desktop";
      }

      SetPushersRequest data = SetPushersRequest(
        app_id: "com.famedly.app",
        device_display_name: device_display_name,
        data: PusherData(
          url: "https://janian.de:7022",
          format: "event_id_only",
        ),
        pushkey: token,
        kind: "http",
        app_display_name: "Famedly",
        lang: "de-DE",
      );
      print("[Push] Setup now!");
      client.setPushers(data);
    }
  }

  Future<void> _handleOnMessage(Map<String, dynamic> message) async {
    // Notification data and matrix data
    Map<dynamic, dynamic> data = message['data'] ?? message;
    String eventID = data["event_id"];
    String roomID = data["room_id"];
    if (roomID == null || eventID == null || roomID == matrix.activeRoomID)
      return;
    Room room = client.getRoomById(roomID) ??
        Room(id: roomID, client: client, membership: Membership.join);
    List<User> room_members = room.getParticipants();
    Event event = await room.getEventById(eventID);
    if (event == null) {
      if (room.membership == Membership.invite) {
        event = room.getState("m.room.member", client.userID)?.timelineEvent;
        if (event == null) return;
      } else
        return;
    }

    final String body = event.typeKey == "m.room.message"
        ? event.getBody()
        : (event.typeKey == "m.room.member"
            ? "Du wurdest zu einer Unterhaltung eingeladen"
            : event.typeKey);

    // Defaults used for notification
    Priority priority = Priority.Default;

    // Pushrule parsing (priorized by first with less priority and last parsing has a higher prio
    PushrulesResponse pushrules = await client.getPushrules();
    if (pushrules != null) {
      List<List<dynamic>> underrideRules = pushrules.global.underride
          .where((r) => _filterPushRule(room_members.length, event, r))
          .map((r) => r.actions)
          .toList();

      if (underrideRules.isNotEmpty) {
        Priority priorityL = _filterActions(underrideRules);
        if (priorityL != null) {
          priority = priorityL;
        } else {
          return;
        }
      }

      List<List<dynamic>> senderRules = pushrules.global.sender
          .where((r) => _filterPushRule(room_members.length, event, r))
          .map((r) => r.actions)
          .toList();

      if (senderRules.isNotEmpty) {
        Priority priorityL = _filterActions(senderRules);
        if (priorityL != null) {
          priority = priorityL;
        } else {
          return;
        }
      }

      List<List<dynamic>> roomRules = pushrules.global.room
          .where((r) => _filterPushRule(room_members.length, event, r))
          .map((r) => r.actions)
          .toList();

      if (roomRules.isNotEmpty) {
        Priority priorityL = _filterActions(roomRules);
        if (priorityL != null) {
          priority = priorityL;
        } else {
          return;
        }
      }

      // These are the content override
      List<List<dynamic>> keywordRules = pushrules.global.content
          .where((r) => _filterPushRule(room_members.length, event, r))
          .map((r) => r.actions)
          .toList();
      if (keywordRules.isNotEmpty) {
        Priority priorityL = _filterActions(keywordRules);
        if (priorityL != null) {
          priority = priorityL;
        } else {
          return;
        }
      }

      // These are the userdefined override
      List<List<dynamic>> userRules = pushrules.global.override
          .where((r) => _filterPushRule(room_members.length, event, r))
          .map((r) => r.actions)
          .toList();
      if (userRules.isNotEmpty) {
        Priority priorityL = _filterActions(userRules);
        if (priorityL != null) {
          priority = priorityL;
        } else {
          return;
        }
      }
    }

    List<Map<String, dynamic>> notifications_raw =
        await client.store.getNotificationByRoom(room.id);

    int id_old;

    List<String> text_messages = [];
    List<Message> notifications = [];
    bool group = false;
    Person me = Person(key: client.userID, name: client.userID);
    ;
    Person author;

    if (!Platform.isIOS) {
      // Compose the notification data
      User ownUser =
          await client.store.getUser(matrixID: client.userID, room: room);

      if (ownUser.avatarUrl.mxc != null && ownUser.avatarUrl.mxc != "") {
        MxContent ownAvatarMXC = ownUser.avatarUrl;
        String ownUserDownloadPath = await _downloadAndSaveImage(
            ownAvatarMXC.getDownloadLink(this.client), "ownAvatar");

        me = Person(
            key: ownUser.id,
            name: ownUser.calcDisplayname(),
            icon: ownUserDownloadPath,
            iconSource: IconSource.FilePath);
      } else if (ownUser.id != null && ownUser.id != "") {
        me = Person(key: ownUser.id, name: ownUser.calcDisplayname());
      }

      if (event?.sender?.id != null &&
          event.sender.id != "" &&
          event.sender?.avatarUrl?.mxc != null &&
          event.sender?.avatarUrl?.mxc != "") {
        MxContent senderAvatarMXC = event.sender.avatarUrl;
        String senderAvatarDownloadPath = await _downloadAndSaveImage(
            senderAvatarMXC.getDownloadLink(this.client), "${event.sender.id}");

        author = Person(
            key: event.sender.id,
            name: event.sender.calcDisplayname(),
            icon: senderAvatarDownloadPath,
            iconSource: IconSource.FilePath);
      } else if (event.sender == null) {
        // BROKEN EVENT! Ignore!
        print("BROKEN EVENT!");
        return;
      } else {
        author = Person(
            key: event.sender?.id, name: event.sender?.calcDisplayname());
      }

      if (notifications_raw != null) {
        for (Map<String, dynamic> notification in notifications_raw) {
          id_old = notification["id"];
          Room room = await client.roomList
              .getRoomById(notification["chat_id"] as String);
          Event event =
              await room.getEventById(notification["event_id"] as String);
          Person author_db;
          if (event?.sender != null &&
              event?.sender?.avatarUrl?.mxc != null &&
              event?.sender?.avatarUrl?.mxc != "") {
            MxContent senderAvatarMXC = event.sender.avatarUrl;
            String senderAvatarDownloadPath = await _downloadAndSaveImage(
                senderAvatarMXC.getDownloadLink(this.client),
                "${event.sender.id}");

            author_db = Person(
                key: event.sender.id,
                name: event.sender.calcDisplayname(),
                icon: senderAvatarDownloadPath,
                iconSource: IconSource.FilePath);
          } else if (event.sender == null) {
            // BROKEN EVENT! Ignore!
            print("BROKEN EVENT!");
            return;
          } else {
            author_db = Person(
                key: event.sender.id, name: event.sender.calcDisplayname());
          }
          notifications.add(Message(body, event.time.dateTime, author_db));
        }
      }

      Message newest_message = Message(
        body,
        event.time.dateTime,
        author,
      );

      notifications.add(newest_message);
      if (room_members.length > 2) {
        group = true;
      }
    } else {
      if (notifications_raw != null) {
        for (Map<String, dynamic> notification in notifications_raw) {
          id_old = notification["id"];
          Room room = await client.roomList
              .getRoomById(notification["chat_id"] as String);
          Event event =
              await room.getEventById(notification["event_id"] as String);
          if (event != null) {
            String addBody = body;
            if (event.typeKey == "m.room.message")
              addBody = "${event.sender.calcDisplayname()}: ${body}";
            text_messages.add(addBody);
          }
        }
      }
      if (event != null) {
        String addBody = body;
        if (event.typeKey == "m.room.message")
          addBody = "${event.sender.calcDisplayname()}: ${body}";
        text_messages.add(addBody);
      }
    }

    int id_new = DateTime.now().millisecondsSinceEpoch.toSigned(32);
    client.store.addNotification(room.id, event.eventId, id_new);
    String messages_string = text_messages.join("\n");

    if (!Platform.isIOS) {
      await _showNotification(
        room.displayname,
        room.id,
        id_new,
        id_old,
        me: me,
        messages: notifications,
        priority: priority,
        group: group,
      );
    } else {
      await _showNotification(
        room.displayname,
        room.id,
        id_new,
        id_old,
        messages_string: messages_string,
      );
    }
  }

  Future<String> _downloadAndSaveImage(String url, String filename) async {
    String dir = (await getTemporaryDirectory()).path;
    String folder = '$dir/images/';
    File file = new File('$folder$filename');

    // If we already have the avatar don't bother downloading it
    // FIXME will not update when avatar gets changed!
    if (!file.existsSync()) {
      var request = await httpClient.getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      await Directory(folder).create();
      await file.writeAsBytes(bytes);
    }

    return file.path;
  }

  Priority _filterActions(List<List<dynamic>> actions) {
    Priority priority = Priority.Default;
    for (dynamic action in actions) {
      if (action is String) {
        if (action == "dont_notify") {
          // Don't post any notification
          return null;
        }
      } else if (action is Map<String, String>) {
        if (action["set_tweak"] == "sound") {
          // Do nothing
        } else if (action["set_tweak"] == "highlight") {
          if (action["value"] == "true") {
            priority = Priority.High;
          } else if (action["value"] != "true") {
            priority = Priority.High;
          }
        }
      }
    }
    return priority;
  }

  bool _filterPushRule(int member_count, Event event, PushRule rule) {
    if (!rule.enabled) return false;
    if (rule == null) return false;
    if (rule.conditions == null) return false;
    for (Condition condition in rule.conditions) {
      if (condition.kind == "event_match") {
        if (condition.key == "room_id") {
          if (rule.pattern != null) {
            return Glob(rule.pattern, caseSensitive: false).match(event.text);
          }
        } else if (condition.key == "type") {
          if (rule.pattern != null) {
            return Glob(rule.pattern, caseSensitive: false)
                .match(event.typeKey);
          }
        }
      } else if (condition.kind == "room_member_count") {
        if (condition.conditionIs == member_count) return true;
      }
    }
    return false;
  }

  Future _showNotification(
    String roomName,
    String roomID,
    int id_new,
    int id_old, {
    Person me,
    List<Message> messages,
    bool group,
    Priority priority,
    String messages_string,
  }) async {
    NotificationDetails platformChannelSpecifics;

    if (!Platform.isIOS) {
      MessagingStyleInformation messagingStyle = MessagingStyleInformation(
        me,
        groupConversation: group,
        conversationTitle: roomName,
        htmlFormatContent: true,
        htmlFormatTitle: true,
        messages: messages,
      );

      AndroidNotificationDetails androidPlatformChannelSpecifics;

      if (priority == Priority.High) {
        androidPlatformChannelSpecifics = AndroidNotificationDetails(
          'com.famedly.mention',
          'Mention Message',
          'Messages with Mentions in them',
          importance: Importance.High,
          priority: Priority.High,
          style: AndroidNotificationStyle.Messaging,
          styleInformation: messagingStyle,
        );
      } else {
        androidPlatformChannelSpecifics = AndroidNotificationDetails(
          'com.famedly.message',
          'Normal Message',
          'Any regular Message',
          style: AndroidNotificationStyle.Messaging,
          styleInformation: messagingStyle,
        );
      }

      platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics,
        null,
      );
    } else {
      platformChannelSpecifics = NotificationDetails(
        null,
        IOSNotificationDetails(),
      );
    }

    if (id_old != null) {
      await _flutterLocalNotificationsPlugin.cancel(id_old);
    }
    await _flutterLocalNotificationsPlugin.show(
      id_new, // Needs to be 32bit
      roomName,
      messages_string ?? "Neue Nachricht",
      platformChannelSpecifics,
      payload: roomID,
    );
  }

  Future<dynamic> _onSelectSystemTrayNotification(
      Map<String, dynamic> message) async {
    if (message["data"] is Map && message["data"]["room_id"] is String)
      _onSelectNotification(message["data"]["room_id"]);
  }

  Future _onSelectNotification(String payload) async {
    client.store.forgetNotification(payload);
    matrix.roomsPlugin.enterRoom(payload);
  }
}
