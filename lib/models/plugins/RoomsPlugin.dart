import 'package:famedly/components/Matrix.dart';
import 'package:famedly/models/plugins/MatrixPlugin.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

class RoomsPlugin extends MatrixPlugin {
  RoomsPlugin(MatrixState matrix) : super(matrix);

  OnLeaveRoomCB onLeaveRoom;

  /// Join this room and wait for a sync that this room has been created.
  Future<bool> joinRoomAndWait(BuildContext context, Room item) async {
    if (item.membership == Membership.invite) {
      final bool isRequestRoom = item.displayname == Request.mRequestNameSpace;
      CustomDialogs dialogs = CustomDialogs(context);
      dialogs.showLoadingDialog();
      Future<RoomUpdate> update = Matrix.of(context)
          .client
          .connection
          .onRoomUpdate
          .stream
          .where((RoomUpdate update) =>
              update.id == item.id && update.membership == Membership.join)
          .first;
      bool waitForBroadcast = false;
      Future<EventUpdate> directoryUpdate = Matrix.of(context)
          .client
          .connection
          .onEvent
          .stream
          .where((EventUpdate update) {
        if (update.eventType == Request.mRequestNameSpace &&
            update.content["content"] != null &&
            update.content["content"]["requested_organisation"] ==
                "Broadcast" &&
            update.content["content"]["contact_description"] == "Broadcast" &&
            update.content["content"]["organisation_id"] == "" &&
            update.content["content"]["contact_id"] == "") {
          waitForBroadcast = true;
        }
        return update.roomID == item.id &&
            update.eventType == Request.mRequestNameSpace;
      }).first;
      Future<EventUpdate> broadcastUpdate = Matrix.of(context)
          .client
          .connection
          .onEvent
          .stream
          .where((EventUpdate update) =>
              update.roomID == item.id &&
              update.eventType == Request.mBroadcastNameSpace)
          .first;
      final success = await dialogs.tryRequestWithErrorSnackbar(item.join());
      if (success == false) return false;
      await update;
      if (isRequestRoom) {
        await directoryUpdate;
        if (waitForBroadcast) {
          await broadcastUpdate;
        }
      }
      if (dialogs.hideLoadingDialog()) return false;
    }
    return true;
  }

  /// Enters a room and joins the room when it is an invite room.
  Future enterRoom(String roomID) async {
    print("[RoomsPlugin] Enter room $roomID");
    if (roomID != matrix.activeRoomID && matrix.context != null) {
      final Room room = client.getRoomById(roomID);
      if (room.membership == Membership.invite) {
        await Matrix.of(matrix.context)
            .roomsPlugin
            .joinRoomAndWait(matrix.context, room);
      }
      Navigator.of(matrix.context)
          ?.pushNamedAndRemoveUntil("/room/$roomID", (route) => route.isFirst);
    }
  }
}

typedef OnLeaveRoomCB = void Function(String roomID);
