class RequestContent {
  final String creator;
  final String requestingOrganisation;
  final String requestedOrganisation;
  final String requestTitle;
  final String requestId;
  final String organisationId;
  final String organisationName;
  final String contactId;
  final String contactDescription;
  final String msgtype;
  final String body;
  final String format;
  final String formattedBody;

  RequestContent(
      {this.creator,
      this.requestingOrganisation,
      this.requestedOrganisation,
      this.requestTitle,
      this.requestId,
      this.organisationId,
      this.organisationName,
      this.contactId,
      this.contactDescription,
      this.msgtype,
      this.body,
      this.format,
      this.formattedBody});

  RequestContent.fromJson(Map<String, dynamic> json)
      : creator = json['creator'],
        requestingOrganisation = json['requesting_organisation'],
        requestedOrganisation = json['requested_organisation'],
        requestTitle = json['request_title'],
        requestId = json['request_id'],
        organisationId = json['organisation_id'],
        organisationName = json['organisation_name'],
        contactId = json['contact_id'],
        contactDescription = json['contact_description'],
        msgtype = json['msgtype'],
        body = json['body'],
        format = json['format'],
        formattedBody = json['formatted_body'];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['creator'] = this.creator;
    data['requesting_organisation'] = this.requestingOrganisation;
    data['requested_organisation'] = this.requestedOrganisation;
    data['request_title'] = this.requestTitle;
    data['request_id'] = this.requestId;
    data['organisation_id'] = this.organisationId;
    data['organisation_name'] = this.organisationName;
    data['contact_id'] = this.contactId;
    data['contact_description'] = this.contactDescription;
    data['msgtype'] = this.msgtype;
    data['body'] = this.body;
    data['format'] = this.format;
    data['formatted_body'] = this.formattedBody;
    return data;
  }
}
