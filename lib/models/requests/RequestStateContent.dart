class RequestStateElementContent {
  bool accept;
  bool reject;
  bool needInfo;

  RequestStateElementContent(
      {this.accept,
      this.reject,
      this.needInfo});

  RequestStateElementContent.fromJson(Map<String, dynamic> json)
      : accept = json['accept'] ?? false,
        reject = json['reject'] ?? false,
        needInfo = json['need_info'] ?? false;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accept'] = this.accept;
    data['reject'] = this.reject;
    data['need_info'] = this.needInfo;
    return data;
  }
}

class RequestStateContent {
  RequestStateElementContent requesting;
  RequestStateElementContent requested;
  bool roomHandoff;

  RequestStateContent(
      {this.requesting,
      this.requested,
      this.roomHandoff});

  RequestStateContent.fromJson(Map<String, dynamic> json)
      : requesting = RequestStateElementContent.fromJson(json['requesting']),
        requested = RequestStateElementContent.fromJson(json['requested']),
        roomHandoff = json['room_handoff'] ?? false;

  RequestStateContent.newBlank()
      : requesting = RequestStateElementContent(accept: false, reject: false, needInfo: false),
        requested = RequestStateElementContent(accept: false, reject: false, needInfo: false),
        roomHandoff = false;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['requesting'] = this.requesting.toJson();
    data['requested'] = this.requested.toJson();
    data['room_handoff'] = this.roomHandoff;
    return data;
  }
}
