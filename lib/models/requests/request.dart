import 'package:famedlysdk/famedlysdk.dart';
import 'package:famedly/models/requests/BroadcastContent.dart';
import 'package:famedly/models/requests/RequestContent.dart';
import 'package:famedly/models/requests/RequestStateContent.dart';
import 'package:famedly/models/directory/directory.dart';
import 'package:famedly/models/directory/contact.dart';

class RequestBundle {
  String id;
  List<Room> rooms;
  Room broadcastRoom;
  
  RequestBundle({
    this.id = null,
    this.rooms}) {
      if (this.rooms == null) {
        this.rooms = [];
      }
    }
}

class Request {
  static const String mRequestNameSpace = "com.famedly.app.request";
  static const String mBroadcastNameSpace = "com.famedly.app.broadcast";
  static const String mRequestStateNameSpace = "com.famedly.app.request_state";
  static const String mUsersNameSpace = "com.famedly.app.users";
  static bool isRequestRoom(Room room) =>
      room.states.containsKey(Request.mRequestNameSpace) ||
      room.name == Request.mRequestNameSpace;

  final Room room;
  bool isRequest;
  bool requesting;
  RequestContent content;
  BroadcastContent broadcast;
  BroadcastContactsContent contact;
  RequestStateContent state;

  Request(this.room, {this.contact, String contactId}) {
    final contentState = room.states[Request.mRequestNameSpace];
    if (contentState == null) {
      // we are likely an invite, so let's assume that as good as possible
      state = RequestStateContent.newBlank();
      requesting = false;
      isRequest = false;
      return;
    }
    isRequest = true;
    content = RequestContent.fromJson(contentState.content);
    requesting = isAuthor();

    final broadcastState = room.states[Request.mBroadcastNameSpace];
    if (broadcastState != null) {
      broadcast = BroadcastContent.fromJson(broadcastState.content);
      if (contact == null && contactId != null) {
        for (final c in broadcast.contacts) {
          if (c.contactId == contactId) {
            contact = c;
            break;
          }
        }
      }
    }

    final stateState = room.getState(Request.mRequestStateNameSpace, broadcast != null && contact != null ? contact.contactId : "");
    if (stateState == null) {
      state = RequestStateContent.newBlank();
    } else {
      state = RequestStateContent.fromJson(stateState.content);
    }
  }

  String get authorId {
    if (!isRequest) {
      return room.states[room.client.userID] != null ? room.states[room.client.userID].sender.id : "";
    }
    if (content.creator != null && content.creator != "") {
      return content.creator;
    }
    return room.states[Request.mRequestNameSpace].sender.id;
  }

  User get author {
    return room.states[authorId]?.asUser ?? User(authorId);
  }

  bool isAuthor([String userId]) {
    if (userId == null) {
      userId = room.client.userID;
    }
    return authorId == userId;
  }

  String get title {
    if (!isRequest || content.requestTitle == null || content.requestTitle == "") {
      return author.calcDisplayname();
    }
    return content.requestTitle;
  }

  String get organisation {
    if (!isRequest) {
      return null;
    }
    return contact == null ? content.organisationName : contact.organisationName;
  }

  String get description {
    if (!isRequest) {
      return null;
    }
    return contact == null ? content.contactDescription : contact.contactDescription;
  }

  Future<String> accept() async {
    if (requesting) {
      state.requesting.accept = true;
      state.requesting.reject = false;
      state.requesting.needInfo = false;
    } else {
      state.requested.accept = true;
      state.requested.reject = false;
      state.requested.needInfo = false;
    }
    return await sendState();
  }

  Future<String> reject() async {
    if (requesting) {
      state.requesting.accept = false;
      state.requesting.reject = true;
      state.requesting.needInfo = false;
    } else {
      state.requested.accept = false;
      state.requested.reject = true;
      state.requested.needInfo = false;
    }
    return await sendState(false);
  }

  Future<String> needInfo() async {
    if (requesting) {
      state.requesting.accept = false;
      state.requesting.reject = false;
      state.requesting.needInfo = true;
    } else {
      state.requested.accept = false;
      state.requested.reject = false;
      state.requested.needInfo = true;
    }
    return await sendState();
  }

  Future<String> sendState([bool createRoom = true]) async {
    if (contact == null || !createRoom) {
      String path = "/client/r0/rooms/" + room.id + "/state/" + Request.mRequestStateNameSpace;
      if (contact != null) {
        path += "/${contact.contactId}";
      }
      await room.client.connection.jsonRequest(
        type: HTTPType.PUT,
        action: path,
        data: state.toJson());
      return room.id;
    } else {
      // first we set the room handoff on the broadcast state
      state.roomHandoff = true;
      // we need to create a new room and stuffs
      Set<String> invites = new Set<String>();
      Map<String, int> userPermissions = {
        room.client.userID: 100,
      };
      invites.add(content.creator);
      userPermissions[content.creator] = 100;
      if (contact.matrixId != null) {
        invites.add(contact.matrixId);
        userPermissions[contact.matrixId] = 100;
      }
      invites.remove(room.client.userID);
      final String roomId = await room.client.createRoom(params: {
        "invite": invites.toList(),
        "visibility": "private",
        "preset": "private_chat",
        "name": Request.mRequestNameSpace,
        "initial_state": [
          {
            "type": Request.mRequestNameSpace,
            "content": RequestContent(
              creator: content.creator,
              requestedOrganisation: contact.requestedOrganisation,
              requestingOrganisation: content.requestingOrganisation,
              requestTitle: broadcast.title,
              requestId: content.requestId,
              msgtype: "m.text",
              body: content.body,
              organisationId: contact.organisationId,
              organisationName: contact.organisationName,
              contactId: contact.contactId,
              contactDescription: contact.contactDescription,
            ).toJson(),
          },
          {
            "type": "m.room.history_visibility",
            "content": {"history_visibility": "shared"}
          },
          {
            "type": "m.room.guest_access",
            "content": {"guest_access": "forbidden"}
          },
          {
            "type": Request.mRequestStateNameSpace,
            "content": state.toJson(),
          }
        ],
        "power_level_content_override": {
          "events": {
            Request.mRequestStateNameSpace: 0,
          },
          "users": userPermissions,
        },
      });
      if (roomId == null) {
        throw ("Room Handoff failed.");
      }
      await sendState(false); // send once the state in the broadcast room
      return roomId;
    }
  }

  bool remoteAccepted() {
    return requesting ? state.requested.accept : state.requesting.accept;
  }

  bool remoteRejected() {
    return requesting ? state.requested.reject : state.requesting.reject;
  }

  bool remoteNeedInfo() {
    return requesting ? state.requested.needInfo : state.requesting.needInfo;
  }

  bool youAccepted() {
    return requesting ? state.requesting.accept : state.requested.accept;
  }

  bool youRejected() {
    return requesting ? state.requesting.reject : state.requested.reject;
  }

  bool youNeedInfo() {
    return requesting ? state.requesting.needInfo : state.requested.needInfo;
  }

  static List<BroadcastContactsContent> getBroadcastContacts(Room room, String userId) {
    final req = Request(room);
    if (req.isAuthor()) return null;
    List<BroadcastContactsContent> ret = [];
    for (final contact in req.broadcast.contacts) {
      // check the URI
      if (contact.matrixId == userId || (room.getState(Request.mUsersNameSpace, contact.matrixId) != null && room.getState(Request.mUsersNameSpace, contact.matrixId).content["users"].contains(userId))) {
        final requestState = room.getState(Request.mRequestStateNameSpace, contact.contactId);
        final state = requestState == null ? RequestStateContent.newBlank() : RequestStateContent.fromJson(requestState.content);
        if (!state.roomHandoff && !state.requested.reject) {
          ret.add(contact);
        }
        continue;
      }
    }
    return ret;
  }

  static RequestBundle getRequestBundle(Client client, String requestId) {
    RequestBundle ret = new RequestBundle();
    for (final room in client.roomList.rooms) {
      if (!Request.isRequestRoom(room)) continue;
      Request req = Request(room);
      if (!req.isRequest) continue;
      if (req.content.requestId != requestId) continue;
      if (req.broadcast != null) {
        ret.broadcastRoom = room;
      } else {
        ret.rooms.add(room);
      }
    }
    return ret;
  }

  /// Starts an official request to this organisation at this contact point. Returns
  /// the Matrix room ID if successfull.
  static Future<String> startBroadcast({
    String requestTitle,
    String requestBody,
    String requestId,
    String tag,
    List<Contact> contactList,
    Client client,
  }) async {
    // Only matrix.to links are supported for now.
    Set<String> inviteSet = new Set<String>();
    BroadcastContent broadcastData = BroadcastContent(title: requestTitle, contacts: []);
    List<Map<String, dynamic>> initialState = [
      {
        "type": Request.mRequestNameSpace,
        "content": RequestContent(
                creator: client.userID,
                requestedOrganisation: "Broadcast",
                requestingOrganisation: client.userID.split(":")[1],
                requestTitle: requestTitle,
                requestId: requestId,
                msgtype: "m.text",
                body: requestBody,
                organisationId: "",
                organisationName: tag,
                contactId: "",
                contactDescription: "Broadcast")
            .toJson()
      },
      {
        "type": "m.room.history_visibility",
        "content": {"history_visibility": "shared"}
      },
      {
        "type": "m.room.guest_access",
        "content": {"guest_access": "forbidden"}
      },
    ];
    Map<String, int> userPermissions = {
      client.userID: 100,
    };

    for (final contact in contactList) {
      if (contact.matrixId == null) {
        continue;
      }

      inviteSet.add(contact.matrixId);

      broadcastData.contacts.add(BroadcastContactsContent(
        requestedOrganisation: contact.matrixId.split(":")[1],
        organisationId: contact.organisation?.id,
        organisationName: contact.organisation?.name,
        contactId: contact.id,
        contactDescription: contact.description,
        contactUri: contact.uri,
      ));

      initialState.add({
        "type": Directory.mPassport,
        "state_key": "!" + contact.matrixId,
        "content": {"token": contact.passport},
      });
      userPermissions[contact.matrixId] = 100;
    }
    initialState.add({
      "type": Request.mBroadcastNameSpace,
      "content": broadcastData.toJson(),
    });
    inviteSet.remove(client.userID);

    final String roomId = await client.createRoom(params: {
      "invite": inviteSet.toList(),
      "visibility": "private",
      "preset": "private_chat",
      "name": Request.mRequestNameSpace,
      "initial_state": initialState,
      "power_level_content_override": {
        "events": {
          Request.mRequestStateNameSpace: 0,
        },
        "users": userPermissions,
      },
    });
    return roomId;
  }
}
