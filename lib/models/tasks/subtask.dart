import 'package:famedly/models/tasks/task.dart';
import 'package:flutter/widgets.dart';

class Subtask {
  String _title;
  bool _done;
  Task _task;

  Subtask({@required String title, bool done = false, @required Task parent}) {
    this._title = title;
    this._done = done;
    this._task = parent;
  }

  String get title => _title;
  set title(String title) => _title = title;
  bool get done => _done;
  set done(bool done) => _done = done;

  Subtask.fromJson(Map<String, dynamic> json, Task parent) {
    _title = json['title'];
    _done = json['done'];
    _task = parent;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this._title;
    data['done'] = this._done;
    return data;
  }

  Future<bool> setTitle(String newTitle) async {
    String oldTitle = _title;
    _title = newTitle;
    bool saved = await _task.save();
    if (!saved) _title = oldTitle;
    return saved;
  }

  Future<bool> setDone(bool newDone) async {
    bool oldDone = _done;
    _done = newDone;
    bool saved = await _task.save();
    if (!saved) _done = oldDone;
    return saved;
  }
}
