import 'dart:async';

import 'package:famedly/models/tasks/tasklist.dart';
import "subtask.dart";

class Task {
  String _title;
  String _description;
  DateTime _date;
  bool _done;
  List<Subtask> _subtask;
  TaskList _tasklist;

  static const mNamespace = "com.famedly.talk.tasks";

  Task(
      {String title,
      String description,
      DateTime date,
      bool done = false,
      TaskList taskList,
      List<Subtask> subtask}) {
    this._title = title;
    this._description = description;
    this._date = date;
    this._done = done;
    this._subtask = subtask;
    this._tasklist;
    this._subtask = subtask ?? [];
  }

  /// Returns the (required) title of this task.
  String get title => _title;

  /// Returns the description of this task if given.
  String get description => _description;

  /// Returns the date duo this task should be finished if given.
  DateTime get date => _date;

  /// Wheither this task is done or not. At beginning this is false.
  bool get done => _done;

  /// Returns the list of subtasks for this task.
  List<Subtask> get subtask => _subtask;

  Future<bool> save() => _tasklist.save();

  int get id => _tasklist.tasks.indexOf(this);

  Task.fromJson(Map<String, dynamic> json, TaskList taskList) {
    _title = json['title'];
    _description = json['description'];
    _date = json['date'] != null && json['date'] is int
        ? DateTime.fromMillisecondsSinceEpoch(json['date'])
        : null;
    _done = json['done'];
    _tasklist = taskList;
    if (json['subtask'] != null) {
      _subtask = new List<Subtask>();
      json['subtask'].forEach((v) {
        _subtask.add(new Subtask.fromJson(v, this));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this._title;
    data['description'] = this._description;
    data['date'] = this._date?.millisecondsSinceEpoch;
    data['done'] = this._done;
    if (this._subtask != null) {
      data['subtask'] = this._subtask.map((v) => v.toJson()).toList();
    }
    return data;
  }

  /// Removes this task from the tasklist and the matrix homeserver.
  Future<bool> remove() async {
    List<Task> currentList = List<Task>.from(_tasklist.tasks);
    currentList.removeWhere(
        (Task testTask) => testTask.toJson().toString() == toJson().toString());
    _tasklist.updateTaskList(currentList);
    return true;
  }

  Future<bool> setTitle(String newTitle) async {
    String oldTitle = _title;
    _title = newTitle;
    bool saved = await save();
    if (saved != true) _title = oldTitle;
    return saved;
  }

  Future<bool> setDescription(String newDescription) async {
    String oldDescription = _description;
    _description = newDescription;
    try {
      await save();
    } catch (_) {
      _description = oldDescription;
      rethrow;
    }
    return true;
  }

  Future<bool> setDate(DateTime newDate) async {
    DateTime oldDate = _date;
    _date = newDate;
    try {
      await save();
    } catch (_) {
      _date = oldDate;
      rethrow;
    }
    return true;
  }

  Future<bool> setDone(bool newDone) async {
    bool oldDone = _done;
    _done = newDone;
    try {
      await save();
    } catch (_) {
      _done = oldDone;
      rethrow;
    }
    return true;
  }

  Future<bool> addSubtask(String title) async {
    List<Subtask> oldSubtask = _subtask;
    _subtask.add(Subtask(title: title, parent: this));
    try {
      await save();
    } catch (_) {
      _subtask = oldSubtask;
      rethrow;
    }

    return true;
  }
}
