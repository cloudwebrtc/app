import 'dart:async';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import "task.dart";

typedef onTaskListUpdate = void Function();

class TaskList {
  final Client client;

  List<Task> tasks;

  List<Task> _loadTasks() {
    List<Task> tasks = [];
    if (client.accountData[mNamespace] != null &&
        client.accountData[mNamespace].content["tasks"] is List<dynamic>) {
      List<dynamic> jsonTaskList =
          client.accountData[mNamespace].content["tasks"];
      for (int i = 0; i < jsonTaskList.length; i++)
        tasks.add(Task.fromJson(jsonTaskList[i], this));
    }
    return tasks;
  }

  onTaskListUpdate onUpdate;

  static const mNamespace = "com.famedly.talk.tasks";

  StreamSubscription<UserUpdate> userEventSub;

  TaskList(this.client, {this.onUpdate}) {
    this.tasks = _loadTasks();

    userEventSub ??= client.connection.onUserEvent.stream
        .where((UserUpdate u) =>
            u.type == "account_data" && u.eventType == mNamespace)
        .listen((UserUpdate u) {
      this.tasks = _loadTasks();
      if (onUpdate != null) onUpdate();
    });
  }

  /// Creates a new task and publishes it in the account data on the matrix homeserver.
  /// Returns the newly created task or null if something went wrong.
  Future<Task> create(
      {@required String title, String description, DateTime date}) async {
    Task newTask = Task(
        taskList: this, title: title, description: description, date: date);
    List<Task> taskList = List<Task>.from(tasks);
    taskList.add(newTask);
    await updateTaskList(taskList);
    return newTask;
  }

  Future<bool> save() async {
    await updateTaskList(tasks);
    return true;
  }

  /// Updates the task list. Throws MatrixException or other exceptions on error.
  Future<void> updateTaskList(List<Task> taskList) async {
    List<dynamic> jsonCurrentList = [];
    for (int i = 0; i < taskList.length; i++)
      jsonCurrentList.add(taskList[i].toJson());
    Map<String, dynamic> data = {"tasks": jsonCurrentList};
    return await client.connection.jsonRequest(
        type: HTTPType.PUT,
        action: "/client/r0/user/${client.userID}/account_data/$mNamespace",
        data: data);
  }
}
