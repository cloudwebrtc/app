import 'package:famedly/styles/fonts.dart';
import 'package:flutter/material.dart';

/// Helper Class to make colors easier to copy
/// This class converts a hex string to an integer value
class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class Themes {
  static ThemeData get famedlyLightTheme {
    final ThemeData base = ThemeData.light();
    return base.copyWith(
      popupMenuTheme: PopupMenuThemeData(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      brightness: Brightness.light,
      backgroundColor: Colors.white,
      scaffoldBackgroundColor: Colors.white,
      accentColor: FamedlyColors.aqua_marine,
      primaryColor: FamedlyColors.aqua_marine,
      bottomAppBarTheme: BottomAppBarTheme(
        color: Colors.white,
      ),
      buttonTheme: base.buttonTheme.copyWith(
          buttonColor: FamedlyColors.aqua_marine,
          padding: EdgeInsets.symmetric(vertical: 16, horizontal: 36),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          )),
      appBarTheme: AppBarTheme(
        color: Colors.white,
        brightness: Brightness.light,
        elevation: 2,
        textTheme: TextTheme(
            title: TextStyle(
                color: FamedlyColors.slate,
                fontSize: 15,
                fontWeight: FontWeight.w500)),
      ),
      dialogTheme: DialogTheme(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25))),
        titleTextStyle: Fonts.dialogTitle,
        contentTextStyle: Fonts.dialogContent,
      ),
      textTheme: _buildFamedlyLightTextTheme(base.textTheme),
      primaryTextTheme: _buildFamedlyLightTextTheme(base.primaryTextTheme),
      accentTextTheme: _buildFamedlyLightTextTheme(base.accentTextTheme),
      primaryIconTheme:
          base.iconTheme.copyWith(color: FamedlyColors.aqua_marine),
    );
  }

  static TextTheme _buildFamedlyLightTextTheme(TextTheme base) {
    return base.copyWith(
      button: TextStyle(
          fontSize: 15, color: FamedlyColors.aqua_marine, fontFamily: "Roboto"),
      headline: Fonts.headline,
      title: Fonts.defaultContent,
      body1: Fonts.defaultContent,
      body2: Fonts.defaultContent,
      display1: Fonts.defaultContent,
      display2: Fonts.defaultContent,
      display3: Fonts.defaultContent,
      display4: Fonts.defaultContent,
      subhead: Fonts.defaultContent,
      caption: Fonts.defaultContent,
      subtitle: Fonts.defaultContent,
      overline: Fonts.defaultContent,
    );
  }

  static ThemeData get famedlyDarkTheme {
    final ThemeData base = ThemeData.dark();
    return base.copyWith(
      brightness: Brightness.dark,
      primaryColor: Colors.black87,
      bottomAppBarTheme: BottomAppBarTheme(
        color: Colors.black,
      ),
      appBarTheme: AppBarTheme(
        brightness: Brightness.dark,
        color: Colors.black,
        elevation: 2,
        textTheme: TextTheme(
            title: TextStyle(
                color: Color(0xff95acb9),
                fontSize: 15,
                fontWeight: FontWeight.w500)),
      ),
      dialogTheme: DialogTheme(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25))),
        titleTextStyle: Fonts.dialogTitle,
        contentTextStyle: Fonts.dialogContent,
      ),
      textTheme: _buildFamedlyDarkTextTheme(base.textTheme),
      primaryTextTheme: _buildFamedlyDarkTextTheme(base.primaryTextTheme),
      accentTextTheme: _buildFamedlyDarkTextTheme(base.accentTextTheme),
      primaryIconTheme:
          base.iconTheme.copyWith(color: FamedlyColors.aqua_marine),
    );
  }

  static TextTheme _buildFamedlyDarkTextTheme(TextTheme base) {
    return base.copyWith(
      button: TextStyle(fontSize: 15, color: FamedlyColors.aqua_marine),
      headline: Fonts.headline,
      title: TextStyle(color: Color(0xff95acb9), fontSize: 15),
      body1: TextStyle(color: Color(0xff95acb9), fontSize: 15),
      body2: TextStyle(color: Color(0xff95acb9), fontSize: 15),
    );
  }
}

/// Defines colors to be used
/// See: https://app.zeplin.io/project/5d07ad07e700fa160ecee1ba/styleguide
/// Last Updated: 05.10.2019 20:49
class FamedlyColors {
  static const Color aqua_green = const Color(0xff14de92);
  static const Color aqua_marine = const Color(0xff1fdcca);
  static const Color black_10 = const Color(0x19000000);
  static const Color bluey_grey =
      const Color(0xff95acb9); // Previously dark_grey!
  static const Color cloudy_blue =
      const Color(0xffbfcdd4); // Previously avatar_grey!
  static const Color dark_sky_blue = const Color(0xff5eb5d7);
  static const Color emerald = const Color(0xff00a74a);
  static const Color grapefruit = const Color(0xffff5252);
  static const Color light_grey = const Color(0xfff6f7f7);
  static const Color metallic_blue = const Color(0xff4e6e81);
  static const Color off_blue = const Color(0xff5373bd);
  static const Color pale_grey = const Color(0xffedf0f2);
  static const Color pale_grey_two = const Color(0xfff6f7f8);
  static const Color slate = const Color(0xff4d576d);
  static const Color black_10percent = const Color(0x19212531);
  static const Color dark_purple = const Color(0xff898998);

  static const Color yellow = const Color(0xffffad27);
}

class FamedlyStyles {
  static const double columnWidth = 360.0;
}
