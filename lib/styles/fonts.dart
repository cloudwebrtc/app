import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

/// Defines fonts to be used
/// See: https://app.zeplin.io/project/5c742f836d6dc440977f7705/styleguide
/// Last Updated: 09.03.2019 12:12
class Fonts {
  static const headline = const TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.w400,
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontSize: 16.0);
  static const tab = const TextStyle(
      color: FamedlyColors.aqua_marine,
      fontWeight: FontWeight.w500,
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontSize: 14.0);
  static const unselectedTab = const TextStyle(
      color: const Color(0x99000000),
      fontWeight: FontWeight.w500,
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontSize: 14.0);
  static const button = const TextStyle(
      color: Colors.black87,
      fontWeight: FontWeight.w500,
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontSize: 14.0);
  static const subtitle = const TextStyle(
      color: const Color(0x99000000),
      fontWeight: FontWeight.w400,
      fontFamily: "Roboto",
      fontStyle: FontStyle.normal,
      fontSize: 14.0);
  static const dialogTitle = const TextStyle(
    fontFamily: 'Roboto',
    color: FamedlyColors.slate,
    fontSize: 15,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    letterSpacing: 0,
  );
  static const dialogContent = const TextStyle(
    fontFamily: 'Roboto',
    color: Color(0xff4e6e81),
    fontSize: 15,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0,
  );
  static const defaultContent = const TextStyle(
    fontFamily: 'Roboto',
    color: Color(0xff4e6e81),
    fontSize: 15,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0,
  );
}
