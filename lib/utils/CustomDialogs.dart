import 'dart:async';
import 'dart:io';

import 'package:famedly/components/Matrix.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:famedly/components/LoadingDialog.dart';

class CustomDialogs {
  final BuildContext context;
  bool finished = false;
  bool dialogVisible = false;
  static const int delay = 500;
  Timer timer;

  CustomDialogs(this.context);

  void showSnackBar(String title, String message) {
    Flushbar(
      backgroundColor: FamedlyColors.slate.withOpacity(0.9),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(30),
      title: title,
      message: message,
      duration: Duration(seconds: 5),
      flushbarPosition: FlushbarPosition.TOP,
      borderRadius: 5,
    )..show(context);
  }

  void showErrorSnackBar(String errorText) =>
      showSnackBar(locale.tr(context).oopsSomethingWentWrong, errorText);

  Future<dynamic> tryRequestWithErrorSnackbar(Future request) async {
    dynamic response = false;
    try {
      response = await request;
    } on MatrixException catch (exception) {
      if (exception.error == MatrixError.M_FORBIDDEN)
        showErrorSnackBar(locale.tr(context).noPermissionForThisAction);
      else if (exception.error == MatrixError.M_LIMIT_EXCEEDED)
        showErrorSnackBar(locale.tr(context).tooManyRequests);
      else
        showErrorSnackBar(locale.tr(context).oopsSomethingWentWrong +
            ". " +
            exception.errorMessage);
    } on SocketException catch (_) {
      showErrorSnackBar(locale.tr(context).organisationServerIsNotResponding);
    } on TimeoutException catch (_) {
      showErrorSnackBar(locale.tr(context).organisationServerIsNotResponding);
    } catch (_) {
      showErrorSnackBar(locale.tr(context).oopsSomethingWentWrong);
    }
    return response;
  }

  Future<dynamic> tryRequestWithLoadingDialogs(Future request) async {
    showLoadingDialog();
    final dynamic response = await tryRequestWithErrorSnackbar(request);
    hideLoadingDialog();
    return response;
  }

  /// Usage example:
  /// ```dart
  /// CustomDialogs dialogs = CustomDialogs(context);
  /// dialogs.showLoadingDialog();
  /// // Do magic
  /// dialogs.hideLoadingDialog();
  /// ````
  void showLoadingDialog() async {
    timer?.cancel();
    timer = Timer(
        Duration(seconds: Matrix.of(context).client.connection.syncTimeoutSec),
        () => hideLoadingDialog());
    finished = dialogVisible = false;
    await Future.delayed(Duration(milliseconds: delay));
    if (finished) return;
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return LoadingDialog();
        });
    dialogVisible = true;
  }

  bool hideLoadingDialog() {
    if (finished) return false;
    finished = true;
    if (dialogVisible) Navigator.of(context)?.pop();
    return Navigator.of(context) == null;
  }
}
