import 'package:famedlysdk/famedlysdk.dart';
import 'package:universal_html/prefer_universal/html.dart' as html;
import 'dart:async';

abstract class MatrixFileWebPicker {
  static Future<MatrixFile> startFilePicker({bool imagePicker = false}) async {
    try {
      final html.InputElement input = html.document.createElement('input');
      input..type = 'file';
      if (imagePicker) input..accept = 'image/*';
      input.click();
      await input.onChange.first;
      final List<html.File> files = input.files;
      final dynamic reader = html
          .AccessibleNode(); // This is FileReader. But using FileReader lets the app crash on android and iOS.
      // Execute this command: sed -i s/AccessibleNode/FileReader/g lib/utils/MatrixFileWebPicker.dart
      // To work on web with this.
      reader.readAsArrayBuffer(files[0]);
      await reader.onLoad.first;
      final List<int> byteList = reader.result as List<int>;
      return MatrixFile(bytes: byteList, path: files[0].name);
    } catch (e) {
      print(e.toString());
    }
    return null;
  }

  static void openUrl(String url) {
    html.window.open(url, "new");
  }
}
