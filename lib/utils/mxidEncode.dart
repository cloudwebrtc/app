import 'package:convert/convert.dart'; // for hex
import 'dart:convert'; // for utf8
import 'dart:typed_data';

String str2mxid(String a) {
  final utf8 = new Utf8Codec();
  final buf = utf8.encode(a);
  String encoded = "";
  for (final b in buf) {
    if (b == 0x5F) {
      // underscore
      encoded += "__";
    } else if ((b >= 0x61 && b <= 0x7A) || (b >= 0x30 && b <= 0x39)) {
      // [a-z0-9]
      encoded += String.fromCharCode(b);
    } else if (b >= 0x41 && b <= 0x5A) {
      encoded += "_" + String.fromCharCode(b + 0x20);
    } else {
      encoded += "=" + hex.encode([b]);
    }
  }
  return encoded;
}

String mxid2str(String b) {
  int j = 0;
  Uint8List decoded = new Uint8List(b.length);
  final utf8 = new Utf8Codec();
  for (int i = 0; i < b.length; i++) {
    final char = b[i];
    if (char == "_") {
      i++;
      if (b[i] == "_") {
        decoded[j] = 0x5F;
      } else {
        decoded[j] = b[i].codeUnitAt(0) - 0x20;
      }
    } else if (char == "=") {
      i++;
      decoded[j] = hex.decode(b[i] + b[i + 1])[0];
      i++;
    } else {
      decoded[j] = b[i].codeUnitAt(0);
    }
    j++;
  }
  return utf8.decode(decoded.sublist(0, j));
}
