import 'package:famedly/components/MatrixChatList.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

/// This page displays a list of archived chats.
class ArchivesPage extends StatelessWidget {
  final matrixChatList = GlobalKey();

  void _clearedDialog(BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          key: Key("ClearedDialog"),
          title: Align(
            alignment: Alignment.topCenter,
            child: Icon(
              Icons.check_circle,
              color: FamedlyColors.aqua_green,
              size: 64,
            ),
          ),
          content: Text(
            locale.tr(context).archiveCleared,
            textAlign: TextAlign.center,
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              key: Key("BackButtonInClearedDialog"),
              child: Text(
                locale.tr(context).back.toUpperCase(),
                style: TextStyle(color: FamedlyColors.aqua_marine),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _clearDialog(BuildContext outerContext) {
    // flutter defined function
    showDialog(
      context: outerContext,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          key: Key("ClearDialog"),
          content: new Text(locale.tr(context).clearWarningText),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              key: Key("BackButtonInClearDialog"),
              child: Text(
                locale.tr(context).back.toUpperCase(),
                style: TextStyle(color: FamedlyColors.aqua_marine),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              key: Key("ClearButtonInClearDialog"),
              child: Text(
                locale.tr(context).clear.toUpperCase(),
                style: TextStyle(color: FamedlyColors.aqua_marine),
              ),
              onPressed: () async {
                Navigator.of(context).pop();
                CustomDialogs dialogs = CustomDialogs(outerContext);
                dialogs.showLoadingDialog();
                List<Room> rooms =
                    (matrixChatList.currentState as MatrixChatListState)
                        .roomList
                        .rooms;
                print("Forget ${rooms.length} rooms");
                for (int i = 0; i < rooms.length; i++) {
                  if (Request.isRequestRoom(rooms[i])) continue;
                  await dialogs.tryRequestWithErrorSnackbar(rooms[i].forget());
                }
                (matrixChatList.currentState as MatrixChatListState)
                    .rebuildView();
                if (dialogs.hideLoadingDialog()) return;
                _clearedDialog(outerContext);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key("ArchivesPage"),
      appBar: AppBar(
        title: Text(locale.tr(context).archive),
        actions: <Widget>[
          PopupMenuButton(
            key: Key("PopupMenu"),
            onSelected: (result) {
              _clearDialog(context);
            },
            icon: Icon(
              FamedlyIcons.menu,
              color: Colors.black87,
            ),
            itemBuilder: (BuildContext context) => <PopupMenuEntry>[
              PopupMenuItem(
                key: Key("Clear"),
                value: "clear",
                child: Text(locale.tr(context).clearArchive),
              ),
            ],
          ),
        ],
      ),
      body: MatrixChatList(null,
          filter: (Room room) =>
              room.membership == Membership.leave &&
              !Request.isRequestRoom(room),
          onlyLeft: true,
          key: matrixChatList),
    );
  }
}
