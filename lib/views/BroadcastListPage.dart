import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/BroadcastList.dart';
import 'package:famedly/components/NavScaffold.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

/// This wraps the Tab view into a view.
/// It includes the AppBar as well as the Tab View and the Fab Menu
class BroadcastListPage extends StatefulWidget {
  final String requestId;

  BroadcastListPage({Key key, @required this.requestId}) : super(key: key);

  @override
  BroadcastListPageState createState() => new BroadcastListPageState();
}

class BroadcastListPageState extends State<BroadcastListPage> {
  Future<bool> waitForFirstSync(MatrixState matrix) async {
    if (matrix.client.prevBatch != null) return true;
    return matrix.client.connection.onFirstSync.stream.first;
  }

  final filterTextController = TextEditingController();
  String title;

  @override
  Widget build(BuildContext context) {
    if (title == null) {
      title = locale.tr(context).broadcast;
    }
    MatrixState matrix = Matrix.of(context);
    BroadcastList chatList = BroadcastList(
      filterTextController,
      widget.requestId,
      setTitle: (String t) {
        final newTitle = locale.tr(context).broadcast + ": " + t;
        if (newTitle != title) {
          SchedulerBinding.instance.addPostFrameCallback((_) => setState(() {
                title = newTitle;
              }));
        }
      },
      emptyListPlaceholder: Center(
        child: Text(locale.tr(context).noInquiries),
      ),
    );

    return NavScaffold(
      key: Key("BroadcastPage"),
      activePage:
          MediaQuery.of(context).size.width > 2 * FamedlyStyles.columnWidth
              ? ActivePage.REQUESTS
              : null,
      editButton: EditScaffoldButton(
        iconData: FamedlyIcons.plus,
        key: Key("WriteButton"),
        onTap: () {
          Navigator.of(context).pushNamed("/facilities");
        },
      ),
      title: Text(title),
      filterTextController: filterTextController,
      body: FutureBuilder<bool>(
        future: waitForFirstSync(matrix),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData)
            return chatList;
          else
            return Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
        },
      ),
    );
  }
}
