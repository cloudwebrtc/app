import 'dart:async';
import 'dart:io';

import 'package:famedly/components/ClickableAppBar.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/MatrixEventList.dart';
import 'package:famedly/components/ChatRoom/RequestStickyHeader.dart';
import 'package:famedly/components/dialogs/ConfirmDialog.dart';
import 'package:famedly/components/dialogs/ArchiveRoomDialog.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../components/ChatRoom/InputField.dart';
import '../styles/colors.dart';

class ChatRoom extends StatefulWidget {
  final String id;

  ChatRoom({Key key, this.id}) : super(key: key);

  @override
  ChatRoomState createState() => ChatRoomState();
}

enum RequestRoomType { Incoming, Outgoing, None }

class ChatRoomState extends State<ChatRoom> implements InputListener {
  final GlobalKey<InputFieldState> focusKey = GlobalKey<InputFieldState>();
  Room room;

  MatrixState matrix;

  final _searchTextController = TextEditingController();

  Event _replyTo;

  Future<void> _acceptRequest(BuildContext context) async {
    final req = Request(room);
    final bundle = Request.getRequestBundle(room.client, req.content.requestId);

    if (!req.isAuthor() ||
        (bundle.broadcastRoom == null && bundle.rooms.length == 1)) {
      final success =
          CustomDialogs(context).tryRequestWithLoadingDialogs(req.accept());
      if (success != false)
        setState(() {
          room = matrix.client.getRoomById(widget.id);
        });
    } else {
      showDialog(
        context: context,
        builder: (BuildContext ctx) {
          return ConfirmDialog(
              text: locale.tr(context).acceptBroadcastNotice,
              callback: (BuildContext ctx) async {
                CustomDialogs dialogs = CustomDialogs(context);
                dialogs.showLoadingDialog();

                if (bundle.broadcastRoom != null) {
                  for (final contact
                      in Request(bundle.broadcastRoom).broadcast.contacts) {
                    if (contact.contactId == req.content.contactId) continue;
                    bool leftRoom = false;
                    for (final r in bundle.rooms) {
                      if (r.id == room.id) continue;
                      final reqq = Request(r);
                      if (reqq.content.contactId != contact.contactId) continue;
                      await dialogs.tryRequestWithErrorSnackbar(reqq.reject());
                      await dialogs.tryRequestWithErrorSnackbar(r.leave());
                      leftRoom = true;
                      break;
                    }
                    if (!leftRoom) {
                      await Request(bundle.broadcastRoom, contact: contact);
                    }
                  }
                  await dialogs.tryRequestWithErrorSnackbar(bundle.broadcastRoom.leave());
                } else {
                  for (final r in bundle.rooms) {
                    if (r.id == room.id) continue;
                    await dialogs.tryRequestWithErrorSnackbar(Request(r).reject());
                    await dialogs.tryRequestWithErrorSnackbar(r.leave());
                  }
                }

                await dialogs.tryRequestWithErrorSnackbar(req.accept());

                dialogs.hideLoadingDialog();
                setState(() {
                  room = matrix.client.getRoomById(widget.id);
                });
              });
        },
      );
    }
  }

  Future<void> _rejectRequest(BuildContext context) async {
    final req = Request(room);
    CustomDialogs dialogs = CustomDialogs(context);
    dialogs.showLoadingDialog();
    await req.reject();
    dialogs.hideLoadingDialog();
    setState(() {
      room = matrix.client.getRoomById(widget.id);
    });
  }

  Widget _requestPopup(BuildContext context) {
    return RequestStickyHeader(
      room: room,
      onAccept: () => _acceptRequest(context),
      onReject: () => _rejectRequest(context),
      onReply: () {
        focusKey.currentState.inputFocus.requestFocus();
      },
    );
  }

  void _goToChatDetails() {
    if (!room.isDirectChat && room.membership != Membership.leave) {
      Navigator.of(context).pushNamed("/room/${widget.id}/settings");
    } else if (room.isDirectChat && room.membership != Membership.leave) {
      Navigator.of(context)
          .pushNamed("/room/${widget.id}/member/${room.directChatMatrixID}");
    }
  }

  void _archiveDialog(BuildContext outerContext) {
    showDialog(
      context: outerContext,
      builder: (BuildContext context) {
        // return object of type Dialog
        return ArchiveRoomDialog(
          room: room,
          postArchiveAction: () {
            Navigator.of(context).pop();
          },
        );
      },
    );
  }

  void _searchDialog(BuildContext outerContext) {
    showDialog(
      context: outerContext,
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: TextField(
              autofocus: true,
              controller: _searchTextController,
              decoration: InputDecoration(
                suffixIcon: Icon(Icons.search),
                hintText: locale.tr(context).searchQuery,
              ),
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                locale.tr(context).search,
                style: TextStyle(color: FamedlyColors.aqua_marine),
              ),
              onPressed: () {
                Navigator.pop(context);
                Navigator.of(context).pushNamed(
                  "/room/${widget.id}/search/${_searchTextController.text}",
                );
                _searchTextController.clear();
              },
            )
          ],
        );
      },
    );
  }

  List<PopupMenuEntry> getPopupList(BuildContext context, bool isRequestRoom) {
    List<PopupMenuEntry> list = <PopupMenuEntry>[];
    if (!isRequestRoom)
      list.add(PopupMenuItem(
        key: Key("ChatDetailsPopupMenuItem"),
        value: "chatDetails",
        child: Text(locale.tr(context).chatDetails),
      ));
    if (room.membership != Membership.leave) {
      if (isRequestRoom)
        list.add(PopupMenuItem(
          key: Key("InvitePopupMenuItem"),
          value: "invite",
          child: Text("Teilnehmer hinzufügen"),
        ));
      list.add(PopupMenuItem(
        key: Key("ArchivePopupMenuItem"),
        value: "archive",
        child: Text(
            isRequestRoom
                ? "Anfrage verlassen"
                : locale.tr(context).archiveAction,
            style: TextStyle(color: FamedlyColors.grapefruit)),
      ));
      if (isRequestRoom) {
        final req = Request(room);
        bool showAcceptDeny =
            ((req.isAuthor() && req.remoteAccepted()) || !req.isAuthor()) &&
                !req.youAccepted() &&
                !req.youRejected();
        if (showAcceptDeny) {
          list.add(PopupMenuItem(
            key: Key("AcceptRequestPopupMenuItem"),
            value: "acceptRequest",
            child: Text(locale.tr(context).acceptInquiry),
          ));
          list.add(PopupMenuItem(
            key: Key("RejectRequestPopupMenuItem"),
            value: "rejectRequest",
            child: Text(locale.tr(context).rejectInquiry),
          ));
        }
      }
      /*list.add(PopupMenuItem(
        value: "mute",
        child: Text(locale.tr(context).mute),
      ));*/
    }

    list.add(
      PopupMenuItem(
        key: Key("InvitePopupMenuItem"),
        value: "search",
        child: Text(locale.tr(context).search),
      ),
    );

    return list;
  }

  StreamSubscription roomNotFoundSub;

  void loadRoom() {
    if (room == null) {
      room = matrix.client.getRoomById(widget.id);
      if (room == null) {
        // It is possible that we join a room which is not yet synced
        roomNotFoundSub = matrix.client.connection.onEvent.stream
            .firstWhere((EventUpdate r) => r.roomID == widget.id)
            .asStream()
            .listen((EventUpdate r) {
          room = matrix.client.getRoomById(widget.id);
          if (room != null && mounted) {
            setState(() {});
          }
        });
        room = Room(
            id: widget.id, client: matrix.client, membership: Membership.leave);
      }
      room.onUpdate = () {
        if (mounted) setState(() {});
      };
    }
  }

  @override
  void dispose() {
    matrix.activeRoomID = null;
    matrix.roomsPlugin.onLeaveRoom = null;
    super.dispose();
  }

  bool directChatLeftDialogVisible = false;

  void showDirectChatLeftDialog(BuildContext outerContext) async {
    if (directChatLeftDialogVisible) return;
    directChatLeftDialogVisible = true;
    final String directChatId = room.directChatMatrixID;
    final String directChatName =
        room.getUserByMXIDSync(directChatId).calcDisplayname();
    CustomDialogs dialogs = CustomDialogs(outerContext);
    dialogs.showLoadingDialog();
    try {
      await room.leave();
    } catch (_) {
      return;
    }
    dialogs.hideLoadingDialog();
    setState(() {
      room.membership = Membership.leave;
    });
    showDialog(
      context: outerContext,
      builder: (context) => ConfirmDialog(
        text: locale.tr(context).hasArchivedConversation(directChatName),
        ok: locale.tr(context).startNewConversation,
        back: locale.tr(context).back,
        callback: (context) async {
          final String roomID =
              await User(directChatId, room: room).startDirectChat();
          if (roomID != null)
            Navigator.of(outerContext).pushNamedAndRemoveUntil(
                "/room/$roomID", (route) => route.isFirst);
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    matrix = Matrix.of(context);

    matrix.roomsPlugin.onLeaveRoom = (String roomID) {
      if (roomID == room.id) {
        setState(() {
          room.membership = Membership.leave;
        });
      }
    };

    loadRoom();

    final bool isRequestRoom = Request.isRequestRoom(room);

    String displayname = room.displayname;
    if (isRequestRoom) {
      final req = Request(room);
      displayname = req.title;
    }

    if (room.isDirectChat &&
        room.getUserByMXIDSync(room.directChatMatrixID).membership ==
            Membership.leave) {
      Timer(
          Duration(milliseconds: 500), () => showDirectChatLeftDialog(context));
    }

    return Scaffold(
      key: Key("ChatRoom"),
      appBar: ClickableAppBar(
        appBar: AppBar(
          key: Key("ChatRoomAppBar"),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: kIsWeb || Platform.isAndroid
                ? CrossAxisAlignment.start
                : CrossAxisAlignment.center,
            children: <Widget>[
              Text(locale.tr(context).trRoomName(displayname),
                  overflow: TextOverflow.ellipsis),
              Text(
                room.membership == Membership.join
                    ? locale.tr(context).connected
                    : locale.tr(context).notConnected,
                style:
                    const TextStyle(color: FamedlyColors.slate, fontSize: 12.0),
              ),
            ],
          ),
          actions: <Widget>[
            room.membership == Membership.leave
                ? Container()
                : PopupMenuButton(
                    key: Key("PopupMenu"),
                    onSelected: (result) async {
                      switch (result) {
                        case "chatDetails":
                          if (room.isDirectChat)
                            Navigator.of(context).pushNamed(
                                "/room/${widget.id}/member/${room.directChatMatrixID}");
                          else
                            Navigator.of(context)
                                .pushNamed("/room/${widget.id}/settings");
                          break;
                        case "invite":
                          Navigator.of(context)
                              .pushNamed("/room/${widget.id}/invite");
                          break;
                        case "archive":
                          _archiveDialog(context);
                          break;
                        case "search":
                          _searchDialog(context);
                          break;
                        case "acceptRequest":
                          _acceptRequest(context);
                          break;
                        case "rejectRequest":
                          _rejectRequest(context);
                          break;
                      }
                    },
                    icon: Icon(
                      FamedlyIcons.menu,
                      color: Colors.black87,
                    ),
                    itemBuilder: (BuildContext context) =>
                        getPopupList(context, isRequestRoom),
                  ),
          ],
        ),
        onTap: () {
          if (!isRequestRoom) _goToChatDetails();
        },
      ),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Request.isRequestRoom(room)
                    ? Container(height: 120)
                    : Container(),
                Expanded(
                  child: GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      if (focusKey.currentState != null &&
                          !focusKey.currentState.mic_or_send) {
                        focusKey.currentState.inputFocus.unfocus();
                        focusKey.currentState.mic_or_send = true;
                      }
                    },
                    child: MatrixEventList(
                      room,
                      onReply: (event) => setState(() => _replyTo = event),
                    ),
                  ),
                ),
                room.membership == Membership.leave
                    ? Container()
                    : InputField(
                        this,
                        room,
                        replyTo: _replyTo,
                        key: focusKey,
                        onCloseReply: () => setState(() => _replyTo = null),
                      ),
              ],
            ),
            Request.isRequestRoom(room)
                ? this._requestPopup(context)
                : Container(),
          ],
        ),
      ),
    );
  }

  @override
  onSend(String message) {
    room.sendEvent(
      {
        "msgtype": "m.text",
        "body": message,
        if (_replyTo != null)
          "m.relates_to": {
            "m.in_reply_to": {
              "event_id": _replyTo.eventId,
            },
          },
      },
    );
  }
}
