import 'package:famedly/components/user_list.dart';

import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

class ContactsPage extends StatelessWidget {
  void _onTapUser(BuildContext context, User contact) async {
    try {
      CustomDialogs dialogs = CustomDialogs(context);
      dialogs.showLoadingDialog();
      final roomID =
          await dialogs.tryRequestWithErrorSnackbar(contact.startDirectChat());
      if (dialogs.hideLoadingDialog() || roomID == false || roomID == null)
        return;
      Navigator.of(context).popUntil((route) => route.isFirst);
      Navigator.of(context).pushNamed("/room/$roomID");
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key("UsersPage"),
      appBar: AppBar(
        title: Text(locale.tr(context).contacts),
      ),
      body: UserList(
        onTap: (user) => _onTapUser(context, user),
        header: Column(
          children: <Widget>[
            ListTile(
              key: Key("NewGroupButton"),
              title: Text(locale.tr(context).new_group),
              trailing: Icon(
                Icons.add,
                color: FamedlyColors.bluey_grey,
              ),
              leading: Container(
                width: 45,
                height: 45,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(45),
                  color: FamedlyColors.slate,
                ),
                child: Icon(
                  Icons.group,
                  color: FamedlyColors.aqua_marine,
                ),
              ),
              onTap: () {
                Navigator.of(context).pushNamed("/newGroup");
              },
            ),
            Divider(
              color: FamedlyColors.pale_grey,
              thickness: 1,
              height: 1,
            )
          ],
        ),
      ),
    );
  }
}
