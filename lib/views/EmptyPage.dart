import 'package:flutter/material.dart';

class EmptyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ClipOval(
          child: Image.asset(
            "assets/images/famedly_icon.png",
            width: 144,
            height: 144,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
