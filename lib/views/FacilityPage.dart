import 'package:famedly/components/ProfileScaffold.dart';

import 'package:famedly/models/directory/directory.dart';
import 'package:famedly/models/directory/organisation.dart';
import 'package:famedly/models/directory/contact.dart';
import 'package:famedly/models/directory/tag.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:flutter/material.dart';

import '../components/Matrix.dart';
import '../styles/colors.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/components/FamedlyIconButton.dart';
import 'package:famedly/components/dialogs/PickerDialog.dart';

class OrganisationData {
  Organisation organisation;
  List<Tag> tagList;
}

class FacilityPage extends StatelessWidget {
  final String organisationID;

  static const TextStyle titleTextStyle = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 13,
  );

  const FacilityPage(
    this.organisationID, {
    Key key,
  }) : super(key: key);

  Future<OrganisationData> getOrganisationData(BuildContext context) async {
    OrganisationData data = OrganisationData();
    Directory directory = Directory(Matrix.of(context).client);
    data.organisation = await directory.getOrganisation(organisationID);
    data.tagList = await directory.getTags();
    return data;
  }

  @override
  Widget build(BuildContext context) {
    Organisation organisation = Directory.organisationsCache[organisationID];
    List<Widget> contactPointTiles = [];
    List<Contact> contactEmails = [];

    final matrix = Matrix.of(context);

    for (final contact in organisation.contacts) {
      if (contact.email != null) {
        contactEmails.add(contact);
      }
      if (contact.matrixId == null || contact.matrixId == matrix.client.userID) {
        continue;
      }
      
      contactPointTiles.add(
        Material(
          color: Theme.of(context).brightness == Brightness.light
              ? Colors.white
              : Colors.black12,
          borderRadius: BorderRadius.circular(10),
          child: ListTile(
            onTap: () {
              Navigator.of(context).pushNamed("/requestCompositing/${organisation.id}",
                  arguments: [contact]);
            },
            trailing: Icon(FamedlyIcons.chat,
                color: FamedlyColors.aqua_marine),
            title: Text(contact.description,
                style: TextStyle(color: FamedlyColors.aqua_marine)),
            subtitle: Text("Anfrage stellen"),
          ),
        ),
      );
    }

    return ProfileScaffold(
        title: Text(
          organisation.name,
          style: TextStyle(
            color: Color(0xff4d576d),
            fontSize: 15,
          ),
        ),
        profileName: organisation.name,
        body: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              contactEmails.length > 0 ?
                FamedlyIconButton(
                  iconData: Icons.alternate_email,
                  onTap: () {
                    if (contactEmails.length < 1) {
                      return;
                    }
                    if (contactEmails.length == 1) {
                      // do single request to contactEmails[0]
                      Navigator.of(context).pushNamed("/requestCompositing/${organisation.id}",
                          arguments: [contactEmails[0]]);
                    } else {
                      // pick whom you want to email
                      showDialog(
                        context: context,
                        builder: (BuildContext ctx) {
                          return PickerDialog<Contact>(
                            items: contactEmails,
                            title: locale.tr(context).pickEmail,
                            buildItem: (BuildContext cctx, Contact contact) {
                              return ListTile(
                                title: Text(contact.description,
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: FamedlyColors.slate,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                onTap: () {
                                  Navigator.of(ctx).pop();
                                  // okay, contact the "contact"
                                  Navigator.of(context).pushNamed("/requestCompositing/${organisation.id}",
                                      arguments: [contact]);
                                },
                              );
                            },
                          );
                        }
                      );
                    }
                  },
                ) : Container(),
              contactPointTiles.length > 0
                  ? Text(locale.tr(context).contactpoints,
                      style: titleTextStyle)
                  : Container(),
              SizedBox(height: 10),
              Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Theme.of(context).brightness == Brightness.light
                        ? Colors.white
                        : Colors.black26,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: contactPointTiles,
                  )),
              SizedBox(height: 10),
              Text(locale.tr(context).information, style: titleTextStyle),
              SizedBox(height: 10),
              Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Theme.of(context).brightness == Brightness.light
                        ? Colors.white
                        : Colors.black26,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                          title: Text(locale.tr(context).category,
                              style: titleTextStyle),
                          subtitle: Text(Directory
                              .subtypeCache[organisation.orgType].description)),
                      organisation.location != null ? ListTile(
                          title: Text(locale.tr(context).address,
                              style: titleTextStyle),
                          subtitle: Text(
                              "${organisation.location.street} ${organisation.location.number}, ${organisation.location.zipCode} ${organisation.location.city}")) : Container()
                    ],
                  )),
              SizedBox(height: 10),
              Text("Tags", style: titleTextStyle),
              SizedBox(height: 10),
              Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Theme.of(context).brightness == Brightness.light
                        ? Colors.white
                        : Colors.black26,
                  ),
                  child: TagListBuilder(
                    tagIDList: organisation.tags,
                  )),
            ],
          ),
        ));
  }
}

class TagListBuilder extends StatelessWidget {
  final List<String> tagIDList;

  const TagListBuilder({this.tagIDList, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<TextSpan> textSpanList = [];
    for (int i = 0; i < tagIDList.length; i++) {
      Tag tag = Directory.tagCache[tagIDList[i]];
      if (tag == null) continue;
      textSpanList.add(TextSpan(
          text: tag.description + (i < tagIDList.length - 1 ? ", " : ""),
          style: TextStyle(
              color: FamedlyColors.metallic_blue,
              fontSize: 13,
              fontFamily: "Roboto")));
    }
    return RichText(text: TextSpan(children: textSpanList));
  }
}
