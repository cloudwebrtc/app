import 'package:famedly/components/FacilityList.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/NavScaffold.dart';
import 'package:famedly/models/directory/directory.dart';
import 'package:famedly/models/directory/subtype.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:flutter/material.dart';

import 'package:famedly/config/FamedlyLocalizations.dart';

class FacilityPickerPage extends StatefulWidget {
  @override
  FacilityPickerPageState createState() => FacilityPickerPageState();
}

class FacilityPickerPageState extends State<FacilityPickerPage> {
  final TextEditingController filterTextController = TextEditingController();

  List<Subtype> subtypeList;

  String selectedTag = "";

  Future<List<Subtype>> getTagList(BuildContext context) async {
    if (subtypeList != null) {
      return subtypeList;
    }

    List<Subtype> newList =
        await Directory(Matrix.of(context).client).getSubtypes();

    setState(() {
      subtypeList = newList;
    });

    return newList;
  }

  Widget _buildSubtypeItem(BuildContext context, Subtype subtype) {
    return Container(
      margin: EdgeInsets.all(5),
      child: Material(
        color: selectedTag == subtype.id
            ? FamedlyColors.aqua_marine
            : FamedlyColors.pale_grey,
        borderRadius: BorderRadius.circular(19),
        child: InkWell(
          borderRadius: BorderRadius.circular(19),
          onTap: () {
            setState(() {
              if (selectedTag == subtype.id) {
                selectedTag = "";
              } else {
                selectedTag = subtype.id;
              }
            });
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
            child: Text(
              subtype.description,
              style: TextStyle(
                color: selectedTag == subtype.id
                    ? Colors.white
                    : FamedlyColors.metallic_blue,
                fontSize: 13,
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return NavScaffold(
        filterTextController: filterTextController,
        title: Text(locale.tr(context).facilities),
        body: FutureBuilder<dynamic>(
            future: Directory(Matrix.of(context).client).requestToken(),
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return Center(child: CircularProgressIndicator());
              else if (snapshot.hasError)
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        FamedlyIcons.error,
                        size: 80,
                        color: FamedlyColors.slate,
                      ),
                      Text(locale.tr(context).oopsSomethingWentWrong),
                      Text(snapshot.data.error),
                    ],
                  ),
                );
              else
                return Column(
                  children: <Widget>[
                    Container(
                      height: 46,
                      child: FutureBuilder<List<Subtype>>(
                        future: getTagList(context),
                        builder: (BuildContext context, snapshot) {
                          if (!snapshot.hasData)
                            return Center(child: CircularProgressIndicator());
                          List<Subtype> subtypes = selectedTag.isNotEmpty
                              ? subtypeList
                                  .where((s) => s.id == selectedTag)
                                  .toList()
                              : subtypeList;

                          return ListView.builder(
                            itemCount: subtypes.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) => _buildSubtypeItem(
                              context,
                              subtypes[index],
                            ),
                          );
                        },
                      ),
                    ),
                    ListTile(
                      title: Text("Neuer Rundruf"),
                      onTap: () =>
                          Navigator.of(context).pushNamed("/newBroadcast"),
                    ),
                    Expanded(
                      child: Container(
                          padding: EdgeInsets.all(5),
                          color:
                              Theme.of(context).brightness == Brightness.light
                                  ? FamedlyColors.light_grey
                                  : null,
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Theme.of(context).brightness ==
                                          Brightness.light
                                      ? Colors.white
                                      : null,
                                  borderRadius: BorderRadius.circular(5)),
                              child: FacilityList(filterTextController,
                                  subtypeList: subtypeList, tag: selectedTag))),
                    ),
                  ],
                );
            }));
  }
}
