import 'package:cached_network_image/cached_network_image.dart';
import 'package:famedly/components/Matrix.dart';

import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:photo_view/photo_view.dart';

class ImageView extends StatelessWidget {
  final String mxcurl;

  const ImageView({Key key, this.mxcurl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String url = MxContent("${this.mxcurl}").getDownloadLink(
      Matrix.of(context).client,
    );
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xff0c0d10),
      systemNavigationBarColor: Color(0xff0c0d10),
      statusBarBrightness: Brightness.light,
    ));
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black.withOpacity(0.75),
        automaticallyImplyLeading: false,
        elevation: 0,
        leading: IconButton(
          color: Color(0xfff6f7f7),
          icon: Icon(
            FamedlyIcons.cancel,
          ),
          onPressed: () {
            SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
              systemNavigationBarColor: Colors.white,
              systemNavigationBarIconBrightness: Brightness.dark,
              statusBarColor: Colors.white,
            ));
            Navigator.of(context).pop();
          },
        ),
      ),
      backgroundColor: Color(0xff0c0d10),
      body: PhotoView(
        minScale: PhotoViewComputedScale.contained * 1.0,
        initialScale: PhotoViewComputedScale.contained * 1.0,
        imageProvider:
            kIsWeb ? NetworkImage(url) : CachedNetworkImageProvider(url),
      ),
    );
  }
}
