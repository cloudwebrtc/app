import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/dialogs/ConfirmDialog.dart';
import 'package:famedly/components/user_list.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class InvitePage extends StatelessWidget {
  final String id;

  InvitePage({this.id});

  void _onTapUser(BuildContext outerContext, User contact) {
    // flutter defined function

    showDialog(
      context: outerContext,
      builder: (BuildContext context) => ConfirmDialog(
        text: locale.tr(context).inviteUserToChat(contact.calcDisplayname()),
        callback: (c) async {
          Navigator.of(context).pop();

          Room room = Matrix.of(outerContext).client.roomList.getRoomById(id);
          final success = CustomDialogs(outerContext)
              .tryRequestWithLoadingDialogs(room.invite(contact.id));
          if (success != false)
            Flushbar(
              message: locale
                  .tr(outerContext)
                  .hasBeenInvited(contact.calcDisplayname()),
            )..show(outerContext);
        },
        ok: locale.tr(context).invite,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key("InvitePage"),
      appBar: AppBar(
        title: Text(locale.tr(context).invite),
      ),
      body: UserList(
        roomId: id,
        onTap: (user) => _onTapUser(context, user),
      ),
    );
  }
}
