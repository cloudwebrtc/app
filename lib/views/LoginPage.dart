import 'dart:math';

import 'package:famedly/components/Matrix.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/config/TestUser.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/fonts.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  LoginPageState createState() => new LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  // This key will uniquely identify the Form widget and allow
  // us to validate the form
  final _formKey = GlobalKey<FormState>();

  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  String _usernameErrorText;
  String _passwordErrorText;

  void submit(BuildContext context) async {
    if (_usernameController.text.isEmpty) {
      setState(
          () => _usernameErrorText = locale.tr(context).pleaseFillOutTextField);
      return;
    }
    setState(() => _usernameErrorText = null);
    if (_passwordController.text.isEmpty) {
      setState(
          () => _passwordErrorText = locale.tr(context).pleaseFillOutTextField);
      return;
    }
    setState(() => _passwordErrorText = null);

    MatrixState matrix = Matrix.of(context);
    CustomDialogs dialogs = CustomDialogs(context);
    dialogs.showLoadingDialog();
    try {
      if (!await matrix.client
          .login(_usernameController.text, _passwordController.text)) {
        setState(() =>
            _passwordErrorText = locale.tr(context).invalidUsernameOrPassword);
        return;
      }
    } on MatrixException catch (exception) {
      if (exception.error == MatrixError.M_FORBIDDEN) {
        setState(() =>
            _passwordErrorText = locale.tr(context).invalidUsernameOrPassword);
      } else if (exception.error == MatrixError.M_LIMIT_EXCEEDED) {
        setState(() => _passwordErrorText = locale.tr(context).tooManyRequests);
      } else {
        setState(() => _passwordErrorText = exception.errorMessage);
      }
      return;
    } catch (_) {
      setState(
          () => _passwordErrorText = locale.tr(context).oopsSomethingWentWrong);
      return;
    } finally {
      dialogs.hideLoadingDialog();
    }
    if (matrix.client.userID.split(":")[0] == "@" + TestUser.username)
      await matrix.loginPlugin.resetUser();
    if (!dialogs.hideLoadingDialog()) {
      if (matrix.client.store == null) matrix.accountStorePlugin.saveAccount();
      Navigator.of(context).pop();
      if (kIsWeb)
        Navigator.of(context)
            .pushNamedAndRemoveUntil("/rooms", (Route r) => false);
      else
        Navigator.of(context).pushNamed("/intro");
    }
  }

  bool _passwordVisible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key("LoginPage"),
      appBar: AppBar(
        title: Text(
          locale.tr(context).loginWithOrganisation,
          key: Key("title"),
          style: const TextStyle(
            color: Colors.black87,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.normal,
          ),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(
            horizontal: max(0, (MediaQuery.of(context).size.width - 400) / 2)),
        shrinkWrap: true,
        children: <Widget>[
          SizedBox(height: 33),
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: TextFormField(
                    key: Key("username"),
                    controller: _usernameController,
                    autocorrect: false,
                    validator: (value) {
                      if (value.isEmpty) {
                        return locale.tr(context).enterUsernameText;
                      }
                      if (!(value.length <= 255)) {
                        return locale.tr(context).usernameIncorrect;
                      }
                      return "";
                    },
                    decoration: new InputDecoration(
                      errorText: _usernameErrorText,
                      border: OutlineInputBorder(),
                      labelText: locale.tr(context).username,
                      labelStyle: Fonts.headline,
                    ),
                  ),
                ),
                SizedBox(height: 23),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: TextFormField(
                    key: Key("password"),
                    autocorrect: false,
                    controller: _passwordController,
                    obscureText: !_passwordVisible,
                    validator: (value) {
                      if (value.isEmpty) {
                        return locale.tr(context).enterPasswordText;
                      }
                      return null;
                    },
                    onFieldSubmitted: (s) => submit(context),
                    decoration: new InputDecoration(
                      border: OutlineInputBorder(),
                      errorText: _passwordErrorText,
                      labelText: locale.tr(context).password,
                      labelStyle: Fonts.headline,
                      suffixIcon: IconButton(
                        icon: Icon(
                          _passwordVisible
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: FamedlyColors.aqua_marine,
                        ),
                        onPressed: () {
                          setState(() {
                            _passwordVisible = !_passwordVisible;
                          });
                        },
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 23),
                RaisedButton(
                  key: Key("login"),
                  onPressed: () => submit(context),
                  child: Text(locale.tr(context).login,
                      style: TextStyle(color: Colors.white)),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
