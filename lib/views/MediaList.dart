import 'package:cached_network_image/cached_network_image.dart';

import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../components/Matrix.dart';
import '../config/FamedlyLocalizations.dart';

class MediaList extends StatefulWidget {
  final String roomID;
  final String userID;
  final String type;

  const MediaList({
    this.userID,
    this.roomID,
    this.type,
    Key key,
  }) : super(key: key);

  @override
  MediaListState createState() => MediaListState();
}

class MediaListState extends State<MediaList> {
  String prevBatch = "";
  final ScrollController _scrollController = ScrollController();
  MatrixState matrix;

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future<Map<Map<String, String>, MxContent>> _loadMedia(
      MatrixState matrix) async {
    dynamic resp;
    if (widget.userID == null || widget.userID == "") {
      resp = await matrix.client.connection.jsonRequest(
          type: HTTPType.GET,
          action:
              "/client/r0/rooms/${widget.roomID}/messages?from=${this.prevBatch}"
              "&limit=100"
              "&dir=b"
              "&filter={\"lazy_load_members\":true,\"filter_json\":{\"contains_url\":true,\"types\":[\"m.room.message\"]},"
              "\"types\":[\"m.room.message\"],\"not_types\":[],\"rooms\":null,\"not_rooms\":[],\"senders\":null,\"not_senders\":[],\"contains_url\":true}");
    } else {
      resp = await matrix.client.connection.jsonRequest(
          type: HTTPType.GET,
          action:
              "/client/r0/rooms/${widget.roomID}/messages?from=${this.prevBatch}"
              "&limit=100"
              "&dir=b"
              "&filter={\"lazy_load_members\":true,\"filter_json\":{\"contains_url\":true,\"types\":[\"m.room.message\"]},"
              "\"types\":[\"m.room.message\"],\"not_types\":[],\"rooms\":null,\"not_rooms\":[],\"senders\":[${widget.userID}],\"not_senders\":[],\"contains_url\":true}");
    }
    Map<Map<String, String>, MxContent> contents =
        Map<Map<String, String>, MxContent>();

    this.prevBatch = resp["end"];

    if (!(resp["chunk"] is List<dynamic> &&
        resp["chunk"].length > 0 &&
        resp["end"] is String)) return contents;

    List<dynamic> history = resp["chunk"];

    history.forEach((e) {
      Map<String, String> id_type = Map<String, String>();
      id_type[e["event_id"]] = e["content"]["msgtype"];
      contents[id_type] = MxContent(e["content"]["url"]);
      return;
    });
    return contents;
  }

  @override
  void initState() {
    /*
    Disabled until we use the store to properly handle this

    _scrollController.addListener(() async {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        if (this.contents.length > 0 && this.matrix != null) {
          await _loadMedia(this.matrix);
        }
      }
    });*/
    super.initState();
  }

  Future<Room> loadRoom(MatrixState matrix) async {
    return matrix.client.getRoomById(widget.roomID);
  }

  Future<User> loadUser(MatrixState matrix) {
    return matrix.client.roomList
        .getRoomById(widget.roomID)
        .getUserByMXID(widget.userID);
  }

  Widget getMedia(BuildContext context, {Room room, User user}) {
    return FutureBuilder<Map<Map<String, String>, MxContent>>(
      future: _loadMedia(this.matrix),
      builder: (BuildContext context,
          AsyncSnapshot<Map<Map<String, String>, MxContent>> snapshot) {
        if (snapshot.hasError)
          debugPrint("error: " + snapshot.error.toString());
        if (!snapshot.hasData)
          return Container(
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );

        var contents = snapshot.data;
        contents.removeWhere((Map<String, String> key, MxContent content) {
          if (content.mxc == null || content.mxc == "") {
            return true;
          }
          if (widget.type == "images" && key.values.toList()[0] == "m.image") {
            return false;
          } else {
            return true;
          }
        });

        // Count where on the grid we are
        int i = 0;
        return Scaffold(
          appBar: AppBar(
            title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  locale.tr(context).imagesTitle,
                  style: TextStyle(
                    color: Color(0xff4d576d),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                  ),
                ),
                Text(
                  room != null ? room.displayname : user.calcDisplayname(),
                  style: TextStyle(
                    color: Color(0xff95acb9),
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                  ),
                ),
              ],
            ),
            automaticallyImplyLeading: false,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Color(0xff1fdcca),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Padding(
            padding: EdgeInsets.only(left: 6, right: 5, bottom: 15, top: 15),
            child: GridView.builder(
              itemCount: contents.length,
              shrinkWrap: true,
              controller: _scrollController,
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
              itemBuilder: (BuildContext context, int index) {
                EdgeInsets padding;
                Widget content;
                if (i == 0) {
                  padding = EdgeInsets.only(right: 5, bottom: 5);
                  i++;
                } else if (i == 2) {
                  padding = EdgeInsets.only(left: 5, bottom: 5);
                  i = 0;
                } else {
                  padding = EdgeInsets.only(bottom: 5);
                  i++;
                }

                if (widget.type == "images" &&
                    contents.keys.toList()[index].values.toList()[0] ==
                        "m.image") {
                  final String url = contents.values.toList()[index] == null
                      ? ""
                      : contents.values.toList()[index].getDownloadLink(
                            Matrix.of(context).client,
                          );
                  content = Material(
                    child: InkWell(
                      onTap: () {
                        debugPrint(
                            "/room/imageView/${contents.values.toList()[index] == null ? "" : contents.values.toList()[index].mxc.toString().replaceFirst("mxc://", "")}");
                        // We need to remove msc:// to prevent side effects of the slashes
                        Navigator.of(context).pushNamed(
                            "/room/imageView/${contents.values.toList()[index] == null ? "" : contents.values.toList()[index].mxc.toString().replaceFirst("mxc://", "")}");
                      },
                      child: Container(
                        width: 118,
                        height: 118,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: kIsWeb
                                ? NetworkImage(url)
                                : CachedNetworkImageProvider(url),
                            fit: BoxFit.fitWidth,
                          ),
                          color: Color(0xffffffff),
                          borderRadius: BorderRadius.circular(3),
                          boxShadow: [
                            BoxShadow(
                              color: Color(0x0c000000),
                              offset: Offset(0, -1),
                              blurRadius: 1,
                              spreadRadius: 0,
                            ),
                            BoxShadow(
                              color: Color(0x19000000),
                              offset: Offset(0, 1),
                              blurRadius: 1,
                              spreadRadius: 0,
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }

                return Padding(
                  padding: padding,
                  child: content,
                );
              },
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    this.matrix = Matrix.of(context);

    if (widget.userID == null && widget.userID == "") {
      return FutureBuilder<Room>(
        future: loadRoom(matrix),
        builder: (BuildContext context, AsyncSnapshot<Room> snapshot) {
          if (snapshot.hasError)
            debugPrint("error: " + snapshot.error.toString());
          if (!snapshot.hasData)
            return Container(
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(),
                ));

          Room room = snapshot.data;
          return getMedia(context, room: room);
        },
      );
    } else {
      return FutureBuilder<User>(
        future: loadUser(matrix),
        builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
          if (snapshot.hasError)
            debugPrint("error: " + snapshot.error.toString());
          if (!snapshot.hasData)
            return Container(
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(),
                ));

          User user = snapshot.data;
          return getMedia(context, user: user);
        },
      );
    }
  }
}
