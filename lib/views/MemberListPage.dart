import 'dart:async';
import 'dart:io';

import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/MemberListItem.dart';

import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

enum Answers { YES, NO }

class MemberListPage extends StatefulWidget {
  final String id;

  const MemberListPage(
    this.id, {
    Key key,
  }) : super(key: key);

  @override
  MemberListPageState createState() => MemberListPageState();
}

class MemberListPageState extends State<MemberListPage> {
  bool remoteParticipants = false;

  Future<List<User>> loadParticipants(Room room) async {
    List<User> participants = room.getParticipants();
    if (participants.length ==
        room.mJoinedMemberCount + room.mInvitedMemberCount) return participants;
    return room.requestParticipants();
  }

  Future<Room> loadRoom(MatrixState matrix) async {
    Room room = await matrix.client.getRoomById(widget.id);
    return room;
  }

  StreamSubscription memberChangeSub;

  @override
  void dispose() {
    memberChangeSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    MatrixState matrix = Matrix.of(context);

    memberChangeSub ??= matrix.client.connection.onEvent.stream
        .where((e) =>
            e.roomID == widget.id &&
            ["m.room.member", "m.room.power_levels"].contains(e.eventType))
        .listen((e) => setState(() {}));
    final Room room = matrix.client.getRoomById(widget.id);

    if (room == null)
      return Scaffold(
        appBar: AppBar(
          title: Text(locale.tr(context).notConnected),
        ),
        body: Center(
          child: Icon(Icons.error),
        ),
      );
    return Scaffold(
      key: Key("MemberListPage"),
      backgroundColor: Theme.of(context).brightness == Brightness.light
          ? Color(0xfff6f7f7)
          : null,
      appBar: AppBar(
        title: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: kIsWeb || Platform.isAndroid
              ? CrossAxisAlignment.start
              : CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              locale.tr(context).membersTitle,
              style: TextStyle(
                color: Color(0xff4d576d),
                fontSize: 15,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            ),
            Text(
              locale.tr(context).trRoomName(room.displayname),
              style: TextStyle(
                color: Color(0xff95acb9),
                fontSize: 12,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            ),
          ],
        ),
        actions: <Widget>[
          IconButton(
            key: Key("GoToInvitePage"),
            icon: Icon(FamedlyIcons.plus),
            onPressed: () {
              Navigator.of(context).pushNamed("/room/${widget.id}/invite");
            },
          )
        ],
      ),
      body: FutureBuilder<List<User>>(
        future: loadParticipants(room),
        builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
          if (snapshot.hasError)
            debugPrint("error: " + snapshot.error.toString());
          if (!snapshot.hasData)
            return Container(
                color: Theme.of(context).brightness == Brightness.light
                    ? Colors.white
                    : Colors.black,
                child: Center(
                  child: CircularProgressIndicator(),
                ));

          List<User> participants = snapshot.data;
          participants.sort((User a, User b) {
            if (a.powerLevel > b.powerLevel) {
              return -1;
            } else if (a.powerLevel == b.powerLevel) {
              return 0;
            } else {
              return 1;
            }
          });
          return Padding(
            padding: EdgeInsets.all(6),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Container(
                constraints: BoxConstraints(
                  minHeight: 0,
                  minWidth: 0,
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).brightness == Brightness.light
                      ? Color(0xffedf0f2)
                      : Colors.transparent,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x0c000000),
                      offset: Offset(0, -1),
                      blurRadius: 1,
                      spreadRadius: 0,
                    ),
                    BoxShadow(
                      color: Color(0x19000000),
                      offset: Offset(0, 1),
                      blurRadius: 1,
                      spreadRadius: 0,
                    ),
                  ],
                ),
                child: ListView.separated(
                  shrinkWrap: true,
                  separatorBuilder: (context, index) => Padding(
                    padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width - 91),
                    // We basicly do the line in a inverse way ^^
                    child: Container(
                      height: 1,
                      color: Theme.of(context).brightness == Brightness.light
                          ? Colors.white
                          : Colors.transparent,
                    ),
                  ),
                  itemCount: participants.length,
                  itemBuilder: (context, index) =>
                      MemberListItem(participants[index]),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
