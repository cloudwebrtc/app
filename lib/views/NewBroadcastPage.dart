import 'package:flutter/material.dart';
import 'package:famedly/models/directory/contact.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/components/NavScaffold.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

import 'package:famedly/components/Matrix.dart';

class NewBroadcastPage extends StatefulWidget {
  final TextEditingController filterTextController = TextEditingController();
  final List<Contact> contacts;

  NewBroadcastPage(this.contacts, {Key key})
      : super(key: key);

  @override
  NewBroadcastPageState createState() => NewBroadcastPageState();
}

class NewBroadcastPageState extends State<NewBroadcastPage> {
  bool allSelected = false;
  Map<String, bool> selectedContacts = Map();
  String filterText = null;

  void handleFilter() {
    if (widget.filterTextController == null) return;
    setState(() {
      filterText = widget.filterTextController.text;
    });
  }

  @override
  initState() {
    super.initState();
    if (widget.filterTextController != null) {
      widget.filterTextController.addListener(handleFilter);
    }
  }

  @override
  void dispose() {
    if (widget.filterTextController != null) {
      widget.filterTextController.removeListener(handleFilter);
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final matrix = Matrix.of(context);
    // TODO: filter contacts
    final contacts = widget.contacts.where(
      (c) => c.matrixId != matrix.client.userID,
    ).toList(growable: false);

    // re-determine the "all" checkbox marker position
    bool areAllSelected = true;
    bool areAllDeselected = true;
    for (final c in contacts) {
      if (selectedContacts[c.id] == true) {
        areAllDeselected = false;
      } else {
        areAllSelected = false;
      }
    }
    if (areAllSelected) allSelected = true;
    if (areAllDeselected) allSelected = false;
    if (!areAllSelected && !areAllDeselected) allSelected = null;

    Widget contactPointList;
    if (contacts.length == 0) {
      contactPointList = Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            locale.tr(context).noContactPointsVisibleToYou,
            textAlign: TextAlign.center,
          ),
        ),
      );
    } else {
      contactPointList = ListView.separated(
        separatorBuilder: (BuildContext context, int index) {
          return Divider(color: FamedlyColors.pale_grey, indent: 15);
        },
        itemCount: contacts.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            trailing: Checkbox(
              value: selectedContacts[contacts[index].id] == true,
              onChanged: (v) {
                setState(() {
                  selectedContacts[contacts[index].id] = v;
                });
              },
            ),
            title: Text(contacts[index].organisation.name,
                style: TextStyle(
                  fontSize: 15,
                  color: FamedlyColors.slate,
                  fontWeight: FontWeight.w600,
                )),
            subtitle: Text(contacts[index].description,
                style: TextStyle(
                  fontSize: 13,
                  color: FamedlyColors.slate,
                )),
            onTap: () {
              setState(() {
                selectedContacts[contacts[index].id] =
                    selectedContacts[contacts[index].id] == false ||
                        selectedContacts[contacts[index].id] == null;
              });
            },
          );
        },
      );
    }

    return NavScaffold(
      filterTextController: widget.filterTextController,
      title: Text(locale.tr(context).newBroadcast),
      editButton: Checkbox(
        value: allSelected,
        tristate: true,
        onChanged: (v) {
          setState(() {
            if (v == null) {
              v = false;
            }
            allSelected = v;
            for (final c in contacts) {
              selectedContacts[c.id] = allSelected;
            }
          });
        },
      ),
      body: Container(
        padding: EdgeInsets.all(5),
        color: FamedlyColors.light_grey,
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(5)),
          child: contactPointList
        ),
      ),
      actions: [IconButton(
        icon: Icon(
          FamedlyIcons.tick,
          color: Colors.black87,
        ),
        onPressed: () async {
          List<Contact> contactsSelected = [];
          for (final c in contacts) {
            if (selectedContacts[c.id] == true) {
              contactsSelected.add(c);
            }
          }
          if (contactsSelected.length == 0) {
            return; // nothing is selected
          }
          Navigator.of(context).pushNamed("/requestCompositing",
            arguments: contactsSelected);
        },
      )],
    );
  }
}
