import 'package:famedly/components/Avatar.dart';
import 'package:famedly/components/Matrix.dart';

import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

class NewGroupPage extends StatefulWidget {
  @override
  newGroupPageState createState() => new newGroupPageState();
}

class newGroupPageState extends State<NewGroupPage> {
  Set<String> inviteSet = Set<String>();

  getTitle(BuildContext context) => locale.tr(context).new_group;

  onTapListTile(BuildContext context, User contact) {}

  getAppBarActions(BuildContext context, MatrixState matrix) => <Widget>[
        IconButton(
          key: Key("StartGroupChat"),
          icon: Icon(
            FamedlyIcons.tick,
            color: Colors.black87,
          ),
          onPressed: () async {
            if (inviteSet.length == 0) return;
            List<User> inviteList = [];
            inviteSet.forEach((String id) {
              inviteList.add(User(id));
            });
            final roomId = await CustomDialogs(context)
                .tryRequestWithLoadingDialogs(
                    matrix.client.createRoom(invite: inviteList));
            if (roomId != null) {
              Navigator.of(context).popUntil((route) => route.isFirst);
              Navigator.of(context).pushNamed("/room/$roomId");
            }
          },
        ),
      ];

  ListTile headingItem(String letter) {
    return ListTile(
      title: Text(
        letter.substring(0, 1),
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    MatrixState matrix = Matrix.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(getTitle(context)),
        actions: getAppBarActions(context, matrix),
      ),
      body: FutureBuilder(
          key: Key("NewGroupPage"),
          future: matrix.contactDiscoveryPlugin.loadContacts(),
          builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
            if (snapshot.data == null)
              return Container(
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            List<User> contactList = snapshot.data;
            contactList.sort((a, b) => a
                .calcDisplayname()
                .toUpperCase()
                .compareTo(b.calcDisplayname().toUpperCase()));

            return ListView.builder(
              key: Key("UsersListView"),
              // Let the ListView know how many items it needs to build
              itemCount: contactList.length * 2,
              // Provide a builder function. This is where the magic happens! We'll
              // convert each item into a Widget based on the type of item it is.
              itemBuilder: (context, index) {
                if (index > contactList.length - 1) return Container();
                final item = contactList[index];
                User prevItem = item;
                if (index > 0) {
                  prevItem = contactList[index - 1];
                }

                if (index == 0 ||
                    prevItem.calcDisplayname().substring(0, 1).toUpperCase() !=
                        item.calcDisplayname().substring(0, 1).toUpperCase()) {
                  contactList.insert(index, item);
                  return headingItem(item.calcDisplayname().toUpperCase());
                } else {
                  return ContactListItem(
                      contact: item,
                      onChecked: (bool selected) {
                        if (selected)
                          inviteSet.add(item.id);
                        else
                          inviteSet.remove(item.id);
                      });
                }
              },
            );
          }),
    );
  }
}

class ContactListItem extends StatefulWidget {
  final onChecked;

  final User contact;

  ContactListItem({Key key, this.onChecked, this.contact}) : super(key: key);

  @override
  _ContactListItemState createState() {
    return _ContactListItemState();
  }
}

class _ContactListItemState extends State<ContactListItem> {
  bool checked = false;

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      value: checked,
      onChanged: (bool selected) {
        widget.onChecked(selected);
        setState(() {
          checked = selected;
        });
      },
      secondary: Avatar(widget.contact.avatarUrl,
          name: widget.contact.calcDisplayname()),
      title: Text(widget.contact.calcDisplayname(),
          style: TextStyle(color: Colors.black87)),
    );
  }
}
