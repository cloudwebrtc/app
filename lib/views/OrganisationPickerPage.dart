import 'dart:math';

import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/SentrySwitchListTile.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/fonts.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class OrganisationPickerPage extends StatefulWidget {
  @override
  _OrganisationPickerPageState createState() => _OrganisationPickerPageState();
}

class _OrganisationPickerPageState extends State<OrganisationPickerPage> {
  final _organisationController = TextEditingController();
  String errorText;

  void submit(BuildContext context) async {
    if (_organisationController.text.isEmpty) {
      setState(() => errorText = locale.tr(context).pleaseFillOutTextField);
      return;
    }
    setState(() => errorText = null);

    if (!_organisationController.text.toLowerCase().endsWith("famedly.de")) {
      _organisationController.text += ".famedly.de";
    }
    String server = _organisationController.text;
    if (!server.startsWith("https://")) server = "https://" + server;
    MatrixState matrix = Matrix.of(context);
    CustomDialogs dialogs = CustomDialogs(context);
    dialogs.showLoadingDialog();
    try {
      if (!await matrix.client.checkServer(server)) {
        setState(() =>
            errorText = locale.tr(context).organisationServerIsNotSupported);
        return;
      }
    } on MatrixException catch (exception) {
      setState(() => errorText = exception.errorMessage);
      return;
    } catch (_) {
      setState(() =>
          errorText = locale.tr(context).organisationServerIsNotResponding);
      return;
    } finally {
      dialogs.hideLoadingDialog();
    }
    Navigator.of(context).pushNamed("/login");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key("OrganisationPickerPage"),
      appBar: AppBar(
        title: Text(locale.tr(context).welcometofamedly),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(
            horizontal: max(0, (MediaQuery.of(context).size.width - 400) / 2)),
        children: <Widget>[
          Center(
            child: Padding(
              padding: EdgeInsets.only(
                top: 16.0,
                bottom: 8.0,
              ),
              child: ClipOval(
                child: Image.asset(
                  "assets/images/famedly_icon.png",
                  width: 144,
                  height: 144,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Divider(),
          Padding(
            padding:
                EdgeInsets.only(left: 24.0, top: 8.0, bottom: 8.0, right: 24.0),
            child: Text((locale.tr(context).welcometext),
                style: TextStyle(
                  fontSize: 16.0,
                )),
          ),
          Divider(),
          Padding(
            padding: EdgeInsets.all(16),
            child: TextFormField(
              keyboardType: TextInputType.url,
              key: Key("organisation"),
              autocorrect: false,
              onFieldSubmitted: (s) => submit(context),
              autofocus: true,
              controller: _organisationController,
              decoration: new InputDecoration(
                border: OutlineInputBorder(),
                prefixText: "https://",
                labelText: "Organisation",
                hintText: "*.famedly.de",
                labelStyle: Fonts.headline,
                errorText: errorText,
              ),
            ),
          ),
          Center(
            child: RaisedButton(
              key: Key("login"),
              onPressed: () => submit(context),
              child: Text(
                locale.tr(context).login,
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          Divider(),
          SentrySwitchListTile(),
        ],
      ),
    );
  }
}
