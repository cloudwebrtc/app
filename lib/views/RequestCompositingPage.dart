import 'dart:io';
import 'dart:math';

import 'package:famedly/components/Matrix.dart';

import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/directory/contact.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:uuid/uuid.dart';

class RequestCompositingPage extends StatefulWidget {
  final List<Contact> contacts;

  const RequestCompositingPage(this.contacts, {Key key}) : super(key: key);
  @override
  RequestCompositingPageState createState() => RequestCompositingPageState();
}

class RequestCompositingPageState extends State<RequestCompositingPage> {
  String title = "";
  String text = "";

  @override
  Widget build(BuildContext context) {
    String titleText = locale.tr(context).loading;
    bool isExternal = false;
    if (widget.contacts.length == 1) {
      titleText = (widget.contacts[0].organisation != null
              ? widget.contacts[0].organisation.name + " - "
              : "") +
          widget.contacts[0].description;
      isExternal = widget.contacts[0].email != null;
    } else {
      titleText = locale.tr(context).broadcast +
          ": " +
          widget.contacts.length.toString() +
          " " +
          locale.tr(context).contacts;
    }
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: kIsWeb || Platform.isAndroid
                ? CrossAxisAlignment.start
                : CrossAxisAlignment.center,
            children: <Widget>[
              Text(locale.tr(context).inquiry, overflow: TextOverflow.ellipsis),
              Text(titleText,
                  style: TextStyle(
                      color: FamedlyColors.cloudy_blue, fontSize: 12)),
            ],
          ),
        ),
        body: Center(
            child: Container(
          width: min(FamedlyStyles.columnWidth,
              MediaQuery.of(context).size.width - 30),
          padding: EdgeInsets.all(15),
          child: Column(
            children: <Widget>[
              isExternal ? Text(locale.tr(context).externalInquiryWarning,
                style: TextStyle(
                  color: FamedlyColors.grapefruit,
                  fontWeight: FontWeight.w600,
                )) : Container(),
              TextField(
                maxLines: 1,
                minLines: 1,
                onChanged: (String newText) {
                  setState(() {
                    title = newText;
                  });
                },
                decoration: InputDecoration(
                    hintText: locale.tr(context).yourtitle,
                    labelText: locale.tr(context).yourtitle),
              ),
              TextField(
                maxLines: 20,
                minLines: 1,
                onChanged: (String newText) {
                  setState(() {
                    text = newText;
                  });
                },
                decoration: InputDecoration(
                    hintText: (locale.tr(context).yourmessage),
                    labelText: (locale.tr(context).yourconcern)),
              ),
              SizedBox(height: 40),
              RaisedButton(
                  elevation: 1,
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 36),
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(15.0),
                  ),
                  child: Text(locale.tr(context).requests,
                      style: TextStyle(color: Colors.white, fontSize: 15)),
                  onPressed: !text.isEmpty
                      ? () async {
                          CustomDialogs dialogs = CustomDialogs(context);
                          final uuid = new Uuid();
                          String requestId = uuid.v4();
                          Future request;
                          if (widget.contacts.length == 1) {
                            request = widget.contacts[0].startRequest(
                                requestTitle: title,
                                requestBody: text,
                                requestId: requestId,
                                client: Matrix.of(context).client);
                          } else {
                            request = Request.startBroadcast(
                              requestTitle: title,
                              requestBody: text,
                              requestId: requestId,
                              tag: title,
                              contactList: widget.contacts,
                              client: Matrix.of(context).client,
                            );
                          }
                          final roomId = await dialogs.tryRequestWithLoadingDialogs(request);
                          if (roomId != false) {
                            if (widget.contacts.length == 1) {
                              if (roomId != null)
                                Navigator.of(context)?.pushNamedAndRemoveUntil(
                                    "/room/outgoingRequests/$roomId",
                                    (route) => route.isFirst);
                            } else {
                              Navigator.of(context)?.pushNamedAndRemoveUntil(
                                  "/requests", (route) => route.isFirst);
                            }
                          }
                        }
                      : null),
            ],
          ),
        )));
  }
}
