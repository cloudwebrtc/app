import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/RequestsList.dart';
import 'package:famedly/components/NavScaffold.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

/// This wraps the Tab view into a view.
/// It includes the AppBar as well as the Tab View and the Fab Menu
class RequestsListPage extends StatelessWidget {
  final bool onlyInbox;
  final bool onlyArchive;

  RequestsListPage({Key key, this.onlyInbox = null, this.onlyArchive = false})
      : super(key: key);

  Future<bool> waitForFirstSync(MatrixState matrix) async {
    if (matrix.client.prevBatch != null) return true;
    return matrix.client.connection.onFirstSync.stream.first;
  }

  final filterTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final String title = onlyArchive
        ? locale.tr(context).archive
        : (onlyInbox
            ? locale.tr(context).incomming
            : locale.tr(context).outgoing);
    MatrixState matrix = Matrix.of(context);
    RequestsList chatList = RequestsList(
      filterTextController,
      emptyListPlaceholder: Center(
        child: Text(locale.tr(context).noInquiries),
      ),
      onlyLeft: onlyArchive,
      filter: (Room room) {
        if (onlyArchive && room.membership == Membership.leave) return true;

        final req = Request(room);
        if (!req.isRequest)
          return room.membership == Membership.invite && onlyInbox;
        if (onlyInbox == true && !req.isAuthor())
          return true;
        else if (onlyInbox == false && req.isAuthor()) return true;
        return false;
      },
    );

    return NavScaffold(
      key: Key("RequestsPage"),
      activePage:
          MediaQuery.of(context).size.width > 2 * FamedlyStyles.columnWidth
              ? ActivePage.REQUESTS
              : null,
      editButton: onlyArchive
          ? null
          : EditScaffoldButton(
              iconData: FamedlyIcons.plus,
              key: Key("WriteButton"),
              onTap: () {
                Navigator.of(context).pushNamed("/facilities");
              },
            ),
      title: Text(title),
      filterTextController: filterTextController,
      body: FutureBuilder<bool>(
        future: waitForFirstSync(matrix),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData)
            return chatList;
          else
            return Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
        },
      ),
    );
  }
}
