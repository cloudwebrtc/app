import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/MatrixChatList.dart';
import 'package:famedly/components/MatrixStatusHeader.dart';
import 'package:famedly/components/NavScaffold.dart';
import 'package:famedly/components/RequestGroupListTile.dart';

import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

typedef RoomFilter = bool Function(Room room);

/// This wraps the Tab view into a view.
/// It includes the AppBar as well as the Tab View and the Fab Menu
class RequestsPage extends StatefulWidget {
  Future<bool> waitForFirstSync(MatrixState matrix) async {
    if (matrix.client.prevBatch != null) return true;
    return matrix.client.connection.onFirstSync.stream.first;
  }

  @override
  State<StatefulWidget> createState() => RequestsPageState();
}

class RequestsPageState extends State<RequestsPage> {
  final filterTextController = TextEditingController();
  bool searchActive = false;

  int getUnread({Client client, RoomFilter filter, bool highlight = false}) {
    int count = 0;
    for (int i = 0; i < client.roomList.rooms.length; i++) {
      Room room = client.roomList.rooms[i];
      if (!Request.isRequestRoom(room) || !filter(room)) continue;
      if (highlight &&
          (room.notificationCount > 0 || room.membership == Membership.invite))
        count++;
      else if (highlight == false) count++;
    }
    return count;
  }

  @override
  Widget build(BuildContext context) {
    MatrixState matrix = Matrix.of(context);

    MatrixChatList chatList = MatrixChatList(
      filterTextController,
      filter: (Room room) =>
          room.membership == Membership.join && Request.isRequestRoom(room),
    );

    RoomFilter incomingFilter = (Room room) {
      final req = Request(room);
      return (!req.isRequest && room.membership == Membership.invite) ||
          !req.isAuthor();
    };
    RoomFilter outgoingFilter = (Room room) => !incomingFilter(room);
    /*RoomFilter archiveFilter = (Room room) =>
        room.membership == Membership.leave ||
        room.membership == Membership.ban;*/

    return NavScaffold(
      key: Key("RequestsPage"),
      editButton: EditScaffoldButton(
        iconData: FamedlyIcons.plus,
        key: Key("WriteButton"),
        onTap: () {
          Navigator.of(context).pushNamed("/facilities");
        },
      ),
      leading: Padding(
        padding: EdgeInsets.only(
          left: 15,
          top: 15,
        ),
        child: IconButton(
          key: Key("SettingsButton"),
          icon: Icon(FamedlyIcons.settings),
          iconSize: 24,
          color: FamedlyColors.slate,
          onPressed: () {
            Navigator.of(context).pushNamed("/settings");
          },
        ),
      ),
      title: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 15.0),
          child: MatrixStatusheader(),
        ),
      ),
      actions: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            right: 15, top: 15, //bottom: 15
          ),
          child: IconButton(
            key: Key("NotificationsButton"),
            icon: Icon(FamedlyIcons.notification),
            iconSize: 24,
            color: FamedlyColors.slate,
            onPressed: () {
              // FIXME Not implemented
            },
          ),
        ),
      ],
      activePage: ActivePage.REQUESTS,
      filterTextController: filterTextController,
      onTextChanged: (String text) {
        if (searchActive != !text.isEmpty)
          setState(() {
            searchActive = !text.isEmpty;
          });
      },
      body: searchActive == false
          ? ListView(
              children: <Widget>[
                RequestGroupListTile(
                  onTap: () {
                    Navigator.of(context).pushNamed("/incomingRequests");
                  },
                  iconData: Icons.inbox,
                  unread:
                      getUnread(client: matrix.client, filter: incomingFilter),
                  highlightUnread: getUnread(
                          client: matrix.client,
                          filter: incomingFilter,
                          highlight: true) >
                      0,
                  title: (locale.tr(context).incominginquiries),
                ),
                RequestGroupListTile(
                  onTap: () {
                    Navigator.of(context).pushNamed("/outgoingRequests");
                  },
                  unread:
                      getUnread(client: matrix.client, filter: outgoingFilter),
                  highlightUnread: getUnread(
                          client: matrix.client,
                          filter: outgoingFilter,
                          highlight: true) >
                      0,
                  iconData: Icons.save_alt,
                  title: (locale.tr(context).outgoinginquiries),
                ),
                RequestGroupListTile(
                    onTap: () {
                      Navigator.of(context).pushNamed("/archivedRequests");
                    },
                    unread: null,
                    iconData: FamedlyIcons.archive,
                    title: "Archiv"),
              ],
            )
          : FutureBuilder<bool>(
              future: widget.waitForFirstSync(matrix),
              builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData)
                  return chatList;
                else
                  return Container(
                    color: Colors.white,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
              },
            ),
    );
  }
}
