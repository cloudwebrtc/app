import 'dart:io';

import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/ProfileScaffold.dart';
import 'package:famedly/components/SettingsMenuItem.dart';
import 'package:famedly/components/dialogs/ArchiveRoomDialog.dart';

import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedly/utils/MatrixFileWebPicker.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class RoomSettings extends StatefulWidget {
  final String id;

  RoomSettings({Key key, this.id}) : super(key: key);

  @override
  RoomSettingsState createState() => RoomSettingsState();
}

class RoomSettingsState extends State<RoomSettings> {
  bool remoteParticipants = false;

  Room room;

  void setChatNameDialog(BuildContext outerContext, Room room) {
    // flutter defined function
    showDialog(
      context: outerContext,
      builder: (BuildContext context) {
        final TextEditingController roomNameController =
            TextEditingController();
        // return object of type Dialog
        return AlertDialog(
          title: Text(locale.tr(context).setChatName),
          content: TextField(
            key: Key("setChatNameDialogTextField"),
            textCapitalization: TextCapitalization.sentences,
            autofocus: true,
            controller: roomNameController,
            decoration: new InputDecoration.collapsed(
              hintText: room.displayname,
            ),
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              key: Key("BackButton_SetChatName"),
              child: Text(
                locale.tr(context).back.toUpperCase(),
                style: TextStyle(color: FamedlyColors.aqua_marine),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              key: Key("OkButton_SetChatName"),
              child: Text(
                locale.tr(context).set.toUpperCase(),
                style: TextStyle(color: FamedlyColors.aqua_marine),
              ),
              onPressed: () async {
                Navigator.of(context).pop();
                CustomDialogs(outerContext).tryRequestWithLoadingDialogs(
                    room.setName(roomNameController.text));
              },
            ),
          ],
        );
      },
    );
  }

  Future<List<User>> loadParticipants(Room room) async {
    if (remoteParticipants)
      return await room.requestParticipants();
    else
      return room.getParticipants();
  }

  @override
  Widget build(BuildContext context) {
    MatrixState matrix = Matrix.of(context);

    if (room == null) {
      room = matrix.client.getRoomById(widget.id);
      room?.onUpdate = () {
        if (mounted) setState(() {});
      };
    }

    if (room == null)
      return Scaffold(
        appBar: AppBar(
          title: Text(locale.tr(context).notConnected),
        ),
        body: Center(
          child: Icon(Icons.error),
        ),
      );

    return ProfileScaffold(
      key: Key("RoomSettings"),
      title: Text(locale.tr(context).trRoomName(room.displayname)),
      profileContent: room.avatar,
      profileName: locale.tr(context).trRoomName(room.displayname),
      onAvatarTap: !room.canSendEvent("m.room.avatar")
          ? null
          : () async {
              MatrixFile file;
              if (kIsWeb) {
                file = await MatrixFileWebPicker.startFilePicker(
                    imagePicker: true);
              } else {
                final File tempFile = await ImagePicker.pickImage(
                    source: ImageSource.gallery,
                    imageQuality: 50,
                    maxWidth: 1600,
                    maxHeight: 1600);
                if (tempFile == null) return;
                file = MatrixFile(
                    bytes: tempFile.readAsBytesSync(), path: tempFile.path);
              }
              if (file != null) {
                CustomDialogs(context)
                    .tryRequestWithLoadingDialogs(room.setAvatar(file));
              }
            },
      actions: <Widget>[
        !room.canSendEvent("m.room.name")
            ? Container()
            : PopupMenuButton(
                key: Key("PopupMenu"),
                onSelected: (result) async {
                  switch (result) {
                    case "editChatName":
                      setChatNameDialog(context, room);
                      break;
                  }
                },
                icon: Icon(
                  FamedlyIcons.menu,
                  color: Colors.black87,
                ),
                itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                  PopupMenuItem(
                    key: Key("EditChatNamePopupMenuItem"),
                    value: "editChatName",
                    child: Text(locale.tr(context).setChatName),
                  )
                ],
              ),
      ],
      buttons: [
        ProfileScaffoldButton(
          key: Key("ChatProfileScaffoldButton"),
          iconData: FamedlyIcons.chat,
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
        ProfileScaffoldButton(
          key: Key("CallProfileScaffoldButton"),
          iconData: Icons.phone,
          onTap: () {
            Navigator.of(context).pushNamed("/call/room/");
          },
        ),
        ProfileScaffoldButton(
            key: room.pushRuleState == PushRuleState.notify
                ? Key("NotificationOnProfileScaffoldButton")
                : Key("NotificationOffProfileScaffoldButton"),
            iconData: room.pushRuleState == PushRuleState.notify
                ? FamedlyIcons.notification
                : FamedlyIcons.notifyOff,
            onTap: () async {
              PushRuleState newState = PushRuleState.notify;
              if (room.pushRuleState == PushRuleState.notify) {
                newState = PushRuleState.mentions_only;
              }
              final Future pushRuleUpdate = room
                  .client.connection.onUserEvent.stream
                  .where((u) => u.eventType == "m.push_rules")
                  .first;
              final success = await CustomDialogs(context)
                  .tryRequestWithLoadingDialogs(
                      room.setPushRuleState(newState));
              if (success == false) return;
              await pushRuleUpdate;
              setState(() {});
            })
      ],
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
              left: 5,
              right: 5,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: SettingsMenuItem(
                Icon(
                  FamedlyIcons.participants,
                  color: Color(0xff95acb9),
                ),
                locale.tr(context).membersTitle,
                Text(
                  ((room?.mJoinedMemberCount ?? 0) +
                          (room?.mInvitedMemberCount ?? 0))
                      .toString(),
                  style: TextStyle(
                    color: Color(0xff95acb9),
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                  ),
                ),
                () {
                  Navigator.of(context).pushNamed("/room/${widget.id}/members");
                },
                key: Key("GoToMembersPage"),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: EdgeInsets.only(top: 20, left: 16, bottom: 10),
              child: Text(
                locale.tr(context).mediaTitle,
                style: TextStyle(
                  color: Color(0xff95acb9),
                  fontSize: 13,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                ),
              ),
            ),
          ),
          // TODO calculate number of images in a room
          Builder(builder: (context) {
            return Padding(
              padding: EdgeInsets.only(
                left: 5,
                right: 5,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Column(
                  children: <Widget>[
                    SettingsMenuItem(
                      Icon(
                        FamedlyIcons.camera,
                        color: Color(0xff95acb9),
                      ),
                      locale.tr(context).imagesTitle,
                      Container(),
                      () {
                        /*AppRouter().router().navigateTo(context,
                                              "/room/${widget.id}/media/images");*/
                        CustomDialogs(context).showSnackBar(
                            null, locale.tr(context).notYetImplemented);
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width - 60),
                      child: Container(
                        height: 1,
                        color: Theme.of(context).brightness == Brightness.light
                            ? Colors.white
                            : Colors.transparent,
                      ),
                    ),
                    /* 
                                      SettingsMenuItem(
                                        Icon(
                                          Icons.videocam,
                                          color: Color(0xff95acb9),
                                        ),
                                        locale.tr(context).videosTitle,
                                        Container(),
                                        () {
                                          AppRouter().router().navigateTo(context,
                                              "/room/${widget.id}/media/videos");
                                        },
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                            right:
                                                MediaQuery.of(context).size.width -
                                                    60),
                                        child: Container(
                                          height: 1,
                                          color: Colors.white,
                                        ),
                                      ),
                                      SettingsMenuItem(
                                        Icon(
                                          Icons.insert_drive_file,
                                          color: Color(0xff95acb9),
                                        ),
                                        locale.tr(context).filesTitle,
                                        Container(),
                                        () {
                                          AppRouter().router().navigateTo(context,
                                              "/room/${widget.id}/media/files");
                                        },
                                      ),*/
                  ],
                ),
              ),
            );
          }),

          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(right: 16, top: 20),
              child: FlatButton.icon(
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      // return object of type Dialog
                      return ArchiveRoomDialog(
                        room: room,
                        postArchiveAction: () {
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              "/rooms", (Route r) => false);
                        },
                      );
                    },
                  );
                },
                icon: Icon(
                  FamedlyIcons.cancel,
                  color: Color(0xffff5252),
                ),
                label: Text(
                  locale.tr(context).leaveAction,
                  style: TextStyle(
                    color: Color(0xffff5252),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
