import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/MatrixChatList.dart';
import 'package:famedly/components/MatrixStatusHeader.dart';
import 'package:famedly/components/NavScaffold.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/models/requests/request.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

/// This wraps the Tab view into a view.
/// It includes the AppBar as well as the Tab View and the Fab Menu
class RoomsPage extends StatefulWidget {
  final String activeChatID;

  const RoomsPage({this.activeChatID});

  Future<bool> waitForFirstSync(MatrixState matrix) async {
    if (matrix.client.prevBatch != null) return true;
    return matrix.client.connection.onFirstSync.stream.first;
  }

  @override
  State<StatefulWidget> createState() => RoomsPageState();
}

class RoomsPageState extends State<RoomsPage> {
  final filterTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    MatrixChatList chatList = MatrixChatList(
      filterTextController,
      emptyListPlaceholder: Center(child: Text(locale.tr(context).noChats)),
      activeChatID: widget.activeChatID,
      filter: (Room room) =>
          room.membership != Membership.leave && !Request.isRequestRoom(room),
    );

    MatrixState matrix = Matrix.of(context);

    return NavScaffold(
      key: Key("RoomsPage"),
      editButton: EditScaffoldButton(
        iconData: FamedlyIcons.plus,
        key: Key("WriteButton"),
        onTap: () {
          Navigator.of(context).pushNamed("/contacts");
        },
      ),
      leading: Padding(
        padding: EdgeInsets.only(
          left: 15,
          top: 15,
        ),
        child: IconButton(
          key: Key("SettingsButton"),
          icon: Icon(FamedlyIcons.settings),
          iconSize: 24,
          color: FamedlyColors.slate,
          onPressed: () {
            Navigator.of(context).pushNamed("/settings");
          },
        ),
      ),
      title: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 15.0),
          child: MatrixStatusheader(),
        ),
      ),
      actions: <Widget>[
        Padding(
          padding: EdgeInsets.only(
            right: 15, top: 15, //bottom: 15
          ),
          child: IconButton(
            key: Key("NotificationsButton"),
            icon: Icon(FamedlyIcons.notification),
            iconSize: 24,
            color: FamedlyColors.slate,
            onPressed: () {
              // FIXME Not implemented
            },
          ),
        ),
      ],
      activePage: ActivePage.CHATS,
      filterTextController: filterTextController,
      body: FutureBuilder<bool>(
        future: widget.waitForFirstSync(matrix),
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
          if (snapshot.hasData)
            return chatList;
          else
            return Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
        },
      ),
    );
  }
}
