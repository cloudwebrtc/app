import 'dart:io';

import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/ProfileScaffold.dart';
import 'package:famedly/components/RoundCornerList.dart';
import 'package:famedly/components/SentrySwitchListTile.dart';
import 'package:famedly/components/SettingsMenuItem.dart';
import 'package:famedly/components/dialogs/ConfirmDialog.dart';

import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedly/utils/MatrixFileWebPicker.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class SettingsPage extends StatelessWidget {
  void logoutDialog(BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return ConfirmDialog(
          text: locale.tr(context).areYouSure,
          ok: locale.tr(context).logout.toUpperCase(),
          callback: (BuildContext ctx) async {
            MatrixState matrix = Matrix.of(context);
            final Future loggedOut = CustomDialogs(context)
                .tryRequestWithLoadingDialogs(matrix.client.logout());
            matrix.loginPlugin.clean();
            await loggedOut;
            Navigator.of(context)
                .pushNamedAndRemoveUntil("/", (Route r) => false);
          },
        );
      },
    );
  }

  Future<String> getUserAvatar(Client client) async {
    try {
      final resp = await client.connection.jsonRequest(
          type: HTTPType.GET,
          action: "/client/r0/profile/${client.userID}/avatar_url");
      return resp["avatar_url"];
    } catch (_) {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    MatrixState matrix = Matrix.of(context);

    return FutureBuilder<String>(
        future: getUserAvatar(matrix.client),
        builder: (context, snapshot) {
          String displayname =
              matrix.client.userID.split(":")[0].replaceFirst("@", "");
          String avatarUrl = snapshot.hasData ? snapshot.data : "";
          return ProfileScaffold(
            key: Key("SettingsPage"),
            title: Text(
              locale.tr(context).settingsTitle,
            ),
            profileName: displayname,
            profileContent: MxContent(avatarUrl),
            actions: <Widget>[
              IconButton(
                key: Key("SignOutAction"),
                icon: Icon(
                  FamedlyIcons.logout,
                  color: Color(0xff95acb9),
                ),
                onPressed: () {
                  logoutDialog(context);
                },
              ),
            ],
            onAvatarTap: () async {
              MatrixFile file;
              if (kIsWeb) {
                file = await MatrixFileWebPicker.startFilePicker(
                    imagePicker: true);
              } else {
                final File tempFile = await ImagePicker.pickImage(
                    source: ImageSource.gallery,
                    imageQuality: 50,
                    maxWidth: 1600,
                    maxHeight: 1600);
                if (tempFile == null) return;
                file = MatrixFile(
                    bytes: tempFile.readAsBytesSync(), path: tempFile.path);
              }
              if (file != null) {
                final success = await CustomDialogs(context)
                    .tryRequestWithLoadingDialogs(
                        matrix.client.setAvatar(file));
                if (success != false) Navigator.of(context)?.pop();
              }
            },
            body: Container(
              color: Theme.of(context).brightness == Brightness.light
                  ? Color(0xfff6f7f8)
                  : null,
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(top: 20, left: 16, bottom: 10),
                      child: Text(
                        locale.tr(context).settingsTitle,
                        style: TextStyle(
                          color: Color(0xff95acb9),
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.normal,
                          letterSpacing: 0,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: 5,
                      right: 5,
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Column(
                        children: <Widget>[
                          SettingsMenuItem(
                            Icon(
                              FamedlyIcons.archive,
                              key: Key("goToArchive"),
                              color: Color(0xff95acb9),
                            ),
                            locale.tr(context).archive,
                            Container(),
                            () {
                              Navigator.of(context).pushNamed("/chatArchive");
                            },
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIcons.security,
                              key: Key("goToPrivacyPolicy"),
                              color: Color(0xff95acb9),
                            ),
                            locale.tr(context).privacyPolicy,
                            Container(),
                            () {
                              Navigator.of(context)
                                  .pushNamed("/documents/privacypolicy");
                            },
                          ),
                          SettingsMenuItem(
                            Icon(
                              FamedlyIcons.document,
                              key: Key("goToLicense"),
                              color: Color(0xff95acb9),
                            ),
                            locale.tr(context).license,
                            Container(),
                            () {
                              Navigator.of(context)
                                  .pushNamed("/documents/license");
                            },
                          ),
                          SentrySwitchListTile(),
                          Padding(
                            padding: EdgeInsets.only(
                                right: MediaQuery.of(context).size.width - 60),
                            child: Container(
                              height: 1,
                              color: Theme.of(context).brightness ==
                                      Brightness.light
                                  ? Colors.white
                                  : Colors.black38,
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}

class SecuritySettings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          locale.tr(context).security,
          style: TextStyle(
            color: Color(0xff4d576d),
            fontSize: 15,
            fontWeight: FontWeight.w500,
            fontStyle: FontStyle.normal,
            letterSpacing: 0,
          ),
        ),
      ),
      body: Container(
        decoration: new BoxDecoration(
          color: Color(0xfff6f7f8),
        ),
        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(top: 20, left: 16, bottom: 10),
                child: Text(
                  (locale.tr(context).screenlock),
                  style: TextStyle(
                    color: Color(0xff95acb9),
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                  ),
                ),
              ),
            ),
            RoundListGen<String>(
              targets: <ActionFunction>[
                () {
                  Navigator.of(context)
                      .pushNamed("/settings/security/passwordChange/1");
                },
              ],
              iconsActivated: false,
              items: <String>[
                (locale.tr(context).changepassword),
              ],
              itemGeneratorFunction: (
                BuildContext context,
                String item,
                ActionFunction target,
                Icon icon,
                dynamic pop,
              ) {
                return Material(
                  color: Colors.white,
                  child: InkWell(
                    child: Container(
                      height: 77,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                left: 16,
                              ),
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    item,
                                    style: TextStyle(
                                      color: Color(0xff4d576d),
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: FontStyle.normal,
                                      letterSpacing: 0,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(right: 16),
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Color(0xff95acb9),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    onTap: target,
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}

// To test changes a full app restart is needed to retrigger a state build
class PasswordChange extends StatefulWidget {
  final int stage;
  final String password;

  PasswordChange({Key key, @required this.stage, this.password})
      : super(key: key);

  @override
  PasswordChangeState createState() => PasswordChangeState();
}

class PasswordChangeState extends State<PasswordChange> {
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = true;

  Future changePassword(MatrixState matrix, String new_password) async {
    Map<String, String> data;
    data["new_password"] = new_password;
    await matrix.client.connection.jsonRequest(
      type: HTTPType.POST,
      action: "/_matrix/client/r0/account/password",
      data: data,
    );
  }

  @override
  Widget build(BuildContext context) {
    RichText infoText;
    MatrixState matrix = Matrix.of(context);

    setState(() {
      _autoValidate =
          true; // Don't ask this is required :D https://github.com/flutter/flutter/issues/15404
    });

    if (widget.stage == 1) {
      infoText = RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: (locale.tr(context).please),
              style: TextStyle(
                color: Color(0xff4e6e81),
                fontSize: 15,
                fontWeight: FontWeight.w300,
                fontStyle: FontStyle.normal,
                fontFamily: "Roboto",
                letterSpacing: 0,
              ),
            ),
            TextSpan(
              text: (locale.tr(context).give),
              style: TextStyle(
                color: Color(0xff4e6e81),
                fontSize: 15,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
                fontFamily: "Roboto",
                letterSpacing: 0,
              ),
            ),
            TextSpan(
              text: "Sie ein neues Passwort ein.", //ist das der richtige satz?
              style: TextStyle(
                color: Color(0xff4e6e81),
                fontSize: 15,
                fontWeight: FontWeight.w300,
                fontStyle: FontStyle.normal,
                fontFamily: "Roboto",
                letterSpacing: 0,
              ),
            ),
          ],
        ),
      );
    } else if (widget.stage == 2) {
      infoText = RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: (locale.tr(context).please),
              style: TextStyle(
                color: Color(0xff4e6e81),
                fontSize: 15,
                fontWeight: FontWeight.w300,
                fontStyle: FontStyle.normal,
                fontFamily: "Roboto",
                letterSpacing: 0,
              ),
            ),
            TextSpan(
              text: (locale.tr(context).torepeat),
              style: TextStyle(
                color: Color(0xff4e6e81),
                fontSize: 15,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
                fontFamily: "Roboto",
                letterSpacing: 0,
              ),
            ),
            TextSpan(
              text: "Sie das Passwort.", //ist auch das der richtige satz?
              style: TextStyle(
                color: Color(0xff4e6e81),
                fontSize: 15,
                fontWeight: FontWeight.w300,
                fontStyle: FontStyle.normal,
                fontFamily: "Roboto",
                letterSpacing: 0,
              ),
            ),
          ],
        ),
      );
    }

    TextEditingController controller = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Row(
          children: <Widget>[
            FlatButton(
              child: Text(
                (locale.tr(context).abort),
                style: TextStyle(
                  color: Color(0xffff5252),
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            Spacer(),
            Text(
              (locale.tr(context).changepassword),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xff4d576d),
                fontSize: 15,
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
                letterSpacing: 0,
              ),
            ),
            Spacer(
              flex: 3,
            ),
          ],
        ),
        automaticallyImplyLeading: false,
      ),
      body: Container(
        width: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                top: 32.5,
              ),
              child: Align(
                alignment: Alignment.topCenter,
                child: infoText,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 60.5, left: 88),
              child: Text(
                (locale.tr(context).password),
                style: TextStyle(
                  color: Color(0xff95acb9),
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  letterSpacing: 0,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 88, top: 11, right: 88),
              child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextFormField(
                        textInputAction: TextInputAction.done,
                        maxLines: null,
                        autovalidate: _autoValidate,
                        controller: controller,
                        onFieldSubmitted: (String text) {
                          if (widget.stage == 1) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => PasswordChange(
                                  stage: 2,
                                  password:
                                      controller.text.replaceAll("\n", ""),
                                ),
                              ),
                            );
                          } else {
                            if (_formKey.currentState.validate()) {
                              CustomDialogs dialogs = CustomDialogs(context);
                              dialogs.showLoadingDialog();
                              changePassword(
                                  matrix, controller.text.replaceAll("\n", ""));
                              Navigator.of(context).pushNamedAndRemoveUntil(
                                  "/", (Route r) => false);
                              dialogs.hideLoadingDialog();
                            }
                          }
                        },
                        validator: (String text) {
                          if (text.isNotEmpty && widget.stage == 2) {
                            if (text != widget.password) {
                              return (locale.tr(context).entriesnotagree);
                            }
                            return null;
                          }
                          return null;
                        },
                        obscureText: true,
                        autocorrect: false,
                        decoration: InputDecoration(
                          hintText: (locale.tr(context).secret),
                          hintStyle: TextStyle(
                            color: Color(0xffbfcdd4),
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          ),
                          border: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Color(0xffbfcdd4),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 35),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: FlatButton(
                            color: Color(0xffbfcdd4)
                                .withOpacity(0.1472498948882226),
                            onPressed: () {
                              if (widget.stage == 1) {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => PasswordChange(
                                      stage: 2,
                                      password: controller.text,
                                    ),
                                  ),
                                );
                              } else {
                                if (_formKey.currentState.validate()) {
                                  CustomDialogs dialogs =
                                      CustomDialogs(context);
                                  dialogs.showLoadingDialog();
                                  changePassword(matrix,
                                      controller.text.replaceAll("\n", ""));
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      "/", (Route r) => false);
                                  dialogs.hideLoadingDialog();
                                }
                              }
                            },
                            child: Text(
                              "Weiter",
                              style: TextStyle(
                                color: Color(0xffffffff),
                                fontSize: 15,
                                fontWeight: FontWeight.w500,
                                fontStyle: FontStyle.normal,
                                letterSpacing: 0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
