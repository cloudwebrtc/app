import 'package:famedly/components/TagList.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/NavScaffold.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:flutter/material.dart';
import 'package:famedly/models/directory/organisation.dart';
import 'package:famedly/models/directory/contact.dart';
import 'package:famedly/models/directory/directory.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

class TagPickerPage extends StatelessWidget {
  final TextEditingController filterTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final tagList = TagList(filterTextController);
    return NavScaffold(
        filterTextController: filterTextController,
        title: Text(locale.tr(context).newBroadcast),
        body: Container(
            padding: EdgeInsets.all(5),
            color: FamedlyColors.light_grey,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(5)),
              child: tagList,
            )),
        actions: [IconButton(
            icon: Icon(
              FamedlyIcons.tick,
              color: Colors.black87,
            ),
            onPressed: () async {
              List<String> tags = [];
              for (final tag in tagList.getTagList()) {
                if (tag.checked) {
                  tags.add(tag.id);
                }
              }
              if (tags.length == 0) {
                return; // nothing is selected
              }
              // we have now the array of the selected tag IDs in tags
              Directory directory = Directory(Matrix.of(context).client);
              final List<Organisation> organisations = await directory.search(tags);
              List<Contact> contacts = [];
              for (final organisation in organisations) {
                for (final contact in organisation.contacts) {
                  if (contact.matrixId != null) {
                    contacts.add(contact);
                  }
                }
              }
              // now we have all the contacts we want to contact!
              Navigator.of(context).pushNamed("/newBroadcast/verify",
                arguments: contacts);
            },
        )]);
  }
}
