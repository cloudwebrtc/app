import 'package:circular_check_box/circular_check_box.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/models/tasks/subtask.dart';
import 'package:famedly/models/tasks/task.dart';
import 'package:famedly/models/tasks/tasklist.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';

/// This wraps the Tab view into a view.
/// It includes the AppBar as well as the Tab View and the Fab Menu
class TaskPage extends StatefulWidget {
  final String id;

  TaskPage({this.id});

  @override
  State<StatefulWidget> createState() => TaskPageState();
}

class TaskPageState extends State<TaskPage> {
  TaskList taskList;
  Task task;

  final TextEditingController descController = TextEditingController();
  final TextEditingController subTaskController = TextEditingController();

  List<Widget> get SubtaskListTiles {
    List<Widget> subtaskListTiles = [];
    for (int i = 0; i < task.subtask.length; i++) {
      Subtask subtask = task.subtask[i];
      subtaskListTiles.add(ListTile(
          leading: CircularCheckBox(
            value: subtask.done,
            activeColor: FamedlyColors.aqua_marine,
            materialTapTargetSize: MaterialTapTargetSize.padded,
            onChanged: (bool newChecked) => CustomDialogs(context)
                .tryRequestWithLoadingDialogs(subtask.setDone(newChecked)),
          ),
          title: Text(
            subtask.title,
            style: TextStyle(
                decoration: subtask.done
                    ? TextDecoration.lineThrough
                    : TextDecoration.none),
          )));
    }
    subtaskListTiles.add(TextField(
      key: Key("SubTaskTextField"),
      controller: subTaskController,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(17.5),
        hintText: locale.tr(context).addSubtask,
        hintStyle: TextStyle(color: FamedlyColors.metallic_blue, fontSize: 15),
        border: InputBorder.none,
        disabledBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        focusedBorder: InputBorder.none,
        focusedErrorBorder: InputBorder.none,
      ),
      onEditingComplete: () {
        CustomDialogs(context).tryRequestWithLoadingDialogs(
            task.addSubtask(subTaskController.text));
        subTaskController.clear();
      },
    ));
    return subtaskListTiles;
  }

  @override
  void dispose() {
    if (descController.text.isNotEmpty &&
        descController.text != task.description) {
      task.setDescription(descController.text);
    }
    if (subTaskController.text.isNotEmpty) {
      task.addSubtask(subTaskController.text);
    }
    super.dispose();
  }

  Widget build(BuildContext context) {
    if (taskList == null) {
      taskList = TaskList(Matrix.of(context).client);
      taskList.onUpdate = () {
        if (mounted) setState(() {});
      };
    }
    final int id = int.parse(widget.id);
    if (id >= taskList.tasks.length)
      return Scaffold(
        appBar: AppBar(
          elevation: 0,
        ),
        body: Center(
          child: Text(locale.tr(context).noTasks),
        ),
      );
    task = taskList.tasks[int.parse(widget.id)];

    return Scaffold(
        key: Key("taskPage"),
        appBar: AppBar(
          elevation: 0,
          title: Text(task.title),
          actions: <Widget>[
            IconButton(
              key: Key("DeleteIconButton"),
              icon: Icon(Icons.delete, color: FamedlyColors.grapefruit),
              onPressed: () async {
                final success = await CustomDialogs(context)
                    .tryRequestWithLoadingDialogs(task.remove());
                if (success != false) Navigator.of(context).pop();
              },
            )
          ],
        ),
        body: ListView(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.all(30),
                child: Text(task.title,
                    style: TextStyle(
                        color: FamedlyColors.slate,
                        fontSize: 19,
                        fontWeight: FontWeight.bold))),
            TextField(
              key: Key("DescriptionTextField"),
              controller: descController,
              decoration: InputDecoration(
                  hintText: task.description.isEmpty
                      ? locale.tr(context).addDescription
                      : task.description,
                  hintStyle: TextStyle(
                      color: FamedlyColors.metallic_blue, fontSize: 15),
                  border: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  focusedErrorBorder: InputBorder.none,
                  prefixIcon: Padding(
                    padding: EdgeInsets.only(left: 30, right: 18, bottom: 5),
                    child: Icon(Icons.menu, color: FamedlyColors.slate),
                  )),
              onEditingComplete: () async => CustomDialogs(context)
                  .tryRequestWithLoadingDialogs(
                      task.setDescription(descController.text)),
            ),
            ListTile(
              leading: Padding(
                  padding: EdgeInsets.only(top: 15, left: 15, bottom: 15),
                  child:
                      Icon(Icons.calendar_today, color: FamedlyColors.slate)),
              title: task.date == null
                  ? Text(locale.tr(context).addDate,
                      style: TextStyle(
                          color: FamedlyColors.metallic_blue, fontSize: 15))
                  : Align(
                      alignment: Alignment.centerLeft,
                      child: DateDisplay(context, task)),
              onTap: () {
                DatePicker.showDateTimePicker(context,
                    theme: DatePickerTheme(
                        itemStyle: TextStyle(color: FamedlyColors.cloudy_blue),
                        doneStyle: TextStyle(color: FamedlyColors.aqua_marine)),
                    showTitleActions: true,
                    onConfirm: (date) => CustomDialogs(context)
                        .tryRequestWithLoadingDialogs(task.setDate(date)),
                    currentTime: DateTime.now(),
                    locale: LocaleType.de);
              },
            ),
            Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 15, left: 30, bottom: 15),
                      child: Icon(Icons.arrow_forward,
                          color: FamedlyColors.slate)),
                  Expanded(
                    child: Container(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: SubtaskListTiles,
                      ),
                    ),
                  ),
                ]),
          ],
        ));
  }

  Widget DateDisplay(BuildContext context, Task task) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            border: Border.all(color: FamedlyColors.cloudy_blue)),
        child: Padding(
            padding: EdgeInsets.all(9),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(DateFormat("dd.MM.yyyy hh:mm").format(task.date),
                      style: TextStyle(
                          color: FamedlyColors.aqua_marine, fontSize: 15)),
                ),
                InkWell(
                    onTap: () => CustomDialogs(context)
                        .tryRequestWithLoadingDialogs(task.setDate(null)),
                    child: Icon(Icons.clear)),
              ],
            )));
  }
}
