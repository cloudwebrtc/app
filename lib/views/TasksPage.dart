import 'package:circular_check_box/circular_check_box.dart';
import 'package:famedly/components/AddTaskModal.dart';
import 'package:famedly/components/DateDisplay.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/NavScaffold.dart';

import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/models/tasks/subtask.dart';
import 'package:famedly/models/tasks/task.dart';
import 'package:famedly/models/tasks/tasklist.dart';
import 'package:famedly/styles/colors.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:flutter/material.dart';

/// This wraps the Tab view into a view.
/// It includes the AppBar as well as the Tab View and the Fab Menu
class TasksPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TasksPageState();
}

class TasksPageState extends State<TasksPage> {
  TaskList taskList;
  bool showDone = false;

  final TextEditingController textController = TextEditingController();

  void showNewTaskModal(TaskList taskList) {
    showModalBottomSheet<void>(
        isScrollControlled: true,
        context: context,
        backgroundColor: Colors.transparent,
        builder: (BuildContext innerContext) {
          return AddTaskModal(taskList);
        });
  }

  Widget TaskListItem(BuildContext context, Task task) {
    return Column(
      children: <Widget>[
        ListTile(
            key: Key("TaskListTile"),
            title: Text(
              task.title,
              style: TextStyle(
                  decoration: task.done
                      ? TextDecoration.lineThrough
                      : TextDecoration.none),
            ),
            subtitle: task.description.isEmpty &&
                    task.date == null &&
                    task.subtask.length == 0
                ? null
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                        task.description.isEmpty
                            ? Container()
                            : Padding(
                                padding: EdgeInsets.only(top: 5, bottom: 5),
                                child: Text(task.description)),
                        task.date == null
                            ? Container()
                            : DateDisplay(task.date),
                        task.subtask.length > 0
                            ? Container(
                                margin: EdgeInsets.only(top: 20),
                                height: 1,
                                color: FamedlyColors.pale_grey)
                            : Container(),
                        task.subtask.length > 0
                            ? Container(
                                width: MediaQuery.of(context).size.width - 60,
                                height: task.subtask.length * 55.0,
                                child: ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: task.subtask.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    Subtask subtask = task.subtask[index];
                                    return ListTile(
                                        leading: CircularCheckBox(
                                          value: subtask.done,
                                          activeColor:
                                              FamedlyColors.aqua_marine,
                                          materialTapTargetSize:
                                              MaterialTapTargetSize.padded,
                                          onChanged: (bool newChecked) =>
                                              CustomDialogs(context)
                                                  .tryRequestWithLoadingDialogs(
                                                      subtask
                                                          .setDone(newChecked)),
                                        ),
                                        title: Text(
                                          subtask.title,
                                          style: TextStyle(
                                              decoration: subtask.done
                                                  ? TextDecoration.lineThrough
                                                  : TextDecoration.none),
                                        ));
                                  },
                                ))
                            : Container(),
                      ]),
            leading: CircularCheckBox(
              key: Key("TagListCheckBox"),
              value: task.done,
              activeColor: FamedlyColors.aqua_marine,
              materialTapTargetSize: MaterialTapTargetSize.padded,
              onChanged: (bool newChecked) => CustomDialogs(context)
                  .tryRequestWithLoadingDialogs(task.setDone(newChecked)),
            ),
            onTap: () {
              Navigator.of(context).pushNamed("/tasks/${task.id}");
            }),
        Container(
            height: 0.0 +
                (task.description.isEmpty ? 0 : 5) +
                (task.date == null ? 0 : 5),
            color: Colors.transparent),
        divider()
      ],
    );
  }

  Widget divider() => Row(
        children: <Widget>[
          Container(
            width: 80,
            height: 1,
            color: Colors.transparent,
          ),
          Expanded(
            child: Container(
              height: 1,
              color: FamedlyColors.pale_grey,
            ),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    MatrixState matrix = Matrix.of(context);

    if (taskList == null) {
      taskList = TaskList(matrix.client);
      taskList.onUpdate = () {
        if (mounted) setState(() {});
      };
    }

    return NavScaffold(
      key: Key("TasksPage"),
      activePage: ActivePage.TASKS,
      appBar: AppBar(
        elevation: 0,
        title: Text(locale.tr(context).myTasks),
        actions: <Widget>[
          IconButton(
            key: Key("AddTaskIconButton"),
            icon: Icon(FamedlyIcons.plus, color: FamedlyColors.slate),
            onPressed: () {
              showNewTaskModal(taskList);
            },
          ),
          PopupMenuButton(
            key: Key("PopupMenu"),
            onSelected: (result) async {
              switch (result) {
                case "clear":
                  List<Task> newList = List<Task>.from(taskList.tasks);
                  newList.removeWhere((Task t) => t.done);
                  await CustomDialogs(context).tryRequestWithLoadingDialogs(
                      taskList.updateTaskList(newList));
                  break;
              }
            },
            icon: Icon(
              FamedlyIcons.menu,
              color: Colors.black87,
            ),
            itemBuilder: (BuildContext context) => <PopupMenuEntry>[
              PopupMenuItem(
                key: Key("ClearPopupMenuItem"),
                value: "clear",
                child: Text(locale.tr(context).clearCompletedTasks),
              )
            ],
          ),
        ],
      ),
      body: Builder(builder: (context) {
        bool allDoneDisplayed = false;
        List<Task> tasks = []..addAll(taskList.tasks);
        int doneCount = 0;
        for (int n = 0; n < tasks.length; n++) if (tasks[n].done) doneCount++;
        tasks.sort((t1, t2) => (t1.done ? 1 : 0).compareTo((t2.done ? 1 : 0)));

        if (tasks.isEmpty) {
          return Center(
            child: Text(locale.tr(context).noTasks),
          );
        }

        return ListView.builder(
          itemCount: tasks.length + 1,
          itemBuilder: (context, index) {
            if ((allDoneDisplayed && !showDone) ||
                (!allDoneDisplayed && index >= tasks.length))
              return Container();
            Task task = tasks[index - (allDoneDisplayed ? 1 : 0)];
            if (task.done && !allDoneDisplayed) {
              allDoneDisplayed = true;
              return Column(
                children: <Widget>[
                  Container(height: 1, color: FamedlyColors.pale_grey),
                  Container(height: 10, color: Colors.transparent),
                  Container(height: 1, color: FamedlyColors.pale_grey),
                  ListTile(
                    key: Key("DoneListTile"),
                    title: Text("Erledigt ($doneCount)"),
                    trailing: Icon(showDone
                        ? Icons.keyboard_arrow_up
                        : Icons.keyboard_arrow_down),
                    onTap: () {
                      setState(() {
                        showDone = !showDone;
                      });
                    },
                  )
                ],
              );
            }
            return TaskListItem(context, task);
          },
        );
      }),
    );
  }
}
