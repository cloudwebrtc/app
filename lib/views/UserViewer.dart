import 'dart:async';

import 'package:famedly/components/ProfileScaffold.dart';
import 'package:famedly/styles/famedly_icons_icons.dart';
import 'package:famedly/utils/CustomDialogs.dart';
import 'package:famedly/components/UserSettingsPopupMenu.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';

import '../components/Matrix.dart';
import '../components/SettingsMenuItem.dart';
import '../config/FamedlyLocalizations.dart';

class UserViewer extends StatefulWidget {
  final String roomID;
  final String mxid;

  const UserViewer(
    this.roomID,
    this.mxid, {
    Key key,
  }) : super(key: key);

  @override
  UserViewState createState() => UserViewState();
}

class UserViewState extends State<UserViewer> {
  ScrollController _scrollController = ScrollController();
  double offset = 0.0;

  @override
  void initState() {
    super.initState();
    _scrollController
      ..addListener(() {
        setState(() {
          offset = _scrollController.offset;
        });
      });
  }

  StreamSubscription memberChangeSub;

  @override
  void dispose() {
    memberChangeSub?.cancel();
    _scrollController
        .dispose(); // it is a good practice to dispose the controller
    super.dispose();
  }

  Future<User> loadUser(MatrixState matrix, Room room) {
    return room.getUserByMXID(widget.mxid);
  }

  Future startDirectChat(BuildContext context, User user) async {
    final roomID = await CustomDialogs(context)
        .tryRequestWithLoadingDialogs(user.startDirectChat());
    if (roomID != false)
      Navigator.of(context)
          .pushNamedAndRemoveUntil("/room/$roomID", (route) => route.isFirst);
  }

  @override
  Widget build(BuildContext context) {
    MatrixState matrix = Matrix.of(context);

    Room room = matrix.client.getRoomById(widget.roomID);

    memberChangeSub ??= matrix.client.connection.onEvent.stream
        .where((e) =>
            e.roomID == widget.roomID &&
            ["m.room.member", "m.room.power_levels"].contains(e.eventType))
        .listen((e) => setState(() {}));

    return FutureBuilder<User>(
      future: loadUser(matrix, room),
      builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
        if (snapshot.hasError)
          debugPrint("error: " + snapshot.error.toString());
        if (!snapshot.hasData)
          return Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(),
              ));
        User user = snapshot.data;
        final UserSettingsPopupMenu userSettingsPopupMenu =
            UserSettingsPopupMenu(context, user);
        return ProfileScaffold(
          key: Key("UserViewer"),
          profileContent: user.avatarUrl,
          profileName: user.calcDisplayname(),
          title: Text(
            user.calcDisplayname(),
          ),
          actions: userSettingsPopupMenu.hasActions
              ? [
                  userSettingsPopupMenu,
                ]
              : null,
          buttons: [
            ProfileScaffoldButton(
              key: Key("ChatProfileScaffoldButton"),
              iconData: FamedlyIcons.chat,
              onTap: () {
                this.startDirectChat(context, user);
              },
            ),
            ProfileScaffoldButton(
              key: Key("CallProfileScaffoldButton"),
              iconData: Icons.phone,
              onTap: () {
                Navigator.of(context).pushNamed("/call/room/");
              },
            ),
            ProfileScaffoldButton(
                key: room.pushRuleState == PushRuleState.notify
                    ? Key("NotificationOnProfileScaffoldButton")
                    : Key("NotificationOffProfileScaffoldButton"),
                iconData: room.pushRuleState == PushRuleState.notify
                    ? FamedlyIcons.notification
                    : FamedlyIcons.notifyOff,
                onTap: () async {
                  PushRuleState newState = PushRuleState.notify;
                  if (room.pushRuleState == PushRuleState.notify) {
                    newState = PushRuleState.mentions_only;
                  }
                  final Future pushRuleUpdate = room
                      .client.connection.onUserEvent.stream
                      .where((u) => u.eventType == "m.push_rules")
                      .first;
                  final success = await CustomDialogs(context)
                      .tryRequestWithLoadingDialogs(
                          room.setPushRuleState(newState));
                  if (success == false) return;
                  await pushRuleUpdate;
                  setState(() {});
                }),
          ],
          body: ListView(
            controller: _scrollController,
            shrinkWrap: true,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 20),
                color: Theme.of(context).brightness == Brightness.light
                    ? Color(0xfff6f7f7)
                    : Colors.transparent,
                child: Column(
                  children: <Widget>[
                    /* Reactivate if implemented
                            // Information Section
                            Align(
                              alignment: Alignment.topLeft,
                              child: Padding(
                                padding: EdgeInsets.only(left: 15, bottom: 10),
                                child: Text(
                                  "Informationen",
                                  style: TextStyle(
                                    color: Color(0xff95acb9),
                                    fontSize: 13,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.normal,
                                    letterSpacing: 0,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                left: 5,
                                right: 5,
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: Column(
                                  children: <Widget>[
                                    InformationListItem(
                                        section: "Beruf",
                                        content: locale
                                            .tr(context)
                                            .departmentPlaceholder),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          right: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              14),
                                      child: Container(
                                        height: 1,
                                        color: Colors.white,
                                      ),
                                    ),
                                    InformationListItem(
                                      section: "Arbeitsplatz",
                                      content: "Placeholder",
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          right: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              14),
                                      child: Container(
                                        height: 1,
                                        color: Colors.white,
                                      ),
                                    ),
                                    InformationListItem(
                                      section: "Abteilung",
                                      content: "Placeholder",
                                    ),
                                  ],
                                ),
                              ),
                            ),*/

                    // Media Section
                    Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: EdgeInsets.only(top: 20, left: 15, bottom: 10),
                        child: Text(
                          locale.tr(context).mediaTitle,
                          style: TextStyle(
                            color: Color(0xff95acb9),
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            fontStyle: FontStyle.normal,
                            letterSpacing: 0,
                          ),
                        ),
                      ),
                    ),
                    // TODO calculate number of images in a room
                    Builder(builder: (context) {
                      return Padding(
                        padding: EdgeInsets.only(
                          left: 5,
                          right: 5,
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Column(
                            children: <Widget>[
                              SettingsMenuItem(
                                Icon(
                                  FamedlyIcons.camera,
                                  color: Color(0xff95acb9),
                                ),
                                locale.tr(context).imagesTitle,
                                Container(),
                                () {
                                  CustomDialogs(context).showSnackBar(null,
                                      locale.tr(context).notYetImplemented);
                                  /*AppRouter().router().navigateTo(context,
                                                "/room/${widget.roomID}/${widget.mxid}/media/images");*/
                                },
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    right:
                                        MediaQuery.of(context).size.width - 60),
                                child: Container(
                                  height: 1,
                                  color: Theme.of(context).brightness ==
                                          Brightness.light
                                      ? Colors.white
                                      : Colors.transparent,
                                ),
                              ),
                              /* TODO reactivate if implemented
                                        // TODO calculate number of videos in a room
                                        SettingsMenuItem(
                                          Icon(
                                            Icons.videocam,
                                            color: Color(0xff95acb9),
                                          ),
                                          locale.tr(context).videosTitle,
                                          Container(),
                                          () {},
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              right: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  60),
                                          child: Container(
                                            height: 1,
                                            color: Colors.white,
                                          ),
                                        ),
                                        // TODO calculate number of files in a room
                                        SettingsMenuItem(
                                          Icon(
                                            Icons.insert_drive_file,
                                            color: Color(0xff95acb9),
                                          ),
                                          locale.tr(context).filesTitle,
                                          Container(),
                                          () {},
                                        ),*/
                            ],
                          ),
                        ),
                      );
                    }),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class InformationListItem extends StatelessWidget {
  final String section;
  final String content;

  const InformationListItem(
      {Key key, @required this.section, @required this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      elevation: 0,
      child: InkWell(
        child: Container(
          padding: EdgeInsets.only(left: 15, top: 15),
          height: 68,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Text(
                    this.section,
                    style: TextStyle(
                      color: Color(0xff95acb9),
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                    ),
                  ),
                ),
                Text(
                  this.content,
                  style: TextStyle(
                    color: Color(0xff4e6e81),
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.normal,
                    letterSpacing: 0,
                  ),
                )
              ],
            ),
          ),
        ),
        onTap: () {
          /*NO-OP*/
        },
      ),
    );
  }
}
