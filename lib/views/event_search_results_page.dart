import 'dart:async';

import 'package:famedly/components/ClickableAppBar.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedly/components/MatrixEventList.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:famedly/config/FamedlyLocalizations.dart';

class EventSearchResultsPage extends StatefulWidget {
  final String id;
  final String searchQuery;

  EventSearchResultsPage({Key key, this.id, this.searchQuery})
      : super(key: key);

  @override
  EventSearchResultsPageState createState() => EventSearchResultsPageState();
}

class EventSearchResultsPageState extends State<EventSearchResultsPage> {
  Room room;

  MatrixState matrix;

  StreamSubscription roomNotFoundSub;

  void loadRoom() {
    if (room == null) {
      room = matrix.client.getRoomById(widget.id);
      if (room == null) {
        // It is possible that we join a room which is not yet synced
        roomNotFoundSub = matrix.client.connection.onEvent.stream
            .where((EventUpdate r) => r.roomID == widget.id)
            .listen((EventUpdate r) {
          room = null;
          loadRoom();
          setState(() {});
          roomNotFoundSub.cancel();
        });
        room = Room(id: widget.id, client: matrix.client);
      }
      room.onUpdate = () {
        if (mounted) setState(() {});
      };
    }
  }

  @override
  void dispose() {
    matrix.activeRoomID = null;
    matrix.roomsPlugin.onLeaveRoom = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    matrix = Matrix.of(context);

    matrix.roomsPlugin.onLeaveRoom = (String roomID) {
      if (roomID == room.id) {
        setState(() {
          room.membership = Membership.leave;
        });
      }
    };

    loadRoom();

    return Scaffold(
      key: Key("EventSearchResultsPage"),
      appBar: ClickableAppBar(
        appBar: AppBar(
          title: Text(locale.tr(context).searchResults),
        ),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: MatrixEventList(
                room,
                searchQuery: widget.searchQuery,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
