import 'package:famedly/components/Dots.dart';
import 'package:famedly/components/IntroPage.dart';

import 'package:famedly/config/FamedlyLocalizations.dart';
import 'package:famedly/styles/colors.dart';
import 'package:flutter/material.dart';

class Intro extends StatefulWidget {
  @override
  IntroState createState() => new IntroState();
}

class IntroState extends State<Intro> {
  int _pageNumber = 0;
  ScrollPhysics _swipePhysics = PageScrollPhysics();

  PageController _controller = PageController();

  @override
  Widget build(BuildContext context) {
    List<Widget> _pages = [
      new IntroPage(
        color: FamedlyColors.pale_grey_two,
        title: locale.tr(context).introTitle1,
        text1: locale.tr(context).introPage1_1,
        text2: locale.tr(context).introPage1_2,
        image: "assets/images/startup.svg",
      ),
      new IntroPage(
        color: FamedlyColors.pale_grey_two,
        title: locale.tr(context).introTitle2,
        text1: locale.tr(context).introPage2_1,
        text2: locale.tr(context).introPage2_2,
        image: "assets/images/speech-bubble.svg",
      ),
      new IntroPage(
        color: FamedlyColors.pale_grey_two,
        title: locale.tr(context).introTitle3,
        text1: locale.tr(context).introPage3_1,
        text2: locale.tr(context).introPage3_2,
        image: "assets/images/union.svg",
      ),
    ];

    return new Stack(
      key: Key("IntroPage"),
      children: <Widget>[
        new PageView.builder(
          itemBuilder: (BuildContext context, int index) {
            return _pages[index];
          },
          itemCount: _pages.length,
          controller: _controller,
          physics: _swipePhysics,
          onPageChanged: (int page) {
            setState(() {
              this._pageNumber = page;
              if (this._pageNumber + 1 == _pages.length) {
                _swipePhysics = NeverScrollableScrollPhysics();
              }
            });
          },
        ),
        Container(
          alignment: Alignment.bottomLeft,
          child: Visibility(
            visible: !(this._pageNumber + 1 == _pages.length),
            child: FlatButton(
              key: Key("SkipButton"),
              onPressed: () {
                Navigator.of(context)
                    .pushNamedAndRemoveUntil("/rooms", (Route r) => false);
              },
              child: Text(locale.tr(context).skip),
            ),
          ),
        ),
        Container(
          alignment: Alignment.bottomCenter,
          margin: EdgeInsets.all(12.0),
          child: Visibility(
            visible: !(this._pageNumber + 1 == _pages.length),
            child: Dots(
              dotActiveColor: Theme.of(context).primaryColor,
              numberOfDot: _pages.length,
              position: _pageNumber,
            ),
          ),
        ),
        Container(
          alignment: Alignment.bottomRight,
          child: FlatButton(
            key: Key("NextButton"),
            onPressed: () {
              if (this._pageNumber + 1 == _pages.length) {
                Navigator.of(context)
                    .pushNamedAndRemoveUntil("/rooms", (Route r) => false);
              } else {
                _controller.nextPage(
                    duration: Duration(milliseconds: 200), curve: Curves.ease);
              }
            },
            child: Text(locale.tr(context).next),
          ),
        ),
      ],
    );
  }
}
