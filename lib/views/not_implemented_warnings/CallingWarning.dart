import 'package:flutter/material.dart';

import './Warning.dart';
import '../../config/FamedlyLocalizations.dart';

class CallingWarningView extends NotImplementedWarning {
  CallingWarningView(String matrixType, String id) : super(matrixType, id);

  @override
  String warningDescription(BuildContext context) =>
      locale.tr(context).call_warning_text;

  @override
  String warningTitle(BuildContext context) =>
      locale.tr(context).call_warning_title;
}
