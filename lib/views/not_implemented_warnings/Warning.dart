import 'package:famedly/styles/fonts.dart';
import 'package:flutter/material.dart';

import '../../config/FamedlyLocalizations.dart';

abstract class NotImplementedWarning extends StatelessWidget {
  String warningTitle(BuildContext context);

  String warningDescription(BuildContext context);

  /// Check if either Room or User to handle ID correct
  final String matrixType;

  final String id;

  NotImplementedWarning(this.matrixType, this.id, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key("NotImplementedPage"),
      appBar: AppBar(
        title: Text(warningTitle(context)),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 200,
            width: 200,
            decoration: BoxDecoration(
              color: Colors.blueGrey.shade200,
              shape: BoxShape.circle,
            ),
            child: Icon(
              Icons.priority_high,
              size: 200,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(48),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  locale.tr(context).notice,
                  style: const TextStyle(
                      color: Colors.black87,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      fontSize: 24.0),
                ),
                SizedBox(
                  height: 16,
                ),
                Text(
                  this.warningDescription(context),
                  style: Fonts.headline,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
