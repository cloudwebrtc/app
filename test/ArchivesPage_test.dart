import 'package:famedly/views/ArchivesPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'Utils.dart';

void main() {
  /// All Tests related to the RoomsPage
  group("ArchivesPage", () {
    final TestObserver observer = TestObserver()
      ..onPushed = (Route<dynamic> route, Route<dynamic> previousRoute) {}
      ..onPopped = (Route<dynamic> route, Route<dynamic> previousRoute) {};

    /// Check if Page has title
    testWidgets("Check if Page has title", (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          Utils.getWidgetWrapper(
            ArchivesPage(),
            observer,
          ),
        );

        await tester.pump(Duration.zero);

        expect(find.text("Archive"), findsOneWidget); // First Item
      });
    });
/*
    /// Check the remove dialog
    testWidgets("Check the remove dialog", (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          Utils.getWidgetWrapper(
            ArchivesPage(),
            observer,
          ),
        );

        await tester.pump(Duration.zero);

        await Utils.tapItem(tester, Key("PopupMenu"));
        expect(find.text("Clear archive"), findsOneWidget);
        await Utils.tapItem(tester, Key("Clear"));

        await tester.pump(Duration.zero);

        // Tap on clear archive and check the dialog box
        expect(find.text("The deletion can not be undone. Delete anyway?"),
            findsOneWidget);
        expect(find.text("Back".toUpperCase()), findsOneWidget);
        expect(find.text("Clear".toUpperCase()), findsOneWidget);

        // Close the dialog
        await Utils.tapItem(tester, Key("BackButtonInClearDialog"));
        expect(find.text("The deletion can not be undone. Delete anyway?"),
            findsNothing);
      });
    });

    // Test clearedDialog
    testWidgets("Check ClearedDialog", (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          Utils.getWidgetWrapper(
            ArchivesPage(),
            observer,
          ),
        );

        await tester.pump(Duration.zero);

        await Utils.tapItem(tester, Key("PopupMenu"));
        expect(find.text("Clear archive"), findsOneWidget);
        await Utils.tapItem(tester, Key("Clear"));

        expect(find.text("The deletion can not be undone. Delete anyway?"),
            findsOneWidget);

        // Fixme requires Sqflite
        //await Utils.tapItem(tester, Key("_clearedDialog"));

        // Check if all items got added
        //expect(find.text("Archive cleared"), findsOneWidget);
        expect(find.text("Back".toUpperCase()), findsOneWidget);
      });
    });*/
  });
}
