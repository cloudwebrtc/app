import 'package:famedly/views/not_implemented_warnings/CallingWarning.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'Utils.dart';

void main() {
  /// All Tests related to the CallingWarning

  group(
    "CallingWarning",
    () {
      /// RoomList Test
      testWidgets(
        'has a title and description',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            final TestObserver observer = TestObserver()
              ..onPushed = (Route<dynamic> route,
                  Route<dynamic> previousRoute) {}
              ..onPopped =
                  (Route<dynamic> route, Route<dynamic> previousRoute) {};

            // Create widget
            await tester.pumpWidget(
              Utils.getWidgetWrapper(
                CallingWarningView("", ""),
                observer,
              ),
            );

            // Check if all items got added
            expect(
                find.text(
                    "Currently, calling via famedly is not yet supported. We will let you know when you can dispose of the old-fashioned telephone lists. :)"),
                findsOneWidget);
            expect(find.text("Calling"), findsOneWidget);
          });
        },
      );
    },
  );
}
