import 'package:famedly/components/ChatRoom/eventWidgets/emote_event_content.dart';
import 'package:famedly/components/ChatRoom/eventWidgets/event.dart';
import 'package:famedly/components/ChatRoom/eventWidgets/file_event_content.dart';
import 'package:famedly/components/ChatRoom/eventWidgets/image_event_content.dart';
import 'package:famedly/components/ChatRoom/eventWidgets/room_avatar_event_content.dart';
import 'package:famedly/components/ChatRoom/eventWidgets/room_create_event.dart';
import 'package:famedly/components/ChatRoom/eventWidgets/room_membership_event_content.dart';
import 'package:famedly/components/ChatRoom/eventWidgets/room_name_event_content.dart';
import 'package:famedly/components/ChatRoom/eventWidgets/room_topic_event_content.dart';
import 'package:famedly/components/ChatRoom/eventWidgets/state_event.dart';
import 'package:famedly/components/ChatRoom/eventWidgets/unknown_event_content.dart';
import 'package:famedlysdk/famedlysdk.dart' hide Event;
import 'package:famedlysdk/src/Event.dart' as MatrixEvent;
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'Utils.dart';

void main() {
  /// All Tests related to the Events

  group(
    "Test all events",
    () {
      Client client = Utils.defaultClient;

      final TestObserver observer = TestObserver()
        ..onPushed = (Route<dynamic> route, Route<dynamic> previousRoute) {}
        ..onPopped = (Route<dynamic> route, Route<dynamic> previousRoute) {};

      Finder findRichText(find, String textToMatch) {
        return find.byWidgetPredicate(
          (Widget widget) {
            if (widget is RichText) {
              if (widget.text.toPlainText() == textToMatch) return true;
              print("FOUND: ${widget.text.toPlainText()}");
              return false;
            }
            return false;
          },
          description: 'Rich text contains $textToMatch',
        );
      }

      /// Emote Event Tests
      testWidgets(
        'EmoteEvent',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            Room room = Room(id: "1234", client: client);
            final event = MatrixEvent.Event(
              eventId: "1234",
              senderId: "@alice:example.com",
              time: ChatTime.now(),
              room: room,
              typeKey: "m.room.message",
              content: {
                "msgtype": "m.emote",
                "body": "test",
              },
            );
            await tester.pumpWidget(
              Utils.getWidgetWrapper(
                Scaffold(
                  body: Event(
                    event,
                    room: room,
                    key: Key("TestEvent"),
                    content: EmoteEventContent(event),
                  ),
                ),
                observer,
              ),
            );

            await tester.pump(Duration.zero);

            // Is in a message bubble?
            expect(find.byKey(Key("MessageBubble")), findsOneWidget);
            expect(find.text("*alice test"), findsOneWidget);
          });
        },
      );

      /// File Event Tests
      testWidgets(
        'FileEvent',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            Room room = Room(id: "1234", client: client);
            final event = MatrixEvent.Event(
              eventId: "1234",
              senderId: "@alice:example.com",
              time: ChatTime.now(),
              room: room,
              typeKey: "m.room.message",
              content: {
                "msgtype": "m.file",
                "body": "test",
                "filename": "Testfile",
                "url": "example.org/1234"
              },
            );

            await tester.pumpWidget(
              Utils.getWidgetWrapper(
                Scaffold(
                  body: Event(
                    event,
                    room: room,
                    key: Key("TestEvent"),
                    content: FileEventContent(event),
                  ),
                ),
                observer,
              ),
            );

            await tester.pump(Duration.zero);

            // Is in a message bubble?
            expect(find.byKey(Key("MessageBubble")), findsOneWidget);
            expect(find.text("Testfile"), findsOneWidget);
          });
        },
      );

      /// Image Event Tests
      testWidgets(
        'ImageEvent',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            Room room = Room(id: "1234", client: client);
            final event = MatrixEvent.Event(
              eventId: "1234",
              senderId: "@alice:example.com",
              time: ChatTime.now(),
              room: room,
              typeKey: "m.room.message",
              content: {
                "msgtype": "m.image",
                "body": "test",
                "url": "example.org/1234"
              },
            );

            await tester.pumpWidget(
              Utils.getWidgetWrapper(
                Scaffold(
                  body: Event(
                    event,
                    room: room,
                    key: Key("TestEvent"),
                    content: ImageEventContent(event),
                  ),
                ),
                observer,
              ),
            );

            await tester.pump(Duration.zero);

            // Is in a message bubble?
            expect(find.byKey(Key("MessageBubble")), findsOneWidget);
          });
        },
      );

      /// RoomAvatar Event Tests
      testWidgets(
        'RoomAvatarEvent',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            Room room = Room(id: "1234", client: client);
            final event = MatrixEvent.Event(
              eventId: "1234",
              senderId: "@alice:example.com",
              time: ChatTime.now(),
              room: room,
              typeKey: "m.room.avatar",
              content: {"avatar_url": "example.org/1234"},
            );

            await tester.pumpWidget(
              Builder(
                builder: (BuildContext context) {
                  return Utils.getWidgetWrapper(
                    Scaffold(
                      body: StateEvent(
                        event,
                        room: room,
                        key: Key("TestEvent"),
                        content: roomAvatarEventContent(context, event),
                      ),
                    ),
                    observer,
                  );
                },
              ),
            );

            await tester.pump(Duration.zero);

            // Is in a message bubble?
            expect(find.byKey(Key("MessageBubble")), findsNothing);

            expect(findRichText(find, "alice changed the room avatar."),
                findsOneWidget);
          });
        },
      );

      /// RoomCreateEvent Tests
      testWidgets(
        'RoomCreateEvent',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            Room room = Room(id: "1234", client: client);
            final event = MatrixEvent.Event(
              eventId: "1234",
              senderId: "@alice:example.com",
              time: ChatTime.now(),
              room: room,
              typeKey: "m.room.create",
              content: {
                "creator": "@example:example.org",
              },
            );

            await tester.pumpWidget(
              Builder(
                builder: (BuildContext context) {
                  return Utils.getWidgetWrapper(
                    Scaffold(
                      body: StateEvent(
                        event,
                        room: room,
                        key: Key("TestEvent"),
                        content: roomCreateEventContent(context, event),
                      ),
                    ),
                    observer,
                  );
                },
              ),
            );

            await tester.pump(Duration.zero);

            // Is in a message bubble?
            expect(find.byKey(Key("MessageBubble")), findsNothing);

            expect(
                findRichText(find, "alice created the room."), findsOneWidget);
          });
        },
      );

      /// RoomMembershipEvent Tests
      testWidgets(
        'RoomMembershipEvent',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            Room room = Room(id: "1234", client: client);
            final event = MatrixEvent.Event(
              eventId: "1234",
              senderId: "@alice:example.com",
              time: ChatTime.now(),
              room: room,
              typeKey: "m.room.membership",
              stateKey: "@alice:example.com",
              content: {"membership": "join"},
            );

            await tester.pumpWidget(
              Builder(
                builder: (BuildContext context) {
                  return Utils.getWidgetWrapper(
                    Scaffold(
                      body: StateEvent(
                        event,
                        room: room,
                        key: Key("TestEvent"),
                        content: roomMembershipEventContent(context, event),
                      ),
                    ),
                    observer,
                  );
                },
              ),
            );

            await tester.pump(Duration.zero);

            // Is in a message bubble?
            expect(find.byKey(Key("MessageBubble")), findsNothing);

            expect(findRichText(find, "alice has joined the room."),
                findsOneWidget);
          });
        },
      );

      /// RoomNameEvent Tests
      testWidgets(
        'RoomNameEvent',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            Room room = Room(id: "1234", client: client);
            final event = MatrixEvent.Event(
              eventId: "1234",
              senderId: "@alice:example.com",
              time: ChatTime.now(),
              room: room,
              typeKey: "m.room.name",
              content: {"name": "testname"},
            );

            await tester.pumpWidget(
              Builder(
                builder: (BuildContext context) {
                  return Utils.getWidgetWrapper(
                    Scaffold(
                      body: StateEvent(
                        event,
                        room: room,
                        key: Key("TestEvent"),
                        content: roomNameEventContent(context, event),
                      ),
                    ),
                    observer,
                  );
                },
              ),
            );

            await tester.pump(Duration.zero);

            // Is in a message bubble?
            expect(find.byKey(Key("MessageBubble")), findsNothing);

            expect(
                findRichText(
                    find, 'alice changed the room name to "testname".'),
                findsOneWidget);
          });
        },
      );

      /// RoomTopicEvent Tests
      testWidgets(
        'RoomTopicEvent',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            Room room = Room(id: "1234", client: client);
            final event = MatrixEvent.Event(
              eventId: "1234",
              senderId: "@alice:example.com",
              time: ChatTime.now(),
              room: room,
              typeKey: "m.room.name",
              content: {"topic": "testtopic"},
            );

            await tester.pumpWidget(
              Builder(
                builder: (BuildContext context) {
                  return Utils.getWidgetWrapper(
                    Scaffold(
                      body: StateEvent(
                        event,
                        room: room,
                        key: Key("TestEvent"),
                        content: roomTopicEventContent(context, event),
                      ),
                    ),
                    observer,
                  );
                },
              ),
            );

            await tester.pump(Duration.zero);

            // Is in a message bubble?
            expect(find.byKey(Key("MessageBubble")), findsNothing);

            expect(
                findRichText(
                    find, 'alice changed the room topic to "testtopic".'),
                findsOneWidget);
          });
        },
      );

      /// UnknownEvent Tests
      testWidgets(
        'UnknownEvent',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            Room room = Room(id: "1234", client: client);
            final event = MatrixEvent.Event(
              eventId: "1234",
              senderId: "@alice:example.com",
              time: ChatTime.now(),
              room: room,
              typeKey: "m.room.unknown",
              content: {"strangekey": "strangevalue"},
            );

            await tester.pumpWidget(
              Builder(
                builder: (BuildContext context) {
                  return Utils.getWidgetWrapper(
                    Scaffold(
                      body: StateEvent(
                        event,
                        room: room,
                        key: Key("TestEvent"),
                        content: unknownEventContent(context, event),
                      ),
                    ),
                    observer,
                  );
                },
              ),
            );

            await tester.pump(Duration.zero);

            // Is in a message bubble?
            expect(find.byKey(Key("MessageBubble")), findsNothing);

            expect(
                findRichText(find,
                    'alice sent an unknown event of type "m.room.unknown".'),
                findsOneWidget);
          });
        },
      );
    },
  );
}
