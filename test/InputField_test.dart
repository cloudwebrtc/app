import 'package:famedly/components/ChatRoom/InputField.dart';
import 'package:famedly/components/Matrix.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart' hide expect;
import 'package:test/test.dart' show expect;

import 'Utils.dart';

class Listeners implements InputListener {
  String message;
  bool attachmentClicked = false;
  bool audioClicked = false;
  bool cameraClicked = false;

  @override
  onSend(String message) {
    this.message = message;
  }
}

void main() {
  /// All Tests related to the InputField
  group("InputField", () {
    /// Check if all Elements react to input
    testWidgets('should react on inputs', (WidgetTester tester) async {
      await tester.runAsync(() async {
        Listeners _listeners = Listeners();

        final TestObserver observer = TestObserver()
          ..onPushed = (Route<dynamic> route, Route<dynamic> previousRoute) {}
          ..onPopped = (Route<dynamic> route, Route<dynamic> previousRoute) {};

        await tester.pumpWidget(
          Utils.getWidgetWrapper(
            Builder(builder: (context) {
              return Scaffold(
                body: InputField(_listeners,
                    Room(id: "1234", client: Matrix.of(context).client)),
              );
            }),
            observer,
          ),
        );

        await tester.pump(Duration.zero);

        // Test Typing in Input
        await tester.enterText(find.byKey(Key("input")), "Test");
        await tester.testTextInput.receiveAction(TextInputAction.done);
        await tester.pump(Duration.zero);

        expect(_listeners.message, "Test");

        // Cleanup input Test
        await tester.enterText(find.byKey(Key("input")), "_"); // Reset Input
        await tester.testTextInput.receiveAction(TextInputAction.done);
        await tester.pump(Duration.zero);
        expect(_listeners.message, "_");

        /* Test if floatingActionButton Works
        1. Mic Action
        2. Submit
       */
        /* Reactivate when Audio Messages are possible

      await tester.tap(find.byType(FloatingActionButton));
      await tester.pump(Duration.zero);

      expect(_listeners.audioClicked, true);
      */

        await tester.enterText(find.byKey(Key("input")), "Test2");
        await tester.tap(find.byKey(Key("sendButton")));
        await tester.pump(Duration.zero);

        expect(_listeners.message, "Test2");

        // Test attachment button
        await tester.tap(find.byKey(Key("attachment")));
        await tester.pump(Duration.zero);

        //expect(_listeners.attachmentClicked, true);
      });
    });
  });
}
