import 'package:flutter_test/flutter_test.dart';
import 'package:famedly/views/intro.dart';
import 'package:flutter/material.dart';

import 'Utils.dart';

const Duration _frameDuration = Duration(milliseconds: 100);

void main() {
  /// All Tests related to the Intro
  group("Intro", () {
    final TestObserver observer = TestObserver()
      ..onPushed = (Route<dynamic> route, Route<dynamic> previousRoute) {}
      ..onPopped = (Route<dynamic> route, Route<dynamic> previousRoute) {};

    /// Check if all Pages are being created and are swipable
    testWidgets("should have all pages when swiping",
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          Utils.getWidgetWrapper(
            Intro(),
            observer,
          ),
        );

        // First Page
        await tester.pump();
        expect(find.text("Welcome"), findsOneWidget); // First Page Title

        // Second Page
        await tester.fling(
            find.byType(PageView), const Offset(-200.0, 0.0), 1000.0);
        await tester.pumpAndSettle(_frameDuration);
        expect(find.text("Talk to each other"),
            findsOneWidget); // Second Page Title

        // Third Page
        await tester.fling(
            find.byType(PageView), const Offset(-200.0, 0.0), 1000.0);
        await tester.pumpAndSettle(_frameDuration);
        expect(find.text("Create connections"),
            findsOneWidget); // Third Page Title
      });
    });

    /// Check if all Pages are being created and the Next Button Works
    testWidgets("should have all pages when tapping",
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(
          Utils.getWidgetWrapper(
            Intro(),
            observer,
          ),
        );

        // First Page
        await tester.pump();
        expect(find.text("Welcome"), findsOneWidget); // First Page Title

        // Second Page
        await tester.tap(find.byKey(Key("NextButton")));
        await tester.pumpAndSettle(_frameDuration);
        expect(find.text("Talk to each other"),
            findsOneWidget); // Second Page Title

        // Third Page
        await tester.tap(find.byKey(Key("NextButton")));
        await tester.pumpAndSettle(_frameDuration);
        expect(find.text("Create connections"),
            findsOneWidget); // Third Page Title
      });
    });
  });
}
