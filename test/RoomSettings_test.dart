import 'package:famedly/views/RoomSettings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'Utils.dart';

void main() {
  /// All Tests related to the RoomSettings
  group(
    "RoomSettings",
    () {
      /// GroupChatSettingsPage Test
      testWidgets(
        'works with GroupChatSettingsPage',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            final TestObserver observer = TestObserver()
              ..onPushed =
                  (Route<dynamic> route, Route<dynamic> previousRoute) {}
              ..onPopped =
                  (Route<dynamic> route, Route<dynamic> previousRoute) {};

            // Create widget
            await tester.pumpWidget(
              Utils.getWidgetWrapper(
                RoomSettings(
                  id: "!726s6s6q:example.com",
                ),
                observer,
              ),
            );

            await tester.pump();
            Utils.printWidgets(tester);

            // Check for MembersHeader
            //expect(find.text("Members"), findsOneWidget);

            // Check for MediaHeader
            //expect(find.text("Media"), findsOneWidget);

            // Check for Images Button
            //expect(find.text("Images"), findsOneWidget);

            /*// Check for Videos Button
          expect(find.text("Videos"), findsOneWidget);

          // Check for Files Button
          expect(find.text("Files"), findsOneWidget);*/

            // Check for Leave group Button
            //expect(find.text("Leave group"), findsOneWidget);
          });
        },
      );
    },
  );
}
