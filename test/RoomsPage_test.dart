//import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

//import 'Utils.dart';

void main() {
  /// All Tests related to the RoomsPage
  group("RoomsPage", () {
    /*final TestObserver observer = TestObserver()
      ..onPushed = (Route<dynamic> route, Route<dynamic> previousRoute) {}
      ..onPopped = (Route<dynamic> route, Route<dynamic> previousRoute) {};

    /// Check if BottomBar buttons are present
    testWidgets("should have BottomBar buttons", (WidgetTester tester) async {
      await tester.pumpWidget(
        Utils.getWidgetWrapper(
          RoomsPage(),
          observer,
        ),
      );

      await tester.pump();

      expect(find.byKey(Key("AnfragenButton")), findsOneWidget);
      expect(find.byKey(Key("ChatsButton")), findsOneWidget);
    });

    /// Check if AppBar buttons are present
    testWidgets("should have AppBar buttons", (WidgetTester tester) async {
      await tester.pumpWidget(
        Utils.getWidgetWrapper(
          RoomsPage(),
          observer,
        ),
      );

      await tester.pump();

      expect(find.byKey(Key("SettingsButton")), findsOneWidget);
      expect(
          find.byKey(
            Key("NotificationsButton"),
          ),
          findsOneWidget);
      expect(
          find.byKey(
            Key("WriteButton"),
          ),
          findsOneWidget);
    });*/

    /// Check if Room tapping works
    /// FIXME we don't load rooms apparently yet
    /*testWidgets("should open a room", (WidgetTester tester) async {
      final TestObserver roomOpeningObserver = TestObserver()
        ..onPushed = (Route<dynamic> route, Route<dynamic> previousRoute) {
          if (previousRoute != null) {
            expect(previousRoute.settings.name, RouteMatcher("/rooms"));
          }
          if (route != null) {
            expect(route.settings.name, RouteMatcher("/room/", contains: true));
          }
        }
        ..onPopped = (Route<dynamic> route, Route<dynamic> previousRoute) {};

      await tester.pumpWidget(
        Utils.getWidgetWrapper(
          ChatRoomsPage(),
          roomOpeningObserver,
        ),
      );

      await tester.pump();

      Utils.printWidgets(tester);
      Utils.tapItem(tester, Key("room_1"));
      // The expect happens in the observer
    });*/
  });
}
