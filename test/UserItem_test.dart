import 'package:famedly/components/UsersList/item.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'Utils.dart';

void main() {
  /// All Tests related to the UserItem

  group(
    "UserItem",
    () {
      /// RoomList Test
      testWidgets(
        'has a displayName',
        (WidgetTester tester) async {
          await tester.runAsync(() async {
            final TestObserver observer = TestObserver()
              ..onPushed = (Route<dynamic> route,
                  Route<dynamic> previousRoute) {}
              ..onPopped =
                  (Route<dynamic> route, Route<dynamic> previousRoute) {};

            // Create widget
            await tester.pumpWidget(
              Utils.getWidgetWrapper(
                Scaffold(
                  body: UserItem(
                    onTap: () {},
                    contact: User("@larodar:famedly.de",
                        displayName: "Larodar", avatarUrl: "placeholder"),
                  ),
                ),
                observer,
              ),
            );

            // Check if all items got added
            expect(find.text("Larodar"), findsOneWidget);
          });
        },
      );
    },
  );
}
