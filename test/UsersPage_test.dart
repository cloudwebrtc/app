void main() {
  // Fixme requires sqflite
  /*
  /// All Tests related to the ChatRoom

  List<User> contactList = DummyData().contacts;

  contactList.sort((a, b) => a.displayName.compareTo(b.displayName));

  group(
    "[UsersPage]",
    () {
      /// ContactsPage Test
      testWidgets(
        'ContactsPage has a working Invite Popup',
        (WidgetTester tester) async {
          final TestObserver observer = TestObserver()
            ..onPushed = (Route<dynamic> route, Route<dynamic> previousRoute) {}
            ..onPopped =
                (Route<dynamic> route, Route<dynamic> previousRoute) {};

          // Create widget
          await tester.pumpWidget(
            Utils.getWidgetWrapper(
              ContactsPage(),
              observer,
            ),
          );

          await Utils.tapItem(tester, Key("UserItem.1"));

          // Check if all items got added
          expect(
              find.text("Invite ${contactList[0].displayName} to this chat?"),
              findsOneWidget);
          expect(find.text("Back".toUpperCase()), findsOneWidget);
          expect(find.text("Invite".toUpperCase()), findsOneWidget);
        },
      );

      /// InvitePage Test
      testWidgets(
        'InvitePage has a working Invite Popup',
        (WidgetTester tester) async {
          final TestObserver observer = TestObserver()
            ..onPushed = (Route<dynamic> route, Route<dynamic> previousRoute) {}
            ..onPopped =
                (Route<dynamic> route, Route<dynamic> previousRoute) {};

          // Create widget
          await tester.pumpWidget(
            Utils.getWidgetWrapper(
              InvitePage(),
              observer,
            ),
          );

          await Utils.tapItem(tester, Key("UserItem.1"));

          // Check if all items got added
          expect(
              find.text("Invite ${contactList[0].displayName} to this chat?"),
              findsOneWidget);
          expect(find.text("Back".toUpperCase()), findsOneWidget);
          expect(find.text("Invite".toUpperCase()), findsOneWidget);
        },
      );

      /// NewGroupPage Test
      testWidgets(
        'NewGroupPage has a working Invite Popup',
        (WidgetTester tester) async {
          final TestObserver observer = TestObserver()
            ..onPushed = (Route<dynamic> route, Route<dynamic> previousRoute) {}
            ..onPopped =
                (Route<dynamic> route, Route<dynamic> previousRoute) {};

          // Create widget
          await tester.pumpWidget(
            Utils.getWidgetWrapper(
              NewGroupPage(),
              observer,
            ),
          );

          await Utils.tapItem(tester, Key("UserItem.1"));

          // Check if all items got added
          expect(
              find.text("Invite ${contactList[0].displayName} to this chat?"),
              findsOneWidget);
          expect(find.text("Back".toUpperCase()), findsOneWidget);
          expect(find.text("Invite".toUpperCase()), findsOneWidget);
        },
      );
    },
  );*/
}
