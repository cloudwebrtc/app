import 'package:famedly/models/directory/contact.dart';
import 'package:famedly/models/directory/directory.dart';
import 'package:famedly/models/directory/location.dart';
import 'package:famedly/models/directory/organisation.dart';
import 'package:famedly/models/directory/scope.dart';
import 'package:famedly/models/directory/subtype.dart';
import 'package:famedly/models/directory/tag.dart';
import 'package:famedly/models/directory/visibility.dart';
import 'package:famedly/utils/FakeDirectory.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter_test/flutter_test.dart';

import 'FakeMatrixApi.dart';

void main() {
  /// All Tests related to the tasks
  group("Directory", () {
    Client client = Client("testclient", debug: true);
    client.homeserver = "https://fakeServer.notExisting";
    client.connection.httpClient = FakeMatrixApi();
    client.connection.onUserEvent.stream.listen(client.handleUserUpdate);
    Directory directory = Directory(client);
    directory.httpClient = FakeDirectory();

    test('search', () async {
      List<Organisation> organisations = await directory.search();
      expect(organisations.length, 3);
      expect(organisations[0].id, "3e4d98f9-1544-4c0d-bce3-24509e92ba0b");
      expect(organisations[0].name, "Petrus Krankenhaus Wuppertal");
      expect(organisations[0].orgType, "4747daf1-acbc-4102-8f6e-52710c14d57e");
      expect(organisations[0].tags, [
        "02f1260e-d140-4a16-8a3a-db4716ee36d9",
        "7d35e4b4-b0a6-483a-a2da-7dfea9ee274d"
      ]);
      expect(organisations[0].children, [
        "3e4d98f9-1544-4c0d-bce3-24509e92ba0b",
        "3d8dffd4-5e03-4337-a016-16cffc290e0b"
      ]);

      Location location = organisations[0].location;
      expect(location.latitude, 52.81);
      expect(location.longitude, 14.32);
      expect(location.street, "some street");
      expect(location.number, "some number");
      expect(location.zipCode, "some zip code");
      expect(location.state, "some state");
      expect(location.country, "some country");

      Visibility visibility = organisations[0].visibility;
      expect(visibility.exclude, []);
      expect(visibility.include, ["*"]);

      expect(organisations[0].contacts.length, 2);
      Contact contact = organisations[0].contacts[0];
      expect(contact.description, "Allgemeinmedizin");
      expect(contact.passport, "1234pass");
      expect(contact.tags, [
        "3e4d98f9-1544-4c0d-bce3-24509e92ba0b",
        "3d8dffd4-5e03-4337-a016-16cffc290e0b"
      ]);
      expect(contact.uri, "https://matrix.to/#/@test:famedly.de");
      expect(contact.visibility.include, [
        "d9d923ad-1efe-4aa6-b66b-967f5bbf300b",
        "d865fcde-5223-4eea-a72c-1c76955b4e89"
      ]);
      expect(contact.visibility.exclude, ["*"]);
      expect(Directory.organisationsCache[organisations[0].id].id,
          organisations[0].id);
      expect(Directory.subtypeCache[organisations[0].orgType].id,
          organisations[0].orgType);
      for (int i = 0; i < organisations[0].tags.length; i++)
        expect(Directory.tagCache[organisations[0].tags[i]].id,
            organisations[0].tags[i]);
    });

    test('Request Organisations', () async {
      List<Organisation> organisations = await directory.getOrganisations();

      expect(organisations.length, 3);
      expect(organisations[0].id, "3e4d98f9-1544-4c0d-bce3-24509e92ba0b");
      expect(organisations[0].name, "Petrus Krankenhaus Wuppertal");
      expect(organisations[0].orgType, "4747daf1-acbc-4102-8f6e-52710c14d57e");
      expect(organisations[0].tags, [
        "02f1260e-d140-4a16-8a3a-db4716ee36d9",
        "7d35e4b4-b0a6-483a-a2da-7dfea9ee274d"
      ]);
      expect(organisations[0].children, [
        "3e4d98f9-1544-4c0d-bce3-24509e92ba0b",
        "3d8dffd4-5e03-4337-a016-16cffc290e0b"
      ]);

      Location location = organisations[0].location;
      expect(location.latitude, 52.81);
      expect(location.longitude, 14.32);
      expect(location.street, "some street");
      expect(location.number, "some number");
      expect(location.zipCode, "some zip code");
      expect(location.state, "some state");
      expect(location.country, "some country");

      Visibility visibility = organisations[0].visibility;
      expect(visibility.exclude, []);
      expect(visibility.include, ["*"]);

      expect(organisations[0].contacts.length, 2);
      Contact contact = organisations[0].contacts[0];
      expect(contact.description, "Allgemeinmedizin");
      expect(contact.passport, "1234pass");
      expect(contact.tags, [
        "3e4d98f9-1544-4c0d-bce3-24509e92ba0b",
        "3d8dffd4-5e03-4337-a016-16cffc290e0b"
      ]);
      expect(contact.uri, "https://matrix.to/#/@test:famedly.de");
      expect(contact.visibility.include, [
        "d9d923ad-1efe-4aa6-b66b-967f5bbf300b",
        "d865fcde-5223-4eea-a72c-1c76955b4e89"
      ]);
      expect(contact.visibility.exclude, ["*"]);
    });

    test('getOrganisation', () async {
      Organisation organisation = await directory
          .getOrganisation("3e4d98f9-1544-4c0d-bce3-24509e92ba0b");

      expect(organisation.id, "3e4d98f9-1544-4c0d-bce3-24509e92ba0b");
    });

    test('getScope', () async {
      Scope scope =
          await directory.getScope("3e4d98f9-1544-4c0d-bce3-24509e92ba0b");
      expect(scope.id, "3e4d98f9-1544-4c0d-bce3-24509e92ba0b");
      expect(scope.subjects, ".*");
    });

    test('getTags', () async {
      List<Tag> tags = await directory.getTags();

      expect(tags.length, 3);
      expect(tags[0].id, "02f1260e-d140-4a16-8a3a-db4716ee36d9");
      expect(tags[0].description, "Medikamentenbringdienst");
      expect(tags[1].id, "3e4d98f9-1544-4c0d-bce3-24509e92ba0b");
      expect(tags[1].description, "Pflegeübernahme");
    });

    test('getTag', () async {
      Tag tag = await directory.getTag("3e4d98f9-1544-4c0d-bce3-24509e92ba0b");

      expect(tag.id, "3e4d98f9-1544-4c0d-bce3-24509e92ba0b");
      expect(tag.description, "Pflegeübernahme");
    });

    test('getSubtypes', () async {
      List<Subtype> subTypes = await directory.getSubtypes();
      expect(subTypes.length, 2);
      expect(subTypes[0].id, "4747daf1-acbc-4102-8f6e-52710c14d57e");
      expect(subTypes[0].description, "Hospital");
    });
    test('getSubtype', () async {
      Subtype subType =
          await directory.getSubtype("4747daf1-acbc-4102-8f6e-52710c14d57e");
      expect(subType.id, "4747daf1-acbc-4102-8f6e-52710c14d57e");
      expect(subType.description, "Hospital");
    });
  });
}
