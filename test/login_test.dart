import 'package:famedly/views/LoginPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'Utils.dart';

void main() {
  /// All Tests related to the Login
  group("LoginPage", () {
    /// Check if all Elements get created
    testWidgets('should get created', (WidgetTester tester) async {
      await tester.runAsync(() async {
        final TestObserver observer = TestObserver()
          ..onPushed = (Route<dynamic> route, Route<dynamic> previousRoute) {}
          ..onPopped = (Route<dynamic> route, Route<dynamic> previousRoute) {};

        await tester.pumpWidget(
          Utils.getWidgetWrapper(
            LoginPage(),
            observer,
          ),
        );

        await tester.pump(Duration.zero);

        expect(find.text("Login with organisation"), findsOneWidget); // Title
        expect(find.text("Username"), findsOneWidget); // Benutzernamen Input
        expect(find.text("Password"), findsOneWidget); // Passwort Input
        expect(find.text("Login"), findsOneWidget); // Button Input
      });
    });
  });
}
