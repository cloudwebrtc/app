import 'package:famedly/models/requests/request.dart';
import 'package:famedly/models/requests/BroadcastContent.dart';
import 'package:famedly/models/requests/RequestContent.dart';
import 'package:famedly/models/requests/RequestStateContent.dart';
import 'package:famedly/models/directory/contact.dart';
import 'package:famedly/models/directory/visibility.dart';
import 'package:famedly/models/directory/directory.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter_test/flutter_test.dart';

import 'dart:convert';

import 'FakeMatrixApi.dart';

void main() {
  group("Requests", () {
    Client client = Client("testclient", debug: true);
    client.homeserver = "https://fakeServer.notExisting";
    client.userID = "@test:fakeServer.notExisting";
    client.connection.httpClient = FakeMatrixApi();
    client.connection.onUserEvent.stream.listen(client.handleUserUpdate);

    final nonRequestRoom = Room(
      id: "!nonrequest:localhost",
      client: client,
    );

    final requestInviteRoom = Room(
      id: "!invite:localhost",
      membership: Membership.invite,
      client: client,
    );
    requestInviteRoom.setState(RoomState(
      content: { "name": Request.mRequestNameSpace },
      typeKey: "m.room.name",
      stateKey: "",
    ));
    requestInviteRoom.setState(RoomState(
      content: { "membership": "invite" },
      typeKey: "m.room.member",
      stateKey: "@test:fakeServer.notExisting",
      senderId: "@inviter:localhost",
      room: requestInviteRoom,
    ));

    final requestRoom = Room(
      id: "!request:localhost",
      client: client,
    );
    requestRoom.setState(RoomState(
      content: RequestContent(
        creator: "@creator:localhost",
        requestingOrganisation: "matrix.org",
        requestedOrganisation: "sorunome.de",
        requestTitle: "Awesome Request",
        requestId: "03793e1b-f1fb-48e4-ae1d-d00ed043f546",
        organisationId: "c1261f71-8cf6-4878-b8c9-ee8e525ae56f",
        organisationName: "Some Hospital",
        contactId: "f54f216e-745a-4c57-8f4a-f9cee375c2ff",
        contactDescription: "Emergency takein",
        body: "Hey, anybody there?",
      ).toJson(),
      typeKey: Request.mRequestNameSpace,
      stateKey: "",
    ));

    final requestRoomOwner = Room(
      id: "!request:localhost",
      client: client,
    );
    requestRoomOwner.setState(RoomState(
      content: RequestContent(
        creator: "@test:fakeServer.notExisting",
        requestingOrganisation: "matrix.org",
        requestedOrganisation: "sorunome.de",
        requestTitle: "Awesome Request",
        requestId: "910d1916-9359-46b5-8900-0c1191e5d385",
        organisationId: "3e4d98f9-1544-4c0d-bce3-24509e92ba0b",
        organisationName: "Some Hospital",
        contactId: "83f42853-9530-40e3-a961-34ef00f6e09a",
        contactDescription: "Emergency takein",
        body: "Hey, anybody there?",
      ).toJson(),
      typeKey: Request.mRequestNameSpace,
      stateKey: "",
    ));

    final oldRequestRoom = Room(
      id: "!oldrequest:localhost",
      client: client,
    );
    oldRequestRoom.setState(RoomState(
      content: RequestContent(
        requestingOrganisation: "matrix.org",
        requestedOrganisation: "sorunome.de",
        requestId: "910d1916-9359-46b5-8900-0c1191e5d385",
        organisationId: "3e4d98f9-1544-4c0d-bce3-24509e92ba0b",
        organisationName: "Some Hospital",
        contactId: "83f42853-9530-40e3-a961-34ef00f6e09a",
        contactDescription: "Emergency takein",
        body: "Hey, anybody there?",
      ).toJson(),
      typeKey: Request.mRequestNameSpace,
      stateKey: "",
      senderId: "@oldinviter:localhost",
      room: oldRequestRoom,
    ));

    final broadcastRoom = Room(
      id: "!broadcast:localhost",
      client: client,
    );
    broadcastRoom.setState(RoomState(
      content: RequestContent(
        creator: "@creator:localhost",
        requestedOrganisation: "Broadcast",
        requestingOrganisation: "fakeServer.notExisting",
        requestTitle: "Awesome Broadcast",
        requestId: "ee9a2d02-1481-4364-9e93-d40ea6808069",
        body: "Hey there to this awesome broadcast",
        organisationId: "",
        organisationName: "Awesome Broadcast",
        contactId: "",
        contactDescription: "Broadcast",
      ).toJson(),
      typeKey: Request.mRequestNameSpace,
      stateKey: "",
      room: broadcastRoom,
    ));
    final broadcastContacts = [
      BroadcastContactsContent(
        requestedOrganisation: "example.com",
        organisationId: "a63973d7-57a6-4882-81e9-3d1761328273",
        organisationName: "Example Organisation",
        contactId: "5030715b-0f21-4e9d-aa80-3b065b969b34",
        contactDescription: "Example Contact",
        contactUri: "https://matrix.to/#/@contact:example.com",
      ),
      BroadcastContactsContent(
        requestedOrganisation: "foxies.com",
        organisationId: "6773cf9d-b896-4f8d-8ac8-3832830bbc8c",
        organisationName: "Foxies Organisation",
        contactId: "35d236c1-33ee-4a4b-b75b-19008619aeb3",
        contactDescription: "Foxies Contact",
        contactUri: "https://matrix.to/#/@contact:foxies.com",
      ),
      BroadcastContactsContent(
        requestedOrganisation: "raccoon.com",
        organisationId: "b4c318d7-a6e7-4708-90b0-7dbb28b2d6b1",
        organisationName: "Raccoon Organisation",
        contactId: "554d1d66-4665-42c6-9dca-d9b487e326f3",
        contactDescription: "Raccoon Contact",
        contactUri: "https://matrix.to/#/@contact:raccoon.com",
      ),
      BroadcastContactsContent(
        requestedOrganisation: "bunny.com",
        organisationId: "ecd1efd5-a55b-4077-a0ba-137646581472",
        organisationName: "Bunny Organisation",
        contactId: "31671c33-e5c9-4930-80ad-d02ac74a461b",
        contactDescription: "Bunny Contact",
        contactUri: "https://matrix.to/#/@contact:bunny.com",
      ),
    ];
    broadcastRoom.setState(RoomState(
      content: BroadcastContent(
        title: "Awesome Broadcast",
        contacts: broadcastContacts,
      ).toJson(),
      typeKey: Request.mBroadcastNameSpace,
      stateKey: "",
    ));
    broadcastRoom.setState(RoomState(
      content: RequestStateContent(
        requesting: RequestStateElementContent(
          accept: false,
          reject: false,
          needInfo: false,
        ),
        requested: RequestStateElementContent(
          accept: true,
          reject: false,
          needInfo: false,
        ),
        roomHandoff: true,
      ).toJson(),
      typeKey: Request.mRequestStateNameSpace,
      stateKey: "35d236c1-33ee-4a4b-b75b-19008619aeb3",
    ));
    broadcastRoom.setState(RoomState(
      content: RequestStateContent(
        requesting: RequestStateElementContent(
          accept: false,
          reject: false,
          needInfo: false,
        ),
        requested: RequestStateElementContent(
          accept: false,
          reject: true,
          needInfo: false,
        ),
        roomHandoff: false,
      ).toJson(),
      typeKey: Request.mRequestStateNameSpace,
      stateKey: "554d1d66-4665-42c6-9dca-d9b487e326f3",
    ));

    final requestRoomFoxies = Room(
      id: "!foxies:localhost",
      client: client,
    );
    requestRoomFoxies.setState(RoomState(
      content: RequestContent(
        creator: "@creator:localhost",
        requestedOrganisation: "foxies.com",
        requestingOrganisation: "fakeServer.notExisting",
        requestTitle: "Awesome Broadcast",
        requestId: "ee9a2d02-1481-4364-9e93-d40ea6808069",
        body: "Hey there to this awesome broadcast",
        organisationId: "6773cf9d-b896-4f8d-8ac8-3832830bbc8c",
        organisationName: "Foxies Organisation",
        contactId: "35d236c1-33ee-4a4b-b75b-19008619aeb3",
        contactDescription: "Foxies Contact",
      ).toJson(),
      typeKey: Request.mRequestNameSpace,
      stateKey: "",
      room: requestRoomFoxies,
    ));
    requestRoomFoxies.setState(RoomState(
      content: RequestStateContent(
        requesting: RequestStateElementContent(
          accept: false,
          reject: false,
          needInfo: false,
        ),
        requested: RequestStateElementContent(
          accept: true,
          reject: false,
          needInfo: false,
        ),
        roomHandoff: true,
      ).toJson(),
      typeKey: Request.mRequestStateNameSpace,
      stateKey: "",
    ));

    client.roomList = RoomList(
      client: client,
      rooms: [nonRequestRoom, requestInviteRoom, requestRoom, requestRoomOwner, broadcastRoom, requestRoomFoxies],
    );

    test("isRequestRoom", () {
      expect(Request.isRequestRoom(nonRequestRoom), false);
      expect(Request.isRequestRoom(requestInviteRoom), true);
      expect(Request.isRequestRoom(requestRoom), true);
    });

    test("constructor", () {
      Request req;
      req = Request(requestInviteRoom);
      expect(req.isRequest, false);
      expect(req.requesting, false);
      req = Request(requestRoom);
      expect(req.isRequest, true);
      expect(req.content.requestId, "03793e1b-f1fb-48e4-ae1d-d00ed043f546");
      expect(req.state.requested.accept, false);
      req = Request(requestRoomFoxies);
      expect(req.content.requestId, "ee9a2d02-1481-4364-9e93-d40ea6808069");
      expect(req.state.requested.accept, true);
      req = Request(broadcastRoom, contact: broadcastContacts[0]);
      expect(req.contact, broadcastContacts[0]);
      req = Request(broadcastRoom, contact: broadcastContacts[2]);
      expect(req.state.requested.reject, true);
      req = Request(broadcastRoom, contactId: "35d236c1-33ee-4a4b-b75b-19008619aeb3");
      expect(req.contact.contactId, "35d236c1-33ee-4a4b-b75b-19008619aeb3");
    });

    test("get authorId", () {
      Request req;
      req = Request(requestInviteRoom);
      expect(req.authorId, "@inviter:localhost");
      req = Request(requestRoom);
      expect(req.authorId, "@creator:localhost");
      req = Request(oldRequestRoom);
      expect(req.authorId, "@oldinviter:localhost");
    });

    test("isAuthor", () {
      final req = Request(requestRoom);
      expect(req.isAuthor(), false);
      expect(req.isAuthor("@creator:localhost"), true);
    });

    test("get title", () {
      Request req;
      req = Request(requestInviteRoom);
      expect(req.title, "inviter");
      req = Request(requestRoom);
      expect(req.title, "Awesome Request");
      req = Request(oldRequestRoom);
      expect(req.title, "oldinviter");
    });

    test("get organisation", () {
      Request req;
      req = Request(requestInviteRoom);
      expect(req.organisation, null);
      req = Request(requestRoom);
      expect(req.organisation, "Some Hospital");
      req = Request(broadcastRoom, contact: broadcastContacts[1]);
      expect(req.organisation, "Foxies Organisation");
    });

    test("get description", () {
      Request req;
      req = Request(requestInviteRoom);
      expect(req.description, null);
      req = Request(requestRoom);
      expect(req.description, "Emergency takein");
      req = Request(broadcastRoom, contact: broadcastContacts[1]);
      expect(req.description, "Foxies Contact");
    });

    test("accept", () async {
      Request req;
      req = Request(requestRoom);
      await req.accept();
      expect(req.state.requested.accept, true);
      expect(req.state.requested.reject, false);
      expect(req.state.requested.needInfo, false);
      expect(req.state.requesting.accept, false);
      expect(req.state.requesting.reject, false);
      expect(req.state.requesting.needInfo, false);
      req = Request(requestRoomOwner);
      await req.accept();
      expect(req.state.requested.accept, false);
      expect(req.state.requested.reject, false);
      expect(req.state.requested.needInfo, false);
      expect(req.state.requesting.accept, true);
      expect(req.state.requesting.reject, false);
      expect(req.state.requesting.needInfo, false);
    });

    test("reject", () async {
      Request req;
      req = Request(requestRoom);
      await req.reject();
      expect(req.state.requested.accept, false);
      expect(req.state.requested.reject, true);
      expect(req.state.requested.needInfo, false);
      expect(req.state.requesting.accept, false);
      expect(req.state.requesting.reject, false);
      expect(req.state.requesting.needInfo, false);
      req = Request(requestRoomOwner);
      await req.reject();
      expect(req.state.requested.accept, false);
      expect(req.state.requested.reject, false);
      expect(req.state.requested.needInfo, false);
      expect(req.state.requesting.accept, false);
      expect(req.state.requesting.reject, true);
      expect(req.state.requesting.needInfo, false);
    });

    test("needInfo", () async {
      Request req;
      req = Request(requestRoom);
      await req.needInfo();
      expect(req.state.requested.accept, false);
      expect(req.state.requested.reject, false);
      expect(req.state.requested.needInfo, true);
      expect(req.state.requesting.accept, false);
      expect(req.state.requesting.reject, false);
      expect(req.state.requesting.needInfo, false);
      req = Request(requestRoomOwner);
      await req.needInfo();
      expect(req.state.requested.accept, false);
      expect(req.state.requested.reject, false);
      expect(req.state.requested.needInfo, false);
      expect(req.state.requesting.accept, false);
      expect(req.state.requesting.reject, false);
      expect(req.state.requesting.needInfo, true);
    });

    test("sendState", () async {
      var sentData = {};
      bool gotStateRequest = false;
      FakeMatrixApi.api["PUT"]["/client/r0/rooms/" + requestRoom.id + "/state/com.famedly.app.request_state"] = (var r) {
        sentData = json.decode(r);
        gotStateRequest = true;
        return {};
      };
      Request req;
      req = Request(requestRoom);
      req.state.requested.accept = true;
      await req.sendState();
      expect(gotStateRequest, true);
      expect(sentData["requested"]["accept"], true);

      sentData = {};
      bool gotStateRequestWithKey = false;
      FakeMatrixApi.api["PUT"]["/client/r0/rooms/" + broadcastRoom.id + "/state/com.famedly.app.request_state/" + broadcastContacts[3].contactId] = (var r) {
        sentData = json.decode(r);
        gotStateRequestWithKey = true;
        return {};
      };
      req = Request(broadcastRoom, contact: broadcastContacts[3]);
      req.state.requested.needInfo = true;
      await req.sendState(false);
      expect(gotStateRequestWithKey, true);
      expect(sentData["requested"]["need_info"], true);

      bool createdRoom = false;
      var createRoomData = {};
      var oldCreateRoom = FakeMatrixApi.api["POST"]["/client/r0/createRoom"];
      FakeMatrixApi.api["POST"]["/client/r0/createRoom"] = (var r) {
        createdRoom = true;
        createRoomData = json.decode(r);
        return { "room_id": "!newroom:localhost" };
      };
      req = Request(broadcastRoom, contact: broadcastContacts[3]);
      req.state.requested.needInfo = true;
      await req.sendState();
      FakeMatrixApi.api["POST"]["/client/r0/createRoom"] = oldCreateRoom;
      expect(createdRoom, true);
      expect(createRoomData["invite"].length, 2);
      expect(createRoomData["invite"].contains("@creator:localhost"), true);
      expect(createRoomData["invite"].contains("@contact:bunny.com"), true);
      expect(createRoomData["power_level_content_override"]["users"]["@test:fakeServer.notExisting"], 100);
      expect(createRoomData["power_level_content_override"]["users"]["@creator:localhost"], 100);
      expect(createRoomData["power_level_content_override"]["users"]["@contact:bunny.com"], 100);
    });

    test("remoteAccepted", () {
      Request req;
      req = Request(requestRoom);
      req.state.requesting.accept = true;
      expect(req.remoteAccepted(), true);
      req = Request(requestRoomOwner);
      req.state.requested.accept = true;
      expect(req.remoteAccepted(), true);
    });

    test("remoteRejected", () {
      Request req;
      req = Request(requestRoom);
      req.state.requesting.reject = true;
      expect(req.remoteRejected(), true);
      req = Request(requestRoomOwner);
      req.state.requested.reject = true;
      expect(req.remoteRejected(), true);
    });

    test("remoteNeedInfo", () {
      Request req;
      req = Request(requestRoom);
      req.state.requesting.needInfo = true;
      expect(req.remoteNeedInfo(), true);
      req = Request(requestRoomOwner);
      req.state.requested.needInfo = true;
      expect(req.remoteNeedInfo(), true);
    });

    test("youAccepted", () {
      Request req;
      req = Request(requestRoom);
      req.state.requested.accept = true;
      expect(req.youAccepted(), true);
      req = Request(requestRoomOwner);
      req.state.requesting.accept = true;
      expect(req.youAccepted(), true);
    });

    test("youRejected", () {
      Request req;
      req = Request(requestRoom);
      req.state.requested.reject = true;
      expect(req.youRejected(), true);
      req = Request(requestRoomOwner);
      req.state.requesting.reject = true;
      expect(req.youRejected(), true);
    });

    test("youNeedInfo", () {
      Request req;
      req = Request(requestRoom);
      req.state.requested.needInfo = true;
      expect(req.youNeedInfo(), true);
      req = Request(requestRoomOwner);
      req.state.requesting.needInfo = true;
      expect(req.youNeedInfo(), true);
    });

    test("getBroadcastContacts", () {
      List<BroadcastContactsContent> ret;
      ret = Request.getBroadcastContacts(broadcastRoom, "@contact:bunny.com");
      expect(ret.length, 1);
      expect(ret[0].contactId, "31671c33-e5c9-4930-80ad-d02ac74a461b");
      ret = Request.getBroadcastContacts(broadcastRoom, "@contact:foxies.com");
      expect(ret.length, 0);
      ret = Request.getBroadcastContacts(broadcastRoom, "@contact:raccoon.com");
      expect(ret.length, 0);
    });

    test("getRequestBundle", () {
      final ret = Request.getRequestBundle(client, "ee9a2d02-1481-4364-9e93-d40ea6808069");
      expect(ret.broadcastRoom, broadcastRoom);
      expect(ret.rooms.length, 1);
      expect(ret.rooms[0], requestRoomFoxies);
    });

    test("startBroadcast", () async {
      bool createdRoom = false;
      var createRoomData = {};
      var oldCreateRoom = FakeMatrixApi.api["POST"]["/client/r0/createRoom"];
      FakeMatrixApi.api["POST"]["/client/r0/createRoom"] = (var r) {
        createdRoom = true;
        createRoomData = json.decode(r);
        return { "room_id": "!newroom:localhost" };
      };
      List<Contact> contactList = [
        Contact(
          visibility: Visibility(include: ["*"], exclude: []),
          description: "New Fox",
          uri: "https://matrix.to/#/@new:foxies.com",
          passport: "blah1",
          tags: [],
          id: "a002a056-dba1-4e34-936f-0ff0ab14bd59",
        ),
        Contact(
          visibility: Visibility(include: ["*"], exclude: []),
          description: "New Bunny",
          uri: "https://matrix.to/#/@new:bunny.com",
          passport: "blah2",
          tags: [],
          id: "e21ca1a3-6c16-43f3-8b4b-c7fd34f434de",
        ),
      ];
      await Request.startBroadcast(
        requestTitle: "New Broadcast",
        requestBody: "Where is my coffee machine?",
        requestId: "5f9cd3c9-5a51-4764-a35e-d307f06b5626",
        tag: "New Broadcast",
        contactList: contactList,
        client: client,
      );
      FakeMatrixApi.api["POST"]["/client/r0/createRoom"] = oldCreateRoom;
      expect(createdRoom, true);
      final requestContent = createRoomData["initial_state"].singleWhere((d) => d["type"] == Request.mRequestNameSpace)["content"];
      final broadcastContent = createRoomData["initial_state"].singleWhere((d) => d["type"] == Request.mBroadcastNameSpace)["content"];
      final token1Content = createRoomData["initial_state"].singleWhere((d) => d["type"] == Directory.mPassport && d["state_key"] == "!@new:foxies.com")["content"];
      final token2Content = createRoomData["initial_state"].singleWhere((d) => d["type"] == Directory.mPassport && d["state_key"] == "!@new:bunny.com")["content"];
      expect(requestContent["creator"], "@test:fakeServer.notExisting");
      expect(requestContent["requested_organisation"], "Broadcast");
      expect(requestContent["contact_description"], "Broadcast");
      expect(requestContent["organisation_id"], "");
      expect(requestContent["contact_id"], "");
      expect(broadcastContent["contacts"].length, 2);
      expect(token1Content["token"], "blah1");
      expect(token2Content["token"], "blah2");
      expect(createRoomData["power_level_content_override"]["users"]["@test:fakeServer.notExisting"], 100);
      expect(createRoomData["power_level_content_override"]["users"]["@new:foxies.com"], 100);
      expect(createRoomData["power_level_content_override"]["users"]["@new:bunny.com"], 100);
    });
  });
}
