import 'package:famedly/models/tasks/subtask.dart';
import 'package:famedly/models/tasks/task.dart';
import 'package:famedly/models/tasks/tasklist.dart';
import 'package:famedlysdk/famedlysdk.dart';
import 'package:flutter_test/flutter_test.dart';

import 'FakeMatrixApi.dart';

void main() {
  /// All Tests related to the tasks
  group("Tasks", () {
    Client client = Client("testclient", debug: true);
    client.homeserver = "https://fakeServer.notExisting";
    client.connection.httpClient = FakeMatrixApi();
    client.connection.onUserEvent.stream.listen(client.handleUserUpdate);
    final String namespace = "com.famedly.talk.tasks";
    final String title = "testtitle";
    final String description = "testDesc";
    final DateTime date = DateTime.now();
    final bool done = true;

    final Map<String, dynamic> jsonTask = {
      "title": title,
      "description": description,
      "date": date.millisecondsSinceEpoch,
      "done": done,
      "subtask": [
        {
          "title": title,
          "done": done,
        },
      ],
    };
    int updateCounter = 0;
    TaskList list = TaskList(client);
    list.onUpdate = () {
      updateCounter++;
    };
    Task testTask = Task.fromJson(jsonTask, list);

    test('CreateFromAndToJson', () async {
      expect(list.tasks.length, 0);
      expect(testTask.title, title);
      expect(testTask.description, description);
      expect(testTask.date.minute, date.minute);
      expect(testTask.done, done);
      expect(testTask.subtask.length, 1);
      expect(testTask.subtask[0].title, title);
      expect(testTask.subtask[0].done, done);
      expect(testTask.toJson(), jsonTask);
    });
    test('getTaskList', () async {
      // Try with no account data
      expect(list.tasks, []);

      // Try empty account data list
      client.accountData[namespace] =
          AccountData(typeKey: namespace, content: {"tasks": []});
      expect(list.tasks, []);
      expect(list.tasks.length, 0);

      // Try with task list in account data
      client.accountData[namespace] = AccountData(typeKey: namespace, content: {
        "tasks": [jsonTask, jsonTask]
      });
      list = TaskList(client);
      expect(list.tasks.length, 2);
      expect(list.tasks[1].title, title);
      await new Future.delayed(new Duration(milliseconds: 100));
      Map<String, dynamic> jsonPayload = {
        "type": namespace,
        "content": {
          "tasks": [
            {"title": "test1"},
            {"title": "test2", "description": "desc2"},
            {"title": "test3", "done": true},
          ]
        }
      };
      UserUpdate userUpdate = UserUpdate(
          type: "account_data", eventType: namespace, content: jsonPayload);
      client.connection.onUserEvent.add(userUpdate);
      await new Future.delayed(new Duration(milliseconds: 100));
      expect(updateCounter, 1);
      expect(list.tasks[0].title, "test1");
      expect(list.tasks[1].title, "test2");
      expect(list.tasks.length, 3);
    });
    test('create, change and remove', () async {
      Task newTask = await list.create(title: title);
      newTask = testTask;
      expect(newTask.title, title);
      await newTask.setTitle("newTitle");
      expect(newTask.title, "newTitle");
      await newTask.setDescription("newDesc");
      expect(newTask.description, "newDesc");
      DateTime newDate = DateTime.now();
      await newTask.setDate(newDate);
      expect(newTask.date, newDate);
      expect(newTask.done, true);
      await newTask.setDone(false);
      expect(newTask.done, false);
      await newTask.remove();
    });
    test('Subtasks', () async {
      Task newTask = await list.create(title: title);
      newTask = testTask;
      Subtask newSub = Subtask(title: title, parent: newTask);
      expect(Subtask.fromJson(newTask.toJson(), newTask).title, "newTitle");

      newTask.addSubtask(title);
      expect(newTask.subtask[0].title, newSub.title);
      expect(newSub.done, false);
      newSub.setDone(true);
      expect(newSub.done, true);
      newSub.setTitle("newTitle");
      expect(newSub.title, "newTitle");
    });
  });
}
