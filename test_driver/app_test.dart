// Imports the Flutter Driver API
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';
import 'package:famedly/config/TestUser.dart';

void main() {
  group('Famdely App', () {
    FlutterDriver driver;

    final String homeserver = TestUser.homeserver;
    final String username = TestUser.username;
    final String password = TestUser.password;
    final String backButtonToolTip = "Zurück"; // Set to your preferred language
    final String testMessage = "Hello world";
    final String testTask = "New test task";

    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    var waitFor = (String key) async {
      print("[Integration Test] Wait for $key");
      await driver.waitFor(find.byValueKey(key));
    };

    var tap = (String key) async {
      print("[Integration Test] Tap on $key");
      await driver.waitFor(find.byValueKey(key));
      await driver.tap(find.byValueKey(key));
    };

    var enterText = (String text) async {
      print("[Integration Test] Enter text: $text");
      await driver.enterText(text);
    };

    var login = () async {
      await waitFor("OrganisationPickerPage");
      await tap("organisation");
      await enterText(homeserver);
      await tap("login");
      await waitFor("LoginPage");
      await tap("username");
      await enterText(username);
      await tap("password");
      await enterText(password);
      await tap("login");
      await waitFor("IntroPage");
    };
    var logout = () async {
      await waitFor("RoomsPage");
      await tap("SettingsButton");
      await waitFor("SettingsPage");
      await tap("SignOutAction");
      await driver.waitFor(find.byType("AlertDialog"));
      await tap("ConfirmDialogOkButton");
      //await waitFor("OrganisationPickerPage");
    };
    var goBack = () async {
      await driver.waitFor(find.byTooltip(backButtonToolTip));
      await driver.tap(find.byTooltip(backButtonToolTip));
    };
    var clearArchive = () async {
      await waitFor("RoomsPage");
      await waitFor("SettingsButton");
      await tap("SettingsButton");
      await waitFor("SettingsPage");
      await tap("goToArchive");
      await waitFor("ArchivesPage");
      await tap("PopupMenu");
      await waitFor("Clear");
      await tap("Clear");
      await waitFor("ClearDialog");
      await tap("BackButtonInClearDialog");
      await waitFor("PopupMenu");
      await tap("PopupMenu");
      await waitFor("Clear");
      await tap("Clear");
      await waitFor("ClearDialog");
      await tap("ClearButtonInClearDialog");
      await waitFor("ClearedDialog");
      await tap("BackButtonInClearedDialog");
      await goBack();
      await goBack();
    };
    var archiveChatInChatPage = () async {
      await tap("PopupMenu");
      await tap("ArchivePopupMenuItem");
      await waitFor("ArchiveRoomDialog");
      await tap("ConfirmDialogOkButton");
    };

    test("Login", () async {
      await login();
      await tap("NextButton");
      await tap("NextButton");
      await tap("NextButton");
    });
    test("Logout", () async {
      await logout();
    });
    test("Login again", () async {
      await login();
      await tap("SkipButton");
    });
    test("Create direct chat", () async {
      await waitFor("RoomsPage");
      await waitFor("WriteButton");
      await tap("WriteButton");
      await waitFor("UsersPage");
      await driver.scroll(find.byValueKey("UsersListView"), 0, -1000,
          Duration(milliseconds: 500));
      await driver.waitFor(find.text("trick"));
      await driver.tap(find.text("trick"));
      await waitFor("ChatRoom");
      await goBack();
      await waitFor("RoomsPage");
      await driver.tap(find.text("trick"));
      await waitFor("ChatRoom");
      await tap("input");
      await enterText(testMessage);
      await tap("sendButton");
      await waitFor("MessageBubble");
      await tap("PopupMenu");
      await tap("ArchivePopupMenuItem");
      await waitFor("ArchiveRoomDialog");
      await tap("ConfirmDialogBackButton");
      await waitFor("PopupMenu");
      await tap("ChatRoomAppBar");
      await waitFor("UserViewer");
      await tap("NotificationOnProfileScaffoldButton");
      await tap("NotificationOffProfileScaffoldButton");
      await tap("CallProfileScaffoldButton");
      await waitFor("NotImplementedPage");
      await goBack();
      await tap("ChatProfileScaffoldButton");
      await tap("PopupMenu");
      await tap("ChatDetailsPopupMenuItem");
      await waitFor("UserViewer");
      await goBack();
      await archiveChatInChatPage();
    });
    test("Create group chat and test chat details", () async {
      await waitFor("RoomsPage");
      await waitFor("WriteButton");
      await tap("WriteButton");
      await waitFor("NewGroupButton");
      await tap("NewGroupButton");
      await waitFor("NewGroupPage");
      // Should do nothing until at least one user is selected...
      await tap("StartGroupChat");
      await driver.scroll(find.byValueKey("UsersListView"), 0, -1000,
          Duration(milliseconds: 500));
      await driver.waitFor(find.text("track"));
      await driver.tap(find.text("track"));
      await tap("StartGroupChat");
      await waitFor("ChatRoom");
      await goBack();
      await waitFor("RoomsPage");
      await driver.tap(find.text("track"));
      await waitFor("ChatRoom");
      await tap("input");
      await enterText(testMessage);
      await tap("sendButton");
      await tap("MessageBubble");
      await waitFor("EventActionsAddToTasks");
      await tap("EventActionsAddToTasks");
      await tap("ChatRoomAppBar");
      await waitFor("RoomSettings");
      await tap("NotificationOnProfileScaffoldButton");
      await tap("NotificationOffProfileScaffoldButton");
      await tap("CallProfileScaffoldButton");
      await waitFor("NotImplementedPage");
      await goBack();
      await tap("ChatProfileScaffoldButton");
      await tap("PopupMenu");
      await tap("ChatDetailsPopupMenuItem");
      await waitFor("RoomSettings");
      await tap("GoToMembersPage");
      await driver.waitFor(find.text("track"));
      await tap("GoToInvitePage");
      await waitFor("InvitePage");
      await driver.scroll(find.byValueKey("UsersListView"), 0, -1000,
          Duration(milliseconds: 500));
      await driver.waitFor(find.text("trick"));
      await driver.tap(find.text("trick"));
      await tap("ConfirmDialogBackButton");
      await driver.tap(find.text("trick"));
      await tap("ConfirmDialogOkButton");
      await goBack();
      await driver.waitFor(find.text("trick"));
      await driver.tap(find.text("trick"));
      await waitFor("UserViewer");
      await tap("userSettingsPopupMenu");
      await tap("UserSettingsPopupMenuKickButton");
      await tap("ConfirmDialogOkButton");
      await waitFor("UserViewer");
      await goBack();
      await waitFor("MemberListPage");
      await goBack();
      await waitFor("RoomSettings");
      await tap("PopupMenu");
      await tap("EditChatNamePopupMenuItem");
      await tap("setChatNameDialogTextField");
      await driver.enterText("TestGroup");
      await tap("OkButton_SetChatName");
      await goBack();
      await archiveChatInChatPage();
    });
    test("Test tasks", () async {
      await tap("NavToTasksFloatingActionButton");
      await waitFor("TasksPage");
      await driver.waitFor(find.text(testMessage));
      await tap("TagListCheckBox");
      await tap("AddTaskIconButton");
      await waitFor("AddTaskModal");
      await tap("NewTaskTextField");
      await enterText(testTask);
      await tap("ShowDescTextFieldIconButton");
      await tap("DescriptionTextField");
      await enterText("Test description");
      await tap("CreateFlatButton");
      await tap("TaskListTile");
      await waitFor("taskPage");
      await tap("DescriptionTextField");
      await enterText("Another Test description");
      await goBack();
      await driver.waitFor(find.text("Another Test description"));
      await tap("TaskListTile");
      await waitFor("taskPage");
      await tap("SubTaskTextField");
      await enterText("New Subtask");
      await goBack();
      await driver.waitFor(find.text("New Subtask"));
      await tap("TaskListTile");
      await waitFor("taskPage");
      await tap("DeleteIconButton");
      await tap("DoneListTile");
      await driver.waitFor(find.text(testMessage));
      await tap("DoneListTile");
      await driver.waitForAbsent(find.text(testMessage));
      await tap("PopupMenu");
      await tap("ClearPopupMenuItem");
      await goBack();
    });
    test("Test Directory", () async {
      await waitFor("RoomsPage");
      await tap("RequestsButton");
      await waitFor("RequestsPage");
      await tap("ChatsButton");
    });
    test("Test and clear archive", () async {
      await waitFor("RoomsPage");
      await waitFor("SettingsButton");
      await tap("SettingsButton");
      await waitFor("SettingsPage");
      await tap("goToArchive");
      await waitFor("ArchivesPage");
      await driver.waitFor(find.text("TestGroup"));
      await driver.tap(find.text("TestGroup"));
      await waitFor("ChatRoom");
      await driver.waitForAbsent(find.byValueKey("input"));
      await goBack();
      await waitFor("RoomsPage");
      await clearArchive();
      await clearArchive(); // Try again with empty archive
    });
    test("Logout", () async {
      await logout();
    });
  });
}
